#ifndef MONTJOIE_FILE_TIMER_HXX

#ifdef MONTJOIE_USE_ACCURATE_TIMING
#include <sys/time.h>
#include <sys/resource.h>
#endif

#include <map>

namespace Montjoie
{

#ifndef MONTJOIE_USE_ACCURATE_TIMING
  
  //! base class to do timings
  /*!
    In order to launch a timing, you need to call first GetNumber
    Then, to start the time counting, you call Start, and to stop it, you call Stop
    If you recall Start, the time is incremented (there is no reset)
    To get the elapsed time, you call GetSeconds
  */
  class BasicTimer
  {
  protected :
    Vector<double> elapsed_time; //!< elapsed time for each timer
    Vector<bool> jeton_libre; //!< free tokens
    Vector<clock_t> value_clock; //!< clock time for each timer that started
    bool test;
  public :
    enum {ALL, COMM, PROD, STIFFNESS, FLUX, MASS, EXTRAPOL, SCHEME, PML,
          OUTPUT, JACOBIAN, SOLVE, FACTO};
    
    BasicTimer();
    
    int GetNumber();
    void ReleaseNumber(int i);
    void ReserveNumber(int i);
    
    void Reset(int i);
    void Start(int i);
    void Stop(int i);
    double GetSeconds(int i);
    
  };

#else
  
  //! base class to do timings
  /*!
    In order to launch a timing, you need to call first GetNumber
    Then, to start the time counting, you call Start, and to stop it, you call Stop
    If you recall Start, the time is incremented (there is no reset)
    To get the elapsed time, you call GetSeconds
  */
  class AccurateTimer
  {
  protected :
    struct timeval tim;        
    struct rusage ru;
    
    Vector<double> elapsed_time; //!< elapsed time for each timer
    Vector<bool> jeton_libre; //!< free tokens
    Vector<double> value_clock; //!< clock time for each timer that started
    
  public :
    enum {ALL, COMM, PROD, STIFFNESS, FLUX, MASS, EXTRAPOL, SCHEME, PML,
          OUTPUT, JACOBIAN, SOLVE, FACTO};
    
    AccurateTimer();
    
    int GetNumber();
    void ReleaseNumber(int i);
    void ReserveNumber(int i);
    
    void Reset(int i);
    void Start(int i);
    void Stop(int i);
    double GetSeconds(int i);
    
  };  

#endif


#ifdef MONTJOIE_USE_REAL_TIMING
  //! base class to do timings in real time
  /*!
    In order to launch a timing, you need to call first GetNumber
    Then, to start the time counting, you call Start, and to stop it, you call Stop
    If you recall Start, the time is incremented (there is no reset)
    To get the elapsed time, you call GetSeconds
  */
  class RealTimer
  {
  protected :
    Vector<double> elapsed_time; //!< elapsed time for each timer
    Vector<bool> jeton_libre; //!< free tokens
    Vector<struct timespec> value_clock; //!< clock time for each timer that started
    
  public :
    enum {ALL, COMM, PROD, STIFFNESS, FLUX, MASS, EXTRAPOL, SCHEME, PML,
          OUTPUT, JACOBIAN, SOLVE, FACTO};
    
    RealTimer();
    
    int GetNumber();
    void ReleaseNumber(int i);
    void ReserveNumber(int i);
    
    void Reset(int i);
    void Start(int i);
    void Stop(int i);
    double GetSeconds(int i);
    
  };
#endif

#ifdef MONTJOIE_USE_ACCURATE_TIMING
  class MontjoieTimer : public AccurateTimer
#elif defined(MONTJOIE_USE_REAL_TIMING)
  class MontjoieTimer : public RealTimer
#else
  class MontjoieTimer : public BasicTimer
#endif
  {
  protected :
#ifdef SELDON_WITH_MPI
    //! MPI communicator
    MPI::Comm* comm_;
#endif

    //! list of timers defined with strings instead of integers as other timers
    map<string, int> liste_chrono;
    //! list of messages associated with timers
    Vector<string> liste_message;
    
  public :

    MontjoieTimer();

#ifdef SELDON_WITH_MPI
    void SetCommunicator(MPI::Comm& comm);
#endif

    void Start(int);
    void Stop(int);
    void Reset(int);
    
    void Start(const string&);
    void Stop(const string&);
    void Reset(const string&);

    int GetNumber();
    
    void SetMessage(const string& name, const string& message);
    void ReserveName(const string& name);
    void ReleaseName(const string& name);
    bool NameExists(const string& name) const;

    void GetGlobalSeconds(int i, double& dt_loc, double& dt_sum,
                          double& dt_min, double& dt_max);
    
    void DisplayTime(int i, const string& message);

    void GetGlobalSeconds(const string&, double& dt_loc, double& dt_sum,
                          double& dt_min, double& dt_max);
    
    void DisplayTime(const string&, const string& message);
    
    void DisplayTime(int i);
    void DisplayTime(const string& name);
    void DisplayAll();
    
  };

  
  //! global stopwatch
  extern MontjoieTimer glob_chrono; 
    
}

#define MONTJOIE_FILE_TIMER_HXX
#endif
