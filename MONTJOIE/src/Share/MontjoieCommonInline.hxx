#ifndef MONTJOIE_FILE_MONTJOIE_COMMON_INLINE_HXX

// including Seldon and other Seldon-like methods
#include "Algebra/MontjoieAlgebraInline.hxx"

#include "Share/MontjoieTypesInline.cxx"
#include "Share/CommonMontjoieInline.cxx"
#include "Share/UnivariatePolynomialInline.cxx"

#define MONTJOIE_FILE_MONTJOIE_COMMON_INLINE_HXX
#endif

