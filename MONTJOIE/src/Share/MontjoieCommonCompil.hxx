#ifndef MONTJOIE_FILE_MONTJOIE_COMMON_COMPIL_HXX

#include "Compil/Share/bessel_function.cpp"
#include "Compil/Share/common.cpp"
#include "Compil/Share/fft_interface.cpp"
#include "Compil/Share/polynomial.cpp"
#include "Compil/Share/random.cpp"
#include "Compil/Share/timer.cpp"

#define MONTJOIE_FILE_MONTJOIE_COMMON_COMPIL_HXX
#endif

