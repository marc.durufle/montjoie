#ifndef MONTJOIE_FILE_FFT_CXX

namespace Montjoie
{
  /***************/
  /* Complex FFT */
  /***************/

#ifndef SELDON_WITH_COMPILED_LIBRARY  
  //! default constructor
  FftInterface<Complex_wp>::FftInterface()
  {
    nb_modes_x = 0;
    nb_modes_y = 0;
    nb_modes_z = 0;
    
#ifdef MONTJOIE_GSL_FFT
    wavetable_x = NULL;
    workspace_x = NULL;

    wavetable_y = NULL;
    workspace_y = NULL;

    wavetable_z = NULL;
    workspace_z = NULL;
#endif

#ifdef _OPENMP
    nb_threads = omp_get_max_threads();
#else
    nb_threads = 1;
#endif
    
  }
  
  
  //! default destructor
  FftInterface<Complex_wp>::~FftInterface()
  {
    if (nb_modes_x > 0)
      {
#ifdef MONTJOIE_GSL_FFT
        if (wavetable_x != NULL)
          {
            gsl_fft_complex_wavetable_free (wavetable_x);
            gsl_fft_complex_workspace_free (workspace_x);
            wavetable_x = NULL;
            workspace_x = NULL;
          }

        if (wavetable_y != NULL)
          {
            gsl_fft_complex_wavetable_free (wavetable_y);
            gsl_fft_complex_workspace_free (workspace_y);
            wavetable_y = NULL;
            workspace_y = NULL;
          }

        if (wavetable_z != NULL)
          {
            gsl_fft_complex_wavetable_free (wavetable_z);
            gsl_fft_complex_workspace_free (workspace_z);
            wavetable_z = NULL;
            workspace_z = NULL;
          }
#endif
        
#ifdef MONTJOIE_FFTW_FFT
        fftw_destroy_plan(plan_forward);
        fftw_destroy_plan(plan_backward);
#endif
        
#ifdef MONTJOIE_MKL_FFT
        DftiFreeDescriptor(&hand_mkl);
#endif
        
        nb_modes_x = 0;
      }
  }
  
  
  //! returns the number of points in the fft (1-D)
  int FftInterface<Complex_wp>::GetNbPoints() const
  {
    return nb_modes_x;
  }


  //! returns the size used by the object in bytes
  int64_t FftInterface<Complex_wp>::GetMemorySize() const
  {
    int64_t taille = data.GetMemorySize() + xsol.GetMemorySize() + phase_x.GetMemorySize()
      + phase_y.GetMemorySize() + phase_z.GetMemorySize() + xtmp.GetMemorySize();
    
    taille += sizeof(*this);
    return taille;
  }
  
  
  //! modifies the number of threads
  void FftInterface<Complex_wp>::SetNbThreads(int n)
  {
    nb_threads = n;
  }

  
  //! initialization for computation of 1-D Discrete Fourier transform
  void FftInterface<Complex_wp>::Init(int n)
  {
    nb_modes_y = 0;
    nb_modes_z = 0;
    
    if (n <= 0)
      return;
    
#ifdef MONTJOIE_GSL_FFT
    wavetable_x = gsl_fft_complex_wavetable_alloc(n);
    workspace_x = gsl_fft_complex_workspace_alloc(n);
#endif
    
    nb_modes_x = n;
    data.Reallocate(n);
    xsol.Reallocate(n);
    xtmp.Reallocate(n);
    phase_x.Reallocate(n);
    phase_x(0) = 1.0;
    Complex_wp alpha = 2.0*pi_wp/n;
    Complex_wp coef = exp(Iwp*alpha);
    for (int i = 0; i < n-1; i++)
      phase_x(i+1) = coef*phase_x(i);

#ifdef MONTJOIE_FFTW_FFT
#ifdef _OPENMP
    fftw_plan_with_nthreads(nb_threads);
#endif
    
    fftw_complex* in = reinterpret_cast<fftw_complex*>(data.GetData());
    fftw_complex* out = reinterpret_cast<fftw_complex*>(xsol.GetData());
    plan_forward = fftw_plan_dft_1d(n, in, out,
                                    FFTW_FORWARD, FFTW_ESTIMATE);

    plan_backward = fftw_plan_dft_1d(n, in, out,
                                     FFTW_BACKWARD, FFTW_ESTIMATE);
#endif

#ifdef MONTJOIE_MKL_FFT
    DftiCreateDescriptor(&hand_mkl, DFTI_DOUBLE, DFTI_COMPLEX, 1, n);
    DftiSetValue(hand_mkl, DFTI_BACKWARD_SCALE, 1.0/n);
    //DftiSetValue(hand_mkl, DFTI_THREAD_LIMIT, nb_threads);
    DftiCommitDescriptor(hand_mkl);
#endif

  }


  //! Initialization for computation of 2-D Discrete Fourier Transform
  void FftInterface<Complex_wp>::Init(int nx, int ny)
  {
    nb_modes_z = 0;
    
    if ((nx <= 0) || (ny <= 0))
      return;
    
#ifdef MONTJOIE_GSL_FFT
    wavetable_x = gsl_fft_complex_wavetable_alloc(nx);
    workspace_x = gsl_fft_complex_workspace_alloc(nx);

    wavetable_y = gsl_fft_complex_wavetable_alloc(ny);
    workspace_y = gsl_fft_complex_workspace_alloc(ny);
#endif
    
    nb_modes_x = nx;
    nb_modes_y = ny;
    data.Reallocate(nx*ny);
    xsol.Reallocate(nx*ny);
    xtmp.Reallocate(nx*ny);
    phase_x.Reallocate(nx);
    phase_y.Reallocate(ny);
    phase_x(0) = 1.0;
    Complex_wp alpha = 2.0*pi_wp/nx;
    Complex_wp coef = exp(Iwp*alpha);
    for (int i = 0; i < nx-1; i++)
      phase_x(i+1) = coef*phase_x(i);

    phase_y(0) = 1.0;
    alpha = 2.0*pi_wp/ny;
    coef = exp(Iwp*alpha);
    for (int i = 0; i < ny-1; i++)
      phase_y(i+1) = coef*phase_y(i);

#ifdef MONTJOIE_FFTW_FFT
#ifdef _OPENMP
    fftw_plan_with_nthreads(nb_threads);
#endif

    fftw_complex* in = reinterpret_cast<fftw_complex*>(data.GetData());
    fftw_complex* out = reinterpret_cast<fftw_complex*>(xsol.GetData());
    plan_forward = fftw_plan_dft_2d(nx, ny, in, out,
                                    FFTW_FORWARD, FFTW_ESTIMATE);

    plan_backward = fftw_plan_dft_2d(nx, ny, in, out,
                                     FFTW_BACKWARD, FFTW_ESTIMATE);
#endif

#ifdef MONTJOIE_MKL_FFT
    MKL_LONG n[2] = {nx, ny};
    MKL_LONG strides[3] = {0, ny, 1};
    DftiCreateDescriptor(&hand_mkl, DFTI_DOUBLE, DFTI_COMPLEX, 2, n);
    DftiSetValue(hand_mkl, DFTI_BACKWARD_SCALE, 1.0/(nx*ny));
    DftiSetValue(hand_mkl, DFTI_INPUT_STRIDES, strides);
    //DftiSetValue(hand_mkl, DFTI_THREAD_LIMIT, nb_threads);
    DftiCommitDescriptor(hand_mkl);
#endif

  }


  //! Initialization for computation of 3-D Discrete Fourier Transform
  void FftInterface<Complex_wp>::Init(int nx, int ny, int nz)
  {
    if ((nx <= 0) || (ny <= 0) || (nz <= 0))
      return;
    
#ifdef MONTJOIE_GSL_FFT
    wavetable_x = gsl_fft_complex_wavetable_alloc(nx);
    workspace_x = gsl_fft_complex_workspace_alloc(nx);

    wavetable_y = gsl_fft_complex_wavetable_alloc(ny);
    workspace_y = gsl_fft_complex_workspace_alloc(ny);

    wavetable_z = gsl_fft_complex_wavetable_alloc(nz);
    workspace_z = gsl_fft_complex_workspace_alloc(nz);
#endif
    
    nb_modes_x = nx; 
    nb_modes_y = ny;
    nb_modes_z = nz;
    data.Reallocate(nx*ny*nz);
    xsol.Reallocate(nx*ny*nz);
    xtmp.Reallocate(nx*ny*nz);
    phase_x.Reallocate(nx);
    phase_x(0) = 1.0;
    Complex_wp alpha = 2.0*pi_wp/nx;
    Complex_wp coef = exp(Iwp*alpha);
    for (int i = 0; i < nx-1; i++)
      phase_x(i+1) = coef*phase_x(i);

    phase_y.Reallocate(ny);
    phase_y(0) = 1.0;
    alpha = 2.0*pi_wp/ny;
    coef = exp(Iwp*alpha);
    for (int i = 0; i < ny-1; i++)
      phase_y(i+1) = coef*phase_y(i);

    phase_z.Reallocate(nz);
    phase_z(0) = 1.0;
    alpha = 2.0*pi_wp/nz;
    coef = exp(Iwp*alpha);
    for (int i = 0; i < nz-1; i++)
      phase_z(i+1) = coef*phase_z(i);

#ifdef MONTJOIE_FFTW_FFT
#ifdef _OPENMP
    fftw_plan_with_nthreads(nb_threads);
#endif

    fftw_complex* in = reinterpret_cast<fftw_complex*>(data.GetData());
    fftw_complex* out = reinterpret_cast<fftw_complex*>(xsol.GetData());
    plan_forward = fftw_plan_dft_3d(nx, ny, nz, in, out,
                                    FFTW_FORWARD, FFTW_ESTIMATE);

    plan_backward = fftw_plan_dft_3d(nx, ny, nz, in, out,
                                     FFTW_BACKWARD, FFTW_ESTIMATE);
#endif

#ifdef MONTJOIE_MKL_FFT
    MKL_LONG n[3] = {nx, ny, nz};
    MKL_LONG strides[4] = {0, nz*ny, nz, 1};
    DftiCreateDescriptor(&hand_mkl, DFTI_DOUBLE, DFTI_COMPLEX, 3, n);
    DftiSetValue(hand_mkl, DFTI_BACKWARD_SCALE, 1.0/(nx*ny*nz));
    DftiSetValue(hand_mkl, DFTI_INPUT_STRIDES, strides);
    //DftiSetValue(hand_mkl, DFTI_THREAD_LIMIT, nb_threads);
    DftiCommitDescriptor(hand_mkl);
#endif

  }
  
  
  void FftInterface<Complex_wp>::ApplyForward(Vector<Complex_wp>& x)
  {
    if (nb_modes_z == 0)
      {
        if (nb_modes_y == 0)
          {
            // 1-D FFT
#ifdef MONTJOIE_GSL_FFT
            gsl_fft_complex_forward(reinterpret_cast<double*> (x.GetData()), 1,
                                    nb_modes_x, wavetable_x, workspace_x);
#endif
            
#ifdef MONTJOIE_FFTW_FFT
            for (int i = 0; i < nb_modes_x; i++)
              data(i) = x(i);
            
            fftw_execute(plan_forward);
            
            for (int i = 0; i < nb_modes_x; i++)
              x(i) = xsol(i);
#endif

#ifdef MONTJOIE_MKL_FFT
            DftiComputeForward(hand_mkl, x.GetDataVoid());
#endif
            
#ifdef MONTJOIE_MANUAL_FFT
            for (int i = 0; i < nb_modes_x; i++)
              data(i) = x(i);
            
            Complex_wp vloc;
            for (int k = 0; k < nb_modes_x; k++)
              {
                int p = 0;
                vloc = 0;
                for (int n = 0; n < nb_modes_x; n++)
                  {
                    vloc += data(n)*conj(phase_x(p));
                    p = (p+k)%nb_modes_x;
                  }
                
                x(k) = vloc;
              }
#endif
          }
        else
          {
            // 2-D FFT
#ifdef MONTJOIE_GSL_FFT
            for (int i = 0; i < nb_modes_x; i++)
              gsl_fft_complex_forward(reinterpret_cast<double*> (&x(i*nb_modes_y)), 1,
                                      nb_modes_y, wavetable_y, workspace_y);
            
            for (int i = 0; i < nb_modes_y; i++)
              gsl_fft_complex_forward(reinterpret_cast<double*> (&x(i)), nb_modes_y,
                                      nb_modes_x, wavetable_x, workspace_x);
#endif
            
#ifdef MONTJOIE_FFTW_FFT
            for (int i = 0; i < data.GetM(); i++)
              data(i) = x(i);
            
            fftw_execute(plan_forward);
            
            for (int i = 0; i < xsol.GetM(); i++)
              x(i) = xsol(i);
#endif

#ifdef MONTJOIE_MKL_FFT
            DftiComputeForward(hand_mkl, x.GetDataVoid());
#endif
            
#ifdef MONTJOIE_MANUAL_FFT
            for (int i = 0; i < nb_modes_x; i++)
              {
                for (int j = 0; j < nb_modes_y; j++)
                  data(j) = x(i*nb_modes_y + j);
                
                Complex_wp vloc;
                for (int k = 0; k < nb_modes_y; k++)
                  {
                    int p = 0;
                    vloc = 0;
                    for (int n = 0; n < nb_modes_y; n++)
                      {
                        vloc += data(n)*conj(phase_y(p));
                        p = (p+k)%nb_modes_y;
                      }
                    
                    xsol(i*nb_modes_y + k) = vloc;
                  }
              }

            for (int j = 0; j < nb_modes_y; j++)
              {
                for (int i = 0; i < nb_modes_x; i++)
                  data(i) = xsol(i*nb_modes_y + j);
                
                Complex_wp vloc;
                for (int k = 0; k < nb_modes_x; k++)
                  {
                    int p = 0;
                    vloc = 0;
                    for (int n = 0; n < nb_modes_x; n++)
                      {
                        vloc += data(n)*conj(phase_x(p));
                        p = (p+k)%nb_modes_x;
                      }
                    
                    x(k*nb_modes_y + j) = vloc;
                  }
              }
#endif
          }
      }
    else
      {
        // 3-D FFT
#ifdef MONTJOIE_GSL_FFT
        for (int i = 0; i < nb_modes_x; i++)
          for (int j = 0; j < nb_modes_y; j++)
            gsl_fft_complex_forward(reinterpret_cast<double*>
                                    (&x(nb_modes_z*(i*nb_modes_y + j))), 1,
                                    nb_modes_z, wavetable_z, workspace_z);

        for (int i = 0; i < nb_modes_x; i++)
          for (int j = 0; j < nb_modes_z; j++)
            gsl_fft_complex_forward(reinterpret_cast<double*>
                                    (&x(nb_modes_z*nb_modes_y*i + j)), nb_modes_z,
                                    nb_modes_y, wavetable_y, workspace_y);
            
        for (int i = 0; i < nb_modes_y; i++)
          for (int j = 0; j < nb_modes_z; j++)
            gsl_fft_complex_forward(reinterpret_cast<double*>
                                    (&x(i*nb_modes_z + j)), nb_modes_y*nb_modes_z,
                                    nb_modes_x, wavetable_x, workspace_x);
#endif
            
#ifdef MONTJOIE_FFTW_FFT
        for (int i = 0; i < data.GetM(); i++)
          data(i) = x(i);
        
        fftw_execute(plan_forward);
        
        for (int i = 0; i < xsol.GetM(); i++)
          x(i) = xsol(i);
#endif
            
#ifdef MONTJOIE_MKL_FFT
        DftiComputeForward(hand_mkl, x.GetDataVoid());
#endif
        
#ifdef MONTJOIE_MANUAL_FFT
        for (int ix = 0; ix < nb_modes_x; ix++)
          for (int iy = 0; iy < nb_modes_y; iy++)
            {
              for (int j = 0; j < nb_modes_z; j++)
                data(j) = x(nb_modes_z*(ix*nb_modes_y + iy) + j);
              
              Complex_wp vloc;
              for (int k = 0; k < nb_modes_z; k++)
                {
                  int p = 0;
                  vloc = 0;
                  for (int n = 0; n < nb_modes_z; n++)
                    {
                      vloc += data(n)*conj(phase_z(p));
                      p = (p+k)%nb_modes_z;
                    }
                  
                  xsol(nb_modes_z*(ix*nb_modes_y + iy) + k) = vloc;
                }
            }

        for (int ix = 0; ix < nb_modes_x; ix++)
          for (int iz = 0; iz < nb_modes_z; iz++)
            {
              for (int j = 0; j < nb_modes_y; j++)
                data(j) = xsol(nb_modes_z*(ix*nb_modes_y + j) + iz);
              
              Complex_wp vloc;
              for (int k = 0; k < nb_modes_y; k++)
                {
                  int p = 0;
                  vloc = 0;
                  for (int n = 0; n < nb_modes_y; n++)
                    {
                      vloc += data(n)*conj(phase_y(p));
                      p = (p+k)%nb_modes_y;
                    }
                  
                  x(nb_modes_z*(ix*nb_modes_y + k) + iz) = vloc;
                }
            }
        

        for (int iy = 0; iy < nb_modes_y; iy++)
          for (int iz = 0; iz < nb_modes_z; iz++)
            {
              for (int j = 0; j < nb_modes_x; j++)
                data(j) = x(nb_modes_z*(j*nb_modes_y + iy) + iz);
              
              Complex_wp vloc;
              for (int k = 0; k < nb_modes_x; k++)
                {
                  int p = 0;
                  vloc = 0;
                  for (int n = 0; n < nb_modes_x; n++)
                    {
                      vloc += data(n)*conj(phase_x(p));
                      p = (p+k)%nb_modes_x;
                    }
                  
                  xsol(nb_modes_z*(k*nb_modes_y + iy) + iz) = vloc;
                }
            }
        
        for (int i = 0; i < xsol.GetM(); i++)
          x(i) = xsol(i);
#endif
      }
  }
  
  
  void FftInterface<Complex_wp>::ApplyInverse(Vector<Complex_wp>& x)
  {
    if (nb_modes_z == 0)
      {
        if (nb_modes_y == 0)
          {
            // 1-D FFT
#ifdef MONTJOIE_GSL_FFT
            gsl_fft_complex_inverse(reinterpret_cast<double*> (x.GetData()), 1,
                                    nb_modes_x, wavetable_x, workspace_x);
#endif
            
#ifdef MONTJOIE_FFTW_FFT
            for (int i = 0; i < nb_modes_x; i++)
              data(i) = x(i);
            
            fftw_execute(plan_backward);
            
            for (int i = 0; i < nb_modes_x; i++)
              x(i) = xsol(i);

            Mlt(1.0/nb_modes_x, x);
#endif

#ifdef MONTJOIE_MKL_FFT
            DftiComputeBackward(hand_mkl, x.GetDataVoid());
#endif
            
#ifdef MONTJOIE_MANUAL_FFT
            for (int i = 0; i < nb_modes_x; i++)
              data(i) = x(i);
            
            Complex_wp vloc;
            for (int k = 0; k < nb_modes_x; k++)
              {
                int p = 0;
                vloc = 0;
                for (int n = 0; n < nb_modes_x; n++)
                  {
                    vloc += data(n)*phase_x(p);
                    p = (p+k)%nb_modes_x;
                  }
                
                x(k) = vloc;
              }
            
            Mlt(Real_wp(1)/nb_modes_x, x);
#endif
          }
        else
          {
            // 2-D FFT
#ifdef MONTJOIE_GSL_FFT
            for (int i = 0; i < nb_modes_x; i++)
              gsl_fft_complex_inverse(reinterpret_cast<double*> (&x(i*nb_modes_y)), 1,
                                      nb_modes_y, wavetable_y, workspace_y);
            
            for (int i = 0; i < nb_modes_y; i++)
              gsl_fft_complex_inverse(reinterpret_cast<double*> (&x(i)), nb_modes_y,
                                      nb_modes_x, wavetable_x, workspace_x);
#endif
            
#ifdef MONTJOIE_FFTW_FFT
            for (int i = 0; i < data.GetM(); i++)
              data(i) = x(i);
            
            fftw_execute(plan_backward);
            
            for (int i = 0; i < xsol.GetM(); i++)
              x(i) = xsol(i);
            
            Mlt(1.0/(nb_modes_x*nb_modes_y), x);
#endif

#ifdef MONTJOIE_MKL_FFT
            DftiComputeBackward(hand_mkl, x.GetDataVoid());
#endif
            
#ifdef MONTJOIE_MANUAL_FFT
            for (int i = 0; i < nb_modes_x; i++)
              {
                for (int j = 0; j < nb_modes_y; j++)
                  data(j) = x(i*nb_modes_y + j);
                
                Complex_wp vloc;
                for (int k = 0; k < nb_modes_y; k++)
                  {
                    int p = 0;
                    vloc = 0;
                    for (int n = 0; n < nb_modes_y; n++)
                      {
                        vloc += data(n)*phase_y(p);
                        p = (p+k)%nb_modes_y;
                      }
                    
                    xsol(i*nb_modes_y + k) = vloc;
                  }
              }

            for (int j = 0; j < nb_modes_y; j++)
              {
                for (int i = 0; i < nb_modes_x; i++)
                  data(i) = xsol(i*nb_modes_y + j);
                
                Complex_wp vloc;
                for (int k = 0; k < nb_modes_x; k++)
                  {
                    int p = 0;
                    vloc = 0;
                    for (int n = 0; n < nb_modes_x; n++)
                      {
                        vloc += data(n)*phase_x(p);
                        p = (p+k)%nb_modes_x;
                      }
                    
                    x(k*nb_modes_y + j) = vloc;
                  }
              }
            
            Mlt(Real_wp(1)/(nb_modes_x*nb_modes_y), x);
#endif
          }
      }
    else
      {
        // 3-D FFT
#ifdef MONTJOIE_GSL_FFT
        for (int i = 0; i < nb_modes_x; i++)
          for (int j = 0; j < nb_modes_y; j++)
            gsl_fft_complex_inverse(reinterpret_cast<double*>
                                    (&x(nb_modes_z*(i*nb_modes_y + j))), 1,
                                    nb_modes_z, wavetable_z, workspace_z);
        
        for (int i = 0; i < nb_modes_x; i++)
          for (int j = 0; j < nb_modes_z; j++)
            gsl_fft_complex_inverse(reinterpret_cast<double*>
                                    (&x(nb_modes_z*nb_modes_y*i + j)), nb_modes_z,
                                    nb_modes_y, wavetable_y, workspace_y);
        
        for (int i = 0; i < nb_modes_y; i++)
          for (int j = 0; j < nb_modes_z; j++)
            gsl_fft_complex_inverse(reinterpret_cast<double*>
                                    (&x(i*nb_modes_z + j)), nb_modes_y*nb_modes_z,
                                    nb_modes_x, wavetable_x, workspace_x);
#endif
            
#ifdef MONTJOIE_MKL_FFT
        DftiComputeBackward(hand_mkl, x.GetDataVoid());
#endif

#ifdef MONTJOIE_FFTW_FFT
        for (int i = 0; i < data.GetM(); i++)
          data(i) = x(i);
        
        fftw_execute(plan_backward);
        
        for (int i = 0; i < xsol.GetM(); i++)
          x(i) = xsol(i);

        Mlt(1.0/(nb_modes_x*nb_modes_y*nb_modes_z), x);
#endif
            
#ifdef MONTJOIE_MANUAL_FFT
        for (int ix = 0; ix < nb_modes_x; ix++)
          for (int iy = 0; iy < nb_modes_y; iy++)
            {
              for (int j = 0; j < nb_modes_z; j++)
                data(j) = x(nb_modes_z*(ix*nb_modes_y + iy) + j);
              
              Complex_wp vloc;
              for (int k = 0; k < nb_modes_z; k++)
                {
                  int p = 0;
                  vloc = 0;
                  for (int n = 0; n < nb_modes_z; n++)
                    {
                      vloc += data(n)*phase_z(p);
                      p = (p+k)%nb_modes_z;
                    }
                  
                  xsol(nb_modes_z*(ix*nb_modes_y + iy) + k) = vloc;
                }
            }

        for (int ix = 0; ix < nb_modes_x; ix++)
          for (int iz = 0; iz < nb_modes_z; iz++)
            {
              for (int j = 0; j < nb_modes_y; j++)
                data(j) = xsol(nb_modes_z*(ix*nb_modes_y + j) + iz);
              
              Complex_wp vloc;
              for (int k = 0; k < nb_modes_y; k++)
                {
                  int p = 0;
                  vloc = 0;
                  for (int n = 0; n < nb_modes_y; n++)
                    {
                      vloc += data(n)*phase_y(p);
                      p = (p+k)%nb_modes_y;
                    }
                  
                  x(nb_modes_z*(ix*nb_modes_y + k) + iz) = vloc;
                }
            }
        

        for (int iy = 0; iy < nb_modes_y; iy++)
          for (int iz = 0; iz < nb_modes_z; iz++)
            {
              for (int j = 0; j < nb_modes_x; j++)
                data(j) = x(nb_modes_z*(j*nb_modes_y + iy) + iz);
              
              Complex_wp vloc;
              for (int k = 0; k < nb_modes_x; k++)
                {
                  int p = 0;
                  vloc = 0;
                  for (int n = 0; n < nb_modes_x; n++)
                    {
                      vloc += data(n)*phase_x(p);
                      p = (p+k)%nb_modes_x;
                    }
                  
                  xsol(nb_modes_z*(k*nb_modes_y + iy) + iz) = vloc;
                }
            }
        
        for (int i = 0; i < xsol.GetM(); i++)
          x(i) = xsol(i);

        Mlt(Real_wp(1)/(nb_modes_x*nb_modes_y*nb_modes_z), x);
#endif
      }
    
  }
#endif
  
    
  void FftInterface<Complex_wp>::
  ApplyForwardPoint(int k, const Vector<Complex_wp>& x, Complex_wp& u)
  {
    int p = 0;
    SetComplexZero(u);
    for (int n = 0; n < nb_modes_x; n++)
      {        
        u += conj(phase_x(p))*x(n);
        p = (p+k)%nb_modes_x;
      }
  }


  void FftInterface<Complex_wp>::
  ApplyForwardPoint(int ix, int iy, const Vector<Complex_wp>& x, Complex_wp& u)
  {
    SetComplexZero(u);
    int px = 0;
    for (int nx = 0; nx < nb_modes_x; nx++)
      {   
        int py = 0;
        for (int ny = 0; ny < nb_modes_y; ny++)
          {
            u += conj(phase_x(px)*phase_y(py))*x(nx*nb_modes_y + ny);
            py = (py + iy)%nb_modes_y;
          }
        
        px = (px + ix)%nb_modes_x;
      }
  }

  
  void FftInterface<Complex_wp>::
  ApplyForwardPoint(int ix, int iy, int iz, const Vector<Complex_wp>& x, Complex_wp& u)
  {
    SetComplexZero(u);
    int px = 0;
    for (int nx = 0; nx < nb_modes_x; nx++)
      {   
        int py = 0;
        for (int ny = 0; ny < nb_modes_y; ny++)
          {
            int pz = 0;
            for (int nz = 0; nz < nb_modes_z; nz++)
              {                
                u += conj(phase_x(px)*phase_y(py)*phase_z(pz))
		  *x(nb_modes_z*(nx*nb_modes_y + ny)+nz);
		
                pz = (pz + iz)%nb_modes_z;
              }
            py = (py + iy)%nb_modes_y;
          }        
        px = (px + ix)%nb_modes_x;
      }
  }


  void FftInterface<Complex_wp>::
  ApplyInversePoint(int k, const Vector<Complex_wp>& x, Complex_wp& u)
  {
    SetComplexZero(u);
    int p = 0;
    for (int n = 0; n < nb_modes_x; n++)
      {        
        u += phase_x(p)*x(n);
        p = (p+k)%nb_modes_x;
      }
    
    u *= Real_wp(1)/nb_modes_x;
  }


  void FftInterface<Complex_wp>::
  ApplyInversePoint(int ix, int iy, const Vector<Complex_wp>& x, Complex_wp& u)
  {
    SetComplexZero(u);
    int px = 0;
    for (int nx = 0; nx < nb_modes_x; nx++)
      {   
        int py = 0;
        for (int ny = 0; ny < nb_modes_y; ny++)
          {
            u += phase_x(px)*phase_y(py)*x(nx*nb_modes_y + ny);
            py = (py + iy)%nb_modes_y;
          }
        
        px = (px + ix)%nb_modes_x;
      }
    
    u *= Real_wp(1)/(nb_modes_x*nb_modes_y);
  }


  void FftInterface<Complex_wp>::
  ApplyInversePoint(int ix, int iy, int iz, const Vector<Complex_wp>& x, Complex_wp& u)
  {
    SetComplexZero(u);
    int px = 0;
    for (int nx = 0; nx < nb_modes_x; nx++)
      {   
        int py = 0;
        for (int ny = 0; ny < nb_modes_y; ny++)
          {
            int pz = 0;
            for (int nz = 0; nz < nb_modes_z; nz++)
              {                
                u += phase_x(px)*phase_y(py)*phase_z(pz)*x(nb_modes_z*(nx*nb_modes_y + ny)+nz);
                pz = (pz + iz)%nb_modes_z;
              }
            py = (py + iy)%nb_modes_y;
          }        
        px = (px + ix)%nb_modes_x;
      }
    
    u *= Real_wp(1)/(nb_modes_x*nb_modes_y*nb_modes_z);
  }
  

  Complex_wp FftInterface<Complex_wp>::GetCoefficient(int ix, int nx)
  {
    Complex_wp coef = phase_x((ix*nx)%nb_modes_x)/Real_wp(nb_modes_x);    
    return coef;
  }
    

  Complex_wp FftInterface<Complex_wp>
  ::GetCoefficient(int ix, int iy, int nx, int ny)
  {
    Complex_wp coef = phase_x((ix*nx)%nb_modes_x)*phase_y((iy*ny)%nb_modes_y)
      /Real_wp(nb_modes_x*nb_modes_y);
    
    return coef;
  }

  
  Complex_wp FftInterface<Complex_wp>
  ::GetCoefficient(int ix, int iy, int iz, int nx, int ny, int nz)
  {
    Complex_wp coef = phase_x((ix*nx)%nb_modes_x)*phase_y((iy*ny)%nb_modes_y)
      *phase_z((iz*nz)%nb_modes_z)
      / Real_wp(nb_modes_x*nb_modes_y*nb_modes_z);
    
    return coef;
  }
  
  
  template<class T0>
  void FftInterface<Complex_wp>::GetCosSinAlpha(int n, T0& cos_, T0& sin_) const
  {
    cos_ = real(phase_x(n));
    sin_ = imag(phase_x(n));
  }


  /************/
  /* Real FFT */
  /************/

#ifndef SELDON_WITH_COMPILED_LIBRARY  
  FftRealInterface::FftRealInterface()
  {
    nb_modes = 0;

#ifdef MONTJOIE_GSL_FFT
    wavetable_real = NULL;
    wavetable_cplx = NULL;
    workspace_real = NULL;
#endif

#ifdef _OPENMP
    nb_threads = omp_get_max_threads();
#else
    nb_threads = 1;
#endif
    
  }

  
  FftRealInterface::~FftRealInterface()
  {
    if (nb_modes > 0)
      {
#ifdef MONTJOIE_GSL_FFT
        if (wavetable_real != NULL)
          {
            gsl_fft_real_wavetable_free(wavetable_real);
            wavetable_real = NULL;
          }

        if (wavetable_cplx != NULL)
          {
            gsl_fft_halfcomplex_wavetable_free(wavetable_cplx);
            wavetable_cplx = NULL;
          }

        if (workspace_real != NULL)
          {
            gsl_fft_real_workspace_free(workspace_real);
            workspace_real = NULL;
          }
#endif

#ifdef MONTJOIE_FFTW_FFT
        fftw_destroy_plan(plan_forward);
        fftw_destroy_plan(plan_backward);
#endif

#ifdef MONTJOIE_MKL_FFT
        DftiFreeDescriptor(&hand_mkl);
#endif
        
        nb_modes = 0;
      }
  }
  
  
  //! returns the number of points of the fft (1-D)
  int FftRealInterface::GetNbPoints() const
  {
    return nb_modes;
  }
  

  //! returns the size used by the object in bytes
  int64_t FftRealInterface::GetMemorySize() const
  {
    int64_t taille = data.GetMemorySize() + phase.GetMemorySize() + xtmp.GetMemorySize()
      + xsol.GetMemorySize();
    
    taille += sizeof(*this);
    return taille;
  }
  
    
  void FftRealInterface::SetNbThreads(int n)
  {
    nb_threads = n;
  }
  
  
  void FftRealInterface::Init(int n)
  {
    nb_modes = n;
    
    data.Reallocate(n);
    xsol.Reallocate(n/2+1);
    xtmp.Reallocate(n);
    
#ifdef MONTJOIE_GSL_FFT
    wavetable_real = gsl_fft_real_wavetable_alloc(n);
    workspace_real = gsl_fft_real_workspace_alloc(n);
    wavetable_cplx = gsl_fft_halfcomplex_wavetable_alloc(n);
#endif
    
#ifdef MONTJOIE_FFTW_FFT
#ifdef _OPENMP
    fftw_plan_with_nthreads(nb_threads);
#endif

    fftw_complex* out = reinterpret_cast<fftw_complex*>(xsol.GetData());
    Real_wp* in = reinterpret_cast<Real_wp*>(data.GetData());
    plan_forward = fftw_plan_dft_r2c_1d(n, in, out, FFTW_ESTIMATE);
    
    plan_backward = fftw_plan_dft_c2r_1d(n, out, in, FFTW_ESTIMATE);    
#endif

#ifdef MONTJOIE_MKL_FFT
    DftiCreateDescriptor(&hand_mkl, DFTI_DOUBLE, DFTI_REAL, 1, n);
    DftiSetValue(hand_mkl, DFTI_BACKWARD_SCALE, 1.0/n);
    DftiSetValue(hand_mkl, DFTI_PLACEMENT, DFTI_NOT_INPLACE);
    DftiSetValue(hand_mkl, DFTI_CONJUGATE_EVEN_STORAGE,
                 DFTI_COMPLEX_COMPLEX);
    
    //DftiSetValue(hand_mkl, DFTI_THREAD_LIMIT, nb_threads);
    DftiCommitDescriptor(hand_mkl);
#endif
    
  }
  
  
  void FftRealInterface::ApplyForward(const Vector<Real_wp>& x,
                                      Vector<Complex_wp>& y)
  {
#ifdef MONTJOIE_GSL_FFT
    if (nb_modes < 2)
      {
        y(0) = x(0);
        return;
      }
    
    for (int i = 0; i < nb_modes/2; i++)
      y(i) = Complex_wp(x(2*i), x(2*i+1));
    
    y(nb_modes/2) = 0;
    
    gsl_fft_real_transform(reinterpret_cast<double*> (y.GetData()), 1,
                           nb_modes, wavetable_real, workspace_real);
    
    Real_wp yr = imag(y(0)), yi, yr_next;
    y(0) = real(y(0));
    for (int i = 1; i < nb_modes/2; i++)
      {
        yr_next = imag(y(i));
        yi = real(y(i));
        y(i) = Complex_wp(yr, yi);
        yr = yr_next;
      }
    
    y(nb_modes/2) = yr;
    
#endif

    // 1-D FFT
#ifdef MONTJOIE_FFTW_FFT
    for (int i = 0; i < nb_modes; i++)
      data(i) = x(i);
    
    fftw_execute(plan_forward);
    
    for (int i = 0; i < nb_modes/2+1; i++)
      y(i) = xsol(i);
#endif

#ifdef MONTJOIE_MKL_FFT
    DftiComputeForward(hand_mkl, x.GetData(), y.GetDataVoid());
#endif

#ifdef MONTJOIE_MANUAL_FFT
    cout << "Please compile Montjoie with a library implementing fft"
	 << ", e.g. USE_MKL = YES, USE_GSL = YES, or USE_FFTW = YES " << endl;
    
    abort();
#endif
  }

  
  void FftRealInterface::ApplyInverse(const Vector<Complex_wp>& x,
                                      Vector<Real_wp>& y)
  {
#ifdef MONTJOIE_GSL_FFT
    y(0) = real(x(0));
    for (int i = 1; i < nb_modes/2; i++)
      {
        y(2*i-1) = real(x(i));
        y(2*i) = imag(x(i));
      }
    
    y(nb_modes-1) = real(x(nb_modes/2));
    
    gsl_fft_halfcomplex_inverse(reinterpret_cast<double*> (y.GetData()), 1,
                                nb_modes, wavetable_cplx, workspace_real);
    
#endif

    // 1-D FFT
#ifdef MONTJOIE_FFTW_FFT
    for (int i = 0; i < nb_modes/2+1; i++)
      xsol(i) = x(i);
    
    fftw_execute(plan_backward);
    
    for (int i = 0; i < nb_modes; i++)
      y(i) = data(i);
    
    Mlt(1.0/nb_modes, y);
#endif

#ifdef MONTJOIE_MKL_FFT
    DftiComputeBackward(hand_mkl, x.GetDataVoid(), y.GetData());
#endif

#ifdef MONTJOIE_MANUAL_FFT
    cout << "Please compile Montjoie with a library implementing fft"
	 << ", e.g. USE_MKL = YES, USE_GSL = YES, or USE_FFTW = YES " << endl;
    
    abort();
#endif    
  }
#endif
  
}

#define MONTJOIE_FILE_FFT_CXX
#endif
