#ifndef MONTJOIE_FILE_TIMER_CXX

namespace Montjoie
{

#ifndef MONTJOIE_USE_ACCURATE_TIMING
  
  //! default constructor
  BasicTimer::BasicTimer()
    : elapsed_time(20), jeton_libre(20), value_clock(20)
  {
    jeton_libre.Fill(true);
    elapsed_time.Fill(0.0);
    jeton_libre(ALL) = false;
    jeton_libre(COMM) = false;
    jeton_libre(PROD) = false;
    jeton_libre(STIFFNESS) = false;
    jeton_libre(FLUX) = false;
    jeton_libre(MASS) = false;
    jeton_libre(EXTRAPOL) = false;
    jeton_libre(SCHEME) = false;
    jeton_libre(PML) = false;
    jeton_libre(OUTPUT) = false;
    jeton_libre(JACOBIAN) = false;
    jeton_libre(SOLVE) = false;
    jeton_libre(FACTO) = false;
    test = false;
  }
  
  
  //! returns an integer identifying the new timer
  int BasicTimer::GetNumber()
  {
    for (int i = 0; i < jeton_libre.GetM(); i++)
      if (jeton_libre(i))
	{
	  jeton_libre(i) = false;
	  return i;
	}
    
    int taille = jeton_libre.GetM() + 1;
    jeton_libre.Resize(taille);
    elapsed_time.Resize(taille);
    value_clock.Resize(taille);
    jeton_libre(taille-1) = false;
    elapsed_time(taille-1) = 0.0;
    return (taille-1);
  }
  
  
  //! we don't want to use no longer the timer identified "i"
  void BasicTimer::ReleaseNumber(int i)
  {
    if (i < jeton_libre.GetM())
      {
	jeton_libre(i) = true;
	elapsed_time(i) = 0.0;
      }
  }
  
  
  //! The timer i is already used
  void BasicTimer::ReserveNumber(int i)
  {
    if (i < jeton_libre.GetM())
      {
	jeton_libre(i) = false;
	elapsed_time(i) = 0.0;
      }
  }
  
  
  //! we want to reset the timer i (elapsed time is set to 0)
  void BasicTimer::Reset(int i)
  {
    if (i < jeton_libre.GetM())
      {
	elapsed_time(i) = 0.0;
      }
  }
  
  
  //! start of timing of the timer i
  void BasicTimer::Start(int i)
  {
    if (i < jeton_libre.GetM())
      {
	value_clock(i) = clock();
      }
  }
  
  
  //! end of timing of the timer i
  void BasicTimer::Stop(int i)
  {
    if (i < jeton_libre.GetM())
      {
	clock_t temps  = clock();
	if ((temps <= 0) || (value_clock(i) <= 0 ))
	  test = true;
	
	if ((temps < 0) && (value_clock(i) > 0))
	  {
	    // case with an overflow of clock_t type
	    // it appears generally on a 32-bits machine
	    clock_t diff = 1;
	    diff += numeric_limits<clock_t>::max() - value_clock(i);
	    diff += (temps - numeric_limits<clock_t>::min());
	    elapsed_time(i) += double(diff)/CLOCKS_PER_SEC;
	  }
	else
	  elapsed_time(i) += double(temps-value_clock(i))/CLOCKS_PER_SEC;
      }
  }
  
  
  //! returns elapsed time of the timer i
  /*!
    The result is expressed in seconds
  */
  double BasicTimer::GetSeconds(int i)
  {
    double temps(0);
    if (i < jeton_libre.GetM())
      {
	temps = elapsed_time(i);
      }
    
    return temps;
  }

#else
  
  //! default constructor
  AccurateTimer::AccurateTimer()
    : elapsed_time(20), jeton_libre(20), value_clock(20)
  {
    jeton_libre.Fill(true);
    elapsed_time.Fill(0.0);
    jeton_libre(ALL) = false;
    jeton_libre(COMM) = false;
    jeton_libre(PROD) = false;
    jeton_libre(STIFFNESS) = false;
    jeton_libre(FLUX) = false;
    jeton_libre(MASS) = false;
    jeton_libre(EXTRAPOL) = false;
    jeton_libre(SCHEME) = false;
    jeton_libre(PML) = false;
    jeton_libre(OUTPUT) = false;
    jeton_libre(JACOBIAN) = false;
    jeton_libre(SOLVE) = false;
    jeton_libre(FACTO) = false;
  }
  
  
  //! returns an integer identifying the new timer
  int AccurateTimer::GetNumber()
  {
    for (int i = 0; i < jeton_libre.GetM(); i++)
      if (jeton_libre(i))
	{
	  jeton_libre(i) = false;
          elapsed_time(i) = 0.0;
	  return i;
	}
    
    int taille = jeton_libre.GetM() + 1;
    jeton_libre.Resize(taille);
    elapsed_time.Resize(taille);
    value_clock.Resize(taille);
    jeton_libre(taille-1) = false;
    elapsed_time(taille-1) = 0.0;
    return (taille-1);
  }
  
  
  //! we don't want to use no longer the timer identified "i"
  void AccurateTimer::ReleaseNumber(int i)
  {
    if (i < jeton_libre.GetM())
      {
	jeton_libre(i) = true;
	elapsed_time(i) = 0.0;
      }
  }
  
  
  //! The timer i is already used
  void AccurateTimer::ReserveNumber(int i)
  {
    if (i < jeton_libre.GetM())
      {
	jeton_libre(i) = false;
	elapsed_time(i) = 0.0;
      }
  }
  
  
  //! we want to reset the timer i (elapsed time is set to 0)
  void AccurateTimer::Reset(int i)
  {
    if (i < jeton_libre.GetM())
      {
	elapsed_time(i) = 0.0;
      }
  }
  
  
  //! start of timing of the timer i
  void AccurateTimer::Start(int i)
  {
    if (i < jeton_libre.GetM())
      {
	getrusage(RUSAGE_SELF, &ru);        
        tim = ru.ru_utime;        
        value_clock(i) = double(tim.tv_sec) + double(tim.tv_usec)/ 1000000.0;
      }
  }
  
  
  //! end of timing of the timer i
  void AccurateTimer::Stop(int i)
  {
    if (i < jeton_libre.GetM())
      {
	getrusage(RUSAGE_SELF, &ru);        
        tim = ru.ru_utime;        
        double temps = double(tim.tv_sec) + double(tim.tv_usec)/ 1000000.0;
	elapsed_time(i) += temps - value_clock(i);
      }
  }
  
  
  //! returns elapsed time of the timer i
  /*!
    The result is expressed in seconds
  */
  double AccurateTimer::GetSeconds(int i)
  {
    double temps(0);
    if (i < jeton_libre.GetM())
      {
	temps = elapsed_time(i);
      }
    
    return temps;
  }

#endif

#ifdef MONTJOIE_USE_REAL_TIMING
  //#ifdef MONTJOIE_USE_ACCURATE_TIMING
  //! default constructor
  RealTimer::RealTimer()
    : elapsed_time(20),jeton_libre(20), value_clock(20)
  {
    jeton_libre.Fill(true);
    elapsed_time.Fill(0);
    jeton_libre(ALL) = false;
    jeton_libre(COMM) = false;
    jeton_libre(PROD) = false;
    jeton_libre(STIFFNESS) = false;
    jeton_libre(FLUX) = false;
    jeton_libre(MASS) = false;
    jeton_libre(EXTRAPOL) = false;
    jeton_libre(SCHEME) = false;
    jeton_libre(PML) = false;
    jeton_libre(OUTPUT) = false;
    jeton_libre(JACOBIAN) = false;
    jeton_libre(SOLVE) = false;
    jeton_libre(FACTO) = false;
  }
  
  
  //! returns an integer identifying the new timer
  int RealTimer::GetNumber()
  {
    for (int i = 0; i < jeton_libre.GetM(); i++)
      if (jeton_libre(i))
	{
	  jeton_libre(i) = false;
	  return i;
	}
    
    int taille = jeton_libre.GetM() + 1;
    jeton_libre.Resize(taille);
    elapsed_time.Resize(taille);
    value_clock.Resize(taille);
    jeton_libre(taille-1) = false;
    elapsed_time(taille-1) = 0;
    return (taille-1);
  }
  
  
  //! we don't want to use no longer the timer identified "i"
  void RealTimer::ReleaseNumber(int i)
  {
    if (i < jeton_libre.GetM())
      {
	jeton_libre(i) = true;
	elapsed_time(i) = 0;
      }
  }
  
  
  //! The timer i is already used
  void RealTimer::ReserveNumber(int i)
  {
    if (i < jeton_libre.GetM())
      {
	jeton_libre(i) = false;
	elapsed_time(i) = 0;
      }
  }
  
  
  //! we want to reset the timer i (elapsed time is set to 0)
  void RealTimer::Reset(int i)
  {
    if (i < jeton_libre.GetM())
      {
	elapsed_time(i) = 0;
      }
  }
  
  
  //! start of timing of the timer i
  void RealTimer::Start(int i)
  {
    if (i < jeton_libre.GetM())
      {
	clock_gettime(CLOCK_MONOTONIC, &value_clock(i));
      }
  }
  
  
  //! end of timing of the timer i
  void RealTimer::Stop(int i)
  {
    if (i < jeton_libre.GetM())
      {
        time_t sec = value_clock(i).tv_sec;
        long nsec = value_clock(i).tv_nsec;
        clock_gettime(CLOCK_MONOTONIC, &value_clock(i));
	elapsed_time(i) += value_clock(i).tv_sec - sec;
        elapsed_time(i) += (value_clock(i).tv_nsec - nsec)/1.0e9;
      }
  }
  
  
  //! returns elapsed time of the timer i
  /*!
    The result is expressed in seconds
  */
  double RealTimer::GetSeconds(int i)
  {
    if (i < jeton_libre.GetM())
      return elapsed_time(i);
    
    return 0.0;
  }
#endif

  
  MontjoieTimer::MontjoieTimer() 
  {
    liste_message.Reallocate(this->jeton_libre.GetM());
    
#ifdef SELDON_WITH_MPI
    comm_ = &MPI::COMM_SELF;
#endif
  }
  
  
#ifdef SELDON_WITH_MPI
  void MontjoieTimer::SetCommunicator(MPI::Comm& comm)
  {
    comm_ = &comm;
  }
#endif
  
  
  //! Timer i is started or restarted
  void MontjoieTimer::Start(int i)
  {
#ifdef MONTJOIE_USE_ACCURATE_TIMING
    AccurateTimer::Start(i);
#elif defined(MONTJOIE_USE_REAL_TIMING)
    RealTimer::Start(i);
#else
    BasicTimer::Start(i);
#endif
  }
  
  
  //! Timer i is stopped
  void MontjoieTimer::Stop(int i)
  {
#ifdef MONTJOIE_USE_ACCURATE_TIMING
    AccurateTimer::Stop(i);
#elif defined(MONTJOIE_USE_REAL_TIMING)
    RealTimer::Stop(i);
#else
    BasicTimer::Stop(i);
#endif
  }
  
  
  //! Timer i is resetted 
  void MontjoieTimer::Reset(int i)
  {
#ifdef MONTJOIE_USE_ACCURATE_TIMING
    AccurateTimer::Reset(i);
#elif defined(MONTJOIE_USE_REAL_TIMING)
    RealTimer::Reset(i);
#else
    BasicTimer::Reset(i);
#endif
  }
    
  
  //! Timer name is started
  void MontjoieTimer::Start(const string& name)
  {
    // adding a timer if name does not exist
    map<string, int>::iterator it = liste_chrono.find(name);
    int n = -1;
    if (it == liste_chrono.end())
      {
        n = GetNumber();
        liste_chrono[name] = n;
      }
    else
      n = it->second;
    
    Start(n);
  }
  
  
  //! Timer name is stopped
  void MontjoieTimer::Stop(const string& name)
  {
    map<string, int>::iterator it = liste_chrono.find(name);
    int n = -1;
    if (it == liste_chrono.end())
      {
        cout << "The timer " << name << " does not exist " << endl;
        cout << "You can't stop it " << endl;
        abort();
      }
    else
      n = it->second;
    
    Stop(n);
  }
  
  
  //! Timer name is resetted
  void MontjoieTimer::Reset(const string& name)
  {
    // adding a timer if name does not exist
    map<string, int>::iterator it = liste_chrono.find(name);
    int n = -1;
    if (it == liste_chrono.end())
      {
        n = GetNumber();
        liste_chrono[name] = n;
      }
    else
      n = it->second;
    
    Reset(n);
  }
  
  
  //! returns an integer identifying the new timer
  int MontjoieTimer::GetNumber()
  {
#ifdef MONTJOIE_USE_ACCURATE_TIMING
    int n = AccurateTimer::GetNumber();
#elif defined(MONTJOIE_USE_REAL_TIMING)
    int n = RealTimer::GetNumber();
#else
    int n = BasicTimer::GetNumber();
#endif
    
    if (this->jeton_libre.GetM() > liste_message.GetM())
      liste_message.Resize(this->jeton_libre.GetM());
    
    return n;
  }
  
  
  //! sets a message for the timer name
  /*!
    The message will be displayed if DisplayTime or DisplayAll is called
  */
  void MontjoieTimer::SetMessage(const string& name, const string& message)
  {
    // adding a timer if name does not exist
    map<string, int>::iterator it = liste_chrono.find(name);
    int n = -1;
    if (it == liste_chrono.end())
      {
        n = GetNumber();
        liste_chrono[name] = n;
      }
    else
      n = it->second;
    
    liste_message(n) = message;
  }
  
  
  //! adds a timer whose name is name
  void MontjoieTimer::ReserveName(const string& name)
  {
    map<string, int>::iterator it = liste_chrono.find(name);
    if (it == liste_chrono.end())
      {
        int n = GetNumber();
        liste_chrono[name] = n;
      }
    else 
      {
        cout << "The timer " << name << " is already used " << endl;
        abort();
      }    
  }
  
  
  //! clears the timer whose name is name
  void MontjoieTimer::ReleaseName(const string& name)
  {
    map<string, int>::iterator it = liste_chrono.find(name);
    if (it == liste_chrono.end())
      {
        cout << "The timer " << name << " does not exist" << endl;
        abort();
      }
    else
      liste_chrono.erase(it);
  }


  //! returns true if the chrono name exists
  bool MontjoieTimer::NameExists(const string& name) const
  {
    map<string, int>::const_iterator it = liste_chrono.find(name);
    if (it == liste_chrono.end())
      return false;
    
    return true;
  }
  
  
  //! fills local time, sum of of time, minimal and maximal time among processors
  void MontjoieTimer::GetGlobalSeconds(int i, double& dt_loc, double& dt_sum,
				       double& dt_min, double& dt_max)
  {
    dt_loc = this->GetSeconds(i);
    
#ifdef SELDON_WITH_MPI
    MPI::Comm& comm = *comm_;
    if (comm.Get_size() > 1)
      {  
        dt_sum = 0;
	dt_max = 0;
        comm.Reduce(&dt_loc, &dt_sum, 1, MPI::DOUBLE, MPI::SUM, 0);
        comm.Reduce(&dt_loc, &dt_min, 1, MPI::DOUBLE, MPI::MIN, 0);
        comm.Reduce(&dt_loc, &dt_max, 1, MPI::DOUBLE, MPI::MAX, 0);
      }
    else
      {
        dt_sum = dt_loc;
        dt_min = dt_loc;
        dt_max = dt_loc;
      }
#endif
  }
    

  //! Displays times associated with timer i
  void MontjoieTimer::DisplayTime(int i, const string& message)
  { 
    double t_loc, t_sum, t_min, t_max;
    GetGlobalSeconds(i, t_loc, t_sum, t_min, t_max);
    
#ifdef SELDON_WITH_MPI
    MPI::Comm& comm = *comm_;
    if (comm.Get_size() > 1)
      {
	if (comm.Get_rank() == 0)
	  {
	    cout << "Global time to " << message << " : " << t_sum << endl;
	    cout << "Maximal time to " << message << " : " << t_max << endl;
            cout << endl;
	  }
      }
    else
#endif
      cout << rank_processor << " Time to " << message << " : " << t_loc << endl;    
  }


  //! fills local time, sum of of time, minimal and maximal time among processors
  void MontjoieTimer::GetGlobalSeconds(const string& name, double& dt_loc, double& dt_sum,
				       double& dt_min, double& dt_max)
  {
    map<string, int>::iterator it = liste_chrono.find(name);
    int n = -1;
    if (it == liste_chrono.end())
      {
        cout << "The timer " << name << " does not exist " << endl;
        abort();
      }
    else
      n = it->second;

    GetGlobalSeconds(n, dt_loc, dt_sum, dt_min, dt_max);
  }
    

  //! Displays times associated with timer name
  void MontjoieTimer::DisplayTime(const string& name, const string& message)
  {
    map<string, int>::iterator it = liste_chrono.find(name);
    int n = -1;
    if (it == liste_chrono.end())
      {
        cout << "The timer " << name << " does not exist " << endl;
        abort();
      }
    else
      n = it->second;
    
    DisplayTime(n, message);
  }


  //! Displays times associated with timer i
  void MontjoieTimer::DisplayTime(int i)
  {
    DisplayTime(i, liste_message(i));
  }
  
  
  //! Displays times associated with timer name
  void MontjoieTimer::DisplayTime(const string& name)
  {
    map<string, int>::iterator it = liste_chrono.find(name);
    int n = -1;
    if (it == liste_chrono.end())
      {
        cout << "The timer " << name << " does not exist " << endl;
        abort();
      }
    else
      n = it->second;
 
    DisplayTime(n, liste_message(n));
  }
  
  
  //! Displays all timers whose name is known (timers associated with integers are not displayed)
  void MontjoieTimer::DisplayAll()
  {
    map<string, int>::iterator it;
    for (it = liste_chrono.begin(); it != liste_chrono.end(); ++it)
      DisplayTime(it->second, liste_message(it->second));    
  }

  MontjoieTimer glob_chrono; 
    
}

#define MONTJOIE_FILE_TIMER_CXX
#endif
