#ifndef MONTJOIE_FILE_FFT_HXX


// you can use only one library
// fastest is fftw
// manual is provided only if you don't know how to install gsl or fftw

// using fftw if possible, then gsl, then manual
#ifdef MONTJOIE_WITH_FFTW
#define MONTJOIE_FFTW_FFT
#else
#ifdef SELDON_WITH_MKL
#define MONTJOIE_MKL_FFT
#else
#ifdef MONTJOIE_WITH_GSL
#define MONTJOIE_GSL_FFT
#else
#define MONTJOIE_MANUAL_FFT
#endif
#endif
#endif

#ifdef MONTJOIE_GSL_FFT
#include <gsl/gsl_fft_complex.h>
#define REAL(z,i) ((z)[2*(i)])
#define IMAG(z,i) ((z)[2*(i)+1])
#include <gsl/gsl_fft_real.h>
#include <gsl/gsl_fft_halfcomplex.h>
#endif

#ifdef MONTJOIE_FFTW_FFT
#include<fftw3.h>
#endif

#ifdef MONTJOIE_MKL_FFT
#include "mkl_dfti.h"
#endif

namespace Montjoie
{
  //! overloaded
  template<class Complexe>
  class FftInterface
  {
  public :
    inline int GetNbPoints() const { return 1; }
    inline int64_t GetMemorySize() const { return 0; }
    inline void SetNbThreads(int) {}
    
    inline void Init(int n) {}
    inline void Init(int nx, int ny) {}
    inline void Init(int nx, int ny, int nz) {}

    inline void ApplyForward(Vector<Complexe>& x) {}
    inline void ApplyInverse(Vector<Complexe>& x) {}
    
    inline void ApplyForwardPoint(int k, const Vector<Complexe>& x, Complexe& u) {}
    inline void ApplyForwardPoint(int kx, int ky, const Vector<Complexe>& x, Complexe& u) {}
    inline void ApplyForwardPoint(int kx, int ky, int kz, const Vector<Complexe>& x, Complexe& u) {}

    inline void ApplyInversePoint(int k, const Vector<Complexe>& x, Complexe& u) {}
    inline void ApplyInversePoint(int kx, int ky, const Vector<Complexe>& x, Complexe& u) {}
    inline void ApplyInversePoint(int kx, int ky, int kz, const Vector<Complexe>& x, Complexe& u) {}

    inline Complexe GetCoefficient(int ix, int nx) { return Complexe(1); }
    inline Complexe GetCoefficient(int ix, int iy, int nx, int ny) { return Complexe(1); }
    inline Complexe GetCoefficient(int ix, int iy, int iz, int nx, int ny, int nz) { return Complexe(1); }
    
    template<class T0>
    inline void GetCosSinAlpha(int n, T0& cos_, T0& sin_) const {}
    
  };
  
  
  //! object implementing Discrete Fourier Transform (interface with Gsl or Fftw)
  template<>
  class FftInterface<Complex_wp>
  {
  protected :

#ifdef MONTJOIE_GSL_FFT
    gsl_fft_complex_wavetable * wavetable_x, *wavetable_y, *wavetable_z;
    gsl_fft_complex_workspace * workspace_x, *workspace_y, *workspace_z;
#endif

#ifdef MONTJOIE_FFTW_FFT
    fftw_plan plan_forward;
    fftw_plan plan_backward;
#endif

#ifdef MONTJOIE_MKL_FFT
    DFTI_DESCRIPTOR_HANDLE hand_mkl;
#endif

    Vector<Complex_wp> data, xsol, phase_x, phase_y, phase_z, xtmp;
    int nb_modes_x, nb_modes_y, nb_modes_z;
    int nb_threads;
    
  public :
    FftInterface();
    
    ~FftInterface();
    
    int GetNbPoints() const;
    int64_t GetMemorySize() const;
    void SetNbThreads(int);
    
    // Initialization (required before effectively computing a fft)
    void Init(int n);
    void Init(int nx, int ny);
    void Init(int nx, int ny, int nz);
    
    // overwrites x with its forward Discrete Fourier Transform
    void ApplyForward(Vector<Complex_wp>& x);
    
    // overwrites x with its inverse Discrete Fourier Transform
    void ApplyInverse(Vector<Complex_wp>& x);
    
    // computation of a single value of inverse Discrete Fourier Transform
    void ApplyForwardPoint(int k, const Vector<Complex_wp>& x, Complex_wp& u);
    void ApplyForwardPoint(int kx, int ky, const Vector<Complex_wp>& x, Complex_wp& u);
    void ApplyForwardPoint(int kx, int ky, int kz, const Vector<Complex_wp>& x, Complex_wp& u);

    void ApplyInversePoint(int k, const Vector<Complex_wp>& x, Complex_wp& u);
    void ApplyInversePoint(int kx, int ky, const Vector<Complex_wp>& x, Complex_wp& u);
    void ApplyInversePoint(int kx, int ky, int kz, const Vector<Complex_wp>& x, Complex_wp& u);

    Complex_wp GetCoefficient(int ix, int nx);
    Complex_wp GetCoefficient(int ix, int iy, int nx, int ny);
    Complex_wp GetCoefficient(int ix, int iy, int iz, int nx, int ny, int nz);
    
    template<class T0>
    void GetCosSinAlpha(int n, T0& cos_, T0& sin_) const;
    
  };


  //! object implementing Discrete Fourier Transform (interface with Gsl or Fftw)
  class FftRealInterface
  {
  protected :

#ifdef MONTJOIE_GSL_FFT
    gsl_fft_real_wavetable * wavetable_real;
    gsl_fft_halfcomplex_wavetable * wavetable_cplx;
    gsl_fft_real_workspace * workspace_real;
#endif

#ifdef MONTJOIE_FFTW_FFT
    fftw_plan plan_forward;
    fftw_plan plan_backward;
#endif

#ifdef MONTJOIE_MKL_FFT
    DFTI_DESCRIPTOR_HANDLE hand_mkl;
#endif

    Vector<Real_wp> data, phase, xtmp;
    Vector<Complex_wp> xsol;
    int nb_modes;
    int nb_threads;
    
  public :    
    FftRealInterface();
    
    ~FftRealInterface();
    
    int GetNbPoints() const;
    int64_t GetMemorySize() const;
    void SetNbThreads(int);
    
    // Initialization (required before effectively computing a fft)
    void Init(int n);
    
    // computes y = fft(x)
    void ApplyForward(const Vector<Real_wp>& x,
                      Vector<Complex_wp>& y);
    
    // computes y = ifft(x)
    void ApplyInverse(const Vector<Complex_wp>& x,
                      Vector<Real_wp>& y);

  };
  
}

#define MONTJOIE_FILE_FFT_HXX
#endif
