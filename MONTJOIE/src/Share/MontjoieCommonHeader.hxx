#ifndef MONTJOIE_FILE_MONTJOIE_COMMON_HEADER_HXX

// including Seldon and other Seldon-like methods
#include "Algebra/MontjoieAlgebraHeader.hxx"

// Files in directory Share : 

// FFT stuff
#include "Share/FFT.hxx"

// other useful functions
#include "Share/CommonMontjoie.hxx"
#include "Share/Timer.hxx"
#include "Share/RandomGenerator.hxx"

// basic polynoms classes (univariate -> one variable x
//  multivariate -> any numbers of variables (for example x,y,z))
#include "Share/UnivariatePolynomial.hxx"
#include "Share/MultivariatePolynomial.hxx"

// interface for Bessel functions
#include "Share/BesselFunctionsInterface.hxx"


#define MONTJOIE_FILE_MONTJOIE_COMMON_HEADER_HXX
#endif

