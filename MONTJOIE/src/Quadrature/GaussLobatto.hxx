#ifndef MONTJOIE_FILE_GAUSS_LOBATTO_HXX

namespace Montjoie
{
  // declaration of classes to compute integration 1-D points
  
  //! base class for 1-D integration
  template<class Dimension, class T = Real_wp>
  class Globatto_Base
  {
  public :
    // typedef
    typedef typename Dimension::R_N R_N; //!< R2 or R3
    typedef typename Dimension::VectR_N VectR_N; //!< vector of R2 or R3
    
    enum {QUADRATURE_GAUSS, QUADRATURE_LOBATTO, QUADRATURE_RADAU,
	  QUADRATURE_JACOBI, QUADRATURE_RADAU_JACOBI, QUADRATURE_LOBATTO_JACOBI,
	  QUADRATURE_GAUSS_SQUARED, QUADRATURE_GAUSS_BLENDED};
      
  protected :
    int order; // order of approximation
    int ndloc; // number of quadrature points on a edge
    int nb_points_quadrature; // number of quadrature points over element
    T threshold;
    
    Vector<T> points_; // Points of integration on the edge
    Vector<T> weights_; // Weights of integration
    //! Constants of basis functions
    /*
      cte_phi(i)= \f$ 1/ \prod_{k=1..(r+1) ,k \ne i} ( \xi_k- \xi_i) \f$
    */
    Vector<T> cte_phi;
    
    //!< matrix so that \f$ \frac{\partial \varphi_j}{\partial x}
    //! (\xi_k) = \mbox{valGrad}(j,k) \f$
    Matrix<T> val_grad;  
    
    // method to compute cte_phi from points
    void ComputeFactorPhi();
  
  public :
    static T blending_default;
    
    Globatto_Base();

    int64_t GetMemorySize() const;
    int GetOrder() const;
    int GetGeometryOrder() const;
    int GetNbPointsQuad() const;
    
    // Gauss or Gauss-Lobatto quadrature according the value of type_quadrature
    void ConstructQuadrature(int, int type_quadrature = QUADRATURE_GAUSS,
                             T alpha = 0, T beta = 0);
    
    // set array Points equal to Points1D, and recompute cte_phi
    template<class Vector1>
    void AffectPoints(const Vector1&);

    template<class Vector1>
    void AffectWeights(const Vector1&);
    
    // direct access to quadrature formulas
    const T& Points(int i) const;
    const T& Weights(int i) const;

    const Vector<T>& Weights() const;
    const Vector<T>& Points() const;
    
    // returns \phi_i(x) 
    T phi1D(int i, const T& x) const;  
    // returns d\phi_i / dx (x) 
    T dphi1D(int i, const T& x) const ;
    
    const T& dphi1D(int, int) const;
    const Matrix<T>& GradPhi() const;
    
    void ComputeValuesPhiRef(const T&, Vector<T>& phi) const;
    
    // computes val_grad
    void ComputeGradPhi(); 
    
    // prints object on stream out
    template<class Dimen, class T2>
    friend ostream& operator <<(ostream& out, const Globatto_Base<Dimen, T2>& e);
    
  };
  

  //! empty class, overloaded
  template<class Dimension, class T = Real_wp>
  class Globatto
  {
  };
  
  //! class for integration over an edge 
  template<class T>
  class Globatto<Dimension1, T> : public Globatto_Base<Dimension1, T>
  {
  public :
    Globatto();
    
    template<class T2>
    friend ostream& operator <<(ostream& out,const Globatto<Dimension1, T2>& e);
    
  };
  
  
  //! Interpolation with n subdivisions and r+1 Gauss-Lobatto points on each interval
  /*!
    The number of shape functions is equal to n r + 1
   */
  template<class Dimension>
  class SubdivGlobatto
  {
  protected : 
    Vector<VectReal_wp> points, CteG;
    VectReal_wp xpos;
    
  public :
    
    const Real_wp& Points(int i) const;
    void Init(bool regular, int nb_subdiv, int r);
    int GetOrder() const;
    int GetGeometryOrder() const;
    Real_wp phi1D(int i, const Real_wp& x) const;
    
  };
  
} // end namespace

#define MONTJOIE_FILE_GAUSS_LOBATTO_HXX
#endif


