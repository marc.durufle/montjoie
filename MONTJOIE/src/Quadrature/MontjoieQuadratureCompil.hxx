#ifndef MONTJOIE_FILE_MONTJOIE_QUADRATURE_COMPIL_HXX

#include "Compil/Quadrature/gauss_jacobi.cpp"
#include "Compil/Quadrature/gauss_lobatto.cpp"
#include "Compil/Quadrature/gauss_lobatto_points.cpp"
#include "Compil/Quadrature/triangle_quadrature.cpp"
#include "Compil/Quadrature/tetrahedron_quadrature.cpp"

#define MONTJOIE_FILE_MONTJOIE_QUADRATURE_COMPIL_HXX
#endif

