#ifndef MONTJOIE_FILE_NONLINEAR_EQUATIONS_HXX

namespace Montjoie

{
  // Minpack functions
  
  //! base class to derive when solving a non-linear system with Minpack algorithm
  template<class T, class VectorSol = Vector<T>, class MatrixJac = Matrix<T> >
  class NonLinearEquations_Base
  {
  public :
    // computes y = F(x) where F is the non-linear system
    virtual void EvaluateFunction(const VectorSol& x, VectorSol& y) = 0;
    
    // computes Jac = DF(x)
    virtual void EvaluateJacobian(const VectorSol&, MatrixJac& Jac) = 0;
    
  };

  // solving non-linear equations knowing derivatives
  template<class T, class Vector, class Matrix>
  int SolveMinpack(NonLinearEquations_Base<T, Vector, Matrix>& eqns,
		   Vector& x_sol, Vector& fvec, Matrix& fjac, Vector& scale_eqn,
		   const IVect& Control, const VectReal_wp& RControl);
  
  template<class Vector, class Matrix>
  void Compute_QR_Factorisation(Matrix& A, bool pivot, IVect& ipvt,
				Vector& rdiag, Vector& acnorm, Vector& wa);

  template<class Vector, class Matrix>
  void QrSolve(Matrix& r, IVect& ipvt, Vector& diag, Vector& qtb,
	       Vector& x, Vector& sdiag, Vector& wa);

  template<class Vector, class Matrix>
  void qform(Matrix& q, Vector& wa);

  template<class real, class Vector, class Vector2>
  void dogleg(Vector2& r, Vector& diag, Vector& qtb,
	      real& delta, Vector& x, Vector& wa1, Vector& wa2);
  
  template<class Vector, class Matrix>
  void r1mpyq(Matrix& a, Vector& v, Vector& w);

  template<class Vector>
  void r1mpyq(Vector& a, Vector& v, Vector& w);
  
  template<class Vector2, class Vector>
  void r1updt(Vector2& s, Vector& u, Vector& v, Vector& w, bool& sing);

  //! base class to specify a function to minimize with gsl or mkl
  template<class T>
  class VirtualMinimizedFunction
  {
  protected :
    int n;
    int type_algo;
    
  public :
    // available algorithms in Gsl
    enum{BFGS, CG, STEEPEST_DESCENT, SIMPLEX};
    
    inline VirtualMinimizedFunction() { n = 1; type_algo = BFGS; }
    
    inline int GetGslAlgorithm() const { return type_algo; }
    inline void SetGslAlgorithm(int type) { type_algo = type; }
    
    inline int GetM() const { return n; }
    
    virtual void FindInitGuess(Vector<T>& param) = 0;
    virtual void EvaluateFunction(const Vector<T>& x, T& feval) = 0;
    virtual void EvaluateFunctionGradient(const Vector<T>& x,
					  T& feval, Vector<T>& fjac) = 0;
    
#ifdef MONTJOIE_WITH_GSL
    static double my_f(const gsl_vector *v, void *params);
    static void my_df(const gsl_vector *v, void *params, gsl_vector *df);
    static void my_fdf(const gsl_vector *v, void *params, double* f, gsl_vector *df);
#endif
    
  };


  //! base class to specify a least squares problem for mkl
  template<class T>
  class VirtualLeastSquaresFunction
  {
  protected:
    int m_, n_;
    
  public:
    inline VirtualLeastSquaresFunction() { m_ = 0; n_ = 0; }
    
    inline int GetM() { return m_; }
    inline int GetN() { return n_; }
    
    virtual void FindInitGuess(Vector<T>& param) = 0;
    virtual void EvaluateF(const Vector<T>& x, Vector<T>& feval) = 0;
    virtual void EvaluateJacobian(const Vector<T>& x, Vector<T>& feval,
                                  Matrix<T, General, ColMajor>& fjac) = 0;
    
  };


#ifdef MONTJOIE_WITH_GSL
  template<class T>
  T MinimizeParametersGsl(VirtualMinimizedFunction<T>& fct, Vector<T>& xsol,
			  Real_wp epsilon = 1e-12, unsigned nb_max_iter = 5000);
#endif
  
#ifdef SELDON_WITH_MKL
  template<class T>
  T SolveLeastSquaresMkl(VirtualLeastSquaresFunction<T>& fct, Vector<T>& xsol,
                         Real_wp epsilon = 1e-12, unsigned nb_max_iter = 5000);
#endif
  
} // end namespace


#ifdef SELDON_WITH_MKL
extern "C"
{
  typedef void* _TRNSP_HANDLE_t;
  
  extern int dtrnlsp_init     (_TRNSP_HANDLE_t*, int*, int*, double*, double*, int*, int*, double*);
  extern int dtrnlsp_check    (_TRNSP_HANDLE_t*, int*, int*, double*, double*, double*, int*);
  extern int dtrnlsp_solve    (_TRNSP_HANDLE_t*, double*, double*, int*);
  extern int dtrnlsp_get      (_TRNSP_HANDLE_t*, int*, int*, double*, double*);
  extern int dtrnlsp_delete   (_TRNSP_HANDLE_t*);
}
#endif

#define MONTJOIE_FILE_NONLINEAR_EQUATIONS_HXX
#endif
