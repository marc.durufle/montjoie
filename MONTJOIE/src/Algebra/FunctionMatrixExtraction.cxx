#ifndef SELDON_FILE_FUNCTION_MATRIX_EXTRACTION_CXX

#include "Algebra/FunctionMatrixExtraction.hxx"

namespace Seldon
{
  
  //! extracts several columns of a sparse matrix
  /*!
    \param[in] A sparse matrix where columns are extracted
    \param[in] col_number numbers of the columns to be extracted
    \param[out] V extracted sparse columns    
   */
  template<class T1, class Prop, class Allocator,
	   class T2, class Allocator2, class Allocator3>
  void GetCol(const Matrix<T1, Prop, ArrayRowSymSparse, Allocator>& A, const IVect& col_number,
	      Vector<Vector<T2, VectSparse, Allocator2>, VectSparse, Allocator3>& V)
  {
    int m = col_number.GetM();
    V.Reallocate(m);
    // counting size of columns
    IVect size_col(m);
    IVect index(A.GetM()); index.Fill(-1);
    for (int i = 0; i < m; i++)
      index(col_number(i)) = i;
    
    size_col.Fill(0); int num, k;
    for (int i = 0; i < A.GetM(); i++)
      {
	if (index(i) != -1)
	  for (int j = 0; j < A.GetRowSize(i); j++)
	    if (A.Index(i,j) != i)
	      size_col(index(i))++;
	
	for (int j = 0; j < A.GetRowSize(i); j++)
	  {
	    num = index(A.Index(i,j));
	    if (num != -1)
	      size_col(num)++;
	  }
      }
    
    // now we fill columns
    for (int i = 0; i < m; i++)
      {
	V.Index(i) = col_number(i);
	V.Value(i).Reallocate(size_col(i));
      }
    
    size_col.Zero();
    for (int i = 0; i < A.GetM(); i++)
      {
	for (int j = 0; j < A.GetRowSize(i); j++)
	  if (index(A.Index(i, j)) != -1)
	    {
	      num = index(A.Index(i, j));
	      k = size_col(num);
	      V.Value(num).Index(k) = i;
	      V.Value(num).Value(k) = A.Value(i, j);
	      size_col(num)++;
	    }
	
	if (index(i) != -1)
	  for (int j = 0; j < A.GetRowSize(i); j++)
	    if (A.Index(i, j) != i)
	      {
		num = index(i);
		k = size_col(num);
		V.Value(num).Index(k) = A.Index(i,j);
		V.Value(num).Value(k) = A.Value(i, j);
		size_col(num)++;
	      }
      }
  }


  //! extracts several columns of a sparse matrix
  /*!
    \param[in] A sparse matrix where columns are extracted
    \param[in] col_number numbers of the columns to be extracted
    \param[out] V extracted sparse columns    
   */  
  template<class T1, class Prop, class Allocator,
	   class T2, class Allocator2, class Allocator3>
  void GetCol(const Matrix<T1, Prop, ArrayRowSparse, Allocator>& A, const IVect& col_number,
	      Vector<Vector<T2, VectSparse, Allocator2>, VectSparse, Allocator3>& V)
  {
    int m = col_number.GetM();
    V.Reallocate(m);
    // counting size of columns
    IVect size_col(m);
    IVect index(A.GetM()); index.Fill(-1);
    for (int i = 0; i < m; i++)
      index(col_number(i)) = i;
    
    size_col.Fill(0);
    for (int i = 0; i < A.GetM(); i++)
      for (int j = 0; j < A.GetRowSize(i); j++)
	if (index(A.Index(i, j)) != -1)
	  size_col(index(A.Index(i, j)))++;
    
    // now we fill columns
    for (int i = 0; i < m; i++)
      {
	V.Index(i) = col_number(i);
	V.Value(i).Reallocate(size_col(i));
      }
    
    size_col.Zero();
    for (int i = 0; i < A.GetM(); i++)
      for (int j = 0; j < A.GetRowSize(i); j++)
	if (index(A.Index(i, j)) != -1)
	  {
	    int num = index(A.Index(i, j));
	    int k = size_col(num);
	    V.Value(num).Index(k) = i;
	    V.Value(num).Value(k) = A.Value(i, j);
	    size_col(num)++;
	  }
  }
  

  //! extracts several columns of a sparse matrix
  /*!
    \param[in] A sparse matrix where columns are extracted
    \param[in] col_number numbers of the columns to be extracted
    \param[out] V extracted sparse columns    
   */
  template<class T1, class Prop, class Allocator,
	   class T2, class Allocator2, class Allocator3>
  void GetCol(const Matrix<T1, Prop, ArrayRowSymComplexSparse, Allocator>& A,
              const IVect& col_number,
	      Vector<Vector<T2, VectSparse, Allocator2>, VectSparse, Allocator3>& V)
  {
    int m = col_number.GetM();
    V.Reallocate(m);
    // counting size of columns
    IVect size_col(m);
    IVect index(A.GetM()); index.Fill(-1);
    for (int i = 0; i < m; i++)
      index(col_number(i)) = i;
    
    size_col.Fill(0); int num, k;
    for (int i = 0; i < A.GetM(); i++)
      {
	if (index(i) != -1)
	  for (int j = 0; j < A.GetRealRowSize(i); j++)
	    if (A.IndexReal(i,j) != i)
	      size_col(index(i))++;
	
	for (int j = 0; j < A.GetRealRowSize(i); j++)
	  {
	    num = index(A.IndexReal(i,j));
	    if (num != -1)
	      size_col(num)++;
	  }

	if (index(i) != -1)
	  for (int j = 0; j < A.GetImagRowSize(i); j++)
	    if (A.IndexImag(i,j) != i)
	      size_col(index(i))++;
	
	for (int j = 0; j < A.GetImagRowSize(i); j++)
	  {
	    num = index(A.IndexImag(i,j));
	    if (num != -1)
	      size_col(num)++;
	  }
      }
    
    // now we fill columns
    for (int i = 0; i < m; i++)
      {
	V.Index(i) = col_number(i);
	V.Value(i).Reallocate(size_col(i));
      }
    
    size_col.Zero();
    for (int i = 0; i < A.GetM(); i++)
      {
	for (int j = 0; j < A.GetRealRowSize(i); j++)
	  if (index(A.IndexReal(i, j)) != -1)
	    {
	      num = index(A.IndexReal(i, j));
	      k = size_col(num);
	      V.Value(num).Index(k) = i;
	      V.Value(num).Value(k) = T1(A.ValueReal(i, j), 0);
	      size_col(num)++;
	    }
	
	if (index(i) != -1)
	  for (int j = 0; j < A.GetRealRowSize(i); j++)
	    if (A.IndexReal(i, j) != i)
	      {
		num = index(i);
		k = size_col(num);
		V.Value(num).Index(k) = A.IndexReal(i,j);
		V.Value(num).Value(k) = T1(A.ValueReal(i, j), 0);
		size_col(num)++;
	      }

	for (int j = 0; j < A.GetImagRowSize(i); j++)
	  if (index(A.IndexImag(i, j)) != -1)
	    {
	      num = index(A.IndexImag(i, j));
	      k = size_col(num);
	      V.Value(num).Index(k) = i;
	      V.Value(num).Value(k) = T1(0, A.ValueImag(i, j));
	      size_col(num)++;
	    }
	
	if (index(i) != -1)
	  for (int j = 0; j < A.GetImagRowSize(i); j++)
	    if (A.IndexImag(i, j) != i)
	      {
		num = index(i);
		k = size_col(num);
		V.Value(num).Index(k) = A.IndexImag(i,j);
		V.Value(num).Value(k) = T1(0, A.ValueImag(i, j));
		size_col(num)++;
	      }
      }
    
    // assembling vector to have sorted row numbers
    for (int i = 0; i < m; i++)
      V.Value(i).Assemble();
  }


  //! extracts several columns of a sparse matrix
  /*!
    \param[in] A sparse matrix where columns are extracted
    \param[in] col_number numbers of the columns to be extracted
    \param[out] V extracted sparse columns    
   */  
  template<class T1, class Prop, class Allocator,
	   class T2, class Allocator2, class Allocator3>
  void GetCol(const Matrix<T1, Prop, ArrayRowComplexSparse, Allocator>& A,
              const IVect& col_number,
	      Vector<Vector<T2, VectSparse, Allocator2>, VectSparse, Allocator3>& V)
  {
    int m = col_number.GetM();
    V.Reallocate(m);
    // counting size of columns
    IVect size_col(m);
    IVect index(A.GetM()); index.Fill(-1);
    for (int i = 0; i < m; i++)
      index(col_number(i)) = i;
    
    size_col.Fill(0);
    for (int i = 0; i < A.GetM(); i++)
      {
	for (int j = 0; j < A.GetRealRowSize(i); j++)
	  if (index(A.IndexReal(i, j)) != -1)
	    size_col(index(A.IndexReal(i, j)))++;

	for (int j = 0; j < A.GetImagRowSize(i); j++)
	  if (index(A.IndexImag(i, j)) != -1)
	    size_col(index(A.IndexImag(i, j)))++;
      }
    
    // now we fill columns
    for (int i = 0; i < m; i++)
      {
	V.Index(i) = col_number(i);
	V.Value(i).Reallocate(size_col(i));
      }
    
    size_col.Zero();
    for (int i = 0; i < A.GetM(); i++)
      {
	for (int j = 0; j < A.GetRealRowSize(i); j++)
	  if (index(A.IndexReal(i, j)) != -1)
	    {
	      int num = index(A.IndexReal(i, j));
	      int k = size_col(num);
	      V.Value(num).Index(k) = i;
	      V.Value(num).Value(k) = T1(A.ValueReal(i, j), 0);
	      size_col(num)++;
	    }

	for (int j = 0; j < A.GetImagRowSize(i); j++)
	  if (index(A.IndexImag(i, j)) != -1)
	    {
	      int num = index(A.IndexImag(i, j));
	      int k = size_col(num);
	      V.Value(num).Index(k) = i;
	      V.Value(num).Value(k) = T1(0, A.ValueImag(i, j));
	      size_col(num)++;
	    }
      }
    
    // assembling vector to have sorted row numbers
    for (int i = 0; i < m; i++)
      V.Value(i).Assemble();
  }
  

  //! extracts several columns of a sparse matrix
  /*!
    \param[in] A sparse matrix where columns are extracted
    \param[in] col_number numbers of the columns to be extracted
    \param[out] V extracted sparse columns    
   */  
  template<class T, class Prop, class Allocator,
           class T2, class Allocator2, class Allocator3>
  void GetCol(const Matrix<T, Prop, BlockDiagRowSym, Allocator>& A, const IVect& col_number,
	      Vector<Vector<T2, VectSparse, Allocator2>, VectSparse, Allocator3>& V)
  {
    abort();
  }
  
  
  //! extracts several columns of a sparse matrix
  /*!
    \param[in] A sparse matrix where columns are extracted
    \param[in] col_number numbers of the columns to be extracted
    \param[out] V extracted sparse columns    
   */
  template<class T, class Prop, class Allocator,
           class T2, class Allocator2, class Allocator3>
  void GetCol(const Matrix<T, Prop, BlockDiagRow, Allocator>& A, const IVect& col_number,
	      Vector<Vector<T2, VectSparse, Allocator2>, VectSparse, Allocator3>& V)
  {
    abort();
  }
  
  
  //! extracts several columns of a sparse matrix
  /*!
    \param[in] A sparse matrix where columns are extracted
    \param[in] col_number numbers of the columns to be extracted
    \param[out] V extracted sparse columns    
   */
  template<class T, class Prop, class Allocator,
           class T2, class Allocator2, class Allocator3>
  void GetCol(const Matrix<T, Prop, DiagonalRow, Allocator>& A, const IVect& col_number,
	      Vector<Vector<T2, VectSparse, Allocator2>, VectSparse, Allocator3>& V)
  {
    int m = col_number.GetM();
    if (m <= 0)
      {
        V.Clear();
        return;
      }
    
    V.Reallocate(m);
    for (int ic = 0; ic < m; ic++)
      {
        int i = col_number(ic);
        V.Index(0) = i;
        V.Value(0).Reallocate(1);
        V.Value(0).Index(0) = i;
        V.Value(0).Value(0) = A(i, i);
      }
  }
  
  
  //! clears several columns of a sparse matrix
  /*!
    \param[in] col_number numbers of the columns to be cleared
    \param[inout] A sparse matrix where columns are erased
   */
  template<class T1, class Prop, class Allocator>
  void EraseCol(const IVect& col_number,
		Matrix<T1, Prop, DiagonalRow, Allocator>& A)
  {
    for (int ic = 0; ic < col_number.GetM(); ic++)
      {
        int i = col_number(ic);
        A.Get(i, i) = 0.0;
      }
  }
   

  //! clears several rows of a sparse matrix
  /*!
    \param[in] col_number numbers of the rows to be cleared
    \param[inout] A sparse matrix where rows are erased
   */
  template<class T1, class Prop, class Allocator>
  void EraseRow(const IVect& col_number,
		Matrix<T1, Prop, DiagonalRow, Allocator>& A)
  {
    EraseCol(col_number, A);
  }
  

  template<class T1, class Prop, class Allocator>
  void EraseCol(const IVect& col_number,
		Matrix<T1, Prop, BandedCol, Allocator>& A)
  {
    int kl = A.GetKL();
    int ku = A.GetKU();
    int m = A.GetM();
    int n = A.GetN();
    Vector<bool> SelectedCol(n);
    SelectedCol.Fill(false);
    for (int i = 0; i < col_number.GetM(); i++)
      SelectedCol(col_number(i)) = true;
    
    T1 zero; SetComplexZero(zero);
    for (int i = 0; i < m; i++)
      for (int k = max(-i, -kl); k <= min(ku, n-1-i); k++)
        {
          int j = i + k;
	  if (SelectedCol(j))
	    A.Set(i, j, zero);
        }
  }
  
  
  template<class T1, class Prop, class Allocator>
  void EraseRow(const IVect& row_number,
		Matrix<T1, Prop, BandedCol, Allocator>& A)
  {
    int kl = A.GetKL();
    int ku = A.GetKU();
    //int m = A.GetM();
    int n = A.GetN();
    T1 zero; SetComplexZero(zero);
    for (int i1 = 0; i1 < row_number.GetM(); i1++)
      {
	int i = row_number(i1);
	for (int k = max(-i, -kl); k <= min(ku, n-1-i); k++)
	  {
	    int j = i + k;
	    A.Set(i, j, zero);
	  }
      }
  }


  template<class T1, class Prop, class Allocator>
  void EraseCol(const IVect& col_number,
		Matrix<T1, Prop, BlockDiagRow, Allocator>& A)
  {
    for (int ic = 0; ic < col_number.GetM(); ic++)
      {
        int i = col_number(ic);
	A.ClearColumn(i);
      }
  }

  
  template<class T1, class Prop, class Allocator>
  void EraseRow(const IVect& col_number,
		Matrix<T1, Prop, BlockDiagRow, Allocator>& A)
  {
    for (int ic = 0; ic < col_number.GetM(); ic++)
      {
        int i = col_number(ic);
	A.ClearRow(i);
      }
  }
  

  template<class T1, class Prop, class Allocator>
  void EraseCol(const IVect& col_number,
		Matrix<T1, Prop, BlockDiagRowSym, Allocator>& A)
  {
    for (int ic = 0; ic < col_number.GetM(); ic++)
      {
        int i = col_number(ic);
	A.ClearColumn(i);
      }
  }
  
  
  template<class T1, class Prop, class Allocator>
  void EraseRow(const IVect& col_number,
		Matrix<T1, Prop, BlockDiagRowSym, Allocator>& A)
  {
    for (int ic = 0; ic < col_number.GetM(); ic++)
      {
        int i = col_number(ic);
	A.ClearRow(i);
      }
  }
  
  
  //! For each row of the matrix, computation of the sum of absolute values
  /*!
    \param[out] diagonal_scale_left vector containing the sum of
                            the magnitudes of non-zero entries of each row
    \param[in] mat given matrix
   */
  template<class T, class Complexe, class Allocator>
  void GetRowSum(Vector<T>& diagonal_scale_left,
		 const Matrix<Complexe, General, ArrowCol, Allocator>& A)
  {
    int kl = A.GetKL();
    int ku = A.GetKU();
    int m = A.GetM() - A.GetNbLastRow();
    int n = A.GetN() - A.GetNbLastCol(); 
    diagonal_scale_left.Reallocate(A.GetM());
    diagonal_scale_left.Fill(0);
    
    for (int i = 0; i < m; i++)
      for (int k = max(-i, -kl); k <= min(ku, n-1-i); k++)
        {
          int j = i + k;
          diagonal_scale_left(i) += abs(A(i, j));
        }
    
    for (int i = 0; i < A.GetNbLastRow(); i++)
      for (int j = 0; j < A.GetN(); j++)
        diagonal_scale_left(m+i) += abs(A(m+i, j));
    
    for (int j = 0; j < A.GetNbLastCol(); j++)
      for (int i = 0; i < m; i++)
        diagonal_scale_left(i) += abs(A(i, n+j));    
  }
  

  //! For each column of the matrix, computation of the sum of absolute values
  /*!
    \param[out] diagonal_scale vector containing the sum of
                            the magnitudes of non-zero entries of each column
    \param[in] mat given matrix
   */
  template<class T, class Complexe, class Allocator>
  void GetColSum(Vector<T>& diagonal_scale,
		 const Matrix<Complexe, General, ArrowCol, Allocator>& A)
  {
    int kl = A.GetKL();
    int ku = A.GetKU();
    int m = A.GetM() - A.GetNbLastRow();
    int n = A.GetN() - A.GetNbLastCol(); 
    diagonal_scale.Reallocate(A.GetN());
    diagonal_scale.Fill(0);
    
    for (int i = 0; i < m; i++)
      for (int k = max(-i, -kl); k <= min(ku, n-1-i); k++)
        {
          int j = i + k;
          diagonal_scale(j) += abs(A(i, j));
        }
    
    for (int i = 0; i < A.GetNbLastRow(); i++)
      for (int j = 0; j < A.GetN(); j++)
        diagonal_scale(j) += abs(A(m+i, j));
    
    for (int j = 0; j < A.GetNbLastCol(); j++)
      for (int i = 0; i < m; i++)
        diagonal_scale(n+j) += abs(A(i, n+j));    
  }

  
  //! For each row and column of the matrix, computation of the sum of absolute values
  /*!
    \param[out] sum_row vector containing the sum of
                            the magnitudes of non-zero entries of each row
    \param[out] sum_col vector containing the sum of
                            the magnitudes of non-zero entries of each column
    \param[in] A given matrix
   */
  template<class T, class Complexe, class Allocator>
  void GetRowColSum(Vector<T>& sum_row, Vector<T>& sum_col,
                    const Matrix<Complexe, General, BandedCol, Allocator>& A)
  {
    int kl = A.GetKL();
    int ku = A.GetKU();
    int m = A.GetM();
    int n = A.GetN();
    sum_row.Reallocate(A.GetM());
    sum_row.Fill(0);
    sum_col.Reallocate(A.GetN());
    sum_col.Fill(0);
    
    for (int i = 0; i < m; i++)
      for (int k = max(-i, -kl); k <= min(ku, n-1-i); k++)
        {
          int j = i + k;
          sum_row(i) += abs(A(i, j));
          sum_col(j) += abs(A(i, j));
        }
  }


  //! For each row and column of the matrix, computation of the sum of absolute values
  /*!
    \param[out] sum_row vector containing the sum of
                            the magnitudes of non-zero entries of each row
    \param[out] sum_col vector containing the sum of
                            the magnitudes of non-zero entries of each column
    \param[in] A given matrix
   */
  template<class T, class Complexe, class Allocator>
  void GetRowColSum(Vector<T>& sum_row, Vector<T>& sum_col,
                    const Matrix<Complexe, General, ArrowCol, Allocator>& A)
  {
    int kl = A.GetKL();
    int ku = A.GetKU();
    int m = A.GetM() - A.GetNbLastRow();
    int n = A.GetN() - A.GetNbLastCol(); 
    sum_row.Reallocate(A.GetM());
    sum_row.Fill(0);
    sum_col.Reallocate(A.GetN());
    sum_col.Fill(0);
    
    for (int i = 0; i < m; i++)
      for (int k = max(-i, -kl); k <= min(ku, n-1-i); k++)
        {
          int j = i + k;
          sum_row(i) += abs(A(i, j));
          sum_col(j) += abs(A(i, j));
        }
    
    for (int i = 0; i < A.GetNbLastRow(); i++)
      for (int j = 0; j < A.GetN(); j++)
        {
          sum_row(m+i) += abs(A(m+i, j));
          sum_col(j) += abs(A(m+i, j));
        }
    
    for (int j = 0; j < A.GetNbLastCol(); j++)
      for (int i = 0; i < m; i++)
        {
          sum_row(i) += abs(A(i, n+j));
          sum_col(n+j) += abs(A(i, n+j));
        }
  }
  
  
  //! Multiplication of rows by coefficients contained in coef_row
  /*!
    Equivalent Matlab function A = coef_row*A
   */
  template<class T, class Allocator, class T0>
  void ScaleLeftMatrix(Matrix<T, General, ArrowCol, Allocator>& A,
                       const Vector<T0>& coef_row)
  {
    int kl = A.GetKL();
    int ku = A.GetKU();
    int m = A.GetM() - A.GetNbLastRow();
    int n = A.GetN() - A.GetNbLastCol(); 
    
    for (int i = 0; i < m; i++)
      for (int k = max(-i, -kl); k <= min(ku, n-1-i); k++)
        {
          int j = i + k;
          A.Get(i, j) *= coef_row(i);
        }
    
    for (int i = 0; i < A.GetNbLastRow(); i++)
      for (int j = 0; j < A.GetN(); j++)
        A.Get(m+i, j) *= coef_row(m+i);
    
    for (int j = 0; j < A.GetNbLastCol(); j++)
      for (int i = 0; i < m; i++)
        A.Get(i, n+j) *= coef_row(i);
  }
  
  
  //! copy real part of A in B
  template<class T, class Prop, class Allocator1, class Allocator2>
  void CopyReal(const Matrix<complex<T>, Prop, ArrayRowSymComplexSparse, Allocator1>& A,
		Matrix<T, Prop, ArrayRowSymSparse, Allocator2>& B)
  {
    B.Reallocate(A.GetM(), A.GetN());
    for (int i = 0; i < A.GetM(); i++)
      {
	int nb = A.GetRealRowSize(i);
	B.ReallocateRow(i, nb);
        for (int j = 0; j < nb; j++)
          {
            B.Index(i, j) = A.IndexReal(i, j);
            B.Value(i, j) = A.ValueReal(i, j);
          }
      }
  }
  
  
  //! copy real part of A in B
  template<class T, class Prop, class Allocator1, class Allocator2>
  void CopyReal(const Matrix<complex<T>, Prop, ArrayRowSymComplexSparse, Allocator1>& A,
		Matrix<T, Prop, ArrayRowSymComplexSparse, Allocator2>& B)
  {
    B.Reallocate(A.GetM(), A.GetN());
    for (int i = 0; i < A.GetM(); i++)
      {
	int nb = A.GetRealRowSize(i);
	B.ReallocateRealRow(i, nb);
        for (int j = 0; j < nb; j++)
          {
            B.IndexReal(i, j) = A.IndexReal(i, j);
            B.ValueReal(i, j) = A.ValueReal(i, j);
          }
      }
  }
  
  
  //! copy real part of A in B
  template<class T, class Prop, class Allocator1, class Allocator2>
  void CopyReal(const Matrix<complex<T>, Prop, ArrayRowSymSparse, Allocator1>& A,
		Matrix<T, Prop, ArrayRowSymSparse, Allocator2>& B)
  {
    B.Reallocate(A.GetM(), A.GetN());
    for (int i = 0; i < A.GetM(); i++)
      {
	int nb = A.GetRowSize(i);
	B.ReallocateRow(i, nb);
        for (int j = 0; j < nb; j++)
          {
            B.Index(i, j) = A.Index(i, j);
            B.Value(i, j) = real(A.Value(i, j));
          }
      }    
  }
  
    
  //! B = real(A)
  template<class T1, class Prop1, class Storage1, class Allocator1,
           class T2, class Prop2, class Storage2, class Allocator2>
  void CopyReal(const Matrix<T1, Prop1, Storage1, Allocator1>& A,
		Matrix<T2, Prop2, Storage2, Allocator2>& B)
  {
    cout << "not implemented" << endl;
    abort();
  }
  
  
  //! Extracts a sub-matrix from a given sparse matrix
  /*!
    \param[in] A given sparse matrix
    \param[in] m first row/column to extract
    \param[in] n last row/column+1 to extract
    \param[out] B extracted sub-matrix of size (m-n) x (m-n)
    Equivalent Matlab function B = A(m:n-1, m:n-1)
   */
  template<class T, class Allocator>
  void GetSubMatrix(const Matrix<T, General, ArrayRowSparse, Allocator>& A,
                    int m, int n, Matrix<T, General, ArrayRowSparse, Allocator>& B)
  {
    GetSubMatrix(A, m, n, m, n, B);
  }

  
  //! Extracts a sub-matrix from a given sparse matrix
  /*!
    \param[in] A given sparse matrix
    \param[in] m1 first row to extract
    \param[in] m2 last row+1 to extract
    \param[in] n1 first column to extract
    \param[in] n2 last column+1 to extract
    \param[out] B extracted sub-matrix of size (m2-m1) x (n2-n1)
    Equivalent Matlab function B = A(m1:m2-1, n1:n2-1)
   */  
  template<class T, class Allocator>
  void GetSubMatrix(const Matrix<T, General, ArrayRowSparse, Allocator>& A,
                    int m1, int m2, int n1, int n2,
                    Matrix<T, General, ArrayRowSparse, Allocator>& B)
  {
    int M = m2 - m1;
    int N = n2 - n1;
    B.Reallocate(M, N);
    for (int i = m1; i < m2; i++)
      {
        int size_row = A.GetRowSize(i);
        int nb = 0;
        // counting the number of elements in the row
        for (int j = 0; j < size_row; j++)
          {
            int icol = A.Index(i, j);
            if ((icol >= n1)&&(icol < n2))
              nb++;
          }
        
        // filling the row
        B.ReallocateRow(i-m1, nb);
        nb = 0;
        for (int j = 0; j < size_row; j++)
          {
            int icol = A.Index(i, j);
            if ((icol >= n1)&&(icol < n2))
              {
                B.Index(i-m1, nb) = icol - n1;
                B.Value(i-m1, nb) = A.Value(i, j);
                nb++;
              }
          }
      }
  }
  

  //! Extracts a sub-matrix from a given sparse matrix
  /*!
    \param[in] A given sparse matrix
    \param[in] m first row/column to extract
    \param[in] n last row/column+1 to extract
    \param[out] B extracted sub-matrix of size (m-n) x (m-n)
    Equivalent Matlab function B = A(m:n-1, m:n-1)
   */
  template<class T, class Allocator>
  void GetSubMatrix(const Matrix<T, Symmetric, ArrayRowSymSparse, Allocator>& A,
                    int m, int n, Matrix<T, Symmetric, ArrayRowSymSparse, Allocator>& B)
  {
    GetSubMatrix(A, m, n, m, n, B);
  }

  
  //! Extracts a sub-matrix from a given sparse matrix
  /*!
    \param[in] A given sparse matrix
    \param[in] m1 first row to extract
    \param[in] m2 last row+1 to extract
    \param[in] n1 first column to extract
    \param[in] n2 last column+1 to extract
    \param[out] B extracted sub-matrix of size (m2-m1) x (n2-n1)
    Equivalent Matlab function B = A(m1:m2-1, n1:n2-1)
   */  
  template<class T, class Allocator>
  void GetSubMatrix(const Matrix<T, Symmetric, ArrayRowSymSparse, Allocator>& A,
                    int m1, int m2, int n1, int n2,
                    Matrix<T, Symmetric, ArrayRowSymSparse, Allocator>& B)
  {
    int M = m2 - m1;
    int N = n2 - n1;
    if (M != N)
      {
        cout << "A symmetric matrix must be squared " << endl;
        abort();
      }
    
    B.Reallocate(M, N);
    for (int i = m1; i < m2; i++)
      {
        int size_row = A.GetRowSize(i);
        int nb = 0;
        for (int j = 0; j < size_row; j++)
          {
            int icol = A.Index(i, j);
            if ((icol >= n1)&&(icol < n2) && (icol-n1 >= i-m1))
              nb++;
          }
        
        B.ReallocateRow(i-m1, nb);
        nb = 0;
        for (int j = 0; j < size_row; j++)
          {
            int icol = A.Index(i, j);
            if ((icol >= n1)&&(icol < n2)&& (icol-n1 >= i-m1))
              {
                B.Index(i-m1, nb) = icol - n1;
                B.Value(i-m1, nb) = A.Value(i, j);
                nb++;
              }
          }
      }
  }
  

  //! Extracts a sub-matrix from a given sparse matrix  
  template<class T, class Prop, class Storage, class Allocator>
  void GetSubMatrix(const Matrix<T, Prop, Storage, Allocator>& A,
                    int m, int n, Matrix<T, Prop, Storage, Allocator>& B)
  {
    cout << "not implemented" << endl;
    abort();
  }
  
  
  //! Extracts a sub-matrix from a given sparse matrix
  template<class T, class Prop, class Storage, class Allocator>
  void GetSubMatrix(const Matrix<T, Prop, Storage, Allocator>& A,
                    int m1, int m2, int n1, int n2,
                    Matrix<T, Prop, Storage, Allocator>& B)
  {
    cout << "not implemented" << endl;
    abort();
  }
  

  //! Returns Frobenius norm of matrix
  template<class T, class Prop, class Storage, class Allocator>
  typename ClassComplexType<T>::Treal NormFro(const Matrix<T, Prop, Storage, Allocator>& A)
  {
    // generic function
    typename ClassComplexType<T>::Treal res(0);
    Vector<T, Vect_Full, Allocator> x;
    if (A.GetDataSize() > 0)
      {
        x.SetData(A.GetDataSize(), A.GetData());
        res = Norm2(x); 
        x.Nullify();
      }
    
    return res;
  }
  

  //! Returns Frobenius norm of matrix
  template<class T, class Prop, class Allocator>
  typename ClassComplexType<T>::Treal NormFro(const Matrix<T, Prop, ArrayRowSparse, Allocator>& A)
  {
    typename ClassComplexType<T>::Treal res(0);
    for (int i = 0; i < A.GetM(); i++)
      for (int j = 0; j < A.GetRowSize(i); j++)
        res += absSquare(A.Value(i, j));
    
    res = sqrt(res);
    return res;
  }


  //! Returns Frobenius norm of matrix
  template<class T, class Prop, class Allocator>
  typename ClassComplexType<T>::Treal
  NormFro(const Matrix<T, Prop, ArrayRowSymSparse, Allocator>& A)
  {
    typename ClassComplexType<T>::Treal res(0);
    for (int i = 0; i < A.GetM(); i++)
      for (int j = 0; j < A.GetRowSize(i); j++)
        {
          if (i == A.Index(i, j))
            res += absSquare(A.Value(i, j));
          else
            res += 2.0*absSquare(A.Value(i, j));
        }
    
    res = sqrt(res);
    return res;
  }


  //! Returns Frobenius norm of matrix
  template<class T, class Prop, class Allocator>
  typename ClassComplexType<T>::Treal
  NormFro(const Matrix<T, Prop, ArrayRowComplexSparse, Allocator>& A)
  {
    typename ClassComplexType<T>::Treal res(0);
    for (int i = 0; i < A.GetM(); i++)
      {
        for (int j = 0; j < A.GetRealRowSize(i); j++)
          res += square(A.ValueReal(i, j));

        for (int j = 0; j < A.GetImagRowSize(i); j++)
          res += square(A.ValueImag(i, j));
      }
    
    res = sqrt(res);
    return res;
  }


  //! Returns Frobenius norm of matrix
  template<class T, class Prop, class Allocator>
  typename ClassComplexType<T>::Treal
  NormFro(const Matrix<T, Prop, ArrayRowSymComplexSparse, Allocator>& A)
  {
    typename ClassComplexType<T>::Treal res(0);
    for (int i = 0; i < A.GetM(); i++)
      {
        for (int j = 0; j < A.GetRealRowSize(i); j++)
          {
            if (i == A.IndexReal(i, j))
              res += square(A.ValueReal(i, j));
            else
              res += 2.0*square(A.ValueReal(i, j));
          }

        for (int j = 0; j < A.GetImagRowSize(i); j++)
          {
            if (i == A.IndexImag(i, j))
              res += square(A.ValueImag(i, j));
            else
              res += 2.0*square(A.ValueImag(i, j));
          }
      }
    
    res = sqrt(res);
    return res;
  }
  
}

#define SELDON_FILE_FUNCTION_MATRIX_EXTRACTION_CXX
#endif

