#ifndef SELDON_FILE_SELDON_REDIRECTION_INLINE_CXX

namespace Seldon
{
  
  /*****************
   * Dense vectors *
   *****************/
  

  template<>
  inline void Vector<bool, VectFull, MallocAlloc<bool> >::Reallocate(int n)
  {
    if (n < this->GetM())
      {
	Clear();
	ReallocateVector(n);
      }
    else
      ReallocateVector(n);
  }


  template<>
  inline void Vector<int, VectFull, MallocAlloc<int> >::Reallocate(int n)
  {
    if (n < this->GetM())
      {
	Clear();
	ReallocateVector(n);
      }
    else
      ReallocateVector(n);
  }


  template<>
  inline void Vector<int64_t, VectFull, MallocAlloc<int64_t> >::Reallocate(int n)
  {
    if (n < this->GetM())
      {
	Clear();
	ReallocateVector(n);
      }
    else
      ReallocateVector(n);
  }


  template<>
  inline void Vector<double, VectFull, MallocAlloc<double> >::Reallocate(int n)
  {
    if (n < this->GetM())
      {
	Clear();
	ReallocateVector(n);
      }
    else
      ReallocateVector(n);
  }


  template<>
  inline void Vector<complex<double>, VectFull, MallocAlloc<complex<double> > >::Reallocate(int n)
  {
    if (n < this->GetM())
      {
	Clear();
	ReallocateVector(n);
      }
    else
      ReallocateVector(n);
  }

  
  template<>
  inline void Vector<bool, VectFull, MallocAlloc<bool> >::Resize(int n)
  {
    if (n < this->GetM())
      ResizeVector(n);
    else
      ReallocateVector(n);
  }


  template<>
  inline void Vector<int, VectFull, MallocAlloc<int> >::Resize(int n)
  {
    if (n < this->GetM())
      ResizeVector(n);
    else
      ReallocateVector(n);
  }


  template<>
  inline void Vector<int64_t, VectFull, MallocAlloc<int64_t> >::Resize(int n)
  {
    if (n < this->GetM())
      ResizeVector(n);
    else
      ReallocateVector(n);
  }


  template<>
  inline void Vector<double, VectFull, MallocAlloc<double> >::Resize(int n)
  {
    if (n < this->GetM())
      ResizeVector(n);
    else
      ReallocateVector(n);
  }


  template<>
  inline void Vector<complex<double>, VectFull, MallocAlloc<complex<double> > >::Resize(int n)
  {
    if (n < this->GetM())
      ResizeVector(n);
    else
      ReallocateVector(n);
  }

  
  /******************
   * Sparse vectors *
   ******************/
  

  template<>
  inline void Vector<int, VectSparse, MallocAlloc<int> >::Reallocate(int n)
  {
    if (n < this->GetM())
      {
	Clear();
	ReallocateVector(n);
      }
    else
      ReallocateVector(n);
  }


  template<>
  inline void Vector<int64_t, VectSparse, MallocAlloc<int64_t> >::Reallocate(int n)
  {
    if (n < this->GetM())
      {
	Clear();
	ReallocateVector(n);
      }
    else
      ReallocateVector(n);
  }


  template<>
  inline void Vector<double, VectSparse, MallocAlloc<double> >::Reallocate(int n)
  {
    if (n < this->GetM())
      {
	Clear();
	ReallocateVector(n);
      }
    else
      ReallocateVector(n);
  }


  template<>
  inline void Vector<complex<double>, VectSparse, MallocAlloc<complex<double> > >::Reallocate(int n)
  {
    if (n < this->GetM())
      {
	Clear();
	ReallocateVector(n);
      }
    else
      ReallocateVector(n);
  }

  
  template<>
  inline void Vector<int, VectSparse, MallocAlloc<int> >::Resize(int n)
  {
    if (n < this->GetM())
      ResizeVector(n);
    else
      ReallocateVector(n);
  }


  template<>
  inline void Vector<int64_t, VectSparse, MallocAlloc<int64_t> >::Resize(int n)
  {
    if (n < this->GetM())
      ResizeVector(n);
    else
      ReallocateVector(n);
  }


  template<>
  inline void Vector<double, VectSparse, MallocAlloc<double> >::Resize(int n)
  {
    if (n < this->GetM())
      ResizeVector(n);
    else
      ReallocateVector(n);
  }


  template<>
  inline void Vector<complex<double>, VectSparse, MallocAlloc<complex<double> > >::Resize(int n)
  {
    if (n < this->GetM())
      ResizeVector(n);
    else
      ReallocateVector(n);
  }

  
  /*****************
   * Zero for mpfr *
   *****************/
  
  
#ifdef MONTJOIE_WITH_MPFR
  template<>
  inline void Vector<mpfr::mpreal>::Zero()
  {
    mpfr::mpreal zero(0);
    Fill(zero);
  }


  template<>
  inline void Vector<complex<mpfr::mpreal> >::Zero()
  {
    complex<mpfr::mpreal> zero(0, 0);
    Fill(zero);
  }


  template<>
  inline void Matrix_Pointers<mpfr::mpreal, General, RowMajor>::Zero()
  {
    mpfr::mpreal zero(0);
    Fill(zero);
  }


  template<>
  inline void Matrix_Pointers<complex<mpfr::mpreal>, General, RowMajor>::Zero()
  {
    complex<mpfr::mpreal> zero(0, 0);
    Fill(zero);
  }


  template<>
  inline void Matrix_Pointers<mpfr::mpreal, General, ColMajor>::Zero()
  {
    mpfr::mpreal zero(0);
    Fill(zero);
  }


  template<>
  inline void Matrix_Pointers<complex<mpfr::mpreal>, General, ColMajor>::Zero()
  {
    complex<mpfr::mpreal> zero(0, 0);
    Fill(zero);
  }


  template<>
  inline void Matrix_Symmetric<mpfr::mpreal, Symmetric, RowSym>::Zero()
  {
    mpfr::mpreal zero(0);
    Fill(zero);
  }


  template<>
  inline void Matrix_Symmetric<complex<mpfr::mpreal>, Symmetric, RowSym>::Zero()
  {
    complex<mpfr::mpreal> zero(0, 0);
    Fill(zero);
  }


  template<>
  inline void Matrix_SymPacked<mpfr::mpreal, Symmetric, RowSymPacked>::Zero()
  {
    mpfr::mpreal zero(0);
    Fill(zero);
  }


  template<>
  inline void Matrix_SymPacked<complex<mpfr::mpreal>, Symmetric, RowSymPacked>::Zero()
  {
    complex<mpfr::mpreal> zero(0, 0);
    Fill(zero);
  }


  template<>
  inline void Matrix_HermPacked<mpfr::mpreal, Hermitian, RowHermPacked>::Zero()
  {
    mpfr::mpreal zero(0);
    Fill(zero);
  }


  template<>
  inline void Matrix_HermPacked<complex<mpfr::mpreal>, Hermitian, RowHermPacked>::Zero()
  {
    complex<mpfr::mpreal> zero(0, 0);
    Fill(zero);
  }


  template<>
  inline void Matrix_Sparse<mpfr::mpreal, General, RowSparse>::Zero()
  {
    mpfr::mpreal zero(0);
    Fill(zero);
  }


  template<>
  inline void Matrix_Sparse<complex<mpfr::mpreal>, General, RowSparse>::Zero()
  {
    complex<mpfr::mpreal> zero(0, 0);
    Fill(zero);
  }


  template<>
  inline void Matrix_SymSparse<mpfr::mpreal, Symmetric, RowSymSparse>::Zero()
  {
    mpfr::mpreal zero(0);
    Fill(zero);
  }


  template<>
  inline void Matrix_SymSparse<complex<mpfr::mpreal>, Symmetric, RowSymSparse>::Zero()
  {
    complex<mpfr::mpreal> zero(0, 0);
    Fill(zero);
  }

  template<>
  inline void Matrix_ComplexSparse<complex<mpfr::mpreal>, General, RowComplexSparse>::Zero()
  {
    complex<mpfr::mpreal> zero(0, 0);
    Fill(zero);
  }


  template<>
  inline void Matrix_SymComplexSparse<complex<mpfr::mpreal>, Symmetric, RowSymComplexSparse>::Zero()
  {
    complex<mpfr::mpreal> zero(0, 0);
    Fill(zero);
  }


  template<>
  inline void Matrix_Band<mpfr::mpreal, General, BandedCol>::Zero()
  {
    mpfr::mpreal zero(0);
    Fill(zero);
  }


  template<>
  inline void Matrix_Band<complex<mpfr::mpreal>, General, BandedCol>::Zero()
  {
    complex<mpfr::mpreal> zero(0, 0);
    Fill(zero);
  }


  template<>
  inline void Matrix_Arrow<mpfr::mpreal, General, ArrowCol>::Zero()
  {
    mpfr::mpreal zero(0);
    Fill(zero);
  }


  template<>
  inline void Matrix_Arrow<complex<mpfr::mpreal>, General, ArrowCol>::Zero()
  {
    complex<mpfr::mpreal> zero(0, 0);
    Fill(zero);
  }
#endif
  
};

#define SELDON_FILE_SELDON_REDIRECTION_INLINE_CXX
#endif

