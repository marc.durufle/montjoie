#ifndef MONTJOIE_FILE_TINY_BLOCK_SOLVER_1D_HXX

namespace Seldon
{
  template<class T, int r>
  class TinyBlockSolver1D
  {
  protected :

    int nb_elem;
    TinyBandMatrix<T, 1> mat_tridiag;
    TinyMatrix<T, General, 2, r> a12;
    TinyMatrix<T, General, r, 2> a21;
    TinyMatrix<T, General, r, r> inv_a22;



  public :

    void Factorize(const Matrix<T, General, BandedCol>& mat);

    template<class T0>
    void Solve(Vector<T0>& X);

  };

}
#define MONTJOIE_FILE_TINY_BLOCK_SOLVER_1D_HXX
#endif
