#ifndef MONTJOIE_FILE_TINY_BLOCK_SOLVER_1D_CXX

namespace Seldon
{

  template<class T, int r>
  void TinyBlockSolver1D<T, r>
  ::Factorize(const Matrix<T, General, BandedCol>& mat)
  {
    // we retrieve a12, a21 and a22
    for(int i = 0; i < r; i++)
      {
	this->a12(0, i) = mat(0, i+1);
	this->a12(1, i) = mat(r+1, i+1);

	this->a21(i, 0) = mat(i+1, 0);
	this->a21(i, 1) = mat(i+1, r+1);

	for(int j = 0; j < r; j++)
	  this->inv_a22(i, j) = mat(i+1, j+1);
      }

    // a22 is inverteed
    GetInverse(this->inv_a22);

    // initializing mat_tridiag with a11
    this->nb_elem = mat.GetM() / (r + 1);
    int N = this->nb_elem+1;
    this->mat_tridiag.Reallocate(N, N);
    this->mat_tridiag.Get(0, 0) = mat(0, 0);
    for (int i = 1; i < N; i++)
      {
	this->mat_tridiag.Get(i, i) = mat(i*(r+1), i*(r+1));
	this->mat_tridiag.Get(i, i-1) = mat(i*(r+1), (i-1)*(r+1));
	this->mat_tridiag.Get(i-1, i) = mat((i-1)*(r+1), i*(r+1));
      }

    this->mat_tridiag.WriteText("mat_tridiag.dat");

    // we compute schur = a_12 a_22^{-1} a_21
    TinyMatrix<T, General, r, 2> prodTmp;
    TinyMatrix<T, General, 2, 2> schur;
    Mlt(this->inv_a22, this->a21, prodTmp);
    Mlt(this->a12, prodTmp, schur);

    // assembling a_11 - a_12 a_22^{-1} a_21
    for(int i = 0; i < this->nb_elem; i++)
      {
	this->mat_tridiag.Get(i,i) -= schur(0,0);
	this->mat_tridiag.Get(i,i+1) -= schur(0,1);
	this->mat_tridiag.Get(i+1,i) -= schur(1,0);
	this->mat_tridiag.Get(i+1,i+1) -= schur(1,1);
      }

    // tridiagonal matrix is factorized
    this->mat_tridiag.Factorize();

    // a_12 is replaced by a_12 a_22^{-1}
    TinyMatrix<T, General, 2, r> B;
    B = this->a12;
    Mlt(B, this->inv_a22, this->a12);
  }

  
  template<class T, int r> template<class T0>
  void TinyBlockSolver1D<T, r>::Solve(Vector<T0>& X)
  {
    int N = this->nb_elem+1;
    Vector<T0> Xtri(N);
    for (int i = 0; i < N; i++)
      Xtri(i) = X(i*(r+1));
    
    TinyVector<T0, r> G, prodY;
    TinyVector<T0, 2> prodG;
    int offset = 1;
    for(int i = 0; i < this->nb_elem; i++)
      {
	TinyVectorLoop<r>::ExtractVector(X, offset, G);
	
	Mlt(this->a12, G, prodG);
	
	Xtri(i) -= prodG(0);
	Xtri(i+1) -= prodG(1);
	offset += r+1;
      }

    this->mat_tridiag.Solve(Xtri);

    for (int i = 0; i < N; i++)
      X(i*(r+1)) = Xtri(i);

    T one(1);
    offset = 1;
    for(int i = 0; i < this->nb_elem; i++)
      {
	TinyVectorLoop<r>::ExtractVector(X, offset, G);
	
	// we compute G - a_12 X
	prodG(0) = Xtri(i);
	prodG(1) = Xtri(i+1);
	
	MltAdd(-one, this->a21, prodG, G);

	// then we put a_22^{-1} G in X
	Mlt(this->inv_a22, G, prodY);

	TinyVectorLoop<r>::ExtractVector(prodY, offset, X);

	offset += r+1;
      }
  }
  
}

#define MONTJOIE_FILE_TINY_BLOCK_SOLVER_1D_CXX
#endif
