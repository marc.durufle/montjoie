#ifndef MONTJOIE_FILE_DIAGONAL_MATRIX_INLINE_CXX

namespace Seldon
{
  //! default constructor
  template<class T, class Prop, class Allocator>
  inline Matrix<T, Prop, DiagonalRow, Allocator>::Matrix()
  {
  }
  
  
  //! matrix with i rows
  template<class T, class Prop, class Allocator>
  inline Matrix<T, Prop, DiagonalRow, Allocator>::Matrix(int i, int j)
    : VirtualMatrix<T>(i, j)
  {    
    T zero;
    SetComplexZero(zero);
    
    diag.Reallocate(i);
    diag.Fill(zero);
  }
  
  
  //! returns the memory in bytes used by the object
  template<class T, class Prop, class Allocator>
  inline int64_t Matrix<T, Prop, DiagonalRow, Allocator>::GetMemorySize() const 
  {
    return diag.GetMemorySize();
  }
  
  
  //! matrix with i rows
  template<class T, class Prop, class Allocator>
  inline void Matrix<T, Prop, DiagonalRow, Allocator>::Reallocate(int i, int j)
  {
    diag.Reallocate(i);
    this->m_ = i;
    this->n_ = i;
  }
  
  
  //! changing the size of the matrix
  template<class T, class Prop, class Allocator>
  inline void Matrix<T, Prop, DiagonalRow, Allocator>::Resize(int i, int j)
  {
    diag.Resize(i);
    this->m_ = i;
    this->n_ = i;
  }
  
  
  //! returns A(i, j)
  template<class T, class Prop, class Allocator>
  inline T Matrix<T, Prop, DiagonalRow, Allocator>::operator() (int i, int j) const
  {
    if (i == j) 
      return this->diag(i); 
    
    T zero;
    SetComplexZero(zero);
    
    return zero;
  }
  
  
  //! returns access to A(i, j)
  template<class T, class Prop, class Allocator>
  inline T& Matrix<T, Prop, DiagonalRow, Allocator>::Get(int i, int j)
  {
    if (i == j)
      return this->diag(i);
    
    throw WrongArgument("Matrix::Get(i, j)", "only diagonal value can be modified");
  }
  
  
  //! adds val, if i == j
  template<class T, class Prop, class Allocator>
  inline void Matrix<T, Prop, DiagonalRow, Allocator>::AddInteraction(int i, int j, const T& val)
  {
    if (i == j)
      this->diag(i) += val;      
  }
  
  
  //! not in the pattern of the matrix => nothing to do
  template<class T, class Prop, class Allocator>
  inline void Matrix<T, Prop, DiagonalRow, Allocator>
  ::AddDistantInteraction(int i, int j, int proc, const T& val)
  {
  }
  
  
  //! not in the pattern of the matrix => nothing to do
  template<class T, class Prop, class Allocator>
  inline void Matrix<T, Prop, DiagonalRow, Allocator>
  ::AddRowDistantInteraction(int i, int j, int proc, const T& val)
  {
  }
  
  
  //! adds val, if i == col(j)
  template<class T, class Prop, class Allocator>
  inline void Matrix<T, Prop, DiagonalRow, Allocator>
  ::AddInteractionRow(int i, int n, const IVect& col, const Vector<T>& val)
  {    
    for (int k = 0; k < n; k++)
      if (i == col(k))
	this->diag(i) += val(k);      
  }
  
  
  //! returns 1
  template<class T, class Prop, class Allocator>
  inline int Matrix<T, Prop, DiagonalRow, Allocator>::GetRowSize(int i) const
  {
    return 1;
  }
  
  
  //! Releases memory used by the matrix
  template<class T, class Prop, class Allocator>
  inline void Matrix<T, Prop, DiagonalRow, Allocator>::Clear()
  {
    diag.Clear();
  }


  //! Clears a row of the matrix
  template<class T, class Prop, class Allocator>
  inline void Matrix<T, Prop, DiagonalRow, Allocator>::ClearRow(int i)
  {
    SetComplexZero(diag(i));
  }
  
  
  //! sets the pointer storing entries of the matrix
  template<class T, class Prop, class Allocator>
  inline void Matrix<T, Prop, DiagonalRow, Allocator>::SetData(int n, T* data)
  {
    diag.SetData(n, data);
  }
  
  
  //! nullifies the pointer without releasing memory
  template<class T, class Prop, class Allocator>
  inline void Matrix<T, Prop, DiagonalRow, Allocator>::Nullify()
  {
    diag.Nullify();
  }


  template<class T, class Prop, class Allocator>
  inline void Matrix<T, Prop, DiagonalRow, Allocator>::WriteText(const string& file_name) const
  {
  }
  
  
  template<class T, class Prop, class Allocator>
  inline void Matrix<T, Prop, DiagonalRow, Allocator>::WriteText(ostream& FileStream) const
  {
  }
  

  //! matrix vector product
  template<class T, class Prop, class Allocator, class AllocatorVect>
  inline void MltVector(const Matrix<T, Prop, DiagonalRow, Allocator>  M,
			const Vector<T, VectFull, AllocatorVect> A, Vector<T, VectFull, AllocatorVect> & B)
  {
    for (int i = 0; i < M.GetM(); i++)
      B(i) = M(i, i)*A(i);    
  }
  
  
  //! matrix vector product y = beta y + alpha A x
  template<class T0, class T1, class T2, class T3, class T4,
           class Prop, class Allocator, class Allocator1,
           class Allocator2, class Allocator3>
  inline void MltAddVector(const T0& alpha, const Matrix<T1, Prop, DiagonalRow, Allocator1>  A,
			   const Vector<T2, VectFull, Allocator2> x, 
			   const T3& beta, Vector<T4, VectFull, Allocator3> & y)
  {
    for (int i = 0; i < A.GetM(); i++)
      y(i) = beta*y(i) + alpha*A(i, i)*x(i);    
  }
  
}

#define MONTJOIE_FILE_DIAGONAL_MATRIX_INLINE_CXX
#endif
  
