#ifndef MONTJOIE_FILE_EDGE_REFERENCE_INLINE_CXX

namespace Montjoie
{
  
  /************************************
   * ElementGeomReference<Dimension1> *
   ************************************/
  
  
  //! Destructor
  inline ElementGeomReference<Dimension1>::~ElementGeomReference<Dimension1>()
  {
  }
  
  
  //! returns true if the mass matrix is diagonal
  inline bool ElementGeomReference<Dimension1>::LumpedMassMatrix() const
  {
    return mass_lumping;
  }
  
  
  //! returns 0
  inline int ElementGeomReference<Dimension1>::GetHybridType() const
  {
    return 0;
  }
  
  
  //! returns the order of approximation
  inline int ElementGeomReference<Dimension1>::GetOrder() const
  {
    return order;
  }
  
  
  //! returns the order of approximation
  inline int ElementGeomReference<Dimension1>::GetGeometryOrder() const
  {
    return order_geom;
  }
  

  inline const VectReal_wp& ElementGeomReference<Dimension1>::PointsDof() const
  {
    return points_dof; 
  }
  
  
  //! returns the number of quadrature points
  inline int ElementGeomReference<Dimension1>::GetNbPointsQuadratureInside() const
  {
    return nb_points;
  }
  
  
  //! returns the number of degrees of freedom
  inline int ElementGeomReference<Dimension1>::GetNbDof() const
  {
    return nb_dof_loc;
  }
    
  
  //! returns the number of nodal points
  template<class T>
  inline int ElementGeomReference<Dimension1>::GetNbPointsNodal(TinyVector<T, 1>&) const
  {
    return nb_dof_loc;
  }

  
  //! allocates a projector (to project from nodal points to any set of points)
  inline void ElementGeomReference<Dimension1>
  ::InitNodalInterpolation(FiniteElementProjector*& proj) const
  {
    proj = new DenseProjector<Dimension1>();
  }
  
  
  //! evaluates nodal shape functions at point x
  inline void ElementGeomReference<Dimension1>
  ::ComputeValuesPhiNodalRef(const Real_wp& x, VectReal_wp& res) const
  {
    ComputeValuesPhiRef(x, res);
  }
  
  
  //! returns stiffness matrix
  inline const Matrix<Real_wp>& ElementGeomReference<Dimension1>::GetStiffnessMatrix() const
  {
    return stiffness_matrix;
  }
  
  
  //! returns gradient matrix
  inline const Matrix<Real_wp>& ElementGeomReference<Dimension1>::GetGradientMatrix() const
  {
    return gradient_matrix;
  }

  
  /******************
   * EdgeHierarchic *
   ******************/
  

  inline Real_wp EdgeHierarchic::GetValuePhi1D(int i, const Real_wp& pointloc) const
  {
    abort();
    return Real_wp(0);
  }


  inline Real_wp EdgeHierarchic::GetGradientPhi1D(int i, const Real_wp& pointloc) const
  {
    abort();
    return Real_wp(0);
  }


  inline void EdgeHierarchic
  ::AddConstantStiffnessMatrix(int m, int n, const Real_wp& C, Matrix<Real_wp>& A) const
  {
    abort();
  }
  
  
  inline void EdgeHierarchic
  ::AddConstantStiffnessMatrix(int m, int n, const Complex_wp& C, Matrix<Complex_wp>& A) const
  {
    abort();
  }
        
  
  inline void EdgeHierarchic
  ::AddVariableStiffnessMatrix(int off_row, int off_col,
			       const Vector<Real_wp>& C, Matrix<Real_wp>& mat) const
  {
    abort();
  }

  
  inline void EdgeHierarchic
  ::AddVariableStiffnessMatrix(int off_row, int off_col,
			       const Vector<Complex_wp>& C, Matrix<Complex_wp>& mat) const
  {
    abort();
  }
  
  
  inline void EdgeHierarchic
  ::AddConstantGradientMatrix(int m, int n, const Real_wp& C, Matrix<Real_wp>& A) const
  {
    abort();
  }
  
  
  inline void EdgeHierarchic
  ::AddConstantGradientMatrix(int m, int n, const Complex_wp& C, Matrix<Complex_wp>& A) const
  {
    abort();
  }
        
  
  inline void EdgeHierarchic
  ::AddVariableGradientMatrix(int off_row, int off_col,
			      const VectReal_wp& C, Matrix<Real_wp>& mat) const
  {
    abort();
  }

  
  inline void EdgeHierarchic
  ::AddVariableGradientMatrix(int off_row, int off_col,
			      const VectComplex_wp& C, Matrix<Complex_wp>& mat) const
  {
    abort(); 
  }
  
  
  inline void EdgeHierarchic
  ::AddConstantTransposeGradientMatrix(int m, int n, const Real_wp& C, Matrix<Real_wp>& A) const
  {
    abort();
  }
  
  
  inline void EdgeHierarchic
  ::AddConstantTransposeGradientMatrix(int m, int n, const Complex_wp& C, Matrix<Complex_wp>& A) const
  {
    abort();
  }
  
  
  inline void EdgeHierarchic
  ::AddVariableTransposeGradientMatrix(int off_row, int off_col,
				       const Vector<Real_wp>& C, Matrix<Real_wp>& mat) const
  {
    abort();
  }
  
  
  inline void EdgeHierarchic
  ::AddVariableTransposeGradientMatrix(int off_row, int off_col,
				       const Vector<Complex_wp>& C, Matrix<Complex_wp>& mat) const
  {
    abort();
  }


  /********************************
   * ElementReference<Dimension1> *
   ********************************/
  
  
  inline ElementReference<Dimension1, 1>
  ::ElementReference(ElementGeomReference<Dimension1>& elt) : elt_geom(elt)
  {
  }
    
  
  inline ElementReference<Dimension1, 1>::~ElementReference()
  {
  }
  
  
  inline int ElementReference<Dimension1, 1>::GetOrder() const
  {
    return elt_geom.GetOrder();
  }
  
    
  inline int ElementReference<Dimension1, 1>::GetGeometryOrder() const
  {
    return elt_geom.GetGeometryOrder();
  }

    
  inline bool ElementReference<Dimension1, 1>::LumpedMassMatrix() const
  {
    return elt_geom.LumpedMassMatrix();
  }

  
  inline int ElementReference<Dimension1, 1>::GetNbPointsQuadratureInside() const
  {
    return elt_geom.GetNbPointsQuadratureInside();
  }
  
  
  inline int ElementReference<Dimension1, 1>::GetNbDof() const
  {
    return elt_geom.GetNbDof();
  }

  
  inline int64_t ElementReference<Dimension1, 1>::GetMemorySize() const
  {
    return elt_geom.GetMemorySize() + sizeof(*this);
  }

  
  inline void ElementReference<Dimension1, 1>
  ::ConstructFiniteElement(int r, int rgeom, int rquad, int type_quad, int type_func)
  {
    elt_geom.ConstructFiniteElement(r, rgeom, rquad, type_quad, type_func);
  }
  
  
  inline void ElementReference<Dimension1, 1>::SetDofPoints(const VectReal_wp& pts)
  {
    elt_geom.SetDofPoints(pts);
  }
  
  
  inline const VectReal_wp& ElementReference<Dimension1, 1>::PointsDof() const
  {
    return elt_geom.PointsDof(); 
  }

  
  inline void ElementReference<Dimension1, 1>
  ::ComputeValuesPhiRef(const Real_wp& pointloc, VectReal_wp& res) const
  {
    elt_geom.ComputeValuesPhiRef(pointloc, res);
  }
  
  
  inline Real_wp ElementReference<Dimension1, 1>::GetValuePhi1D(int i, const Real_wp& pointloc) const
  {
    return elt_geom.GetValuePhi1D(i, pointloc);
  }


  inline Real_wp ElementReference<Dimension1, 1>::GetGradientPhi1D(int i, const Real_wp& pointloc) const
  {
    return elt_geom.GetGradientPhi1D(i, pointloc);
  }
  
  
  inline void ElementReference<Dimension1, 1>
  ::ComputeGradientPhiRef(const Real_wp& pointloc, VectReal_wp& res) const
  {
    elt_geom.ComputeGradientPhiRef(pointloc, res);
  }

  
  inline const VectReal_wp& ElementReference<Dimension1, 1>::Points() const
  {
    return elt_geom.Points;
  }
  
  
  inline const VectReal_wp& ElementReference<Dimension1, 1>::Weights() const
  {
    return elt_geom.Weights;
  }

  
  inline const Real_wp& ElementReference<Dimension1, 1>::Points(int i) const
  {
    return elt_geom.Points(i);
  }
  
  
  inline const Real_wp& ElementReference<Dimension1, 1>::Weights(int i) const
  {
    return elt_geom.Weights(i);
  }

  
  inline const Matrix<Real_wp>& ElementReference<Dimension1, 1>::GetStiffnessMatrix() const
  {
    return elt_geom.GetStiffnessMatrix();
  }
  
  
  inline const Matrix<Real_wp>& ElementReference<Dimension1, 1>::GetGradientMatrix() const
  {
    return elt_geom.GetGradientMatrix();
  }

  
  inline void ElementReference<Dimension1, 1>
  ::GetPolynomialFunctions(Vector<UnivariatePolynomial<Real_wp> >& Phi,
			   Vector<UnivariatePolynomial<Real_wp> >& dPhi) const
  {
    elt_geom.GetPolynomialFunctions(Phi, dPhi);
  }
  

  inline void ElementReference<Dimension1, 1>::FjElem(const VectReal_wp& s, VectReal_wp& points) const
  {
    return elt_geom.FjElem(s, points);
  }

  
  inline void ElementReference<Dimension1, 1>
  ::ComputeIntegralRef(const VectReal_wp& u, VectReal_wp& v) const
  {
    elt_geom.ComputeIntegralRef(u, v);
  }
  
  
  inline void ElementReference<Dimension1, 1>
  ::ComputeIntegralGradientRef(const VectReal_wp& u, VectReal_wp& v) const
  {
    elt_geom.ComputeIntegralGradientRef(u, v);
  }   
  
  
  inline void ElementReference<Dimension1, 1>
  ::ComputeIntegralRef(const VectComplex_wp& u, VectComplex_wp& v) const
  {
    elt_geom.ComputeIntegralRef(u, v);
  }
  
    
  inline void ElementReference<Dimension1, 1>
  ::ComputeIntegralGradientRef(const VectComplex_wp& u, VectComplex_wp& v) const
  {
    elt_geom.ComputeIntegralGradientRef(u, v);
  }   


  inline void ElementReference<Dimension1, 1>
  ::ComputeProjectionDofRef(const VectReal_wp& feval, VectReal_wp& contrib) const
  {
    elt_geom.ComputeProjectionDofRef(feval, contrib);
  }
  
  
  inline void ElementReference<Dimension1, 1>
  ::ComputeProjectionDofRef(const VectComplex_wp& feval, VectComplex_wp& contrib) const
  {
    elt_geom.ComputeProjectionDofRef(feval, contrib);
  }


  inline void ElementReference<Dimension1, 1>
  ::AddConstantStiffnessMatrix(int m, int n, const Real_wp& C, Matrix<Real_wp>& A) const
  {
    elt_geom.AddConstantStiffnessMatrix(m, n, C, A);
  }
  
  
  inline void ElementReference<Dimension1, 1>
  ::AddConstantStiffnessMatrix(int m, int n, const Complex_wp& C, Matrix<Complex_wp>& A) const
  {
    elt_geom.AddConstantStiffnessMatrix(m, n, C, A);
  }
        
  
  inline void ElementReference<Dimension1, 1>
  ::AddVariableStiffnessMatrix(int off_row, int off_col,
			       const Vector<Real_wp>& C, Matrix<Real_wp>& mat) const
  {
    elt_geom.AddVariableStiffnessMatrix(off_row, off_col, C, mat);
  }
  
  
  inline void ElementReference<Dimension1, 1>
  ::AddVariableStiffnessMatrix(int off_row, int off_col,
			       const Vector<Complex_wp>& C, Matrix<Complex_wp>& mat) const
  {
    elt_geom.AddVariableStiffnessMatrix(off_row, off_col, C, mat);
  }

      
  inline void ElementReference<Dimension1, 1>
  ::AddConstantGradientMatrix(int m, int n, const Real_wp& C, Matrix<Real_wp>& A) const
  {
    elt_geom.AddConstantGradientMatrix(m, n, C, A);
  }
  
  
  inline void ElementReference<Dimension1, 1>
  ::AddConstantGradientMatrix(int m, int n, const Complex_wp& C, Matrix<Complex_wp>& A) const
  {
    elt_geom.AddConstantGradientMatrix(m, n, C, A);
  }
        
  
  inline void ElementReference<Dimension1, 1>
  ::AddVariableGradientMatrix(int off_row, int off_col,
			      const VectReal_wp& C, Matrix<Real_wp>& mat) const
  {
    elt_geom.AddVariableGradientMatrix(off_row, off_col, C, mat);
  }
  
  
  inline void ElementReference<Dimension1, 1>
  ::AddVariableGradientMatrix(int off_row, int off_col,
			      const VectComplex_wp& C, Matrix<Complex_wp>& mat) const
  {
    elt_geom.AddVariableGradientMatrix(off_row, off_col, C, mat);
  }
  
  
  inline void ElementReference<Dimension1, 1>
  ::AddConstantTransposeGradientMatrix(int m, int n, const Real_wp& C, Matrix<Real_wp>& A) const
  {
    elt_geom.AddConstantTransposeGradientMatrix(m, n, C, A);
  }
  
  
  inline void ElementReference<Dimension1, 1>
  ::AddConstantTransposeGradientMatrix(int m, int n, const Complex_wp& C, Matrix<Complex_wp>& A) const
  {
    elt_geom.AddConstantTransposeGradientMatrix(m, n, C, A);
  }
  
  
  inline void ElementReference<Dimension1, 1>
  ::AddVariableTransposeGradientMatrix(int off_row, int off_col,
				       const Vector<Real_wp>& C, Matrix<Real_wp>& mat) const
  {
    elt_geom.AddVariableTransposeGradientMatrix(off_row, off_col, C, mat);
  }
  
  
  inline void ElementReference<Dimension1, 1>
  ::AddVariableTransposeGradientMatrix(int off_row, int off_col,
				       const Vector<Complex_wp>& C, Matrix<Complex_wp>& mat) const
  {
    elt_geom.AddVariableTransposeGradientMatrix(off_row, off_col, C, mat);
  }


  /***********************************************
   * EdgeLobattoReference and other constructors *
   ***********************************************/
  
  
  
  inline EdgeLobattoReference::EdgeLobattoReference() : ElementReference<Dimension1, 1>(edge)
  {
  }


  
  inline EdgeGaussReference::EdgeGaussReference() : ElementReference<Dimension1, 1>(edge)
  {
  }


  
  inline EdgeHierarchicReference::EdgeHierarchicReference() : ElementReference<Dimension1, 1>(edge)
  {
  }
  
}

#define MONTJOIE_FILE_EDGE_REFERENCE_INLINE_CXX
#endif
