#ifndef MONTJOIE_FILE_EDGE_REFERENCE_CXX

namespace Montjoie
{
  /*****************
   * EdgeReference *
   *****************/
  
  
  //! default constructor
  ElementGeomReference<Dimension1>::ElementGeomReference()
  {
    print_level = 0;
    order = 0;
    order_geom = 0;
    order_quad = 0;
    nb_points = 0;
    nb_dof_loc = 0;
  }
  
  
  //! returns the size of memory used by the object
  int64_t ElementGeomReference<Dimension1>::GetMemorySize() const
  {
    int64_t taille = sizeof(Real_wp)*(points_dof.GetM()+Points.GetM()+Weights.GetM());
    taille += stiffness_matrix.GetMemorySize() + gradient_matrix.GetMemorySize();
    taille += sizeof(*this);
    return taille;
  }
  
  
  //! constructs quadrature formulas in 1-D
  /*!
    \param order_ : order of approximation
  */
  void ElementGeomReference<Dimension1>::ConstructQuadrature(int r, int type_quadrature)
  {
    order_quad = r;
    nb_points = order_quad + 1;
    Globatto<Dimension1> gauss;
    gauss.ConstructQuadrature(order_quad, type_quadrature);
    
    Weights = gauss.Weights();
    Points = gauss.Points();
  }
  
  
  void ElementGeomReference<Dimension1>
  ::GetPolynomialFunctions(Vector<UnivariatePolynomial<Real_wp> >& Phi,
			   Vector<UnivariatePolynomial<Real_wp> >& dPhi) const
  {
    cout << "This function is not implemented for this finite element" << endl;
    abort(); 
  }
  

  //! computation of F_i
  /*!
    \param s : list of vertices of the edge
    \param points : list of quadrature points
   */
  void ElementGeomReference<Dimension1>::FjElem(const VectReal_wp& s, VectReal_wp& points) const
  {
    int nb_quad = Points.GetM();
    points.Reallocate(nb_quad);
    for (int i = 0; i < nb_quad; i++)
      points(i) = Points(i) * s(1) + (1.0-Points(i) ) * s(0) ;
    
  }


  //! computation of F_i
  /*!
    \param s : list of vertices of the edge
    \param points : list of dof points
   */
  void ElementGeomReference<Dimension1>::FjElemDof(const VectReal_wp& s, VectReal_wp& points) const
  {
    int nb_dof = points_dof.GetM();
    points.Reallocate(nb_dof);
    for (int i = 0; i < nb_dof; i++)
      points(i) = points_dof(i) * s(1) + (1.0-points_dof(i) ) * s(0) ;
    
  }

  
  //! displays information about the object ElementGeomReference<Dimension1>
  ostream& operator<<(ostream& out, const ElementGeomReference<Dimension1>& e)
  {
    out<<"Points of integration "<<endl;
    out<<e.Points<<endl;
    out<<"Weights of integration "<<endl;
    out<<e.Weights<<endl;
    return out;
  }


  /*************
   * EdgeGauss *
   *************/
  
  
  //! default constructor
  EdgeGauss::EdgeGauss() : ElementGeomReference<Dimension1>()
  {
    mass_lumping = false;
  }
  

  //! returns the size of memory used by the object
  int64_t EdgeGauss::GetMemorySize() const
  {
    int64_t taille = ElementGeomReference<Dimension1>::GetMemorySize();
    taille += sizeof(Real_wp)*(Value_Phi.GetDataSize()+Gradient_Phi.GetDataSize());
    taille += lob_geom.GetMemorySize()+lob_basis.GetMemorySize();
    taille += sizeof(*this) - sizeof(ElementGeomReference<Dimension1>);
    return taille;
  }
  
  
  //! how to number the 1D-mesh
  void EdgeGauss::ConstructNumberMap(NumberMap& nmap, bool dg_form) const
  {
    if (dg_form)
      {
	nmap.SetNbDofVertex(order, 0);
	nmap.SetNbDofEdge(order, order+1);
	return;
      }

    nmap.SetNbDofVertex(order, 1);
    nmap.SetNbDofEdge(order, order-1);
  }
  
  
  //! construction of finite element for order equal to r
  void EdgeGauss::ConstructFiniteElement(int r, int rgeom, int rquad, int type_quad,
					 int type_func)
  {
    if (rgeom == 0)
      rgeom = r;
    
    if (rquad == 0)
      rquad = r;
    
    if (type_quad == -1)
      type_quad = Globatto<Dimension1>::QUADRATURE_GAUSS;
    
    ConstructQuadrature(rquad, type_quad);
    
    order_geom = rgeom;
    order = r;
    lob_geom.ConstructQuadrature(rgeom, Globatto<Dimension1>::QUADRATURE_LOBATTO);
    nb_dof_loc = order+1;
    ConstructFunctions(type_func);
    ConstructStiffnessMatrix();
  }


  //! changes points for dofs
  void EdgeGauss::SetDofPoints(const VectReal_wp& pts)
  {
    lob_basis.AffectPoints(pts);
    
    order = pts.GetM()-1;
    nb_dof_loc = order+1;
    points_dof = pts;
    ConstructStiffnessMatrix();
  }
  
  
  //! construction of basis functions
  void EdgeGauss::ConstructFunctions(int type_func)
  {
    if (type_func == -1)
      type_func = LOBATTO;

    if (type_func == LOBATTO)
      lob_basis.ConstructQuadrature(order, Globatto<Dimension1>::QUADRATURE_LOBATTO);
    else if (type_func == GAUSS)
      lob_basis.ConstructQuadrature(order, Globatto<Dimension1>::QUADRATURE_GAUSS);
    else if (type_func == LOBATTO_INT)
      {
	VectReal_wp points_dof1d, points, weights;
	ComputeGaussLobatto(points, weights, order+2);
	points_dof1d.Reallocate(order+1);
	for (int i = 0; i <= order; i++)
	  points_dof1d(i) = points(i+1);
	
	lob_basis.AffectPoints(points_dof1d);    
      }
    
    points_dof = lob_basis.Points();
  }
  
  
  //! computation of stiffness matrix
  void EdgeGauss::ConstructStiffnessMatrix()
  {
    stiffness_matrix.Reallocate(nb_dof_loc, nb_dof_loc);
    gradient_matrix.Reallocate(nb_dof_loc, nb_dof_loc);
    Gradient_Phi.Reallocate(nb_dof_loc, nb_points);
    Value_Phi.Reallocate(nb_dof_loc, nb_points);
    for (int i = 0; i < nb_dof_loc; i++)
      {
	for (int j = 0; j < nb_points; j++)
	  {
	    Value_Phi(i, j) = lob_basis.phi1D(i, Points(j));
	    Gradient_Phi(i, j) = lob_basis.dphi1D(i, Points(j));
	  }
      }
    
    for (int i = 0; i < nb_dof_loc; i++)
      for (int j = 0; j < nb_dof_loc; j++)
	{
	  Real_wp val = 0.0;
	  for (int k = 0; k < nb_points; k++)
	    val += Weights(k)*Gradient_Phi(i, k)*Gradient_Phi(j, k);
	  
	  stiffness_matrix(i, j) = val;
	  
	  val = 0.0;
	  for (int k = 0; k < nb_points; k++)
	    val += Weights(k)*Gradient_Phi(j, k)*Value_Phi(i, k);
	  
	  gradient_matrix(i, j) = val;
	}
    
    if ((rank_processor == root_processor)||(this->print_level >= 10))
      if (print_level >= 1)
	cout<<rank_processor<<" Edge Reference constructed "<<endl;
  }
  
  
  //! compute res(i) = \f$ \hat{\varphi}_i(\mbox{pointloc}) \f$
  void EdgeGauss::ComputeValuesPhiRef(const Real_wp& point_loc, VectReal_wp& res) const
  {
    if (this->order >= 8)
      lob_basis.ComputeValuesPhiRef(point_loc, res);
    else
      {
        res.Reallocate(nb_dof_loc);
        for (int i = 0; i < nb_dof_loc; i++)
          res(i) = lob_basis.phi1D(i, point_loc);
      }
  }
  

  //! compute res(i) = \f$ \hat{\varphi}'_i(\mbox{pointloc}) \f$
  void EdgeGauss::ComputeGradientPhiRef(const Real_wp& point_loc, VectReal_wp& res) const
  {
    res.Reallocate(nb_dof_loc);
    for (int i = 0; i < nb_dof_loc; i++)
      res(i) = lob_basis.dphi1D(i, point_loc);
  }

  
  //! compute res(i) = \f$ \hat{\varphi}_i(\mbox{pointloc}) \f$
  void EdgeGauss::ComputeValuesPhiNodalRef(const Real_wp& point_loc, VectReal_wp& res) const
  {
    if (this->order >= 8)
      lob_geom.ComputeValuesPhiRef(point_loc, res);
    else
      {
        res.Reallocate(lob_geom.GetOrder()+1);
        for (int i = 0; i <= lob_geom.GetOrder(); i++)
          res(i) = lob_geom.phi1D(i, point_loc);
      }
  }
  
  
  //! returns varphi_i(point_loc)
  Real_wp EdgeGauss::GetValuePhi1D(int i, const Real_wp& point_loc) const
  {
    return lob_basis.phi1D(i, point_loc);
  }
  

  //! returns varphi_i'(point_loc)
  Real_wp EdgeGauss::GetGradientPhi1D(int i, const Real_wp& point_loc) const
  {
    return lob_basis.dphi1D(i, point_loc);
  }

  
  //! fills polynomials associated with basis functions
  void EdgeGauss::GetPolynomialFunctions(Vector<UnivariatePolynomial<Real_wp> >& Phi,
					 Vector<UnivariatePolynomial<Real_wp> >& dPhi) const
  {
    Real_wp one(1), coef;
    
    // Lagrange polynomials with points of lob_basis
    int r = lob_basis.GetOrder();
    Phi.Reallocate(r+1);
    UnivariatePolynomial<Real_wp> monome;
    monome.SetOrder(1);
    for (int i = 0; i <= r; i++)
      {
	Phi(i).SetOrder(0); Phi(i)(0) = one;
	for (int j = 0; j <= r; j++)
	  if (i != j)
	    {
	      coef = one/(lob_basis.Points(i) - lob_basis.Points(j));
	      monome(0) = -lob_basis.Points(j)*coef;
	      monome(1) = coef;
	      Phi(i) = Phi(i)*monome;
	    }
      }
    
    // derivatives of these functions
    dPhi.Reallocate(r+1);
    for (int i = 0; i <= r; i++)
      DerivatePolynomial(Phi(i), dPhi(i));
  }

  
  //! compute contrib(i) = \f$ \int_{[0,1]} f(x) \hat{\varphi}_i(x) dx \f$
  /*!
    \param[in] feval feval(j) is the evaluation of f on the quadrature point j
    \param[out] contrib result vector
  */
  template<class Vector1>
  void EdgeGauss::ComputeIntegralGen(const Vector1& feval, Vector1& contrib) const
  {
    contrib.Reallocate(this->nb_dof_loc);
    contrib.Fill(0);
    for (int i = 0; i < this->nb_dof_loc; i++)
      for (int j = 0; j < nb_points; j++)
	contrib(i) += feval(j) * Value_Phi(i, j) * Weights(j);
  }

  
  //! compute contrib(i) = \f$ \int_{[0,1]} f(x) \hat{\varphi}_i'(x) dx \f$
  /*! 
    \param[in] feval feval(j) is the evalution of f on the quadrature point j
    \param[out] contrib result vector
  */
  template<class Vector1>
  void EdgeGauss::ComputeIntegralGradientGen(const Vector1& feval,Vector1& contrib) const
  {
    contrib.Reallocate(this->nb_dof_loc);
    contrib.Fill(0);
    for (int i = 0; i < this->nb_dof_loc; i++)
      for (int j = 0; j < nb_points; j++)
	contrib(i) += feval(j) * Gradient_Phi(i, j) * Weights(j);
  }


  void EdgeGauss::ComputeProjectionDofRef(const VectReal_wp& feval, VectReal_wp& contrib) const
  {
    contrib = feval;
  }
  
  
  void EdgeGauss::ComputeProjectionDofRef(const VectComplex_wp& feval, VectComplex_wp& contrib) const
  {
    contrib = feval;
  }
  
  
  //! computes A = A + C \int d/dx(\varphi_i) d/dx(\varphi_j) dx
  /*!
    \param[in] m offset for rows
    \param[in] n offset for columns
    \param[in] C coefficient
    \param[inout] A matrix to which stiffness matrix is added
    This function performs the operation :
    A(m:m+N, n:n+N) += C * S
    where N is the size of the stiffness matrix S
    S is the usual stiffness matrix S_{i, j} = \int d/dx(\varphi_i) d/dx(\varphi_j) dx
   */
  template<class T0, class Matrix1>
  void EdgeGauss::AddConstantStiffnessMatrixGen(int m, int n, const T0& C, Matrix1& A) const
  {
    bool sym = IsSymmetricMatrix(A);
    for (int i = 0; i < nb_dof_loc; i++)
      {
        for (int j = 0; j < nb_dof_loc; j++)
	  if ((!sym) || (m+i <= n+j))
	    A(m+i, n+j) += stiffness_matrix(i, j)*C;
      }
  }
  

  //! computes A = A + \int C d/dx(\varphi_i) d/dx(\varphi_j) dx
  /*!
    \param[in] m offset for rows
    \param[in] n offset for columns
    \param[in] C values C(xi_i)/h omega_i for each quadrature point
    \param[inout] A matrix to which stiffness matrix is added
    This function performs the operation :
    A(m:m+N, n:n+N) += S
    where N is the size of the stiffness matrix S
    S is the stiffness matrix S_{i, j} = \int C d/dx(\varphi_i) d/dx(\varphi_j) dx
    C stores directly the coefficients C(xi_i) / h omega_i
    where h is the length of the considered interval, (omega_i, xi_i) the quadrature formula
   */
  template<class T, class Matrix1>
  void EdgeGauss::AddVariableStiffnessMatrixGen(int m, int n, const Vector<T>& C, Matrix1& A) const
  {
    typename Matrix1::value_type val;
    bool sym = IsSymmetricMatrix(A);
    for (int i = 0; i < nb_dof_loc; i++)
      {
        for (int j = 0; j < nb_dof_loc; j++)
          {
	    if ((!sym) || (m+i <= n+j))
	      {
		SetComplexZero(val);
		for (int k = 0; k <= order_quad; k++)
		  val += C(k)*Gradient_Phi(i, k)*Gradient_Phi(j, k);
				
		A(m+j, n+i) += val;
	      }
          }
      }
  }
  

  //! computes A = A + C \int d/dx(\varphi_j) \varphi_i dx
  /*!
    \param[in] m offset for rows
    \param[in] n offset for columns
    \param[in] C coefficient
    \param[inout] A matrix to which gradient matrix is added
    This function performs the operation :
    A(m:m+N, n:n+N) += C * S
    where N is the size of the gradient matrix S
    S is the gradient matrix S_{i, j} = \int d/dx(\varphi_j) \varphi_i dx
   */
  template<class T0, class Matrix1>
  void EdgeGauss::AddConstantGradientMatrixGen(int m, int n, const T0& C, Matrix1& A) const
  {
    bool sym = IsSymmetricMatrix(A);
    for (int i = 0; i < nb_dof_loc; i++)
      for (int j = 0; j < nb_dof_loc; j++)
	{
	  if ((!sym) || (m+i) <= (n+j))
	    A(m+i, n+j) += gradient_matrix(i, j)*C;
	}
  }
  

  //! computes A = A + \int C d/dx(\varphi_j) \varphi_i dx
  /*!
    \param[in] m offset for rows
    \param[in] n offset for columns
    \param[in] C values C(xi_i) omega_i for each quadrature point
    \param[inout] A matrix to which gradient matrix is added
    This function performs the operation :
    A(m:m+N, n:n+N) += S
    where N is the size of the gradient matrix S
    S is the stiffness matrix S_{i, j} = \int C d/dx(\varphi_j) \varphi_i dx
    C stores directly the coefficients C(xi_i) omega_i
    where h is the length of the considered interval, (omega_i, xi_i) the quadrature formula
   */
  template<class T, class Matrix1>
  void EdgeGauss::AddVariableGradientMatrixGen(int m, int n, const Vector<T>& C, Matrix1& A) const
  {
    typename Matrix1::value_type val;
    bool sym = IsSymmetricMatrix(A);
    for (int i = 0; i < nb_dof_loc; i++)
      {
        for (int j = 0; j < nb_dof_loc; j++)
	  if ((!sym) || (m+i <= n+j))
	    {
	      SetComplexZero(val);
	      for (int k = 0; k <= order_quad; k++)
		val += C(k)*Value_Phi(i, k)*Gradient_Phi(j, k);	      
	      
              A(m+i, n+j) += val;
	    }
      }
  }


  //! computes A = A + C \int d/dx(\varphi_i) \varphi_j dx
  /*!
    \param[in] m offset for rows
    \param[in] n offset for columns
    \param[in] C coefficient
    \param[inout] A matrix to which gradient matrix is added
    This function performs the operation :
    A(m:m+N, n:n+N) += C * S
    where N is the size of the gradient matrix S
    S is the gradient matrix S_{i, j} = \int d/dx(\varphi_i) \varphi_j dx
   */
  template<class T0, class Matrix1>
  void EdgeGauss::AddConstantTransposeGradientMatrixGen(int m, int n, const T0& C, Matrix1& A) const
  {
    bool sym = IsSymmetricMatrix(A);
    for (int i = 0; i < nb_dof_loc; i++)
      for (int j = 0; j < nb_dof_loc; j++)
	{
	  if ((!sym) || (m+i) <= (n+j))
	    A(m+i, n+j) += gradient_matrix(j, i)*C;
	}
  }
  

  //! computes A = A + \int C d/dx(\varphi_i) \varphi_j dx
  /*!
    \param[in] m offset for rows
    \param[in] n offset for columns
    \param[in] C values C(xi_i) omega_i for each quadrature point
    \param[inout] A matrix to which gradient matrix is added
    This function performs the operation :
    A(m:m+N, n:n+N) += S
    where N is the size of the gradient matrix S
    S is the stiffness matrix S_{i, j} = \int C d/dx(\varphi_i) \varphi_j dx
    C stores directly the coefficients C(xi_i) omega_i
    where h is the length of the considered interval, (omega_i, xi_i) the quadrature formula
   */
  template<class T, class Matrix1>
  void EdgeGauss
  ::AddVariableTransposeGradientMatrixGen(int m, int n, const Vector<T>& C, Matrix1& A) const
  {
    typename Matrix1::value_type val;
    bool sym = IsSymmetricMatrix(A);
    for (int i = 0; i < nb_dof_loc; i++)
      {
        for (int j = 0; j < nb_dof_loc; j++)
	  if ((!sym) || (m+i <= n+j))
	    {
	      SetComplexZero(val);
	      for (int k = 0; k <= order_quad; k++)
		val += C(k)*Value_Phi(j, k)*Gradient_Phi(i, k);
	      
	      A(m+i, n+j) += val;
	    }
      }
  }


  void EdgeGauss::ComputeIntegralRef(const VectReal_wp& feval, VectReal_wp& contrib) const
  {
    ComputeIntegralGen(feval, contrib);
  }

  void EdgeGauss::ComputeIntegralRef(const VectComplex_wp& feval, VectComplex_wp& contrib) const
  {
    ComputeIntegralGen(feval, contrib);
  }

  void EdgeGauss::ComputeIntegralGradientRef(const VectReal_wp& feval, VectReal_wp& contrib) const
  {
    ComputeIntegralGradientGen(feval, contrib);
  }

  void EdgeGauss::ComputeIntegralGradientRef(const VectComplex_wp& feval, VectComplex_wp& contrib) const
  {
    ComputeIntegralGradientGen(feval, contrib);
  }

  void EdgeGauss::AddConstantStiffnessMatrix(int m, int n, const Real_wp& C, Matrix<Real_wp>& A) const
  {
    AddConstantStiffnessMatrixGen(m, n, C, A);
  }
  
  void EdgeGauss::AddConstantStiffnessMatrix(int m, int n, const Complex_wp& C, Matrix<Complex_wp>& A) const
  {
    AddConstantStiffnessMatrixGen(m, n, C, A);
  }
        
  void EdgeGauss::AddVariableStiffnessMatrix(int off_row, int off_col,
					     const Vector<Real_wp>& C, Matrix<Real_wp>& mat) const
  {
    AddVariableStiffnessMatrixGen(off_row, off_col, C, mat);
  }
  
  void EdgeGauss::AddVariableStiffnessMatrix(int off_row, int off_col,
					     const Vector<Complex_wp>& C, Matrix<Complex_wp>& mat) const
  {
    AddVariableStiffnessMatrixGen(off_row, off_col, C, mat);
  }
    
  void EdgeGauss::AddConstantGradientMatrix(int m, int n, const Real_wp& C, Matrix<Real_wp>& A) const
  {
    AddConstantGradientMatrixGen(m, n, C, A);
  }
  
  void EdgeGauss::AddConstantGradientMatrix(int m, int n, const Complex_wp& C, Matrix<Complex_wp>& A) const
  {
    AddConstantGradientMatrixGen(m, n, C, A);
  }
        
  void EdgeGauss::AddVariableGradientMatrix(int off_row, int off_col,
					    const VectReal_wp& C, Matrix<Real_wp>& mat) const
  {
    AddVariableGradientMatrixGen(off_row, off_col, C, mat);
  }
  
  void EdgeGauss::AddVariableGradientMatrix(int off_row, int off_col,
					    const VectComplex_wp& C, Matrix<Complex_wp>& mat) const
  {
    AddVariableGradientMatrixGen(off_row, off_col, C, mat);
  }
  
  void EdgeGauss::AddConstantTransposeGradientMatrix(int m, int n, const Real_wp& C, Matrix<Real_wp>& A) const
  {
    AddConstantTransposeGradientMatrixGen(m, n, C, A);
  }
  
  void EdgeGauss::AddConstantTransposeGradientMatrix(int m, int n, const Complex_wp& C, Matrix<Complex_wp>& A) const
  {
    AddConstantTransposeGradientMatrixGen(m, n, C, A);
  }
  
  void EdgeGauss::AddVariableTransposeGradientMatrix(int off_row, int off_col,
						     const Vector<Real_wp>& C, Matrix<Real_wp>& mat) const
  {
    AddVariableTransposeGradientMatrixGen(off_row, off_col, C, mat);
  }
  
  void EdgeGauss::AddVariableTransposeGradientMatrix(int off_row, int off_col,
						     const Vector<Complex_wp>& C, Matrix<Complex_wp>& mat) const
  {
    AddVariableTransposeGradientMatrixGen(off_row, off_col, C, mat);
  }
  
  
  //! displays information about object EdgeGauss
  ostream& operator<<(ostream& out, const EdgeGauss& e)
  {
    out<<"Stiffness Matrix "<<endl;
    out<<e.stiffness_matrix<<endl;
    return out;
  }
  
  
  /***************
   * EdgeLobatto *
   ***************/
  
  
  //! default constructor
  EdgeLobatto::EdgeLobatto() : EdgeGauss()
  {
    mass_lumping = true;
  }
  
  
  //! construction of finite element for order equal to r
  void EdgeLobatto::ConstructFiniteElement(int r, int rgeom, int rquad, int type_quad, int type_func)
  {
    rgeom = r;
    if (rquad == 0)
      rquad = r;
    
    if (type_quad == -1)
      type_quad = lob_geom.QUADRATURE_LOBATTO;
    
    if ((rquad != r) || (type_quad != lob_geom.QUADRATURE_LOBATTO) )
      {
        cout << "this element does not authorize specific quadrature rule " << endl;
        abort();
      }
    
    ConstructQuadrature(rquad, lob_geom.QUADRATURE_LOBATTO);
    
    order_geom = rgeom;
    order = r;
    nb_dof_loc = order+1;
    ConstructFunctions();
    ConstructStiffnessMatrix();
  }
  
  
  //! compute contrib(i) = \f$ \int_{[0,1]} f(x) \hat{\varphi}_i(x) dx \f$
  /*!
    \param[in] feval feval(j) is the evaluation of f on the quadrature point j
    \param[out] contrib result vector
  */
  template<class Vector1>
  void EdgeLobatto::ComputeIntegralGen(const Vector1& feval, Vector1& contrib) const
  {
    contrib.Reallocate(nb_dof_loc);
    contrib.Fill(0);
    for (int i = 0; i < nb_dof_loc; i++)
      contrib(i) = feval(i) * Weights(i);
  }
  

  void EdgeLobatto::ComputeIntegralRef(const VectReal_wp& feval, VectReal_wp& contrib) const
  {
    ComputeIntegralGen(feval, contrib);
  }

  void EdgeLobatto::ComputeIntegralRef(const VectComplex_wp& feval, VectComplex_wp& contrib) const
  {
    ComputeIntegralGen(feval, contrib);
  }


  /******************
   * EdgeHierarchic *
   ******************/
  
  
  //! default constructor
  EdgeHierarchic::EdgeHierarchic() : ElementGeomReference<Dimension1>()
  {
    this->mass_lumping = false;
    type_function = JACOBI_11;
  }
  
  
  //! how to number the 1D-mesh
  void EdgeHierarchic::ConstructNumberMap(NumberMap& nmap, bool dg_form) const
  {
    if (dg_form)
      {
	nmap.SetNbDofVertex(order, 0);
	nmap.SetNbDofEdge(order, order+1);
	return;
      }
    
    nmap.SetNbDofVertex(order, 1);
    nmap.SetNbDofEdge(order, order-1);
  }
  

  //! returns the size of memory used by the object
  int64_t EdgeHierarchic::GetMemorySize() const
  {
    int64_t taille = ElementGeomReference<Dimension1>::GetMemorySize();
    taille += sizeof(Real_wp)*(Value_Phi.GetDataSize()+Gradient_Phi.GetDataSize());
    taille += mass_matrix.GetMemorySize() + jacobi_11_pol.GetMemorySize();
    taille += CoefLeg11.GetMemorySize() + legendre_pol.GetMemorySize();
    taille += sizeof(*this) - sizeof(ElementGeomReference<Dimension1>);
    return taille;
  }


  //! construction of finite element for order equal to r  
  void EdgeHierarchic::ConstructFiniteElement(int r, int rgeom, int rquad, int type_quad, int type_func)
  {
    if (rgeom == 0)
      rgeom = r;
    
    if (rquad == 0)
      rquad = r;
    
    if (type_quad == -1)
      type_quad = Globatto<Dimension1>::QUADRATURE_GAUSS;
    
    if (type_func == -1)
      type_func = JACOBI_11;

    ConstructQuadrature(rquad, type_quad);
    
    order_geom = rgeom;
    order = r;
    nb_dof_loc = order+1;
    ConstructFunctions(type_func);
  }


  void EdgeHierarchic::SetDofPoints(const VectReal_wp& pts)
  {
    abort();
  }
  
  
  //! computation of stiffness matrix
  void EdgeHierarchic::ConstructFunctions(int type_func)
  {
    jacobi_11_pol.Clear();
    legendre_pol.Clear();
    type_function = type_func;
    if ((type_func == LEGENDRE) || (type_func == LEGENDRE_COMBINED))
      {
	GetJacobiPolynomial(legendre_pol, order+1, Real_wp(0), Real_wp(0));	
      }
    else
      {
	// computing P_m^{1,1}
	GetJacobiPolynomial(jacobi_11_pol, order, Real_wp(1), Real_wp(1));
	
	// orthonormalisation coefficients
	CoefLeg11.Reallocate(order-1); CoefLeg11.Fill(0);
	VectReal_wp Pn;
	for (int i = 0; i <= order; i++)
	  {
	    EvaluateJacobiPolynomial(jacobi_11_pol, order-2, 2*Points(i) - 1.0, Pn);
	    for (int j = 1; j < order; j++)
	      CoefLeg11(j-1) += Weights(i)*square(Points(i)*(1.0-Points(i))*Pn(j-1));
	  }
	
	for (int j = 1; j < order; j++)
	  CoefLeg11(j-1) = 1.0/sqrt(CoefLeg11(j-1));
      }
    
    // stiffness and mass matrix
    stiffness_matrix.Reallocate(nb_dof_loc, nb_dof_loc);
    gradient_matrix.Reallocate(nb_dof_loc, nb_dof_loc);
    mass_matrix.Reallocate(nb_dof_loc, nb_dof_loc);
    Gradient_Phi.Reallocate(nb_dof_loc, nb_points);
    Value_Phi.Reallocate(nb_dof_loc, nb_points);
    VectReal_wp phi, grad_phi;
    for (int j = 0; j < nb_points; j++)
      {
        ComputeValuesPhiRef(Points(j), phi);
        ComputeGradientPhiRef(Points(j), grad_phi);
        for (int i = 0; i < nb_dof_loc; i++)
          {
	    Value_Phi(i, j) = phi(i);
	    Gradient_Phi(i, j) = grad_phi(i);
	  }
      }
    
    for (int i = 0; i < nb_dof_loc; i++)
      for (int j = 0; j < nb_dof_loc; j++)
	{
	  Real_wp val = 0.0;
	  for (int k = 0; k < nb_points; k++)
	    val += Weights(k)*Gradient_Phi(i, k)*Gradient_Phi(j, k);
	  
	  stiffness_matrix(i, j) = val;

	  val = 0.0;
	  for (int k = 0; k < nb_points; k++)
	    val += Weights(k)*Gradient_Phi(j, k)*Value_Phi(i, k);
	  
	  gradient_matrix(i, j) = val;

	  val = 0.0;
	  for (int k = 0; k < nb_points; k++)
	    val += Weights(k)*Value_Phi(i, k)*Value_Phi(j, k);
	  
	  mass_matrix(i, j) = val;
	}
    
    GetCholesky(mass_matrix);
    
    // quadrature points are used to project on basis functions
    this->points_dof = this->Points;

    if ((rank_processor == root_processor)||(this->print_level >= 10))
      if (print_level >= 1)
	cout<<rank_processor<<" Edge Reference constructed "<<endl; 
  }
  
  
  //! compute res(i) = \f$ \hat{\varphi}_i(\mbox{pointloc}) \f$
  void EdgeHierarchic::ComputeValuesPhiRef(const Real_wp& point_loc, VectReal_wp& res) const
  {
    res.Reallocate(nb_dof_loc);
    
    if (legendre_pol.GetM() > 0)
      {
	res.Fill(0);
	EvaluateJacobiPolynomial(legendre_pol, order, 2.0*point_loc-1.0, res);
	
	if (type_function == LEGENDRE_COMBINED)
	  {
	    VectReal_wp Pn(res);
	    res(0) = 1.0;
	    if (nb_dof_loc >= 2)
	      res(1) = 2.0*point_loc-1.0;
	    
	    Real_wp coef_phi0, coef_phi1;
	    for (int k = 2; k <= order; k++)
	      {
		coef_phi0 = -(k-1)*Pn(k-2)/k ;
		coef_phi1 = (2*k-1)*Pn(k-1)/k;
		res(k) = coef_phi0 + coef_phi1*res(1);
	      }
	  }
      }
    else
      {
	res(0) = 1.0 - point_loc;
	res(order) = point_loc;
	
	if (order >= 2)
	  {
	    VectReal_wp Pn;
	    EvaluateJacobiPolynomial(jacobi_11_pol, order-2, 2.0*point_loc - 1.0, Pn);
	    Real_wp vloc = (1.0-point_loc)*point_loc;
	    for (int j = 1; j < order; j++)
	      res(j) = vloc*Pn(j-1)*CoefLeg11(j-1);
	  }
      }
  }
  
  
  //! compute res(i) = \f$ \hat{\varphi}_i(\mbox{pointloc}) \f$
  void EdgeHierarchic::ComputeValuesPhiNodalRef(const Real_wp& point_loc, VectReal_wp& res) const
  {
    abort();
  }
  
  
  //! computation of derivative of basis functions on point_loc
  void EdgeHierarchic::ComputeGradientPhiRef(const Real_wp& point_loc, VectReal_wp& res) const
  {  
    res.Reallocate(nb_dof_loc);
    
    if (legendre_pol.GetM() > 0)
      {
	VectReal_wp Pn;
	res.Fill(0);
	EvaluateJacobiPolynomial(legendre_pol, order, 2.0*point_loc-1.0, Pn, res);
	Mlt(Real_wp(2), res);

	if (type_function == LEGENDRE_COMBINED)
	  {
	    // To be done ...
	  }
      }
    else
      {
	res(0) = -1.0;
	res(order) = 1.0;
	
	if (order >= 2)
	  {
	    VectReal_wp Pn, dPn;
	    EvaluateJacobiPolynomial(jacobi_11_pol, order-2, 2.0*point_loc - 1.0, Pn, dPn);
	    Real_wp vloc = (1.0-point_loc)*point_loc;
	    Real_wp dvloc = 1.0 - 2.0*point_loc;
	    for (int j = 1; j < order; j++)
	      res(j) = (2.0*vloc*dPn(j-1) + dvloc*Pn(j-1))*CoefLeg11(j-1);
	  }
      }
  }
  
  
  //! compute contrib(i) = \f$ \int_{[0,1]} f(x) \hat{\varphi}_i(x) dx \f$
  /*!
    \param[in] feval feval(j) is the evaluation of f on the quadrature point j
    \param[out] contrib result vector
  */
  template<class Vector1>
  void EdgeHierarchic::ComputeIntegralGen(const Vector1& feval, Vector1& contrib) const
  {
    contrib.Reallocate(this->nb_dof_loc);
    contrib.Fill(0);
    for (int i = 0; i < this->nb_dof_loc; i++)
      for (int j = 0; j < nb_points; j++)
	contrib(i) += feval(j) * Value_Phi(i, j) * Weights(j);
  }
  
  
  //! compute contrib(i) = \f$ \int_{[0,1]} f(x) \hat{\varphi}_i'(x) dx \f$
  /*! 
    \param[in] feval feval(j) is the evalution of f on the quadrature point j
    \param[out] contrib result vector
  */
  template<class Vector1>
  void EdgeHierarchic::ComputeIntegralGradientGen(const Vector1& feval,Vector1& contrib) const
  {
    contrib.Reallocate(this->nb_dof_loc);
    contrib.Fill(0);
    for (int i = 0; i < this->nb_dof_loc; i++)
      for (int j = 0; j < nb_points; j++)
	contrib(i) += feval(j) * Gradient_Phi(i, j) * Weights(j);
  }
  
  
  //! x is overwritten by M^{-1} x where M is the mass matrix
  template<class Vector1>
  void EdgeHierarchic::SolveMassMatrix(Vector1& x) const
  {
    Seldon::SolveCholesky(SeldonNoTrans, mass_matrix, x);
    Seldon::SolveCholesky(SeldonTrans, mass_matrix, x);
  }
  
  
  void EdgeHierarchic::ComputeProjectionDofRef(const VectReal_wp& feval, VectReal_wp& contrib) const
  {
    ComputeIntegralRef(feval, contrib);
    SolveMassMatrix(contrib);
  }
  
  
  void EdgeHierarchic::ComputeProjectionDofRef(const VectComplex_wp& feval, VectComplex_wp& contrib) const
  {
    ComputeIntegralRef(feval, contrib);
    SolveMassMatrix(contrib);
  }
  

  void EdgeHierarchic::ComputeIntegralRef(const VectReal_wp& feval, VectReal_wp& contrib) const
  {
    ComputeIntegralGen(feval, contrib);
  }

  void EdgeHierarchic::ComputeIntegralRef(const VectComplex_wp& feval, VectComplex_wp& contrib) const
  {
    ComputeIntegralGen(feval, contrib);
  }


  void EdgeHierarchic::ComputeIntegralGradientRef(const VectReal_wp& feval, VectReal_wp& contrib) const
  {
    ComputeIntegralGradientGen(feval, contrib);
  }

  void EdgeHierarchic::ComputeIntegralGradientRef(const VectComplex_wp& feval, VectComplex_wp& contrib) const
  {
    ComputeIntegralGradientGen(feval, contrib);
  }

  
  //! displays information about object EdgeGauss
  ostream& operator<<(ostream& out, const EdgeHierarchic& e)
  {
    out<<static_cast<const ElementGeomReference<Dimension1>& >(e); return out;
    out<<"Stiffness Matrix "<<endl;
    out<<e.stiffness_matrix<<endl;
    return out;
  }
    
}

#define MONTJOIE_FILE_EDGE_REFERENCE_CXX
#endif
