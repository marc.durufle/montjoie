#ifndef MONTJOIE_FILE_EDGE_REFERENCE_HXX

namespace Montjoie
{
  //! Base class for finite element in 1-D
  template<>
  class ElementGeomReference<Dimension1>
  {
  protected :
    //! informations to print
    int print_level;
    //! order of approximation (r)
    int order;
    //! order of quadrature
    int order_quad;
    //! order of approximation for geometry
    int order_geom;
    //! number of quadrature points
    int nb_points;
    //! number of dofs per edge
    int nb_dof_loc;
    //! dof points
    VectReal_wp points_dof;
    //! true for finite elements achieving mass lumping (diagonal mass matrix)
    bool mass_lumping;

    //! stiffness_matrix(i,j) = \f$ \int \hat{\varphi}_i'(x) \hat{\varphi}_j'(x) dx  \f$
    //! on the unit edge [0,1]
    Matrix<Real_wp> stiffness_matrix;
    
    //! gradient_matrix(i,j) = \f$ \int \hat{\varphi}_j'(x) \hat{\varphi}_i(x) dx  \f$
    //! on the unit edge [0,1]
    Matrix<Real_wp> gradient_matrix;

  public :
    
    VectReal_wp Weights; //!< integration weights
    VectReal_wp Points; //!< integration points
    
    ElementGeomReference<Dimension1>();
    virtual ~ElementGeomReference<Dimension1>();

    bool LumpedMassMatrix() const;
    int GetHybridType() const;
    
    int64_t GetMemorySize() const;
    int GetOrder() const;
    int GetGeometryOrder() const;
    const VectReal_wp& PointsDof() const;
    
    // construction of quadrature formulas
    void ConstructQuadrature(int r, int type_quadrature =
			     Globatto<Dimension1>::QUADRATURE_GAUSS);
    
    int GetNbPointsQuadratureInside() const;
    int GetNbDof() const;
    
    template<class T>
    int GetNbPointsNodal(TinyVector<T, 1>&) const;

    void InitNodalInterpolation(FiniteElementProjector*& proj) const;

    virtual void ConstructFiniteElement(int r, int rgeom = 0, int rquad = 0, int type_quad = -1,
					int type_func = -1) = 0;
    
    virtual void GetPolynomialFunctions(Vector<UnivariatePolynomial<Real_wp> >& Phi,
					Vector<UnivariatePolynomial<Real_wp> >& dPhi) const;

    virtual void SetDofPoints(const VectReal_wp&) = 0;
    virtual void ComputeValuesPhiRef(const Real_wp& pointloc, VectReal_wp& res) const = 0;
    virtual void ComputeGradientPhiRef(const Real_wp& pointloc, VectReal_wp& res) const = 0;

    virtual Real_wp GetValuePhi1D(int i, const Real_wp& pointloc) const = 0;
    virtual Real_wp GetGradientPhi1D(int i, const Real_wp& pointloc) const = 0;
    
    void ComputeValuesPhiNodalRef(const Real_wp& x, VectReal_wp& res) const;
    
    // x = \f$ F_j(\hat{x}) \f$
    // where \f$ \hat{x} \f$ is defined on the unit edge [0,1]
    void FjElem(const VectReal_wp& s, VectReal_wp& points) const; 
    void FjElemDof(const VectReal_wp& s, VectReal_wp& points) const; 

    const Matrix<Real_wp>& GetStiffnessMatrix() const;
    const Matrix<Real_wp>& GetGradientMatrix() const;
    
    virtual void ComputeIntegralRef(const VectReal_wp& feval, VectReal_wp& contrib) const = 0;
    virtual void ComputeIntegralRef(const VectComplex_wp& feval, VectComplex_wp& contrib) const = 0;
    
    // compute contrib(i) = \f$ \int_{[0,1]} f(x) \hat{\varphi}_i'(x) dx \f$
    // feval(j) is the evalution of f on the quadrature point j
    virtual void ComputeIntegralGradientRef(const VectReal_wp& feval, VectReal_wp& contrib) const = 0;
    virtual void ComputeIntegralGradientRef(const VectComplex_wp& feval, VectComplex_wp& contrib) const = 0;

    virtual void ComputeProjectionDofRef(const VectReal_wp& feval, VectReal_wp& contrib) const = 0;
    virtual void ComputeProjectionDofRef(const VectComplex_wp& feval, VectComplex_wp& contrib) const = 0;

    virtual void AddConstantStiffnessMatrix(int m, int n, const Real_wp& C, Matrix<Real_wp>& A) const = 0;
    virtual void AddConstantStiffnessMatrix(int m, int n, const Complex_wp& C,
					    Matrix<Complex_wp>& A) const = 0;
        
    virtual void AddVariableStiffnessMatrix(int off_row, int off_col,
					    const Vector<Real_wp>& C, Matrix<Real_wp>& mat) const = 0;

    virtual void AddVariableStiffnessMatrix(int off_row, int off_col,
					    const Vector<Complex_wp>& C, Matrix<Complex_wp>& mat) const = 0;
    
    virtual void AddConstantGradientMatrix(int m, int n, const Real_wp& C, Matrix<Real_wp>& A) const = 0;
    virtual void AddConstantGradientMatrix(int m, int n, const Complex_wp& C,
					   Matrix<Complex_wp>& A) const = 0;
        
    virtual void AddVariableGradientMatrix(int off_row, int off_col,
					   const VectReal_wp& C, Matrix<Real_wp>& mat) const = 0;

    virtual void AddVariableGradientMatrix(int off_row, int off_col,
					   const VectComplex_wp& C, Matrix<Complex_wp>& mat) const = 0;

    virtual void AddConstantTransposeGradientMatrix(int m, int n, const Real_wp& C,
						    Matrix<Real_wp>& A) const = 0;

    virtual void AddConstantTransposeGradientMatrix(int m, int n, const Complex_wp& C,
						    Matrix<Complex_wp>& A) const = 0;
        
    virtual void AddVariableTransposeGradientMatrix(int off_row, int off_col,
						    const Vector<Real_wp>& C,
						    Matrix<Real_wp>& mat) const = 0;

    virtual void AddVariableTransposeGradientMatrix(int off_row, int off_col,
						    const Vector<Complex_wp>& C,
						    Matrix<Complex_wp>& mat) const = 0;
    
    // to display object ElementGeomReference<Dimension1>
    friend ostream& operator<<(ostream& out, const ElementGeomReference<Dimension1>& e);

  };


  //! Gauss-Lobatto finite element in 1-D with Gauss quadrature
  class EdgeGauss : public ElementGeomReference<Dimension1>
  {
    
  protected :
    //! value of basis functions on quadrature points
    Matrix<Real_wp> Value_Phi;
    //! value of gradient of basis functions on quadrature points
    Matrix<Real_wp> Gradient_Phi;
    //! object defining basis functions
    Globatto<Dimension1> lob_geom, lob_basis;
    
  public :
    enum{ GAUSS, LOBATTO, LOBATTO_INT};
    
    EdgeGauss();

    int64_t GetMemorySize() const;
    void ConstructFiniteElement(int r, int rgeom = 0, int rquad = 0, int type_quad = -1,
				int type_func = -1);

    void SetDofPoints(const VectReal_wp&);
    
  protected :
    // computation of stiffness matrix
    void ConstructFunctions(int type_func = -1);
    void ConstructStiffnessMatrix();

    template<class Vector1>
    void ComputeIntegralGen(const Vector1& feval, Vector1& contrib) const;

    template<class Vector1>
    void ComputeIntegralGradientGen(const Vector1& feval, Vector1& contrib) const;
    
    template<class T0, class Matrix1>
    void AddConstantStiffnessMatrixGen(int m, int n, const T0& C, Matrix1& A) const;
        
    template<class T, class Matrix1>
    void AddVariableStiffnessMatrixGen(int off_row, int off_col,
				       const Vector<T>& C, Matrix1& mat) const;
    
    template<class T0, class Matrix1>
    void AddConstantGradientMatrixGen(int m, int n, const T0& C, Matrix1& A) const;
        
    template<class T, class Matrix1>
    void AddVariableGradientMatrixGen(int off_row, int off_col,
				      const Vector<T>& C, Matrix1& mat) const;

    template<class T0, class Matrix1>
    void AddConstantTransposeGradientMatrixGen(int m, int n, const T0& C, Matrix1& A) const;
        
    template<class T, class Matrix1>
    void AddVariableTransposeGradientMatrixGen(int off_row, int off_col,
					       const Vector<T>& C, Matrix1& mat) const;

  public :
    // how to number the 1D-mesh
    void ConstructNumberMap(NumberMap& map, bool) const;
    
    // compute res(i) = \f$ \hat{\varphi}_i(\mbox{pointloc}) \f$
    void ComputeValuesPhiRef(const Real_wp& pointloc, VectReal_wp& res) const;
    void ComputeGradientPhiRef(const Real_wp& pointloc, VectReal_wp& res) const;
    void ComputeValuesPhiNodalRef(const Real_wp& pointloc, VectReal_wp& res) const;
    
    Real_wp GetValuePhi1D(int i, const Real_wp& pointloc) const;
    Real_wp GetGradientPhi1D(int i, const Real_wp& pointloc) const;
    void GetPolynomialFunctions(Vector<UnivariatePolynomial<Real_wp> >& Phi,
				Vector<UnivariatePolynomial<Real_wp> >& dPhi) const;
    
    // compute contrib(i) = \f$ \int_{[0,1]} f(x) \hat{\varphi}_i(x) dx \f$
    // feval(j) is the evalution of f on the quadrature point j
    void ComputeIntegralRef(const VectReal_wp& feval, VectReal_wp& contrib) const;
    void ComputeIntegralRef(const VectComplex_wp& feval, VectComplex_wp& contrib) const;
    
    // compute contrib(i) = \f$ \int_{[0,1]} f(x) \hat{\varphi}_i'(x) dx \f$
    // feval(j) is the evalution of f on the quadrature point j
    void ComputeIntegralGradientRef(const VectReal_wp& feval, VectReal_wp& contrib) const;
    void ComputeIntegralGradientRef(const VectComplex_wp& feval, VectComplex_wp& contrib) const;

    void ComputeProjectionDofRef(const VectReal_wp& feval, VectReal_wp& contrib) const;
    void ComputeProjectionDofRef(const VectComplex_wp& feval, VectComplex_wp& contrib) const;

    void AddConstantStiffnessMatrix(int m, int n, const Real_wp& C, Matrix<Real_wp>& A) const;
    void AddConstantStiffnessMatrix(int m, int n, const Complex_wp& C, Matrix<Complex_wp>& A) const;
        
    void AddVariableStiffnessMatrix(int off_row, int off_col,
                                    const Vector<Real_wp>& C, Matrix<Real_wp>& mat) const;

    void AddVariableStiffnessMatrix(int off_row, int off_col,
                                    const Vector<Complex_wp>& C, Matrix<Complex_wp>& mat) const;
    
    void AddConstantGradientMatrix(int m, int n, const Real_wp& C, Matrix<Real_wp>& A) const;
    void AddConstantGradientMatrix(int m, int n, const Complex_wp& C, Matrix<Complex_wp>& A) const;
        
    void AddVariableGradientMatrix(int off_row, int off_col,
				   const VectReal_wp& C, Matrix<Real_wp>& mat) const;

    void AddVariableGradientMatrix(int off_row, int off_col,
				   const VectComplex_wp& C, Matrix<Complex_wp>& mat) const;

    void AddConstantTransposeGradientMatrix(int m, int n, const Real_wp& C, Matrix<Real_wp>& A) const;
    void AddConstantTransposeGradientMatrix(int m, int n, const Complex_wp& C, Matrix<Complex_wp>& A) const;
        
    void AddVariableTransposeGradientMatrix(int off_row, int off_col,
					    const Vector<Real_wp>& C, Matrix<Real_wp>& mat) const;

    void AddVariableTransposeGradientMatrix(int off_row, int off_col,
					    const Vector<Complex_wp>& C, Matrix<Complex_wp>& mat) const;
    
    friend ostream& operator<<(ostream& out, const EdgeGauss& e);
  };
  
  
  //! Gauss-Lobatto finite element in 1-D with Lobatto quadrature
  class EdgeLobatto : public EdgeGauss
  {
  private:
    template<class Vector1>
    void ComputeIntegralGen(const Vector1& feval, Vector1& contrib) const;

  public :
    EdgeLobatto();
    
    void ConstructFiniteElement(int r, int rgeom = 0, int rquad = 0, int type_quad = -1,
				int type_func = -1);
    
    // compute contrib(i) = \f$ \int_{[0,1]} f(x) \hat{\varphi}_i(x) dx \f$
    // feval(j) is the evalution of f on the quadrature point j
    void ComputeIntegralRef(const VectReal_wp& feval, VectReal_wp& contrib) const;
    void ComputeIntegralRef(const VectComplex_wp& feval, VectComplex_wp& contrib) const;
    
  };
  
  
  //! Hierarchical basis functions over interval [0,1]
  class EdgeHierarchic : public ElementGeomReference<Dimension1>
  {
    
  protected :
    //! mass_matrix(i, j) = \int \varphi_j \varphi dx on the unit interval
    Matrix<Real_wp, Symmetric, RowSymPacked> mass_matrix;
    //! value of basis functions on quadrature points
    Matrix<Real_wp> Value_Phi;
    //! value of gradient of basis functions on quadrature points
    Matrix<Real_wp> Gradient_Phi;
    Matrix<Real_wp> jacobi_11_pol;
    VectReal_wp CoefLeg11;
    Matrix<Real_wp> legendre_pol;
    int type_function;
    
  public :
    enum {LEGENDRE, JACOBI_11, LEGENDRE_COMBINED}; 

    EdgeHierarchic();

    int64_t GetMemorySize() const;
      
    void ConstructFiniteElement(int r, int rgeom = 0, int rquad = 0, int type_quad = -1,
				int type_func = -1);

    void SetDofPoints(const VectReal_wp&);
    
  protected :
    void ConstructFunctions(int type_func);

    template<class Vector1>
    void ComputeIntegralGen(const Vector1& feval, Vector1& contrib) const;
    
    template<class Vector1>
    void ComputeIntegralGradientGen(const Vector1& feval, Vector1& contrib) const;

  public :
    // how to number the 1D-mesh
    void ConstructNumberMap(NumberMap& map, bool) const;

    Real_wp GetValuePhi1D(int i, const Real_wp& pointloc) const;
    Real_wp GetGradientPhi1D(int i, const Real_wp& pointloc) const;
    
    // compute res(i) = \f$ \hat{\varphi}_i(\mbox{pointloc}) \f$
    void ComputeValuesPhiRef(const Real_wp& pointloc, VectReal_wp& res) const;
    void ComputeValuesPhiNodalRef(const Real_wp& pointloc, VectReal_wp& res) const;
    void ComputeGradientPhiRef(const Real_wp& pointloc, VectReal_wp& res) const;
    
    // compute contrib(i) = \f$ \int_{[0,1]} f(x) \hat{\varphi}_i(x) dx \f$
    // feval(j) is the evalution of f on the quadrature point j
    void ComputeIntegralRef(const VectReal_wp& feval, VectReal_wp& contrib) const;
    void ComputeIntegralRef(const VectComplex_wp& feval, VectComplex_wp& contrib) const;
    
    // compute contrib(i) = \f$ \int_{[0,1]} f(x) \hat{\varphi}_i'(x) dx \f$
    // feval(j) is the evalution of f on the quadrature point j
    void ComputeIntegralGradientRef(const VectReal_wp& feval, VectReal_wp& contrib) const;
    void ComputeIntegralGradientRef(const VectComplex_wp& feval, VectComplex_wp& contrib) const;
    
    template<class Vector1>
    void SolveMassMatrix(Vector1& contrib) const;

    void ComputeProjectionDofRef(const VectReal_wp& feval, VectReal_wp& contrib) const;
    void ComputeProjectionDofRef(const VectComplex_wp& feval, VectComplex_wp& contrib) const;

    void AddConstantStiffnessMatrix(int m, int n, const Real_wp& C, Matrix<Real_wp>& A) const;
    void AddConstantStiffnessMatrix(int m, int n, const Complex_wp& C, Matrix<Complex_wp>& A) const;
        
    void AddVariableStiffnessMatrix(int off_row, int off_col,
                                    const Vector<Real_wp>& C, Matrix<Real_wp>& mat) const;

    void AddVariableStiffnessMatrix(int off_row, int off_col,
                                    const Vector<Complex_wp>& C, Matrix<Complex_wp>& mat) const;
    
    void AddConstantGradientMatrix(int m, int n, const Real_wp& C, Matrix<Real_wp>& A) const;
    void AddConstantGradientMatrix(int m, int n, const Complex_wp& C, Matrix<Complex_wp>& A) const;
        
    void AddVariableGradientMatrix(int off_row, int off_col,
				   const VectReal_wp& C, Matrix<Real_wp>& mat) const;

    void AddVariableGradientMatrix(int off_row, int off_col,
				   const VectComplex_wp& C, Matrix<Complex_wp>& mat) const;

    void AddConstantTransposeGradientMatrix(int m, int n, const Real_wp& C, Matrix<Real_wp>& A) const;
    void AddConstantTransposeGradientMatrix(int m, int n, const Complex_wp& C, Matrix<Complex_wp>& A) const;
        
    void AddVariableTransposeGradientMatrix(int off_row, int off_col,
					    const Vector<Real_wp>& C, Matrix<Real_wp>& mat) const;

    void AddVariableTransposeGradientMatrix(int off_row, int off_col,
					    const Vector<Complex_wp>& C, Matrix<Complex_wp>& mat) const;
    
    friend ostream& operator<<(ostream& out, const EdgeHierarchic& e);
  };

  
  template<>
  class ElementReference<Dimension1, 1>    
  {
  protected :
    ElementGeomReference<Dimension1>& elt_geom;
    
  public :
    ElementReference(ElementGeomReference<Dimension1>& elt);
    virtual ~ElementReference();
    
    int GetOrder() const;
    int GetGeometryOrder() const;
    bool LumpedMassMatrix() const;
    
    int GetNbPointsQuadratureInside() const;
    int GetNbDof() const;

    int64_t GetMemorySize() const;
    
    void ConstructFiniteElement(int r, int rgeom = 0, int rquad = 0, int type_quad = -1,
				int type_func = -1);
    
    void SetDofPoints(const VectReal_wp&);
    const VectReal_wp& PointsDof() const;
    
    void ComputeValuesPhiRef(const Real_wp& pointloc, VectReal_wp& res) const;
    Real_wp GetValuePhi1D(int i, const Real_wp& pointloc) const;
    Real_wp GetGradientPhi1D(int i, const Real_wp& pointloc) const;
    void ComputeGradientPhiRef(const Real_wp& pointloc, VectReal_wp& res) const;
    
    const VectReal_wp& Points() const;
    const VectReal_wp& Weights() const;

    const Real_wp& Points(int i) const;
    const Real_wp& Weights(int i) const;

    const Matrix<Real_wp>& GetStiffnessMatrix() const;
    const Matrix<Real_wp>& GetGradientMatrix() const;

    void GetPolynomialFunctions(Vector<UnivariatePolynomial<Real_wp> >& Phi,
				Vector<UnivariatePolynomial<Real_wp> >& dPhi) const;

    void FjElem(const VectReal_wp& s, VectReal_wp& points) const;

    void ComputeIntegralRef(const VectReal_wp& u, VectReal_wp& v) const;
    void ComputeIntegralGradientRef(const VectReal_wp& u, VectReal_wp& v) const;

    void ComputeIntegralRef(const VectComplex_wp& u, VectComplex_wp& v) const;
    void ComputeIntegralGradientRef(const VectComplex_wp& u, VectComplex_wp& v) const;

    void ComputeProjectionDofRef(const VectReal_wp& feval, VectReal_wp& contrib) const;
    void ComputeProjectionDofRef(const VectComplex_wp& feval, VectComplex_wp& contrib) const;

    void AddConstantStiffnessMatrix(int m, int n, const Real_wp& C, Matrix<Real_wp>& A) const;
    void AddConstantStiffnessMatrix(int m, int n, const Complex_wp& C, Matrix<Complex_wp>& A) const;
        
    void AddVariableStiffnessMatrix(int off_row, int off_col,
                                    const Vector<Real_wp>& C, Matrix<Real_wp>& mat) const;

    void AddVariableStiffnessMatrix(int off_row, int off_col,
                                    const Vector<Complex_wp>& C, Matrix<Complex_wp>& mat) const;
    
    void AddConstantGradientMatrix(int m, int n, const Real_wp& C, Matrix<Real_wp>& A) const;
    void AddConstantGradientMatrix(int m, int n, const Complex_wp& C, Matrix<Complex_wp>& A) const;
        
    void AddVariableGradientMatrix(int off_row, int off_col,
				   const VectReal_wp& C, Matrix<Real_wp>& mat) const;

    void AddVariableGradientMatrix(int off_row, int off_col,
				   const VectComplex_wp& C, Matrix<Complex_wp>& mat) const;

    void AddConstantTransposeGradientMatrix(int m, int n, const Real_wp& C, Matrix<Real_wp>& A) const;
    void AddConstantTransposeGradientMatrix(int m, int n, const Complex_wp& C, Matrix<Complex_wp>& A) const;
        
    void AddVariableTransposeGradientMatrix(int off_row, int off_col,
					    const Vector<Real_wp>& C, Matrix<Real_wp>& mat) const;

    void AddVariableTransposeGradientMatrix(int off_row, int off_col,
					    const Vector<Complex_wp>& C, Matrix<Complex_wp>& mat) const;

  };


  class EdgeLobattoReference : public ElementReference<Dimension1, 1>
  {
    EdgeLobatto edge;
    
  public :
    EdgeLobattoReference();
    
  };

  
  class EdgeGaussReference : public ElementReference<Dimension1, 1>
  {
    EdgeGauss edge;
    
  public :
    EdgeGaussReference();

  };


  class EdgeHierarchicReference : public ElementReference<Dimension1, 1>
  {
    EdgeHierarchic edge;

  public :
    EdgeHierarchicReference();
    
  };
  
}

#define MONTJOIE_FILE_EDGE_REFERENCE_HXX
#endif


