#ifndef MONTJOIE_FILE_QUADRANGLE_GEOM_REFERENCE_INLINE_CXX

namespace Montjoie
{
  
  //! returns the number of points strictly inside the element
  inline int QuadrangleGeomReference::GetNbPointsNodalInside() const
  {
    return this->points_nodal2d.GetM() - 4*this->order_geom;
  }


  //! return the center of the unit square
  inline R2 QuadrangleGeomReference::GetCenterReferenceElement() const
  {
    return R2(0.5, 0.5);
  }


  inline void QuadrangleGeomReference::InitNodalInterpolation(FiniteElementProjector*& proj) const
  {
    proj = new TensorizedProjector<Dimension2>();
  }

  
  /****************
   * Fj transform *
   ****************/
  
  
  //! computes res = Fi(point)
  /*!
    \param[in] s list of vertices of the quad
    \param[in] PTReel "reference points" after transformation Fi
    \param[in] point local coordinates on the unit square
    \param[out] res the result of transformation Fi
    \param[in] mesh quadrilateral mesh considered
    \param[in] nquad quadrilateral element number
   */  
  inline void QuadrangleGeomReference
  ::Fj(const VectR2& s, const SetPoints<Dimension2>& PTReel,
       const R2& point, R2& res, const Mesh<Dimension2>& mesh, int nquad) const
  {
    if (mesh.Element(nquad).IsCurved())
      FjCurve(PTReel,point,res);
    else
      FjLinear(s,point,res);
  }
  
  
  //! computes res = DFi(point)
  /*!
    \param[in] s list of vertices of the quad
    \param[in] PTReel "reference points" after transformation Fi
    \param[in] point local coordinates on the unit square
    \param[out] res the jacobian matrix
    \param[in] mesh quadrilateral mesh considered
    \param[in] nquad quadrilateral element number
   */  
  inline void QuadrangleGeomReference
  ::DFj(const VectR2& s, const SetPoints<Dimension2>& PTReel,
        const R2& point, Matrix2_2& res,
        const Mesh<Dimension2>& mesh, int nquad) const
  {
    if (mesh.Element(nquad).IsCurved())
      DFjCurve(PTReel,point,res);
    else
      DFjLinear(s,point,res);
  }
  
  
  //! not implemented
  inline void QuadrangleGeomReference
  ::ComputeCoefJacobian(const VectR2& s, VectReal_wp& CoefJacobian) const
  {
    abort();
  }
  
  
  //! transformation Fi in the case of straight quadrilateral
  inline void QuadrangleGeomReference
  ::FjLinear(const VectR2& s, const R2& point, R2& res) const
  {
    Real_wp one(1);
    res.Init(s(0)(0)*(one-point(0))*(one-point(1))+s(1)(0)*point(0)*(one-point(1))+
	     s(2)(0)*point(0)*point(1)+s(3)(0)*(one-point(0))*point(1),
	     s(0)(1)*(one-point(0))*(one-point(1))+s(1)(1)*point(0)*(one-point(1))+
	     s(2)(1)*point(0)*point(1)+s(3)(1)*(one-point(0))*point(1));
  }
  
  
  //! transformation DFi in the case of straight quadrilateral
  inline void QuadrangleGeomReference::DFjLinear(const VectR2& s,
                                                 const R2& point, Matrix2_2& res) const
  {
    Real_wp one(1);
    res(0,0) = (one-point(1))*(s(1)(0)-s(0)(0))+point(1)*(s(2)(0)-s(3)(0));
    res(1,0) = (one-point(1))*(s(1)(1)-s(0)(1))+point(1)*(s(2)(1)-s(3)(1));
    res(0,1) = (one-point(0))*(s(3)(0)-s(0)(0))+point(0)*(s(2)(0)-s(1)(0));
    res(1,1) = (one-point(0))*(s(3)(1)-s(0)(1))+point(0)*(s(2)(1)-s(1)(1));
  }

  
  //! returns 2-D points of a point on a edge
  /*!
    \param[in] num_loc local edge number in the quad
    \param[in] t_loc abscisse on the edge
    \param[out] res local coordinates of the point in the unit square
  */
  inline void QuadrangleGeomReference::
  GetLocalCoordOnBoundary(int num_loc, const Real_wp& t_loc, R2& res) const
  {
    if (num_loc == 0)
      res.Init(t_loc,0);
    else if (num_loc == 1)
      res.Init(1,t_loc);
    else if (num_loc == 2)
      res.Init(Real_wp(1)-t_loc,Real_wp(1));
    else
      res.Init(0,Real_wp(1)-t_loc);
  }
  
  
  //! evaluation of 1-D nodal basis functions at a given point x
  inline void QuadrangleGeomReference
  ::ComputeValuesNodalPhi1D(const Real_wp& x, VectReal_wp& phi) const
  {
    phi.Reallocate(this->order_geom+1);
    for (int i = 0; i <= this->order_geom; i++)
      phi(i) = lob_geom.phi1D(i, x);
  }
  
  
  
  //! computes 2-D point res located at the local coordinate t_loc on the edge num_loc
  inline void QuadrangleGeomReference::
  GetLocalCoordOnBoundary(int num_loc, const TinyVector<Real_wp, 1>& t_loc, R2& res) const 
  {
    GetLocalCoordOnBoundary(num_loc, t_loc(0), res);
  }

  inline void QuadrangleGeomReference
  ::ComputeNodalGradientRef(const Vector<Real_wp>& Uloc, VectR2& gradU) const
  {
    ComputeNodalGradientRefT(Uloc, gradU);
  }
  
  
  inline void QuadrangleGeomReference
  ::ComputeNodalGradientRef(const Vector<Complex_wp>& Uloc, Vector<R2_Complex_wp>& gradU) const
  {
    ComputeNodalGradientRefT(Uloc, gradU);
  }
  
}
  
#define MONTJOIE_FILE_QUADRANGLE_GEOM_REFERENCE_INLINE_CXX
#endif
