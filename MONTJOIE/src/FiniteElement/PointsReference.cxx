#ifndef MONTJOIE_FILE_POINTS_REFERENCE_CXX

namespace Montjoie
{  

#ifdef MONTJOIE_WITH_MPFR
  template<class Dimension>
  Real_wp FjInverseProblem<Dimension>::threshold_newton(Real_wp(100, MONTJOIE_DEFAULT_PRECISION)*epsilon_machine);
#else
  template<class Dimension>
  Real_wp FjInverseProblem<Dimension>::threshold_newton(100.0*epsilon_machine);
#endif
  
  template<class Dimension>
  int FjInverseProblem<Dimension>::nb_max_iterations(50);
  
  template<class Dimension>
  int FjInverseProblem<Dimension>::non_linear_solver(1);
  
  /************************
   * SetPoints<Dimension> *
   ************************/
  
  
  //! points are rotated of an angle teta
  template<class Dimension>
  void SetPoints<Dimension>::RotatePoints(const Real_wp& teta)
  {
    Real_wp cos_teta = cos(teta);
    Real_wp sin_teta = sin(teta);
    R_N pt1, pt2;
    
    for (int k = 0; k < PointsQuadrature.GetM(); k++)
      {
        pt1 = PointsQuadrature(k); pt2 = pt1;
        pt2(0) = cos_teta*pt1(0) - sin_teta*pt1(1);
        pt2(1) = sin_teta*pt1(0) + cos_teta*pt1(1);
        PointsQuadrature(k) = pt2;
      }
    
    for (int k = 0; k < PointsNodal.GetM(); k++)
      {
        pt1 = PointsNodal(k); pt2 = pt1;
        pt2(0) = cos_teta*pt1(0) - sin_teta*pt1(1);
        pt2(1) = sin_teta*pt1(0) + cos_teta*pt1(1);
        PointsNodal(k) = pt2;
      }

    for (int k = 0; k < PointsDof.GetM(); k++)
      {
        pt1 = PointsDof(k); pt2 = pt1;
        pt2(0) = cos_teta*pt1(0) - sin_teta*pt1(1);
        pt2(1) = sin_teta*pt1(0) + cos_teta*pt1(1);
        PointsDof(k) = pt2;
      }
              
    for (int k = 0; k < PointsBoundary.GetM(); k++)
      {
        pt1 = PointsBoundary(k); pt2 = pt1;
        pt2(0) = cos_teta*pt1(0) - sin_teta*pt1(1);
        pt2(1) = sin_teta*pt1(0) + cos_teta*pt1(1);
        PointsBoundary(k) = pt2;
      }

    for (int k = 0; k < PointsDofBoundary.GetM(); k++)
      {
        pt1 = PointsDofBoundary(k); pt2 = pt1;
        pt2(0) = cos_teta*pt1(0) - sin_teta*pt1(1);
        pt2(1) = sin_teta*pt1(0) + cos_teta*pt1(1);
        PointsDofBoundary(k) = pt2;
      }
  }
  
  
  //! points are translated from a constant vector
  template<class Dimension>
  void SetPoints<Dimension>::TranslatePoints(const R_N& vec_u)
  {
    for (int k = 0; k < PointsQuadrature.GetM(); k++)
      PointsQuadrature(k) += vec_u;
    
    for (int k = 0; k < PointsNodal.GetM(); k++)
      PointsNodal(k) += vec_u;

    for (int k = 0; k < PointsDof.GetM(); k++)
      PointsDof(k) += vec_u;
              
    for (int k = 0; k < PointsBoundary.GetM(); k++)
      PointsBoundary(k) += vec_u;

    for (int k = 0; k < PointsDofBoundary.GetM(); k++)
      PointsDofBoundary(k) += vec_u;
  }
  
  
  //! displays informations about class SetPoints
  template<class Dimension>
  ostream& operator <<(ostream& out, const SetPoints<Dimension>& e)
  {
    out<<"Nodal points "<<e.PointsNodal<<endl;
    out<<"Points on boundary "<<e.PointsBoundary<<endl;
    return out;
  }
  
  
  /**************************
   * SetMatrices<Dimension> * 
   **************************/
  
  
  //! allocate the array containing jacobian matrices on quadrature points of the boundary
  template<class Dimension>
  void SetMatrices<Dimension>::ReallocatePointsQuadratureBoundary(int N)
  {
    MatricesBoundary.Reallocate(N);
    NormaleQuadrature.Reallocate(N);
    DsQuadrature.Reallocate(N);
    K1_curve.Reallocate(N);
    K2_curve.Reallocate(N);
    K1_curve.Fill(0);
    K2_curve.Fill(0);
  }

  
  //! compute jacobian matrices DFi on nodal points of the boundary
  template<class Dimension> template<int type>
  void SetMatrices<Dimension>::ComputeNodalBoundary(int num_loc, const ElementReference<Dimension, type>& Fb)
  {
    int nb_pts = Fb.GetNbNodalBoundary(num_loc);
    MatricesNodalBoundary.Reallocate(nb_pts);
    invMatricesNodalBoundary.Reallocate(nb_pts);    
    for (int i = 0; i < nb_pts; i++)
      {
        int node = Fb.GetNodalNumber(num_loc, i);
        MatricesNodalBoundary(i) = MatricesNodal(node);
        GetInverse(MatricesNodalBoundary(i), invMatricesNodalBoundary(i));
      }
  }
  
  
  //! normale are rotated of an angle teta
  template<class Dimension>
  void SetMatrices<Dimension>::RotateNormale(const Real_wp& teta)
  {
    Real_wp cos_teta = cos(teta);
    Real_wp sin_teta = sin(teta);
    R_N pt1, pt2;
    for (int k = 0; k < NormaleQuadrature.GetM(); k++)
      {
        pt1 = NormaleQuadrature(k); pt2 = pt1;
        pt2(0) = cos_teta*pt1(0) - sin_teta*pt1(1);
        pt2(1) = sin_teta*pt1(0) + cos_teta*pt1(1);
        NormaleQuadrature(k) = pt2;
      }
  }
  
  
  //! displays informations about class SetMatrices
  template<class Dimension>
  ostream& operator <<(ostream& out, const SetMatrices<Dimension>& e)
  {
    out<<"Jacobian matrices on quadrature points "<<e.MatricesQuadrature<<endl;
    out<<"Jacobian matrices on nodal points "<<e.MatricesNodal<<endl;
    out<<"Jacobian matrices on boundary "<<e.MatricesBoundary<<endl;
    out<<"Normales on boundary "<<e.NormaleQuadrature<<endl;
    out<<"Surfacic element Ds "<<e.DsQuadrature<<endl;
    return out;
  }
  
  
  /********************
   * FjInverseProblem *
   ********************/
  
  
  //! constructor with parameters
  template<class Dimension>  
  FjInverseProblem<Dimension>::
  FjInverseProblem(const ElementGeomReference_Base<Dimension>& felt,
		   const typename Dimension::VectR_N& s,
		   const SetPoints<Dimension>& PTReel,
		   const Mesh<Dimension>& mesh_, int nquad)
    : Fb(felt), mesh(mesh_), Vertices(s), num_elem(nquad), PointsElem(PTReel)
  {    
  }
  
  
  //! modification of the class with a line of the input file
  template<class Dimension>
  void FjInverseProblem<Dimension>::SetInputData(const string& description_field,
						 const VectString& parameters)
  {
    if (!description_field.compare("NonLinearSolver"))
      {
	if (parameters.GetM() <= 2)
	  {
	    cout << "In SetInputData of FjInverseProblem" << endl;
	    cout << "NonLinearSolver needs more parameters, for instance :" << endl;
	    cout << "NonLinearSolver = MINPACK AUTO nb_iterations" << endl;
	    cout << "Current parameters are : " << endl << parameters << endl;
	    abort();
	  }
        
	if (!parameters(0).compare("MINPACK"))
	  non_linear_solver = MINPACK_RESOLUTION;
	else
	  non_linear_solver = NEWTON_RESOLUTION;
	
	if (parameters(1) == "AUTO")
          threshold_newton = 100*epsilon_machine;
        else
          to_num(parameters(1), threshold_newton);
        
	to_num(parameters(2), nb_max_iterations);
      }
  }
  
  
  //! we solve non-linear system F_i(x) = pt
  template<class Dimension>
  bool FjInverseProblem<Dimension>::Solve(const R_N& point, R_N& res)
  {
    point_global = point;
    FindInitGuess(res);
    
    if (non_linear_solver == MINPACK_RESOLUTION)
      {
	VectReal_wp RControl(10); RControl.Fill(Real_wp(0));
	IVect Control(10);
	R_N fvec, scale;
	typename Dimension::MatrixN_N fjac;
	        
	// we set control parameters
	RControl(0) = threshold_newton;
	RControl(1) = Real_wp(1);
        	
	Control(0) = nb_max_iterations;
	Control(3) = 1;
	Control(4) = 0;       
        
	// we call function SolveMinpack defined in file NonLinear_Equations.cxx
	Montjoie::SolveMinpack(*this, res, fvec, fjac, scale, Control, RControl);
      }
    else if (non_linear_solver == NEWTON_RESOLUTION)
      {
	abort();
      }
    
    // we compute the residual reached
    R_N X;
    Fb.Fj(Vertices, PointsElem, res, X, mesh, num_elem);
    X -= point_global;
    
    // if residual is small enough and point inside the element
    Real_wp epsilon = 100*threshold_newton;
    if ((Norm2(X) <= epsilon)&&(!Fb.OutsideReferenceElement(res, epsilon)))
      return true;
    
    // point outside the element
    return false;
  }
  
} // end namespace

#define MONTJOIE_FILE_POINTS_REFERENCE_CXX
#endif
