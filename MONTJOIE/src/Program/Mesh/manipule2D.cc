#define MONTJOIE_WITH_TWO_DIM

#include "Mesh/MontjoieMesh.hxx"

using namespace Montjoie;

void AddVertex(Mesh<Dimension2>& mesh, const R2& point)
{
  int nv = mesh.GetNbVertices();
  mesh.ResizeVertices(nv+1);
  mesh.Vertex(nv) = point;
  cout<<"Numero du nouveau point"<<nv+1<<endl;
}

void AddEdge(Mesh<Dimension2>& mesh, int n1, int n2, int ref)
{
  int nb_vert = mesh.GetNbVertices();
  int n = mesh.GetNbBoundaryRef();
  mesh.ResizeBoundariesRef(n+1);
  
  n1--;
  if ((n1 < 0)||(n1 >= nb_vert))
    {
      cout<<"Vertex number must be between 1 and "<<nb_vert<<endl;
      return;
    }
  
  n2--;
  if ((n2 < 0)||(n2 >= nb_vert))
    {
      cout<<"Vertex number must be between 1 and "<<nb_vert<<endl;
      return;
    }
  
  mesh.BoundaryRef(n).Init(n1, n2, ref);
  cout<<"Numero de la nouvelle arete "<<n+1<<endl;
}

void AddElement(Mesh<Dimension2>& mesh, IVect num, int ref)
{
  int n = mesh.GetNbElt();
  int nb_vert = mesh.GetNbVertices();
  mesh.ResizeElements(n+1);
  for (int i = 0; i < num.GetM(); i++)
    {
      num(i)--;
     if ((num(i) < 0)||(num(i) >= nb_vert))
       {
	 cout<<"Vertex number must be between 1 and "<<nb_vert<<endl;
	 return;
       }
    }
  
  mesh.Element(n).Init(num, ref);
  cout<<"Numero du nouvel element "<<n+1<<endl;
}

void RemoveVertex(Mesh<Dimension2>& mesh, const Vector<bool>& VertexUsed)
{
  int nb_vert = mesh.GetNbVertices();
  IVect new_number(nb_vert);
  nb_vert = 0;
  for (int i = 0; i < VertexUsed.GetM(); i++)
    {
      if (VertexUsed(i))
	{
	  mesh.Vertex(nb_vert) = mesh.Vertex(i);
	  new_number(i) = nb_vert++;
	}
      else
	new_number(i) = -1;
    }
  
  mesh.ResizeVertices(nb_vert);
    
  // on modifie les aretes de reference
  int nb_edge = 0;
  int r = mesh.GetGeometryOrder();
  for (int i = 0; i < mesh.GetNbBoundaryRef(); i++)
    {
      int n1 = mesh.BoundaryRef(i).numVertex(0);
      int n2 = mesh.BoundaryRef(i).numVertex(1);
      if ( (VertexUsed(n1)) && (VertexUsed(n2)) )
	{
	  int ref = mesh.BoundaryRef(i).GetReference();
	  mesh.BoundaryRef(nb_edge).Init(new_number(n1), new_number(n2), ref);
	    
	  if (nb_edge != i)
	    {
	      for (int k = 0; k < r-1; k++)
		mesh.SetPointInsideEdge(nb_edge, k, mesh.GetPointInsideEdge(i, k));
	    }
	  
	  nb_edge++;
	}
    }
  
  mesh.ResizeBoundariesRef(nb_edge);
  
  // on modifie les elements
  int nb_elt = 0;
  for (int i = 0; i < mesh.GetNbElt(); i++)
    {
      bool keep_elt = true;
      nb_vert = mesh.Element(i).GetNbVertices();
      int ref = mesh.Element(i).GetReference();
      IVect numv(nb_vert);
      for (int j = 0; j < nb_vert; j++)
	{
	  int nv = mesh.Element(i).numVertex(j);
	  numv(j) = new_number(nv);
	  if (!VertexUsed(nv))
	    keep_elt = false;
	}
      
      if (keep_elt)
	mesh.Element(nb_elt++).Init(numv, ref);      
    }
  
  if (nb_elt != mesh.GetNbElt())
    mesh.ResizeElements(nb_elt);
  
  mesh.FindConnectivity();
}


void RemoveVertex(Mesh<Dimension2>& mesh, IVect num)
{
  int nb_vert = mesh.GetNbVertices();
  for (int i = 0; i < num.GetM(); i++)
    {
      num(i)--;
      if ((num(i) < 0)||(num(i) >= nb_vert))
	{
	  cout<<"Vertex number must be between 1 and "<<nb_vert<<endl;
	  return;
	}
    }
  
  Vector<bool> VertexUsed(nb_vert); VertexUsed.Fill(true);
  for (int i = 0; i < num.GetM(); i++)
    VertexUsed(num(i)) = false;
  
  RemoveVertex(mesh, VertexUsed);
}

void RemoveVertex(Mesh<Dimension2>& mesh, const VectReal_wp& delim)
{
  if (delim.GetM() < 4)
    {
      cout<<"Enter at least four real numbers : xmin, xmax, ymin, ymax"<<endl;
      return;
    }
  
  Real_wp xmin = delim(0), xmax = delim(1), ymin = delim(2), ymax = delim(3);
  int nb_vert = mesh.GetNbVertices();
  Vector<bool> VertexUsed(nb_vert);
  for (int i = 0; i < nb_vert; i++)
    {
      if ((mesh.Vertex(i)(0) > xmin)&&(mesh.Vertex(i)(0) < xmax)&&
	  (mesh.Vertex(i)(1) > ymin)&&(mesh.Vertex(i)(1) < ymax) )
	VertexUsed(i) = false;
      else
	VertexUsed(i) = true;
    }
  
  RemoveVertex(mesh, VertexUsed);
}

void RemoveEdge(Mesh<Dimension2>& mesh, const Vector<bool>& EdgeUsed)
{
  int nb_edge = 0;
  int r = mesh.GetGeometryOrder();
  for (int i = 0; i < mesh.GetNbBoundaryRef(); i++)
    {
      if (EdgeUsed(i))
	{
	  if (nb_edge != i)
	    {
              mesh.BoundaryRef(nb_edge) = mesh.BoundaryRef(i);
              for (int k = 0; k < r-1; k++)
		mesh.SetPointInsideEdge(nb_edge, k, mesh.GetPointInsideEdge(i, k));
	      
            }
          
	  nb_edge++;
	}
    }
  
  if (nb_edge != mesh.GetNbBoundaryRef())
    mesh.ResizeBoundariesRef(nb_edge);
  
}

void RemoveEdge(Mesh<Dimension2>& mesh, IVect num)
{
  int nb_edge = mesh.GetNbBoundaryRef();
  Vector<bool> EdgeUsed(nb_edge); EdgeUsed.Fill(true);
  for (int i = 0; i < num.GetM(); i++)
    {
      num(i)--;
      if ((num(i) < 0)||(num(i) >= nb_edge))
	{
	  cout<<"Edge number must be between 1 and "<<nb_edge<<endl;
	  return;
	}
      
      EdgeUsed(num(i)) = false;
    }
  
  RemoveEdge(mesh, EdgeUsed);
}

void RemoveEdgeRef(Mesh<Dimension2>& mesh, IVect num)
{
  int nb_edge = mesh.GetNbBoundaryRef();
  Vector<bool> EdgeUsed(nb_edge); EdgeUsed.Fill(true);
  int ref_max = 0;
  for (int i = 0; i < num.GetM(); i++)
    ref_max = max(ref_max, num(i));
  
  Vector<bool> RemoveRef(ref_max+1); RemoveRef.Fill(false);
  for (int i = 0; i < num.GetM(); i++)
    RemoveRef(num(i)) = true;
  
  for (int i = 0; i < nb_edge; i++)
    {
      int ref = mesh.BoundaryRef(i).GetReference();
      if (ref >= 0)
        if (ref <= ref_max)
          if (RemoveRef(ref))
            EdgeUsed(i) = false;
    }
  
  RemoveEdge(mesh, EdgeUsed);
}

void RemoveElement(Mesh<Dimension2>& mesh, const Vector<bool>& ElementUsed)
{
  int nb_elt = 0;
  for (int i = 0; i < mesh.GetNbElt(); i++)
    {
      if (ElementUsed(i))
	{
	  if (nb_elt != i)
	    mesh.Element(nb_elt) = mesh.Element(i);
	  
	  nb_elt++;
	}
    }
  
  if (nb_elt != mesh.GetNbElt())
    mesh.ResizeElements(nb_elt);

}

void RemoveElement(Mesh<Dimension2>& mesh, IVect num)
{
  int nb_elt = mesh.GetNbElt();
  Vector<bool> ElementUsed(nb_elt); ElementUsed.Fill(true);
  for (int i = 0; i < num.GetM(); i++)
    {
      num(i)--;
      if ((num(i) < 0)||(num(i) >= nb_elt))
	{
	  cout<<"Element number must be between 1 and "<<nb_elt<<endl;
	  return;
	}
      
      ElementUsed(num(i)) = false;
    }
  
  RemoveElement(mesh, ElementUsed);
}

void RemoveElementRef(Mesh<Dimension2>& mesh, IVect num)
{
  int nb_elt = mesh.GetNbElt();
  Vector<bool> ElementUsed(nb_elt); ElementUsed.Fill(true);
  int ref_max = 0;
  for (int i = 0; i < num.GetM(); i++)
    ref_max = max(ref_max, num(i));
  
  Vector<bool> RemoveRef(ref_max+1); RemoveRef.Fill(false);
  for (int i = 0; i < num.GetM(); i++)
    RemoveRef(num(i)) = true;
  
  for (int i = 0; i < nb_elt; i++)
    {
      int ref = mesh.Element(i).GetReference();
      if (ref >= 0)
        if (ref <= ref_max)
          if (RemoveRef(ref))
            ElementUsed(i) = false;
    }
  
  RemoveElement(mesh, ElementUsed);
}

void ModifyVertex(Mesh<Dimension2>& mesh, int n, const R2& pt)
{
  if ((n <= 0)||(n > mesh.GetNbVertices()))
    {
      cout<<"Vertex number must be between 1 and "<<mesh.GetNbVertices()<<endl;
      return;
    }
  
  mesh.Vertex(n-1) = pt;
}

void ModifyEdge(Mesh<Dimension2>& mesh, int n, int n1, int n2)
{
  if ((n <= 0)||(n > mesh.GetNbBoundaryRef()))
    {
      cout<<"Edge number must be between 1 and "<<mesh.GetNbBoundaryRef()<<endl;
      return;
    }
  
  n1--;
  if ((n1 < 0)||(n1 >= mesh.GetNbVertices()))
    {
      cout<<"Vertex number must be between 1 and "<<mesh.GetNbVertices()<<endl;
      return;
    }

  n2--;
  if ((n2 < 0)||(n2 >= mesh.GetNbVertices()))
    {
      cout<<"Vertex number must be between 1 and "<<mesh.GetNbVertices()<<endl;
      return;
    }
    
  mesh.BoundaryRef(n-1).Init(n1, n2, mesh.BoundaryRef(n-1).GetReference());
}

void ModifyEdgeRef(Mesh<Dimension2>& mesh, int n, int ref)
{
  if ((n <= 0)||(n > mesh.GetNbBoundaryRef()))
    {
      cout<<"Edge number must be between 1 and "<<mesh.GetNbBoundaryRef()<<endl;
      return;
    }
    
  mesh.BoundaryRef(n-1).SetReference(ref);
}

void ModifyElement(Mesh<Dimension2>& mesh, int n, IVect num)
{
  if ((n <= 0)||(n > mesh.GetNbElt()))
    {
      cout<<"Element number must be between 1 and "<<mesh.GetNbElt()<<endl;
      return;
    }
  
  for (int i = 0; i < num.GetM(); i++)
    {
      num(i)--;
      if ((num(i) < 0)||(num(i) >= mesh.GetNbVertices()))
	{
	  cout<<"Vertex number must be between 1 and "<<mesh.GetNbVertices()<<endl;
	  return;
	}
    }
    
  mesh.Element(n-1).Init(num, mesh.Element(n-1).GetReference());
}

void ModifyElementRef(Mesh<Dimension2>& mesh, int n, int ref)
{
  if ((n <= 0)||(n > mesh.GetNbElt()))
    {
      cout<<"Element number must be between 1 and "<<mesh.GetNbElt()<<endl;
      return;
    }
    
  mesh.Element(n-1).SetReference(ref);
}


void ModifySurfaceRef(Mesh<Dimension2>& mesh, int old_ref, int new_ref)
{
  for (int i = 0; i < mesh.GetNbBoundaryRef(); i++)
    if (mesh.BoundaryRef(i).GetReference() == old_ref)
      mesh.BoundaryRef(i).SetReference(new_ref);  
}

void ModifyVolumeRef(Mesh<Dimension2>& mesh, int old_ref, int new_ref)
{
  for (int i = 0; i < mesh.GetNbElt(); i++)
    if (mesh.Element(i).GetReference() == old_ref)
      mesh.Element(i).SetReference(new_ref);  
}

template<class T>
void ReadParameters(Vector<T>& param)
{
  string chaine;
  // cin>>flush;
  // getline(cin, chaine ,'\n');
  getline(cin, chaine ,'\n');
  istringstream stream_data(chaine);
  
  param.ReadText(stream_data);
}

void ReadParameters(string& param)
{
  getline(cin, param ,'\n');
}


void SaveMesh(Mesh<Dimension2>& mesh, const string& output_name)
{
  // we detect unused vertices
  int nb_vert = mesh.GetNbVertices();
  Vector<bool> VertexUsed(nb_vert); VertexUsed.Fill(false);
  for (int i = 0; i < mesh.GetNbElt(); i++)
    for (int j = 0; j < mesh.Element(i).GetNbVertices(); j++)
      VertexUsed(mesh.Element(i).numVertex(j)) = true;
  
  for (int i = 0; i < mesh.GetNbBoundaryRef(); i++)
    for (int j = 0; j < 2; j++)
      VertexUsed(mesh.BoundaryRef(i).numVertex(j)) = true;
  
  // we remove those vertices
  RemoveVertex(mesh, VertexUsed);
  
  mesh.Write(output_name);  
}


template<class T>
void TransformPolarToCartesianMesh(Mesh<Dimension2>& mesh, const T& rmin, const T& rmax,
                                   const T& teta_min, const T& teta_max)
{
  Real_wp xmin = mesh.GetXmin();
  Real_wp xmax = mesh.GetXmax();
  Real_wp ymin = mesh.GetYmin();
  Real_wp ymax = mesh.GetYmax();
  for (int i = 0; i < mesh.GetNbVertices(); i++)
    {
      Real_wp lambda = (mesh.Vertex(i)(1) - ymin)/(ymax-ymin);
      Real_wp teta = (1.0-lambda)*teta_min + lambda*teta_max;

      lambda = (mesh.Vertex(i)(0) - xmin)/(xmax-xmin);
      Real_wp r = (1.0-lambda)*rmin + lambda*rmax;
      
      Real_wp x = r*cos(teta);
      Real_wp y = r*sin(teta);
      mesh.Vertex(i).Init(x, y);
    }
  
  int r = mesh.GetGeometryOrder();
  R2 ptA, ptB;
  for (int i = 0; i < mesh.GetNbBoundaryRef(); i++)
    for (int k = 0; k < r-1; k++)
      {
        ptA = mesh.GetPointInsideEdge(i, k);
        Real_wp lambda = (ptA(1) - ymin)/(ymax-ymin);
        Real_wp teta = (1.0-lambda)*teta_min + lambda*teta_max;
        
        lambda = (ptA(0) - xmin)/(xmax-xmin);
        Real_wp r = (1.0-lambda)*rmin + lambda*rmax;
        
        Real_wp x = r*cos(teta);
        Real_wp y = r*sin(teta);
        ptB.Init(x, y);
        mesh.SetPointInsideEdge(i, k, ptB);
      }
}

int main(int argc, char** argv)
{
  InitMontjoie(argc, argv);
  
  Mesh<Dimension2> mesh;
  int order_geom = 1;
  string output_name("toto.mesh");
  if (argc > 1)
    {      
      string nom_init(argv[1]);
      int dimension = GetDimensionMesh(nom_init, GetExtension(nom_init));
      if (dimension == 3)
        {
          cout << " Only two-dimensional mesh are handled " << endl;
          abort();
        }
      
      if (argc > 2)
        {
          order_geom = atoi(argv[2]);
          mesh.SetGeometryOrder(order_geom);
	  if (order_geom > 1)
	    output_name = string("toto.msh");
          
	  if (argc > 3)
            output_name = string(argv[3]);
        }
      
      // lecture du maillage
      mesh.Read(nom_init);
    }
  
  // boucle de manipulation
  bool test_loop = true;
  IVect num, num2; VectReal_wp coord;
  while (test_loop)
    {
      cout<<"1 - AJOUTER sommet, arete, element, maillage"<<endl;
      cout<<"2 - ENLEVER sommet, arete, element"<<endl;
      cout<<"3 - MODIFIER sommet, arete, element, references "<<endl;
      cout<<"4 - DECOUPER"<<endl;      
      cout<<"5 - VERIFIER / INFORMER"<<endl;      
      cout<<"6 - SAUVER"<<endl;
      cout<<"7 - FINIR"<<endl;
      cout<<"8 - METTRE A JOUR CONNECTIVITE"<<endl;
      
      ReadParameters(num); int choix = num(0);
      
      switch (choix)
	{
	case 1 :
	  {
	    // menu pour ajouter
	    cout<<"1- Sommet"<<endl;
	    cout<<"2- Arete"<<endl;
	    cout<<"3- Element"<<endl;
	    cout<<"4- Maillage"<<endl;
	    cout<<"5- Zone PML" << endl;
	    cout<<"Autre- Retour au menu principal" << endl;
	    ReadParameters(num); int choix_ajout = num(0);
	    Real_wp x, y; int ref;
	    string nom_maillage; 
	    switch (choix_ajout)
	      {
	      case 1 :
		cout<<"Entrez les coordonnees x, y du sommet"<<endl;
		ReadParameters(coord);
		x = coord(0); y = coord(1);
		AddVertex(mesh, R2(x, y));
		break;
	      case 2 :
		cout<<"Entrez les numeros des extremites de l'arete"<<endl;
		ReadParameters(num);
		cout<<"Entrez la reference de cette arete "<<endl;
		ReadParameters(num2); ref = num2(0);
		AddEdge(mesh, num(0), num(1), ref);
		break;
	      case 3 :
		cout<<"Entrez les numeros des sommets de l'element"<<endl;
		ReadParameters(num);
		cout<<"Entrez la reference de cet element "<<endl;
		ReadParameters(num2); ref = num2(0);
		AddElement(mesh, num, ref);
		break;
	      case 4 :
                {
                  cout << "1- Rajouter un rectangle regulier " << endl;
                  cout << "2- Rajouter un maillage regulier d'un carre avec trou circulaire (non implemente) " << endl;
                  cout << "3- Rajouter un maillage regulier d'un carre avec un dielectrique circulaire (non implemente) " << endl;
                  cout << "4- Rajouter un maillage d'un fichier " << endl;
                  cout << "5- Rajouter un rectangle regulier avec progression geometrique" << endl;
		  cout<<"Autre- Retour au menu principal" << endl;
                  ReadParameters(num); int choix_maillage = num(0);
                  Mesh<Dimension2> new_mesh;
                  new_mesh.SetGeometryOrder(order_geom);
                  switch (choix_maillage)
                    {
                    case 1 :
                      {
                        cout << "Donnez xmin, xmax, ymin, ymax " << endl;
                        Real_wp xmin, xmax, ymin, ymax;
			ReadParameters(coord);
			xmin = coord(0); xmax = coord(1); ymin = coord(2); ymax = coord(3);
			cout << "Donnez le nombre de points suivant x et y " << endl;
                        ReadParameters(num);
			int nbx = num(0), nby = num(1);
                        cout << "Reference du domaine " << endl;
                        ReadParameters(num); int ref_domain = num(0);
                        cout << "References des bords y = ymin, x = xmax, y = ymax, x = xmin " << endl;
                        TinyVector<int, 4> ref_boundary;
			ReadParameters(num);
			for (int i = 0; i < 4; i++)
			  ref_boundary(i) = num(i);
			
                        cout << "Type de maillage " << endl;
                        cout << "0- Triangles " << endl;
                        cout << "1- Quadrangles " << endl;
                        ReadParameters(num); int type_mesh = num(0);
                        new_mesh.CreateRegularMesh(R2(xmin, ymin), R2(xmax, ymax), TinyVector<int, 2>(nbx, nby),
                                                   ref_domain, ref_boundary, type_mesh);
			
                      }
                      break;
                    case 2 :
                      {
			cout << "pas implemente" << endl;
                      }
                      break;
                    case 3 :
                      {                        
			cout << "pas implemente" << endl;
                      }
                      break;
                    case 4 :
                      {
                        cout << "Donnez le nom du fichier de maillage " << endl;
                        ReadParameters(nom_maillage);
                        new_mesh.Read(nom_maillage);
                      }
                      break;
                    case 5 :
                      {
                        cout << "Donnez xmin, xmax, ymin, ymax " << endl;
			ReadParameters(coord);
                        Real_wp xmin = coord(0), xmax  = coord(1), ymin  = coord(2), ymax  = coord(3);
			cout << "Donnez le nombre de points suivant x et y " << endl;
                        ReadParameters(num); int nbx = num(0), nby = num(1);
                        cout << "Donnez le ratio pour x y " << endl;
                        R2 ratio;
                        ReadParameters(coord);
			ratio(0) = coord(0); ratio(1) = coord(1);
                        cout << "Reference du domaine " << endl;
                        ReadParameters(num); int ref_domain = num(0);
                        cout << "References du bord y = ymin, x = xmax, y = ymax et x = xmin " << endl;
                        TinyVector<int, 4> ref_boundary;
                        ReadParameters(num);
			for (int i = 0; i < 4; i++)
			  ref_boundary(i) = num(i);
                                                
                        cout << "Type de maillage " << endl;
                        cout << "0- Triangles " << endl;
                        cout << "1- Quadrangles " << endl;
                        ReadParameters(num); int type_mesh = num(0);
                        new_mesh.CreateRegularMesh(R2(xmin, ymin), R2(xmax, ymax), TinyVector<int, 2>(nbx, nby),
                                                   ref_domain, ref_boundary, type_mesh, ratio);
                        
                      }
                      break;
                    }
		  
		  // on rajoute le maillage
                  mesh.AppendMesh(new_mesh, true);
                }
		break;
	      case 5 :
		{
		  cout << "1- Indiquer les bornes xmin, xmax, ymin, ymax" << endl;
		  ReadParameters(coord);
		  Real_wp xmin = coord(0), xmax  = coord(1), ymin  = coord(2), ymax  = coord(3);
		  cout << "2- Indiquer les pas de mailles dx et dy souhaites" << endl;
		  ReadParameters(coord);
		  Real_wp dx = coord(0), dy = coord(1);
		  
		  Real_wp xmin_m = mesh.GetXmin(), xmax_m = mesh.GetXmax();
		  Real_wp ymin_m = mesh.GetYmin(), ymax_m = mesh.GetYmax();
		  Real_wp delta_xmin(0), delta_ymin(0), delta_xmax(0), delta_ymax(0);
		  int nb_div_x0(0), nb_div_xN(0), nb_div_y0(0), nb_div_yN(0);
		  if (abs(xmin-xmin_m) > R2::threshold)
		    {
		      delta_xmin = xmin - xmin_m;
		      nb_div_x0 = toInteger(round(abs(delta_xmin)/dx));
		    }

		  if (abs(xmax-xmax_m) > R2::threshold)
		    {
		      delta_xmax = xmax - xmax_m;
		      nb_div_xN = toInteger(round(abs(delta_xmax)/dx));
		    }

		  if (abs(ymin-ymin_m) > R2::threshold)
		    {
		      delta_ymin = ymin - ymin_m;
		      nb_div_y0 = toInteger(round(abs(delta_ymin)/dy));
		    }

		  if (abs(xmin-xmin_m) > R2::threshold)
		    {
		      delta_ymax = ymax - ymax_m;
		      nb_div_yN = toInteger(round(abs(delta_ymax)/dy));
		    }
		  
		  if (nb_div_x0 > 0)
		    mesh.ExtrudeCoordinate(0, nb_div_x0, xmin_m, delta_xmin);
		  
		  if (nb_div_xN > 0)
		    mesh.ExtrudeCoordinate(0, nb_div_xN, xmax_m, delta_xmax);
		  
		  if (nb_div_y0 > 0)
		    mesh.ExtrudeCoordinate(1, nb_div_y0, ymin_m, delta_ymin);
		  
		  if (nb_div_yN > 0)
		    mesh.ExtrudeCoordinate(1, nb_div_yN, ymax_m, delta_ymax);
		}
		break;
	      }
	    break;
	  }
	case 2 :
	  {
	    // menu pour supprimer
	    cout<<"1- Sommet"<<endl;
	    cout<<"2- Sommets contenus dans un pave"<<endl;
	    cout<<"3- Arete"<<endl;
	    cout<<"4- Aretes de meme reference"<<endl;
	    cout<<"5- Element"<<endl;
	    cout<<"6- Elements de meme reference"<<endl;
	    cout<<"7- Tout sauf certains elements"<<endl;
	    cout<<"Autre- Retour au menu principal" << endl;
	    ReadParameters(num); int choix_retrait = num(0);
	    VectReal_wp delim;
	    switch (choix_retrait)
	      {
	      case 1 :
		cout<<"Entrez les numeros des sommets"<<endl;
		ReadParameters(num);
		RemoveVertex(mesh, num);
		break;
	      case 2 :
		cout<<"Entrez xmin xmax ymin ymax de la zone a supprimer"<<endl;
		ReadParameters(delim);
		RemoveVertex(mesh, delim);
		break;
	      case 3 :
		cout<<"Entrez les numeros des aretes"<<endl;
		ReadParameters(num);
		RemoveEdge(mesh, num);
		break;
	      case 4 :
		cout<<"Entrez les references des aretes a enlever"<<endl;
		ReadParameters(num);
		RemoveEdgeRef(mesh, num);
		break;
	      case 5 :
		cout<<"Entrez les numeros des elements a enlever"<<endl;
		ReadParameters(num);
		RemoveElement(mesh, num);
		break;
	      case 6 :
		cout<<"Entrez les references des elements a enlever"<<endl;
		ReadParameters(num);
		RemoveElementRef(mesh, num);
		break;
	      case 7 :
		{
		  cout<<"Entrez les numeros des elements a conserver"<<endl;
		  ReadParameters(num);
		  Vector<bool> ElementToRemove(mesh.GetNbElt());
		  ElementToRemove.Fill(true);
		  for (int i = 0; i < num.GetM(); i++)
		    ElementToRemove(num(i)) = false;
		  
		  IVect other_num(mesh.GetNbElt()-num.GetM());
		  int ind = 0;
		  for (int i = 0; i < mesh.GetNbElt(); i++)
		    if (ElementToRemove(i))
		      other_num(ind++) = i+1;
		
		  RemoveElement(mesh, other_num);
		}
		break;
	      }
	    break;
	  }
	case 3 :
	  {
	    // menu pour modifier
	    cout<<"1- Coordonnees d'un sommet"<<endl;
	    cout<<"2- Connectique d'une arete"<<endl;
	    cout<<"3- Reference d'une arete"<<endl;
	    cout<<"4- Connectique d'un element"<<endl;
	    cout<<"5- Reference d'un element"<<endl;
	    cout<<"6- Reference surfacique"<<endl;
	    cout<<"7- Reference volumique"<<endl;
	    cout<<"8- Rotation"<<endl;
	    cout<<"9- Translation"<<endl;
	    cout<<"10- Homothetie"<<endl;
	    cout<<"11- (r,teta) -> (x, y)"<<endl;
            cout<<"12- Ordre d'approximation de la geometrie"<<endl;
            cout<<"13- Forcer la coherence du maillage" << endl; 
	    cout<<"Autre- Retour au menu principal" << endl;
	    ReadParameters(num); int choix_modif = num(0);
	    VectReal_wp delim; int n, ref, nref; Real_wp x, y, teta;
            Real_wp x0, y0;
	    switch (choix_modif)
	      {
	      case 1 :
		cout<<"Entrez le numero du sommet a modifier"<<endl;
		ReadParameters(num); n = num(0);
		cout<<"Entrez les nouvelles coordoonees de ce sommet :  x y "<<endl;
		ReadParameters(coord); x = coord(0); y = coord(1);
		ModifyVertex(mesh, n, R2(x, y));
		break;
	      case 2 :
		cout<<"Entrez le numero de l'arete a modifier"<<endl;
		ReadParameters(num); n = num(0); cout<<"Arete : "<<mesh.BoundaryRef(n-1)<<endl;
		cout<<"Entrez les numeros des sommets de cette arete"<<endl;
		ReadParameters(num);
		ModifyEdge(mesh, n, num(0), num(1));
		break;
	      case 3 :
		cout<<"Entrez le numero de l'arete a modifier"<<endl;
		ReadParameters(num); n = num(0);
		cout<<"Arete : "<<mesh.BoundaryRef(n-1)<<endl;
		cout<<"Entrez la nouvelle reference "<<endl;
		ReadParameters(num); ref = num(0);
		ModifyEdgeRef(mesh, n, ref);
		break;
	      case 4 :
		cout<<"Entrez le numero de l'element a modifier"<<endl;
		ReadParameters(num); n = num(0);
		cout<<"Element : "<<mesh.Element(n-1)<<endl;
		cout<<"Entrez les numeros des sommets de cet element"<<endl;
		ReadParameters(num);
		ModifyElement(mesh, n, num);
		break;
	      case 5 :
		cout<<"Entrez le numero de l'element a modifier"<<endl;
		ReadParameters(num); n = num(0);
		cout<<"Element : "<<mesh.Element(n-1)<<endl;
		cout<<"Entrez la nouvelle reference "<<endl;
		ReadParameters(num); ref = num(0);
		ModifyElementRef(mesh, n, ref);
		break;
	      case 6 :
		cout<<"Entrez la reference surfacique a changer"<<endl;
		ReadParameters(num); ref = num(0);
		cout<<"Entrez la nouvelle reference"<<endl;
		ReadParameters(num); nref = num(0);
		ModifySurfaceRef(mesh, ref, nref);
		break;
	      case 7 :
		cout<<"Entrez la reference volumique a changer"<<endl;
		ReadParameters(num); ref = num(0);
		cout<<"Entrez la nouvelle reference"<<endl;
		ReadParameters(num); nref = num(0);
		ModifyVolumeRef(mesh, ref, nref);
		break;
	      case 8 :
		cout<<"Entrez le centre de la rotation"<<endl;
		ReadParameters(coord); x0 = coord(0); y0 = coord(1);
		cout<<"Entrez l'angle de rotation"<<endl;
		ReadParameters(coord); teta = coord(0)*pi_wp/180;
		RotateMesh(mesh, R2(x0, y0), teta);
		break;
	      case 9 :
		cout<<"Entrez le vecteur de translation"<<endl;
		ReadParameters(coord); x = coord(0); y = coord(1);
		TranslateMesh(mesh, R2(x, y));
		break;
	      case 10 :
		cout<<"Entrez les facteurs d'echelle"<<endl;
		ReadParameters(coord); x = coord(0); y = coord(1);
		ScaleMesh(mesh, R2(x, y));
		break;
	      case 11 :
		cout<<"Entrez rmin, rmax, teta_min and teta_max in degrees"<<endl;
		ReadParameters(coord); 
		x0= coord(0); y0 = coord(1); x = coord(2); y = coord(3);
		x *= pi_wp/180;
                y *= pi_wp/180;
		TransformPolarToCartesianMesh(mesh, x0, y0, x, y);
		break;
              case 12 :
                cout<<"Entrez l'ordre d'approximation de la geometrie"<<endl;
                ReadParameters(num); order_geom = num(0);
                mesh.SetGeometryOrder(order_geom);
                if (order_geom > 1)
                  output_name = string("toto.msh");
		else
		  output_name = string("toto.mesh");
                
                break;
              case 13 :
                mesh.ForceCoherenceMesh();
                break;
	      }
	    break;
	  }
	case 4 :
	  {
	    // menu pour decouper
	    cout<<"1- Decouper chaque element en triangles (un quad -> 2 triangles) "<<endl;
	    cout<<"2- Decouper chaque element en petits elements (chaque arete est subdivise en n morceaux) "<<endl;
	    cout<<"3- Decouper chaque triangle en trois quads, et chaque quad en quatre quads "<<endl;
	    cout<<"4- Decouper chaque element en petits elements (avec points de Gauss-Lobatto) "<<endl;
	    cout<<"Autre- Retour au menu principal" << endl;
	    ReadParameters(num); int choix_decoupe = num(0);
	    switch (choix_decoupe)
	      {
	      case 1 :
		mesh.SplitIntoTriangles();
		break;
              case 2 :
                {
		  cout<<"Entrez le nombre de subdivisions"<<endl;
		  ReadParameters(num); int nb_subdiv = num(0);
		  VectReal_wp step_x(nb_subdiv+1);
		  step_x.Fill();
		  Mlt(1.0/nb_subdiv, step_x);
		  mesh.SubdivideMesh(step_x);
		}
		break;
	      case 3 :
		mesh.SplitIntoQuadrilaterals();
		break;
              case 4 :
                {
		  cout<<"Entrez le nombre de subdivisions"<<endl;
		  ReadParameters(num); int nb_subdiv = num(0);
		  VectReal_wp step_x, poids;
		  ComputeGaussLobatto(step_x, poids, nb_subdiv);
		  mesh.SubdivideMesh(step_x);
		}
		break;
	      }
	    break;
	  }
	case 5 :
	  {
	    cout << "1- Verifier conformite maillage volumique / maillage surfacique " << endl;
	    cout << "2- Afficher les statistiques du maillage " << endl;
            cout << "3- Afficher les details d'une face "<< endl;
	    cout << "5- Distance entre deux sommets" << endl;
	    cout<<"Autre- Retour au menu principal" << endl;
	    ReadParameters(num); int choix_verif = num(0);
	    switch (choix_verif)
	      {
	      case 1 :
		try 
		  {
		    mesh.FindConnectivity();
                    for (int i = 0; i < mesh.GetNbBoundaryRef(); i++)
                      if (mesh.BoundaryRef(i).GetNbElements() == 0)
                        {
                          cout << "Arete de reference " << i+1 << " n appartient pas au maillage volumique " << endl;
                          cout << endl;
                        }                   
		  }
		catch (const WrongMesh& err)
		  {
		    cout<<"WARNING : Mesh not conform"<<endl;
		  }
		break;
	      case 2 :
		{
                  int ref_max = 0;
                  for (int i = 0; i < mesh.GetNbElt(); i++)
                    ref_max = max(ref_max, mesh.Element(i).GetReference());
                  
                  Vector<int> nb_elt_per_ref(ref_max+1);
                  nb_elt_per_ref.Fill(0);
                  for (int i = 0; i < mesh.GetNbElt(); i++)
                    nb_elt_per_ref(mesh.Element(i).GetReference())++;
                  
                  VectReal_wp length_edge(mesh.GetNbEdges());
                  Real_wp h_min(1e300), h_max(0), h_average(0);
                  for (int i = 0; i < mesh.GetNbEdges(); i++)
                    {
                      int n1 = mesh.GetEdge(i).numVertex(0);
                      int n2 = mesh.GetEdge(i).numVertex(1);
                      length_edge(i) = mesh.Vertex(n1).Distance(mesh.Vertex(n2));
                      h_min = min(h_min, length_edge(i));
                      h_max = max(h_max, length_edge(i));
                      h_average += length_edge(i);
                    }
                  
                  h_average /= mesh.GetNbEdges();
                  Sort(length_edge);
                  length_edge.WriteText("h.dat");
                  
                  cout << "Nombre global d'elements : " << mesh.GetNbElt() << endl;
                  cout << "Nombre global d'aretes : " << mesh.GetNbEdges() << endl;
                  cout << "Pas de maillage (longueur moyenne des aretes) : " << h_average << endl;
                  cout << "h_min, h_max = " << h_min << " " << h_max << endl;
                  
                  for (int ref = 0; ref < nb_elt_per_ref.GetM(); ref++)
                    if (nb_elt_per_ref(ref) > 0)
                      cout << "Nombre elements dans le domaine " << ref << " = " << nb_elt_per_ref(ref) << endl;
                  
		}
		break;
              case 3 :
                {
                  cout << "Entrez le numero de la face de reference " <<endl;
                  ReadParameters(num); int nf = num(0) - 1;
                  cout << mesh.BoundaryRef(nf) << endl;
                }
                break;
              case 5 :
		{
		  cout << "Entrez les numeros des deux sommets" << endl;
		  ReadParameters(num); 
		  int nv0 = num(0) - 1, nv1 = num(1) - 1;
		  cout << "Distance entre les deux : " << mesh.Vertex(nv0).Distance(mesh.Vertex(nv1)) << endl;
		}
		break;

	      }
	  }
	  break;
	case 6 :
	  SaveMesh(mesh, output_name);
	  break;
	case 7 :
	  test_loop = false;
	  break;
	case 8 :
	  mesh.FindConnectivity();
	  break;
	}
    }
  
  return FinalizeMontjoie();
}
