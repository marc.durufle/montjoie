#define MONTJOIE_WITH_TWO_DIM
#define MONTJOIE_WITH_THREE_DIM

#include "Mesh/MontjoieMesh.hxx"

using namespace Montjoie;

int main(int argc, char** argv)
{
  
  InitMontjoie(argc, argv);
  
  if (argc < 3)
    {
      cout<<"This command requires at least two arguments "<<endl;
      cout<<"sym_mesh.x input.mesh X"<<endl;
      abort();
    }
  
  int r = 1;
  if (argc >= 4)
    r = to_num<int>(argv[3]);
  
  string input_name(argv[1]), symmetry_info(argv[2]);
  int dim_N = GetDimensionMesh(input_name, GetExtension(input_name));
  
  if (dim_N == 2)
    {
      Mesh<Dimension2> mesh;
      R2 normale;
      
      mesh.SetGeometryOrder(r);
      mesh.Read(input_name);

      if (symmetry_info == "Plane")
        {
          if (argc < 7)
            {
              cout << "Enter parameters of the plane " << endl;
              abort();
            }
          
          normale(0) = to_num<Real_wp>(argv[4]);
          normale(1) = to_num<Real_wp>(argv[5]);
          Real_wp cte = to_num<Real_wp>(argv[6]);
          mesh.SymmetrizeMesh(-1, cte, normale);
        }
      else
        {      
          if (symmetry_info.find("X") != string::npos)
            mesh.SymmetrizeMesh(0, 0.0, normale);
          
          if (symmetry_info.find("Y") != string::npos)
            mesh.SymmetrizeMesh(1, 0.0, normale);
        }
      
      mesh.Write(input_name);
    }
  else if (dim_N == 3)
    {
      Mesh<Dimension3> mesh;
      R3 normale;
      
      mesh.SetGeometryOrder(r);
      mesh.Read(input_name);
      
      if (symmetry_info == "Plane")
        {
          if (argc < 8)
            {
              cout << "Enter parameters of the plane " << endl;
              abort();
            }
          
          normale(0) = to_num<Real_wp>(argv[4]);
          normale(1) = to_num<Real_wp>(argv[5]);
          normale(2) = to_num<Real_wp>(argv[6]);
          Real_wp cte = to_num<Real_wp>(argv[7]);
          mesh.SymmetrizeMesh(-1, cte, normale);
        }
      else
        {
          if (symmetry_info.find("X") != string::npos)
            mesh.SymmetrizeMesh(0, 0.0, normale);
          
          if (symmetry_info.find("Y") != string::npos)
            mesh.SymmetrizeMesh(1, 0.0, normale);
          
          if (symmetry_info.find("Z") != string::npos)
            mesh.SymmetrizeMesh(2, 0.0, normale);
        }
      
      mesh.Write(input_name);
    }
  
  return FinalizeMontjoie();
  
}
