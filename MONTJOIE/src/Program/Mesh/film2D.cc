#define MONTJOIE_WITH_THREE_DIM
#define MONTJOIE_WITH_TWO_DIM

#include "Output/MontjoieOutput.hxx"

#include "Output/OutputOpenCV.cxx"

using namespace Montjoie;

int main(int argc, char **argv) 
{
  InitMontjoie(argc, argv);
  
  if (argc < 6)
    {
      cout<<"Cette commande demande au moins deux arguments"<<endl;
      cout<<"film2D toto _U0.jpg cmin cmax file_coef"<<endl;
      return -1;
    }
  
  string root_input(argv[1]), ext_input(argv[2]);
  Real_wp cmin = atof(argv[3]), cmax = atof(argv[4]);  
  string file_coef(argv[5]);

  string base_ext = GetBaseString(ext_input);
  string ext_img = GetExtension(ext_input);
  
  int n0_inst = -1, n1_inst = -1;
  if (argc == 8)
    {
      // cas de plusieurs instantanes
      n0_inst = atoi(argv[6]);
      n1_inst = atoi(argv[7]);
    }
  
  Matrix<Real_wp> coef;
  if (file_coef != "ONE")
    coef.Read(file_coef);
  
  // on lit le fichier matlab
  bool ascii = false;
  VectReal_wp val;
  GridInterpolationFull<Dimension2> grid;  
  
  string file_name = root_input + base_ext + ".dat";
  if (n0_inst >= 0)
    file_name = root_input + NumberToString(n1_inst-1) + base_ext + ".dat";
  
  DISP(file_name);
  
  ReadMatlab(val, grid, file_name, ascii);
  int m = grid.GetNbPointsX();
  int n = grid.GetNbPointsY();
  int m0 = m + 20, n0 = n+20, i0 = 10, j0 = 10;
  //m0 = m; n0 = n; i0 = 0; j0 = 0;

  Vector<bool> place_nan(val.GetM());
  place_nan.Fill(false);
  Real_wp NaN = sqrt(-1.0);
  for (int k = 0; k < val.GetM(); k++)
    {
      int i = k/n, j = k%n;
      if (val(k) == 0)
	{
	  val(k) = NaN;
	  place_nan(k) = true;
	}
      else
	{
	  if (coef.GetM() > 0)
	    val(k) *= coef(i, j);
	}
    }
  
  // on ecrit les fichier jpeg
  string file_output;
  if (n0_inst >= 0)
    {
      for (int p = n0_inst; p < n1_inst; p++)
	{
	  file_name = root_input + NumberToString(p) + base_ext + ".dat";
	  ReadMatlab(val, grid, file_name, ascii);
	  for (int k = 0; k < val.GetM(); k++)
	    {
	      int i = k/n, j = k%n;
	      if (place_nan(k))
		val(k) = NaN;
	      else
		{
		  if (coef.GetM() > 0)
		    val(k) *= coef(i, j);
		}
	    }
	  
	  file_output = root_input + NumberToString(p) + base_ext + "." + ext_img;
	  WriteJpeg(val, file_output, m, m0, n0,
		    i0, j0, cmin, cmax, ColorMapEnum::JET);

	}
    }
  else
    {
      file_output = root_input + base_ext + "." + ext_img;
      WriteJpeg(val, file_output, m, m0, n0,
		i0, j0, cmin, cmax, ColorMapEnum::JET);
    }
  
  return FinalizeMontjoie();
}  

