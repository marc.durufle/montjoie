#include "Share/MontjoieCommon.hxx"

using namespace Montjoie;

int main(int argc, char** argv)
{
  InitMontjoie(argc, argv);
  
  VarRandomGenerator var;
  int n = 245, nx, ny, nz;
  var.RoundToSquare(n, 1.5, nx, ny);
  DISP(nx); DISP(ny); DISP(n);

  n = 2456;
  var.RoundToSquare(n, 1.5, 1.8, nx, ny, nz);
  DISP(nx); DISP(ny); DISP(nz); DISP(n);
  
  DISP(var.GetRand());
  
  VectReal_wp x;
  var.GenerateRandomNumbers(20, x);
  DISP(x);
  
  IVect permut;
  var.SetRandomPermutation(20, permut);
  DISP(permut);
  var.ApplyRandomPermutation(x);
  DISP(x);
  
  var.SetRandomGenerator(var.GSL_MT19937);
  var.GenerateRandomNumbers(20, x);
  DISP(x);
  var.SetRandomPermutation(20, permut);
  DISP(permut);
  
  return FinalizeMontjoie();
}
