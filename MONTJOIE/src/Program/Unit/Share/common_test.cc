#include "Share/MontjoieCommon.hxx"

using namespace Montjoie;

int main(int argc, char** argv)
{
  InitMontjoie(argc, argv);
  
  srand(time(NULL));
  
  // testing string stuff
  string nom("fileTest1.5toto.08.dat");
  if (GetExtension(nom) != "dat")
    {
      cout << "GetExtension incorrect" << endl;
      abort();
    }
  
  if (GetBaseString(nom) != "fileTest1.5toto.08")
    {
      cout << "GetBaseString incorrect" << endl;
      abort();
    }
  
  nom = string("SansNom");
  if (GetExtension(nom).size() > 0)
    {
      cout << "GetExtension incorrect" << endl;
      abort();
    }

  if (GetBaseString(nom) != "SansNom")
    {
      cout << "GetBaseString incorrect" << endl;
      abort();
    }
  
  nom = string("value1.0_@#.dat totoY -9");
  Vector<string> param;
  StringTokenize(nom, param, " ");
  if ((param.GetM() != 3) || (param(0) != "value1.0_@#.dat") || (param(1) != "totoY") || (param(2) != "-9"))
    {
      cout << "StringTokenize incorrect" << endl;
      abort();
    }

  nom = string(" value1 .0_@#.dat \t  totoY \t-9  ");
  StringTokenize(nom, param, " \t");
  if ((param.GetM() != 4) || (param(0) != "value1") || (param(1) != ".0_@#.dat") || (param(2) != "totoY") || (param(3) != "-9"))
    {
      cout << "StringTokenize incorrect" << endl;
      abort();
    }
  
  nom = string("OnlyOneParam");
  StringTokenize(nom, param, " \t");
  if ((param.GetM() != 1) && (param(0) != nom))
    {
      cout << "StringTokenize incorrect" << endl;
      abort();
    }

  nom = string("   \t \t   ");
  StringTokenize(nom, param, " \t");
  if (param.GetM() != 0)
    {
      cout << "StringTokenize incorrect" << endl;
      abort();
    }
  
  nom = string("Nom TOto \tprod \t");
  StringTrim(nom);
  if (nom != "NomTOtoprod")
    {
      cout << "StringTrim incorrect" << endl;
      abort();
    }

  StringTrim(nom);
  if (nom != "NomTOtoprod")
    {
      cout << "StringTrim incorrect" << endl;
      abort();
    }

  nom = string("\t\t   \t \t");
  StringTrim(nom);
  if (nom.size() > 0)
    {
      cout << "StringTrim incorrect" << endl;
      abort();
    }

  nom = string("  MyValue \t Prend  cette \t\t ");
  DeleteSpaceAtExtremityOfString(nom);
  if (nom != "MyValue \t Prend  cette")
    {
      cout << "DeleteSpaceAtExtremityOfString incorrect" << endl;
      abort();
    }
  
  nom = string("NoSpaceString");
  DeleteSpaceAtExtremityOfString(nom);
  if (nom != "NoSpaceString")
    {
      cout << "DeleteSpaceAtExtremityOfString incorrect" << endl;
      abort();
    }

  nom = string("   \t \t   ");
  DeleteSpaceAtExtremityOfString(nom);
  if (nom.size() > 0)
    {
      cout << "StringTokenize incorrect" << endl;
      abort();
    }
  
  if (NumberToString(2) != "0002")
    {
      cout << "NumberToString incorrect" << endl;
      abort();
    }

  if (NumberToString(34) != "0034")
    {
      cout << "NumberToString incorrect" << endl;
      abort();
    }

  if (NumberToString(328) != "0328")
    {
      cout << "NumberToString incorrect" << endl;
      abort();
    }

  if (NumberToString(1238) != "1238")
    {
      cout << "NumberToString incorrect" << endl;
      abort();
    }

  if (NumberToString(19234, 7) != "0019234")
    {
      cout << "NumberToString incorrect" << endl;
      abort();
    }
  
  // testing Copy
  Vector<string> xvec, xref; list<string> xl;
  xl.push_back(string("toto")); xref.PushBack(string("toto"));
  xl.push_back(string("other")); xref.PushBack(string("other"));
  xl.push_back(string("nom")); xref.PushBack(string("nom"));
  Copy(xl, xvec);
  
  if (xvec.GetM() != xref.GetM())
    {
      cout << "Copy incorrect" << endl;
      abort();
    }
  else
    {
      for (int j = 0; j < xvec.GetM(); j++)
	if (xvec(j) != xref(j))
	  {
	    cout << "Copy incorrect" << endl;
	    abort();
	  }
    }
  
  xl.clear(); xref.Clear();
  Copy(xl, xvec);
  
  if (xvec.GetM() != 0)
    {
      cout << "Copy incorrect" << endl;
      abort();
    }
  
  Vector<double, VectSparse> x_sparse(4), x_sp;
  Vector<double> x_dense(20);
  x_sparse.Index(0) = 1; x_sparse.Value(0) = 1.2;
  x_sparse.Index(1) = 4; x_sparse.Value(1) = -0.5;
  x_sparse.Index(2) = 9; x_sparse.Value(2) = 2.6;
  x_sparse.Index(3) = 13; x_sparse.Value(3) = -3.2;
  x_dense.Fill(0);
  for (int i = 0; i < x_sparse.GetM(); i++)
    x_dense(x_sparse.Index(i)) = x_sparse.Value(i);
  
  Copy(x_dense, x_sp);
  if (x_sparse.GetM() != x_sp.GetM())
    {
      cout << "Copy incorrect" << endl;
      abort();
    }
  else
    {
      for (int i = 0; i < x_sp.GetM(); i++)
	if ((x_sp.Index(i) != x_sparse.Index(i)) || (x_sp.Value(i) != x_sparse.Value(i)))
	  {
	    cout << "Copy incorrect" << endl;
	    abort();
	  }
    }
  
  DISP(GetHumanReadableMemory(int64_t(12300)));
  DISP(GetHumanReadableMemory(int64_t(123000)));
  DISP(GetHumanReadableMemory(int64_t(1230000)));
  DISP(GetHumanReadableMemory(int64_t(12300000)));
  DISP(GetHumanReadableMemory(int64_t(123000000)));
  DISP(GetHumanReadableMemory(int64_t(1230000000)));
  DISP(GetHumanReadableMemory(int64_t(12300000000)));
  DISP(GetHumanReadableMemory(int64_t(123000000000)));
  DISP(GetHumanReadableMemory(int64_t(1230000000000)));
  DISP(GetHumanReadableMemory(int64_t(12300000000000)));
  
  // testing conversion of coordinates  
  for (int n = 0; n < 20; n++)
    {
      Real_wp x = 2.0*Real_wp(rand())/RAND_MAX-1.0;
      Real_wp y = 2.0*Real_wp(rand())/RAND_MAX-1.0;
      Real_wp z = 2.0*Real_wp(rand())/RAND_MAX-1.0;
      Real_wp r, theta;
      
      if (n == 0) { x = 0; y = 0; z = 0;}
      if (n == 1) { x = 0; y = 0;}
      if (n == 2) { x = 0; z = 0;}
      if (n == 3) { y = 0; z = 0;}
      if (n == 4) { x = 0; }
      if (n == 5) { y = 0; }
      if (n == 6) { z = 0; }
      
      CartesianToPolar(x, y, r, theta);
      
      if ((abs(x-r*cos(theta)) > 1e-12) || isnan(r) || isnan(theta))
	{
	  DISP(x); DISP(y); DISP(r); DISP(theta);
	  cout << "CartesianToPolar incorrect" << endl;
	  abort();
	}
      
      Real_wp phi, cos_theta, sin_theta;
      CartesianToSpherical(x, y, z, r, theta, phi, cos_theta, sin_theta);

      if ((abs(x-r*sin(theta)*cos(phi)) > 1e-12) || isnan(r) || isnan(theta) || isnan(phi) ||
	  (abs(y-r*sin(theta)*sin(phi)) > 1e-12) || (abs(cos_theta-cos(theta)) > 1e-12) ||
	  (abs(z-r*cos(theta)) > 1e-12) || (abs(sin_theta-sin(theta)) > 1e-12))
	{
	  cout << "CartesianToSpherical incorrect" << endl;
	  abort();
	}
    }

  cout << "All tests passed successfully" << endl;

  return FinalizeMontjoie();
}
