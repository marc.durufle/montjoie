#include "Share/MontjoieCommon.hxx"

using namespace Montjoie;

Real_wp threshold = 1e-12;

int main(int argc, char** argv)
{
  InitMontjoie(argc, argv);
  
  Real_wp a = -10.0, b = 10.0;
  int N = 200;
  
  VectReal_wp t;
  Linspace(a, b, N, t);
  
  VectReal_wp f(N), frev(N), foriginal;
  VectComplex_wp fchap(N/2+1);
  for (int i = 0; i < N; i++)
    f(i) = exp(-t(i)*t(i))*cos(2.0*pi_wp*t(i));

  foriginal = f;
  VectReal_wp omega(N/2+1);
  for (int i = 0; i < N/2+1; i++)
    omega(i) = 2.0*pi_wp*Real_wp(i)/(b-a);    
  
  FftRealInterface fft;
  fft.Init(N);
  
  fft.ApplyForward(f, fchap);
  
  //fchap.WriteText("EvalFchap.dat");
  
  //for (int i = 0; i < N/2+1; i++)
  //fchap(i) *= -Iwp*omega(i);
  
  fft.ApplyInverse(fchap, frev);
  
  for (int i = 0; i < N; i++)
    if (abs(foriginal(i) - frev(i)) > threshold)
      {
        cout << "Fft incorrect" << endl;
        DISP(i); DISP(foriginal(i)); DISP(frev(i));
        abort();
      }
  
  //foriginal.WriteText("EvalF.dat");
  //frev.WriteText("EvalFder.dat");
  
  return FinalizeMontjoie();
}
