#include "Algebra/MontjoieAlgebra.hxx"

using namespace Montjoie;

Real_wp threshold;

template<class T, int m, int n, int p>
void CheckTinyArray3D(TinyArray3D<T, m, n, p>& A)
{
  TinyVector<T, m>::threshold = threshold;
  TinyVector<T, n>::threshold = threshold;
  TinyVector<T, p>::threshold = threshold;
  
  A.FillRand(); A *= 1e-9;
  
  if (A.GetLength1() != m)
    {
      cout << "GetLength1 incorrect" << endl;
      abort();
    }

  if (A.GetLength2() != n)
    {
      cout << "GetLength2 incorrect" << endl;
      abort();
    }

  if (A.GetLength3() != p)
    {
      cout << "GetLength3 incorrect" << endl;
      abort();
    }

  if (A.GetSize() != m*n*p)
    {
      cout << "GetSize incorrect" << endl;
      abort();
    }
  
  // checking Zero, Fill() and operator *=
  A.Fill(2.3);  
  for (int i = 0; i < m; i++)
    for (int j = 0; j < n; j++)
      for (int k = 0; k < p; k++)
        if ((abs(A(i, j, k) - 2.3) > threshold) || isnan(abs(A(i, j, k) - 2.3)))
          {
            cout << "Fill incorrect" << endl;
            abort();
          }

  A.Zero();
  for (int i = 0; i < m; i++)
    for (int j = 0; j < n; j++)
      for (int k = 0; k < p; k++)
        if ((abs(A(i, j, k)) > threshold) || isnan(A(i, j, k)))
          {
            cout << "Zero incorrect" << endl;
            abort();
          }
  
  A.FillRand(); A *= 1e-9;
  TinyArray3D<T, m, n, p> B; B = A;
  B *= 1.7;
  for (int i = 0; i < m; i++)
    for (int j = 0; j < n; j++)
      for (int k = 0; k < p; k++)
        if ((abs(B(i, j, k) - 1.7*A(i, j, k)) > threshold) || isnan(abs(B(i, j, k) - 1.7*A(i, j, k))))
          {
            cout << "Operator *= incorrect" << endl;
            abort();
          }
  
  // checking operator *
  TinyVector<T, n> x;
  TinyMatrix<T, General, m, p> y, z;
  
  x.FillRand(); x *= 1e-9;
  y = A*x;
  for (int i = 0; i < m; i++)
    for (int j = 0; j < p; j++)
      {
        T vloc; SetComplexZero(vloc);
        for (int k = 0; k < n; k++)
          vloc += A(i, k, j)*x(k);
        
        if ((abs(vloc - y(i, j)) > threshold) || isnan(abs(vloc - y(i, j))))
          {
            cout << "Operator * incorrect" << endl;
            abort();
          }
      }
      
  // checking ExtractMatrix
  ExtractMatrix(A, 1, y);
  TinyVector<int, p> theta;
  for (int i = 0; i < p; i++)
    theta(i) = (i+2)%p;
  
  ExtractMatrix(A, theta, z);
  for (int i = 0; i < m; i++)
    for (int j = 0; j < p; j++)
      if ( (abs(y(i, j) - A(i, 1, j)) >threshold) || 
           (abs(z(i, j) - A(i, theta(j), j)) >threshold)
           || isnan(abs(y(i, j) - A(i, 1, j))) || isnan(abs(z(i, j) - A(i, theta(j), j))) )
        {
          cout << "ExtractMatrix incorrect" << endl;
          abort();
        }
}

int main(int argc, char** argv)
{
  InitMontjoie(argc, argv);
  
  bool overall_success = true;  
  threshold = 1e4*epsilon_machine;  
  cout.precision(15);
  
  {
    TinyArray3D<Real_wp, 5, 4, 3> A;
    CheckTinyArray3D(A);  
  }
  
  if (overall_success)
    cout << "All tests passed successfully " << endl;
  
  return FinalizeMontjoie();
}
