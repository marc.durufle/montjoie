#ifndef MONTJOIE_FILE_MESH_GENERATION_CXX

#include "MeshGeneration.hxx"

namespace Montjoie
{
  void CreatePyramidalLayer(const Mesh<Dimension3>& mesh_surf, Mesh<Dimension3>& mesh_tri,
			    Mesh<Dimension3>& mesh_pyramid,
                            const Real_wp& height_pyramid, int new_ref)
  {
    // on decoupe le maillage de peau en triangles
    int nb_tri = mesh_surf.GetNbTrianglesRef();
    int nb_quad = mesh_surf.GetNbQuadranglesRef();
    mesh_tri = mesh_surf;
    Mesh<Dimension3> mesh_tet;
    
    mesh_tri.SplitIntoTetrahedra();
    
    // on mets des tetras dedans pour savoir ou est l'interieur
    CreateTetrahedralMesh(mesh_tri, mesh_tet);
    
    // puis sur chaque quadrangle on essaie de rajouter une pyramide
    int nb_vert_surf = mesh_surf.GetNbVertices();
    int nb_vertices = nb_vert_surf + nb_quad;
    int nb_faces = nb_tri + 5*nb_quad;
    mesh_tri.ReallocateVertices(nb_vertices);
    mesh_tri.ReallocateBoundariesRef(nb_tri + 4*nb_quad);

    mesh_pyramid.ReallocateVertices(nb_vertices);
    mesh_pyramid.ReallocateBoundariesRef(nb_faces);
    mesh_pyramid.ReallocateElements(nb_quad);
    for (int i = 0; i < nb_vert_surf; i++)
      {
	mesh_tri.Vertex(i) = mesh_surf.Vertex(i);
	mesh_pyramid.Vertex(i) = mesh_surf.Vertex(i);
      }
    
    R3 vec_u, vec_v, vec_w, center, ptTet;
    nb_vertices = nb_vert_surf;
    nb_faces = nb_tri + nb_quad;
    int nb = 0, nb_elt = 0;
    for (int i = 0; i < mesh_surf.GetNbBoundaryRef(); i++)
      {
	int nb_vert = mesh_surf.BoundaryRef(i).GetNbVertices();
	// on copie l'ancienne face
	mesh_pyramid.BoundaryRef(i) = mesh_surf.BoundaryRef(i);
	
	if (nb_vert == 3)
	  {
	    mesh_tri.BoundaryRef(nb) = mesh_surf.BoundaryRef(i);
	    nb++;
	  }
	else
	  {
	    int n1 = mesh_surf.BoundaryRef(i).numVertex(0);
	    int n2 = mesh_surf.BoundaryRef(i).numVertex(1);
	    int n3 = mesh_surf.BoundaryRef(i).numVertex(2);
	    int n4 = mesh_surf.BoundaryRef(i).numVertex(3);
	    	    
	    center = 0.25*(mesh_surf.Vertex(n1) + mesh_surf.Vertex(n2)
                           + mesh_surf.Vertex(n3) + mesh_surf.Vertex(n4));
	    vec_u = mesh_surf.Vertex(n2) - mesh_surf.Vertex(n1);
	    vec_v = mesh_surf.Vertex(n4) - mesh_surf.Vertex(n1);
	    
	    TimesProd(vec_u, vec_v, vec_w);
	    Mlt(1.0/sqrt(Norm2(vec_w)), vec_w);
	    
	    // on chope le centre du tetra
	    int ne = mesh_tet.BoundaryRef(i).numElement(0);
	    for (int k = 0; k < 4; k++)
	      {
		int nv = mesh_tet.Element(ne).numVertex(k);
		if ((nv != n1) && (nv != n2) && (nv != n3) && (nv != n4))
		  ptTet = mesh_tet.Vertex(nv);
	      }
	    
	    ptTet -= mesh_surf.Vertex(n1);
	    // on change le signe si besoin
	    if (DotProd(ptTet, vec_w) < 0)
	      vec_w *= -1.0;
	    
	    // on cree l'apex de la pyramide
	    mesh_pyramid.Vertex(nb_vertices) = center + height_pyramid*vec_w;
	    mesh_tri.Vertex(nb_vertices) = center + height_pyramid*vec_w;
	    
	    // et les 4 faces triangulaires
	    mesh_pyramid.BoundaryRef(nb_faces).InitTriangular(n1, n2, nb_vertices, 0);
	    mesh_pyramid.BoundaryRef(nb_faces+1).InitTriangular(n2, n3, nb_vertices, 0);
	    mesh_pyramid.BoundaryRef(nb_faces+2).InitTriangular(n3, n4, nb_vertices, 0);
	    mesh_pyramid.BoundaryRef(nb_faces+3).InitTriangular(n1, n4, nb_vertices, 0);

	    mesh_tri.BoundaryRef(nb).InitTriangular(n1, n2, nb_vertices, new_ref);
	    mesh_tri.BoundaryRef(nb+1).InitTriangular(n2, n3, nb_vertices, new_ref);
	    mesh_tri.BoundaryRef(nb+2).InitTriangular(n3, n4, nb_vertices, new_ref);
	    mesh_tri.BoundaryRef(nb+3).InitTriangular(n1, n4, nb_vertices, new_ref);
	    
	    // la pyramide
	    mesh_pyramid.Element(nb_elt).InitPyramidal(n1, n2, n3, n4, nb_vertices, new_ref);
	    
	    nb_faces += 4; nb += 4;
	    nb_vertices++; nb_elt++;
	  }
      }
    
    mesh_tri.FindConnectivity();
    mesh_pyramid.ReorientElements();
    mesh_pyramid.FindConnectivity();
  }
  
    
  // creation d'un maillage tetraedrique a partir d'un maillage triangulaire en utilisant Ghs3D
  void CreateTetrahedralMesh(Mesh<Dimension3>& mesh_surf, Mesh<Dimension3>& mesh_tet)
  {
    std::remove("tri.mesh");
    std::remove("tri.noboiteb"); std::remove("tri.bb");
    std::remove("tet.meshb"); std::remove("don_ghs");
    
    // si on a des quadrangles dans le maillage de peau, on rajoute
    // une pyramide sur chaque quadrangle pour n'avoir que des triangles
    bool pyramid_layer = false;
    Mesh<Dimension3> mesh_pyramid, mesh_tri;
    int new_ref(0);
    if (mesh_surf.GetNbQuadranglesRef() > 0)
      {
	new_ref = mesh_surf.GetNewReference();
	pyramid_layer = true;
	CreatePyramidalLayer(mesh_surf, mesh_tri, mesh_pyramid, 0.25, new_ref);
	mesh_tri.Write("tri.mesh");
      }
    else
      {
	mesh_surf.Write("tri.mesh");
      }

    // on appelle Ghs3D pour avoir les tetras
    ofstream file_ghs("don_ghs");
    file_ghs << "10" << endl;
    file_ghs << "tri" << endl;
    file_ghs << "0" << endl;
    
    string command_gmsh = string("ghs3d < don_ghs > sortie");
    system(command_gmsh.data());
    
    command_gmsh = string("noboiteb2meshb tri.noboiteb tet.meshb > sortie");
    system(command_gmsh.data());
    
    mesh_tet.Read("tet.meshb");    
    
    // on ecrase les premiers sommets parce que Ghs3d marche en simple precision
    // et donc on recupere les sommets double precision a partir du maillage de peau initial
    if (pyramid_layer)
      {
	for (int i = 0; i < mesh_tri.GetNbVertices(); i++)
	  mesh_tet.Vertex(i) = mesh_tri.Vertex(i);
	
	// on remet les bords
	mesh_tet.ReallocateBoundariesRef(mesh_tri.GetNbBoundaryRef());
	for (int i = 0; i < mesh_tri.GetNbBoundaryRef(); i++)
	  mesh_tet.BoundaryRef(i) = mesh_tri.BoundaryRef(i);
      }
    else
      {
	for (int i = 0; i < mesh_surf.GetNbVertices(); i++)
	  mesh_tet.Vertex(i) = mesh_surf.Vertex(i);
	
	// on remet les bords
	mesh_tet.ReallocateBoundariesRef(mesh_surf.GetNbBoundaryRef());
	for (int i = 0; i < mesh_surf.GetNbBoundaryRef(); i++)
	  mesh_tet.BoundaryRef(i) = mesh_surf.BoundaryRef(i);
      }
    
    mesh_tet.FindConnectivity();
    
    // on rajoute les pyramides si necessaire
    if (pyramid_layer)
      {
	mesh_tet.AppendMesh(mesh_pyramid);
	mesh_tet.RemoveReference(new_ref);
      }
    
    std::remove("tri.mesh");
    std::remove("tri.noboiteb"); std::remove("tri.bb");
    std::remove("tet.meshb"); std::remove("don_ghs");
  }
  
  
  // creation d'un maillage quadrangulaire
  void CreateSquareMesh(int nb_points_x, int nb_points_y, const R3& ptA, const R3& ptB,
                        const R3& ptC, const R3& ptD, Mesh<Dimension3>& mesh)
  {
    Mesh<Dimension2> mesh2d;
    int ref = 1;
    TinyVector<int, 4> ref_boundary;
    ref_boundary.Fill(1);
    mesh2d.CreateRegularMesh(R2(0,0), R2(1,1), TinyVector<int, 2>(nb_points_x, nb_points_y),
			     ref, ref_boundary, mesh2d.QUADRILATERAL_MESH);
    
    mesh.ReallocateVertices(mesh2d.GetNbVertices());
    for (int i = 0; i < mesh2d.GetNbVertices(); i++)
      {
	Real_wp x = mesh2d.Vertex(i)(0);
	Real_wp y = mesh2d.Vertex(i)(1);
	R3 point = (1.0-x)*(1.0-y)*ptA + x*(1.0-y)*ptB + x*y*ptC + (1.0-x)*y*ptD;
	mesh.Vertex(i) = point;
      }
    
    mesh.ReallocateBoundariesRef(mesh2d.GetNbElt());
    IVect num(4);
    for (int i = 0; i < mesh2d.GetNbElt(); i++)
      {
	for (int k = 0; k < 4; k++)
	  num(k) = mesh2d.Element(i).numVertex(k);
	
	mesh.BoundaryRef(i).Init(num, 1);
      }
    
    mesh.FindConnectivity();
  }
  
  
  // creation d'un maillage triangulaire
  void CreateTriangularMesh(int n1, int n2, int n3, int n4,
			    const R3& ptA, const R3& ptB, const R3& ptC,
                            const R3& ptD, Mesh<Dimension3>& mesh)
  {
    ofstream file_out("tmp_geo.geo");
    file_out.precision(15);
    Real_wp dx;
    dx = 0.25*(ptA.Distance(ptB) + ptB.Distance(ptC) + ptC.Distance(ptD) + ptA.Distance(ptD));
    file_out << "lc = " << dx << ";" << endl << endl;
    file_out << "Point(1) = {" << ptA(0) << ", " << ptA(1) << ", " << ptA(2) << ", lc};" << endl;
    file_out << "Point(2) = {" << ptB(0) << ", " << ptB(1) << ", " << ptB(2) << ", lc};" << endl;
    file_out << "Point(3) = {" << ptC(0) << ", " << ptC(1) << ", " << ptC(2) << ", lc};" << endl;
    file_out << "Point(4) = {" << ptD(0) << ", " << ptD(1) 
             << ", " << ptD(2) << ", lc};" << endl << endl;
    
    file_out << "Line(1) = {1, 2};" << endl;
    file_out << "Line(2) = {2, 3};" << endl;
    file_out << "Line(3) = {3, 4};" << endl;
    file_out << "Line(4) = {4, 1};" << endl << endl;
    
    file_out << "Transfinite Line{1} = " << n1 <<";" << endl;
    file_out << "Transfinite Line{2} = " << n2 <<";" << endl;
    file_out << "Transfinite Line{3} = " << n3 <<";" << endl;
    file_out << "Transfinite Line{4} = " << n4 <<";" << endl;
    
    file_out << "Line Loop(1) = {1, 2, 3, 4};" << endl;
    file_out << "Plane Surface(1) = {1};" << endl;
    file_out << "Physical Surface(1) = {1};" << endl;
    
    file_out.close();
    
    string command_gmsh("gmsh -o plan0.msh tmp_geo.geo -3 > sortie");
    system(command_gmsh.data());
    
    mesh.Read("plan0.msh");
    std::remove("tmp_geo.geo");
    std::remove("plan0.msh");
  }
  
}

#define MONTJOIE_FILE_MESH_GENERATION_CXX
#endif

