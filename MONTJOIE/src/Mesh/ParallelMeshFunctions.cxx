#ifndef MONTJOIE_FILE_PARALLEL_MESH_FUNCTIONS_CXX

namespace Montjoie
{
  
  //! sending a mesh to a processor
  /*!
    \param[in] glob_mesh global mesh (a part of this mesh is actually sent)
    \param[in] glob_mesh_num global numbering
    \param[in] NumElement list of element numbers associated with the sub-mesh to send
    \param[in] Epart Epart(i) is the processor number associated with element i
    \param[in] NumLoc NumLoc(i) is the local element number of global element i
    \param[inout] mesh sub-mesh to send
    \param[inout] proc mesh will be send to this processor
    \param[inout] comm MPI communicator
    \param[in] tag MPI communications will use tag+something as MPI tag
    \param[out] num integer numbers that will be sent to distant processor
    \param[out] all_param double numbers that will be sent to distant processor
    \param[out] PointsEdgeRef points on curved edges that will be sent
    \param[out] NumBoundary integer numbers associated with boundaries that will be sent
    \param[out] NumDofNeighbor dof numbers of neighbor elements that will be sent
    \param[out] NumElem integers associated with elements that will be sent
    \param[out] NumDof integers associated with dofs that will be sent
    \param[out] MinimalProc minimal processor number for each dof
    \param[out] PeriodicDof
    \param[out] TranslatPeriodicDof
    \param[out] OffsetDofV
    \param[out] NumDofPML
   */
  void SendMeshToProcessor(const Mesh<Dimension2>& glob_mesh,
                           const MeshNumbering<Dimension2>& glob_mesh_num,
			   const IVect& NumElement, const IVect& Epart, const IVect& NumLoc,
                           Mesh<Dimension2>& mesh, int proc, const MPI::Comm& comm, int tag,
                           IVect& num, VectReal_wp& all_param, VectR2& PointsEdgeRef, IVect& NumBoundary,
                           IVect& NumDofNeighbor, IVect& NumElem, IVect& NumDof,
                           IVect& MinimalProc, IVect& PeriodicDof, VectR2& TranslatPeriodicDof,
                           IVect& OffsetDofV, IVect& NumDofPML)
  {    
    int rank = comm.Get_rank();
    
    // the array all_param is constructed and will contain
    // all the parameters (such as parameters of the curves) of the mesh
    // that we want to send to other processors
    int nb_ref = mesh.GetNbReferences();
    int nb_param = 5*nb_ref+5;
    VectReal_wp param;
    for (int i = 1; i <= nb_ref; i++)
      {
        mesh.GetCurveParameter(i, param);
        nb_param += param.GetM();
      }
    
    Vector<int64_t> all_param_tmp;
    IVect ref_neighbor = mesh.GetOriginalNeighborReference();
    all_param.Reallocate(nb_param); all_param.Fill(0);    
    nb_param = 5*nb_ref+1;
    all_param(0) = nb_param;
    for (int i = 1; i <= mesh.GetNbReferences(); i++)
      {
        mesh.GetCurveParameter(i, param);
        for (int j = 0; j < param.GetM(); j++)
          all_param(nb_param+j) = param(j);
        
        nb_param += param.GetM();
        all_param(i) = nb_param;
        all_param(nb_ref+i) = mesh.GetBodyNumber(i);
        all_param(2*nb_ref+i) = mesh.GetBoundaryCondition(i);
        all_param(3*nb_ref+i) = mesh.GetCurveType(i);
        all_param(4*nb_ref+i) = ref_neighbor(i);
      }    
    
    all_param(nb_param) = glob_mesh.GetPeriodicAlpha(); nb_param++;
    all_param(nb_param) = glob_mesh.GetRadiusPML(); nb_param++;
    all_param(nb_param) = glob_mesh.GetOriginRadialPML()(0); nb_param++;
    all_param(nb_param) = glob_mesh.GetOriginRadialPML()(1); nb_param++;
    
    // sending number of vertices, referenced edges and elements
    int nb_vertices = mesh.GetNbVertices();
    int nb_edges_ref = mesh.GetNbBoundaryRef();
    int nb_elt = mesh.GetNbElt();
    int nb_int_elt = 0;
    for (int i = 0; i < nb_elt; i++)
      nb_int_elt += mesh.Element(i).GetNbVertices();

    // counting dofs
    int nb_periodic_dof = 0, nb_mesh_dof = 0;
    int nb_matching_proc = 0, nb_matching_dofs = 0;
    Vector<IVect> MatchingDofs(comm.Get_size());
    if (!glob_mesh_num.number_map.FormulationDG())
      {
        // returning to positive numbers
        for (int i = 0; i < MinimalProc.GetM(); i++)
          MinimalProc(i) = abs(MinimalProc(i));
        
        // we put opposite signs in MinimalProc for dofs associated with the processor proc            
        for (int i = 0; i < nb_elt; i++)
          {
            int ne = NumElement(i);
            for (int j = 0; j < glob_mesh_num.Element(ne).GetNbDof(); j++)
              {
                int num_dof = glob_mesh_num.Element(ne).GetNumberDof(j);
                if ((num_dof >= 0) && (MinimalProc(num_dof) > 0))
                  {
                    MinimalProc(num_dof) = -MinimalProc(num_dof);
                    nb_mesh_dof++;
                  }
              }
          }
        
        // the number of dofs shared with other processors is estimated
        IVect nb_dofs_proc(comm.Get_size());
        nb_dofs_proc.Zero();
        for (int i = 0; i < glob_mesh.GetNbElt(); i++)
          for (int j = 0; j < glob_mesh_num.Element(i).GetNbDof(); j++)
            {
              int num_dof = glob_mesh_num.Element(i).GetNumberDof(j);
              if ((num_dof >= 0) && (MinimalProc(num_dof) < 0))
                nb_dofs_proc(Epart(i))++;
            }
        
        for (int p = 0; p < MatchingDofs.GetM(); p++)
          if (p != proc)
            MatchingDofs(p).Reallocate(nb_dofs_proc(p));
        
        nb_dofs_proc.Zero();
        for (int i = 0; i < glob_mesh.GetNbElt(); i++)
          {
            int p = Epart(i);
            if (p != proc)
              for (int j = 0; j < glob_mesh_num.Element(i).GetNbDof(); j++)
                {
                  int num_dof = glob_mesh_num.Element(i).GetNumberDof(j);
                  if ((num_dof >= 0) && (MinimalProc(num_dof) < 0))
                    {
                      MatchingDofs(p)(nb_dofs_proc(p)) = num_dof;
                      nb_dofs_proc(p)++;
                    }
                }
          }
        
        // duplicates are removed and numbers sorted
        for (int p = 0; p < MatchingDofs.GetM(); p++)
          if (MatchingDofs(p).GetM() > 0)
            {
              nb_matching_proc++;
              RemoveDuplicate(MatchingDofs(p));
              nb_matching_dofs += MatchingDofs(p).GetM();
            }
        
        if (glob_mesh_num.GetNbPeriodicDof() > 0)
          {
            // now counting periodic dofs on processor proc
            for (int i = 0; i < glob_mesh_num.GetNbPeriodicDof(); i++)
              {
                int num_dof = glob_mesh_num.GetPeriodicDof(i);
                if (MinimalProc(num_dof) < 0)
                  nb_periodic_dof++;
              }
          }
      }

    num.Reallocate(20);
    num.Fill(0);
    num(0) = nb_vertices;
    num(1) = nb_edges_ref;
    num(2) = nb_elt;
    num(3) = nb_ref;
    num(4) = nb_param;
    num(5) = nb_int_elt;
    num(6) = mesh.GetGeometryOrder();
    num(7) = glob_mesh_num.GetOrder();
    num(8) = nb_matching_proc;
    num(9) = nb_matching_dofs;
    num(10) = nb_periodic_dof;
    num(11) = OffsetDofV.GetM();
    num(12) = glob_mesh_num.compute_dof_pml;
    num(13) = glob_mesh_num.GetVariableOrder();
    
    if (rank != proc)
      {
        comm.Ssend(num.GetData(), 20, MPI::INTEGER, proc, tag);
        tag++;
      }
    
    // sending parameters of curves
    if (rank != proc)
      {
        MpiSsend(comm, all_param, all_param_tmp, nb_param, proc, tag);
        tag++;
      }
    
    // sending vertices and internal points of edges
    int r = mesh.GetGeometryOrder();
    PointsEdgeRef.Reallocate(nb_vertices + (r-1)*nb_edges_ref);
    for (int i = 0; i < nb_vertices; i++)
      PointsEdgeRef(i) = mesh.Vertex(i);
    
    int nb = nb_vertices;
    if (r > 1)
      {
        for (int i = 0; i < nb_edges_ref; i++)
          for (int k = 0; k < r-1; k++)
            PointsEdgeRef(nb++) = mesh.GetPointInsideEdge(i, k);
      }
    
    if (rank != proc)
      {
        MpiSsend(comm, reinterpret_cast<Real_wp*>(PointsEdgeRef.GetData()),
                 all_param_tmp, 2*nb, proc, tag);
        tag++;
      }    
    
    // sending edges    
    NumBoundary.Reallocate(15*nb_edges_ref);
    NumBoundary.Fill(-1);
    nb = 0; int nb_dof_neighbor = 0;
    for (int i = 0; i < nb_edges_ref; i++)
      {
        NumBoundary(nb) = mesh.BoundaryRef(i).numVertex(0);
        NumBoundary(nb+1) = mesh.BoundaryRef(i).numVertex(1);
        NumBoundary(nb+2) = mesh.BoundaryRef(i).GetReference();
        int ne = mesh.BoundaryRef(i).numElement(0);
        int num_loc = mesh.Element(ne).GetPositionBoundary(i);
        int num_elem = NumElement(ne);
        int iglob = glob_mesh.Element(num_elem).numEdge(num_loc);
        NumBoundary(nb+3) = glob_mesh_num.GetOrderQuadrature(iglob);
        int ref = mesh.BoundaryRef(i).GetReference();
        
        if (mesh.GetBoundaryCondition(ref) == BoundaryConditionEnum::LINE_NEIGHBOR)
          {
            // referenced edge at the interface between subdomains            
            int num_elt2 = glob_mesh.Boundary(iglob).numElement(0);
            if (num_elt2 == num_elem)
              num_elt2 = glob_mesh.Boundary(iglob).numElement(1);
            
            //int num_loc2 = glob_mesh.Element(num_elt2).numEdge(num_loc);
            int num_loc2 = -1, rot = -1;
            glob_mesh_num.GetBoundaryRotation(iglob, num_elem, num_elt2, num_loc, num_loc2, rot);
            
            NumBoundary(nb+4) = num_elt2;
            NumBoundary(nb+5) = Epart(num_elt2); 
            NumBoundary(nb+6) = NumLoc(num_elt2);
            NumBoundary(nb+7) = num_loc2;
            NumBoundary(nb+8) = rot;
            NumBoundary(nb+9) = glob_mesh_num.GetOrderElement(num_elt2);
            NumBoundary(nb+10) = glob_mesh.GetTypeElement(num_elt2);
            NumBoundary(nb+11) = glob_mesh_num.GetNbLocalDof(num_elt2);
            NumBoundary(nb+12) = iglob;
            NumBoundary(nb+13) = glob_mesh_num.GetPeriodicBoundary(iglob);
            NumBoundary(nb+14) = glob_mesh.Element(num_elt2).GetReference();
            nb_dof_neighbor += glob_mesh_num.GetNbLocalDof(num_elt2);
          }
        
        nb += 15;
      }
    
    if (rank != proc)
      {
        comm.Ssend(NumBoundary.GetData(), nb, MPI::INTEGER, proc, tag);    
        tag++;
      }
    
    // dofs associated with neighboring elements    
    if (glob_mesh_num.compute_dof_pml)
      NumDofNeighbor.Reallocate(2*nb_dof_neighbor);
    else
      NumDofNeighbor.Reallocate(nb_dof_neighbor);
    
    nb = 0;
    for (int i = 0; i < nb_edges_ref; i++)
      {
        int ref = mesh.BoundaryRef(i).GetReference();
        if (mesh.GetBoundaryCondition(ref) == BoundaryConditionEnum::LINE_NEIGHBOR)
          {
            int num_elt2 = NumBoundary(15*i + 4);
            IVect Nodle = glob_mesh_num.Element(num_elt2).GetNodle();
            if (glob_mesh_num.number_map.FormulationDG())
              {
                Nodle.Resize(glob_mesh_num.GetNbLocalDof(num_elt2));
                for (int j = 1; j < Nodle.GetM(); j++)
                  Nodle(j) = Nodle(0) + j;
              }
            
            for (int j = 0; j < Nodle.GetM(); j++)
              NumDofNeighbor(nb+j) = Nodle(j);
                        
            nb += Nodle.GetM();

            if (glob_mesh_num.compute_dof_pml)
              {
                for (int j = 0; j < Nodle.GetM(); j++)
                  NumDofNeighbor(nb+j) = glob_mesh_num.GetDofPML(Nodle(j));
                
                nb += Nodle.GetM();
              }
          }
      }
    
    if (rank != proc)
      {
        comm.Ssend(NumDofNeighbor.GetData(), nb, MPI::INTEGER, proc, tag);    
        tag++;
      }

    // and elements
    NumElem.Reallocate(7*nb_elt + 4*nb_int_elt);
    nb = 0; int nb_dof = 0;
    for (int i = 0; i < nb_elt; i++)
      {
        NumElem(nb) = mesh.Element(i).GetNbVertices();
        NumElem(nb+1) = mesh.Element(i).GetReference();
        NumElem(nb+2) = mesh.Element(i).IsPML();
        int num_elem = NumElement(i);
        NumElem(nb+3) = glob_mesh_num.GetOrderElement(num_elem);
        NumElem(nb+4) = glob_mesh_num.GetOrderInside(num_elem);
        NumElem(nb+5) = NumElement(i);
        NumElem(nb+6) = glob_mesh_num.GetNbLocalDof(num_elem);
        nb_dof += NumElem(nb+6);
        nb += 7;
      }
        
    for (int i = 0; i < nb_elt; i++)
      {
        int ne = NumElement(i);
        int nv = mesh.Element(i).GetNbVertices();
        for (int j = 0; j < nv; j++)
          {
            NumElem(nb+j) = mesh.Element(i).numVertex(j); 
            NumElem(nb+nv+j) = glob_mesh.Element(ne).numVertex(j);
            
            int num_edge = glob_mesh.Element(ne).numEdge(j);
            NumElem(nb+2*nv+j) = num_edge;
            NumElem(nb+3*nv+j) = glob_mesh_num.GetOrderEdge(num_edge);
          }

        nb += 4*nv;
      }
    
    if (rank != proc)
      {
        comm.Ssend(NumElem.GetData(), nb, MPI::INTEGER, proc, tag);    
        tag++;
      }
        
    // then dofs
    if (!glob_mesh_num.number_map.FormulationDG())
      {
        NumDof.Reallocate(nb_dof + 2*nb_matching_proc + nb_matching_dofs);
        nb = 0 ;
        for (int i = 0; i < nb_elt; i++)
          {
            int ne = NumElement(i);
            IVect Nodle = glob_mesh_num.Element(ne).GetNodle();
            for (int j = 0; j < Nodle.GetM(); j++)
              NumDof(nb+j) = Nodle(j);
            
            nb += Nodle.GetM();
          }

        for (int p = 0; p < MatchingDofs.GetM(); p++)
          if (MatchingDofs(p).GetM() > 0)
            {
              NumDof(nb) = p;
              NumDof(nb+1) = MatchingDofs(p).GetM();
              for (int j = 0; j < MatchingDofs(p).GetM(); j++)
                NumDof(nb+2+j) = MatchingDofs(p)(j);
              
              nb += 2+MatchingDofs(p).GetM();
            }
        
        if (rank != proc)
          {
            comm.Ssend(NumDof.GetData(), nb, MPI::INTEGER, proc, tag);    
            tag++;
          }
        
        if (glob_mesh_num.compute_dof_pml)
          {
            // we send dofs for PML if needed
            NumDofPML.Reallocate(nb_dof);
            nb = 0;
            for (int i = 0; i < nb_elt; i++)
              {
                int ne = NumElement(i);
                for (int j = 0; j < glob_mesh_num.Element(ne).GetNbDof(); j++)
                  NumDofPML(nb+j) = glob_mesh_num.GetDofPML(NumDof(nb+j));
                
                nb += glob_mesh_num.Element(ne).GetNbDof();
              } 
            
            if (rank != proc)
              {
                comm.Ssend(NumDofPML.GetData(), nb, MPI::INTEGER, proc, tag);    
                tag++;
              }        
          }
      }
    else
      {
        NumDof.Reallocate(nb_elt);
        for (int i = 0; i < nb_elt; i++)
          {
            int ne = NumElement(i);
            NumDof(i) = glob_mesh_num.Element(ne).GetNumberDof(0);
          }
        
        if (rank != proc)
          {
            comm.Ssend(NumDof.GetData(), nb_elt, MPI::INTEGER, proc, tag);
            tag++;
          }

        if (glob_mesh_num.compute_dof_pml)
          {
            // we send dofs for PML if needed
            NumDofPML.Reallocate(nb_elt);
            for (int i = 0; i < nb_elt; i++)
              NumDofPML(i) = glob_mesh_num.GetDofPML(NumDof(i));
            
            if (rank != proc)
              {
                comm.Ssend(NumDofPML.GetData(), nb_elt, MPI::INTEGER, proc, tag);    
                tag++;
              }        
          }
      }
    
    // and periodic dofs
    if (nb_periodic_dof > 0)
      {
        // for each dof, sending periodic dof, original dof
        // processor of the original dof and type of periodicity
        PeriodicDof.Reallocate(4*nb_periodic_dof);
        TranslatPeriodicDof.Reallocate(nb_periodic_dof);
        nb_periodic_dof = 0;
        for (int i = 0; i < glob_mesh_num.GetNbPeriodicDof(); i++)
          {
            int num_dof = glob_mesh_num.GetPeriodicDof(i);
            if (MinimalProc(num_dof) < 0)
              {
                int num_orig = glob_mesh_num.GetOriginalPeriodicDof(i);
                PeriodicDof(4*nb_periodic_dof) = num_dof;
                PeriodicDof(4*nb_periodic_dof+1) = num_orig;
                PeriodicDof(4*nb_periodic_dof+2) = abs(MinimalProc(num_orig)) - 1;
                PeriodicDof(4*nb_periodic_dof+3) = glob_mesh_num.GetPeriodicityTypeForDof(i);
                TranslatPeriodicDof(nb_periodic_dof) = glob_mesh_num.GetTranslationPeriodicDof(i);
                nb_periodic_dof++;
              }
          }

        if (rank != proc)
          {
            comm.Ssend(PeriodicDof.GetData(), 4*nb_periodic_dof, MPI::INTEGER, proc, tag);
            tag++;

            MpiSsend(comm, reinterpret_cast<Real_wp*>(TranslatPeriodicDof.GetData()), all_param_tmp,
                     2*nb_periodic_dof, proc, tag);
            tag++;
          }
      }
    
    // and offsets for vectorial dofs
    if (OffsetDofV.GetM() > 0)
      {
        if (rank != proc)
          {
            comm.Ssend(OffsetDofV.GetData(), nb_elt, MPI::INTEGER, proc, tag);
            tag++;
          }
      }
  }


  void RecvMeshFromProcessor(Mesh<Dimension2>& mesh, MeshNumbering<Dimension2>& mesh_num,
			     Vector<IVect>& ConnecEdge, IVect& MatchingProc, Vector<IVect>& MatchingDofs,
                             bool dg_form, int proc, MPI::Comm& comm, int tag, int nodl_mesh,
                             IVect& num, VectReal_wp& all_param,
                             VectR2& PointsEdgeRef, IVect& NumBoundary,
                             IVect& NumDofNeighbor, IVect& NumElem, IVect& NumDof,
                             IVect& PeriodicDof, VectR2& TranslatPeriodicDof,
                             IVect& OffsetDofV, IVect& NumDofPML)
  {
    MPI::Status status;
    int rank = comm.Get_rank();
    
    if (mesh.print_level >= 7)
      cout << rank << " Receiving numbers of vertices, edges, etc " << endl;
    
    // receiving number of vertices, edges and elements
    if (rank != proc)
      {
        num.Reallocate(20);
        comm.Recv(num.GetData(), 20, MPI::INTEGER, proc, tag, status);    
        tag++;
      }
    
    Vector<int64_t> all_param_tmp;
    int nb_vertices = num(0);
    int nb_edges_ref = num(1);
    int nb_elt = num(2);
    int nb_ref = num(3);
    int nb_param = num(4);
    int nb_int_elt = num(5);
    mesh.SetGeometryOrder(num(6));
    mesh_num.SetOrder(num(7));
    int nb_matching_proc = num(8);
    int nb_matching_dofs = num(9);
    int nb_periodic_dof = num(10);
    int size_offset_v = num(11);
    mesh_num.compute_dof_pml = num(12);
    bool variable_order = false;
    if (num(13) != mesh_num.CONSTANT_ORDER)
      variable_order = true;
    
    if (nb_ref > mesh.GetNbReferences())
      mesh.ResizeNbReferences(nb_ref);
    
    if (mesh.print_level >= 7)
      cout << rank << " Receiving curves " << endl;
    
    // parameters of curves
    if (rank != proc)
      {
        all_param.Reallocate(nb_param);
        MpiRecv(comm, all_param, all_param_tmp, nb_param, proc, tag, status);
        tag++;
      }

    if (mesh.print_level >= 7)
      cout << rank << " Receiving curves completed" << endl;
    
    VectReal_wp param;
    IVect ref_neighbor(nb_ref+1); ref_neighbor.Fill(-3);
    nb_param = 5*nb_ref + 1;
    for (int i = 1; i <= nb_ref; i++)
      {
        int p0 = toInteger(all_param(i-1));
        int p1 = toInteger(all_param(i));
        
        mesh.SetBodyNumber(i, toInteger(all_param(nb_ref+i)));
        mesh.SetBoundaryCondition(i, toInteger(all_param(2*nb_ref+i)));
        mesh.SetCurveType(i, toInteger(all_param(3*nb_ref+i)));
        if (p1 > p0)
          {
            param.Reallocate(p1-p0);
            for (int p = p0; p < p1; p++)
              param(p-p0) = all_param(p);
            
            mesh.SetCurveParameter(i, param);
            nb_param += param.GetM();
          }        
        
        ref_neighbor(i) = toInteger(all_param(4*nb_ref+i));
      }
    
    Real_wp alpha = all_param(nb_param); nb_param++;
    mesh.SetPeriodicAlpha(alpha);
    Real_wp radius_pml = all_param(nb_param); nb_param++;
    mesh.SetRadiusPML(radius_pml);
    Real_wp x0_pml = all_param(nb_param), y0_pml = all_param(nb_param); nb_param += 2;
    mesh.SetOriginRadialPML(R2(x0_pml, y0_pml));
    
    mesh.SetOriginalNeighborReference(ref_neighbor);
    
    // receiving vertices and internal points of edges
    mesh.ReallocateBoundariesRef(nb_edges_ref);
    mesh.ReallocateVertices(nb_vertices);
    int r = mesh.GetGeometryOrder();
    if (mesh.print_level >= 7)
      cout << rank << " Receiving internal points of the mesh " << endl;
    
    if (rank != proc)
      {
        PointsEdgeRef.Reallocate(nb_vertices + (r-1)*nb_edges_ref);
        MpiRecv(comm, reinterpret_cast<Real_wp*>(PointsEdgeRef.GetData()), all_param_tmp,
                2*PointsEdgeRef.GetM(), proc, tag, status);
        tag++;
      }
    
    for (int i = 0; i < nb_vertices; i++)
      mesh.Vertex(i) = PointsEdgeRef(i);
    
    int nb = nb_vertices;
    if (r > 1)
      {
        for (int i = 0; i < nb_edges_ref; i++)
          for (int k = 0; k < r-1; k++)
            mesh.SetPointInsideEdge(i, k, PointsEdgeRef(nb++));
      }
    
    // receiving edge numbers
    PointsEdgeRef.Clear();
    ConnecEdge.Reallocate(nb_edges_ref);
    if (mesh.print_level >= 7)
      cout << rank << " Receiving referenced edges " << endl;
    
    if (rank != proc)
      {
        NumBoundary.Reallocate(15*nb_edges_ref);
        comm.Recv(NumBoundary.GetData(), NumBoundary.GetM(),
                  MPI::INTEGER, proc, tag, status);
        
        tag++;
      }
    
    IVect OrderQuad(nb_edges_ref);
    nb = 0; int nb_dof_neighbor = 0;
    for (int i = 0; i < nb_edges_ref; i++)
      {
        int n1 = NumBoundary(nb);
        int n2 = NumBoundary(nb+1);
        int ref = NumBoundary(nb+2);
        OrderQuad(i) = NumBoundary(nb+3);
        mesh.BoundaryRef(i).Init(n1, n2, ref);
        if (mesh.GetBoundaryCondition(ref) == BoundaryConditionEnum::LINE_NEIGHBOR)
          {
            int num_elt2 = NumBoundary(nb+4);
            int proc2 = NumBoundary(nb+5);
            int ne_loc2 = NumBoundary(nb+6);
            int num_loc2 = NumBoundary(nb+7);
            int rot = NumBoundary(nb+8);
            int re = NumBoundary(nb+9);
            int type_elt = NumBoundary(nb+10);
            int nb_dof = NumBoundary(nb+11);
            int iglob = NumBoundary(nb+12);
            int ref2 = NumBoundary(nb+14);
            ConnecEdge(i).Reallocate(12+nb_dof);
            ConnecEdge(i)(0) = iglob;
            ConnecEdge(i)(1) = num_elt2;
            ConnecEdge(i)(2) = proc2;
            ConnecEdge(i)(3) = ne_loc2;
            ConnecEdge(i)(4) = num_loc2;
            ConnecEdge(i)(5) = rot;
            ConnecEdge(i)(6) = re;
            ConnecEdge(i)(7) = type_elt;
            ConnecEdge(i)(8) = nb_dof;
            ConnecEdge(i)(9) = NumBoundary(nb+13);
            ConnecEdge(i)(10) = 0;
            ConnecEdge(i)(11) = ref2;
            nb_dof_neighbor += nb_dof;
          }
        else
          ConnecEdge(i).Clear();
        
        nb += 15;
      }

    if (mesh.print_level >= 7)
      cout << rank << " Receiving dof number of neighboring elements " << endl;
    
    // receiving dof numbers of neighboring elements
    if (rank != proc)
      {
        if (mesh_num.compute_dof_pml)
          NumDofNeighbor.Reallocate(2*nb_dof_neighbor);
        else
          NumDofNeighbor.Reallocate(nb_dof_neighbor);
        
        comm.Recv(NumDofNeighbor.GetData(),
                  NumDofNeighbor.GetM(), MPI::INTEGER, proc, tag, status);
        tag++;
      }
    
    nb = 0;
    for (int i = 0; i < nb_edges_ref; i++)
      {
        int ref = mesh.BoundaryRef(i).GetReference();
        if (mesh.GetBoundaryCondition(ref) == BoundaryConditionEnum::LINE_NEIGHBOR)
          {
            for (int j = 0; j < ConnecEdge(i)(8); j++)
              ConnecEdge(i)(11+j) =  NumDofNeighbor(nb+j);
            
            nb += ConnecEdge(i)(8);

            if (mesh_num.compute_dof_pml)
              {
                bool element_in_pml = false;
                for (int j = 0; j < ConnecEdge(i)(8); j++)
                  if (NumDofNeighbor(nb+j) >= 0)
                    element_in_pml = true;
                
                if (element_in_pml)
                  {
                    ConnecEdge(i).Resize(11+2*ConnecEdge(i)(8));
                    ConnecEdge(i)(10) = ConnecEdge(i)(8);
                    for (int j = 0; j < ConnecEdge(i)(8); j++)
                      ConnecEdge(i)(11+ConnecEdge(i)(8)+j) =  NumDofNeighbor(nb+j);
                  }
                
                nb += ConnecEdge(i)(8);
              }
          }
      }
    
    // receiving elements
    if (mesh.print_level >= 7)
      cout << rank << " Receiving elements " << endl;
    
    mesh.ReallocateElements(nb_elt);
    if (rank != proc)
      {
        NumElem.Reallocate(7*nb_elt + 4*nb_int_elt);
        comm.Recv(NumElem.GetData(), NumElem.GetM(),
                  MPI::INTEGER, proc, tag, status);
        
        tag++;
      }
    
    nb = 0;
    IVect OrderElt(nb_elt), OrderInside(nb_elt), NbDofElt(nb_elt);
    int offset = 7*nb_elt, nb_dof = 0;
    mesh.GlobElementNumber_Subdomain.Reallocate(nb_elt);
    mesh.GlobVertexNumber_Subdomain.Reallocate(nb_vertices);
    for (int i = 0; i < nb_elt; i++)
      {
        int nb_vert = NumElem(nb);
        int ref = NumElem(nb+1);
        bool pml = NumElem(nb+2);
        num.Reallocate(nb_vert);
        for (int j = 0; j < nb_vert; j++)
          {
            num(j) = NumElem(offset+j);
            mesh.GlobVertexNumber_Subdomain(num(j)) = NumElem(offset + nb_vert + j);
          }
        
        mesh.Element(i).Init(num, ref);
        if (pml)
          mesh.Element(i).SetPML();
        
        OrderElt(i) = NumElem(nb+3);
        OrderInside(i) = NumElem(nb+4);
        mesh.GlobElementNumber_Subdomain(i) = NumElem(nb+5);
        NbDofElt(i) = NumElem(nb+6);
        nb_dof += NbDofElt(i);
        
        nb += 7;
        offset += 4*nb_vert;
      }
    
    // updating connectivity
    mesh.ReorientElements();
    mesh.FindConnectivity();
    mesh.ProjectPointsOnCurves();
    
    if (variable_order)
      {
        mesh_num.SetVariableOrder(mesh_num.USER_ORDER);
        
        for (int i = 0; i < nb_elt; i++)
          mesh_num.SetOrderElement(i, OrderElt(i));
        
        nb = 7*nb_elt;
        for (int i = 0; i < nb_elt; i++)
          {
            mesh_num.SetOrderInside(i, OrderInside(i));
            int nb_vert = mesh.Element(i).GetNbEdges();
            for (int j = 0; j < nb_vert; j++)
              {
                int ne = mesh.Element(i).numEdge(j);
                mesh_num.SetOrderEdge(ne, NumElem(nb + 3*nb_vert + j));
              }
            
            nb += 4*nb_vert;
          }
        
        // order for referenced boundaries
        for (int i = 0; i < mesh.GetNbBoundaryRef(); i++)
          mesh_num.SetOrderQuadrature(i, OrderQuad(i));
        
        // order for internal boundaries
        for (int i = mesh.GetNbBoundaryRef(); i < mesh.GetNbBoundary(); i++)
          {
            int rf = 0;
            for (int k = 0; k < mesh.Boundary(i).GetNbElements(); k++)
              rf = max(rf, mesh_num.GetOrderElement(mesh.Boundary(i).numElement(k)));
            
            mesh_num.SetOrderQuadrature(i, rf);
          }        
      }
    
    nb = 7*nb_elt;
    mesh.GlobEdgeNumber_Subdomain.Reallocate(mesh.GetNbEdges());
    mesh.GlobEdgeNumber_Subdomain.Fill(-1);
    for (int i = 0; i < nb_elt; i++)
      {
        int nb_vert = mesh.Element(i).GetNbEdges();
        for (int j = 0; j < nb_vert; j++)
          {
            int ne = mesh.Element(i).numEdge(j);
            mesh.GlobEdgeNumber_Subdomain(ne) = NumElem(nb + 2*nb_vert + j);
          }
        
        nb += 4*nb_vert;
      }
    
    // retrieving dof numbers
    if (mesh.print_level >= 7)
      cout << rank << " Receiving dof numbers " << endl;
    
    if (dg_form)
      {
        if (rank != proc)
          {
            NumDof.Reallocate(nb_elt);
            comm.Recv(NumDof.GetData(), NumDof.GetM(),
                      MPI::INTEGER, proc, tag, status);
            
            tag++;
          }
        
        nb_dof = 0;
        for (int i = 0; i < nb_elt; i++)
          nb_dof += NbDofElt(i);
        
        mesh_num.GlobDofNumber_Subdomain.Reallocate(nb_dof);
        mesh_num.GlobDofNumber_Subdomain.Fill(-1);
        int offset = 0;
	mesh_num.ReallocateElements(nb_elt);
        for (int i = 0; i < nb_elt; i++)
          {
            nb = NumDof(i);
            mesh_num.Element(i).ReallocateDof(1);
            mesh_num.Element(i).SetNumberDof(0, offset);
            for (int j = 0; j < NbDofElt(i); j++)
              mesh_num.GlobDofNumber_Subdomain(offset+j) = nb+j;
            
            offset += NbDofElt(i);
          }
        
        mesh_num.SetNbDof(offset);

        if (mesh_num.compute_dof_pml)
          {
            IVect IndexDof(nb_elt); IndexDof.Fill(-1);
            if (rank != proc)
              {
                NumDofPML.Reallocate(nb_elt);
                comm.Recv(NumDofPML.GetData(), NumDofPML.GetM(),
                          MPI::INTEGER, proc, tag, status);
                
                tag++;
              }
            
            nb_dof = 0;
            for (int i = 0; i < nb_elt; i++)
              if (NumDofPML(i) >= 0)
                {
                  IndexDof(i) = nb_dof;
                  nb_dof += NbDofElt(i);
                }
            
            mesh_num.GlobDofPML_Subdomain.Reallocate(nb_dof);
            mesh_num.GlobDofPML_Subdomain.Fill(-1);
            mesh_num.ReallocateDofPML(nb_dof);
            nb = 0;
            for (int i = 0; i < nb_elt; i++)
              if (NumDofPML(i) >= 0)
                {
                  int npml = NumDofPML(i);
                  int n = mesh_num.Element(i).GetNumberDof(0);
                  for (int j = 0; j < NbDofElt(i); j++)
                    {
                      mesh_num.GlobDofPML_Subdomain(IndexDof(i) + j) = npml + j;
                      mesh_num.SetDofPML(n+j, IndexDof(i)+j);
                    }
                }
	  }
      }
    else
      {
        IVect IndexDof(nodl_mesh);
        IndexDof.Fill(-1);
        if (rank != proc)
          {
            NumDof.Reallocate(nb_dof + 2*nb_matching_proc + nb_matching_dofs);
            comm.Recv(NumDof.GetData(), NumDof.GetM(),
                      MPI::INTEGER, proc, tag, status);
            
            tag++;
          }
                
        nb = 0;
	mesh_num.ReallocateElements(nb_elt);
        for (int i = 0; i < nb_elt; i++)
          {
            mesh_num.Element(i).ReallocateDof(NbDofElt(i));
            
            for (int j = 0; j < NbDofElt(i); j++)
              {
                int n = NumDof(nb+j);
                mesh_num.Element(i).SetNumberDof(j, n);
                if (n >= 0)
                  IndexDof(n) = 1;
              }
            
            nb += NbDofElt(i);
          }
        
        MatchingDofs.Reallocate(nb_matching_proc);
        MatchingProc.Reallocate(nb_matching_proc);
        for (int p = 0; p < nb_matching_proc; p++)
          {
            MatchingProc(p) = NumDof(nb);
            MatchingDofs(p).Reallocate(NumDof(nb+1));
            for (int j = 0; j < MatchingDofs(p).GetM(); j++)
              MatchingDofs(p)(j) = NumDof(nb+2+j);   
            
            nb += 2+MatchingDofs(p).GetM();
          }
        
        int nb_dof_true = 0;
        for (int i = 0; i < IndexDof.GetM(); i++)
          if (IndexDof(i) > 0)
            IndexDof(i) = nb_dof_true++;
        
        mesh_num.SetNbDof(nb_dof_true);
        mesh_num.GlobDofNumber_Subdomain.Reallocate(nb_dof_true);
        mesh_num.GlobDofNumber_Subdomain.Fill(-1);
        for (int i = 0; i < nb_elt; i++)
          for (int j = 0; j < NbDofElt(i); j++)
            {
              int n = mesh_num.Element(i).GetNumberDof(j);
              if (n >= 0)
                {
                  mesh_num.GlobDofNumber_Subdomain(IndexDof(n)) = n;
                  mesh_num.Element(i).SetNumberDof(j, IndexDof(n));
                }
            }
                                        
        if (mesh_num.compute_dof_pml)
          {
            if (rank != proc)
              {
                NumDofPML.Reallocate(nb_dof);
                comm.Recv(NumDofPML.GetData(), NumDofPML.GetM(),
                          MPI::INTEGER, proc, tag, status);
                
                tag++;
              }

            IndexDof.Fill(-1);
            
            nb = 0;
            for (int i = 0; i < nb_elt; i++)
              {
                for (int j = 0; j < NbDofElt(i); j++)
                  {
                    int n = NumDofPML(nb+j);
                    if (n >= 0)
                      IndexDof(n) = 1;
                  }
                
                nb += NbDofElt(i);
              }
            
            nb_dof = 0;
            for (int i = 0; i < IndexDof.GetM(); i++)
              if (IndexDof(i) > 0)
                IndexDof(i) = nb_dof++;
           
            mesh_num.GlobDofPML_Subdomain.Reallocate(nb_dof);
            mesh_num.GlobDofPML_Subdomain.Fill(-1);
            mesh_num.ReallocateDofPML(nb_dof);
            nb = 0;
            for (int i = 0; i < nb_elt; i++)
              {                
                for (int j = 0; j < NbDofElt(i); j++)
                  {
                    int n = mesh_num.Element(i).GetNumberDof(j);
                    int npml = NumDofPML(nb+j);
                    if (npml>=0)
                      {
                        mesh_num.GlobDofPML_Subdomain(IndexDof(npml)) = npml;
                        mesh_num.SetDofPML(n, IndexDof(npml));
                      }
                  }                
                nb += NbDofElt(i);
              }
          }
      }
    
    // retrieving periodic dofs
    if (nb_periodic_dof > 0)
      {
        if (mesh.print_level >= 7)
          cout << rank << " Receiving periodic dof numbers " << endl;
    
        if (rank != proc)
          {
            PeriodicDof.Reallocate(4*nb_periodic_dof);
            comm.Recv(PeriodicDof.GetData(), PeriodicDof.GetM(),
                      MPI::INTEGER, proc, tag, status);
            
            tag++;
            
            TranslatPeriodicDof.Reallocate(nb_periodic_dof);
            MpiRecv(comm, reinterpret_cast<Real_wp*>(TranslatPeriodicDof.GetData()),
                    all_param_tmp, 2*nb_periodic_dof, proc, tag, status);
            
            tag++;
          }
        
        mesh_num.ReallocatePeriodicDof(nb_periodic_dof);
        IVect num_periodic(nb_periodic_dof), num_orig(nb_periodic_dof);
        for (int i = 0; i < mesh_num.GetNbPeriodicDof(); i++)
          {
            int nb = 4*i;
            mesh_num.SetPeriodicDof(i, PeriodicDof(nb));
            mesh_num.SetOriginalPeriodicDof(i, PeriodicDof(nb+1));
            mesh_num.SetProcOriginalPeriodicDof(i, PeriodicDof(nb+2));
            mesh_num.SetPeriodicityTypeForDof(i, PeriodicDof(nb+3));
            mesh_num.SetTranslationPeriodicDof(i, TranslatPeriodicDof(i));
            num_periodic(i) = PeriodicDof(nb);
            num_orig(i) = PeriodicDof(nb+1);
          }
        
        // now we replace global dofs numbers (returned by GetPeriodicDof)
        // by local dof number (in the current processor)
        IVect permut(nb_periodic_dof), permut_orig(nb_periodic_dof);
        IVect LocalDofNumber(nb_dof), GlobalDofNumber;
        LocalDofNumber.Fill(); permut.Fill(); permut_orig.Fill();
        GlobalDofNumber = mesh_num.GlobDofNumber_Subdomain;
        Sort(nb_dof, GlobalDofNumber, LocalDofNumber);
        Sort(nb_periodic_dof, num_periodic, permut);
        Sort(nb_periodic_dof, num_orig, permut_orig);
        
        int k = 0, k2 = 0;
        for (int i = 0; i < nb_periodic_dof; i++)
          {
            int n = num_periodic(i);
            while ((k < nb_dof) && (GlobalDofNumber(k) < n))
              k++;
            
            if ((k < nb_dof) && (GlobalDofNumber(k) == n))
              {
                mesh_num.SetPeriodicDof(permut(i), LocalDofNumber(k));
              }
            else
              {
                cout << "Problems when searching local dof numbers for periodic dofs " << endl;
                abort();
              }

            n = num_orig(i);
            int p = permut_orig(i);
            if (mesh_num.GetProcOriginalPeriodicDof(p) == rank)
              {
                while ((k2 < nb_dof) && (GlobalDofNumber(k2) < n))
                  k2++;
                
                if ((k2 < nb_dof) && (GlobalDofNumber(k2) == n))
                  {
                    mesh_num.SetOriginalPeriodicDof(p, LocalDofNumber(k2));
                  }
                else
                  {
                    cout << "Problems when searching local dof numbers for original dofs " << endl;
                    abort();
                  }
              }
          }
      }
    
    // receiving offsets for vectorial dofs
    if (size_offset_v > 0)
      {
        if (rank != proc)
          {
            OffsetDofV.Reallocate(nb_elt);
            comm.Recv(OffsetDofV.GetData(), nb_elt, MPI::INTEGER, proc, tag, status);
            tag++;
          }
      }
  }
  
   
#ifdef MONTJOIE_WITH_THREE_DIM
  //! sending a mesh to a processor
  void SendMeshToProcessor(const Mesh<Dimension3>& glob_mesh,
                           const MeshNumbering<Dimension3>& glob_mesh_num,
			   const IVect& NumElement, const IVect& Epart, const IVect& NumLoc,
                           Mesh<Dimension3>& mesh, int proc, const MPI::Comm& comm, int tag,
                           IVect& num, VectReal_wp& all_param, VectR3& PointsEdgeRef, IVect& NumBoundary,
                           IVect& NumDofNeighbor, IVect& NumElem, IVect& NumDof,
                           IVect& MinimalProc, IVect& PeriodicDof, VectR3& TranslatPeriodicDof,
                           IVect& OffsetDofV, IVect& NumDofPML)
  {    
    int rank = comm.Get_rank();
    
    int nb_ref = mesh.GetNbReferences();
    int nb_param = 5*nb_ref+2;
    VectReal_wp param;
    for (int i = 1; i <= nb_ref; i++)
      {
        mesh.GetCurveParameter(i, param);
        nb_param += param.GetM();
      }

    Vector<int64_t> all_param_tmp;    
    IVect ref_neighbor = mesh.GetOriginalNeighborReference();
    all_param.Reallocate(nb_param); all_param.Fill(0);    
    nb_param = 5*nb_ref+1;
    all_param(0) = nb_param;
    for (int i = 1; i <= mesh.GetNbReferences(); i++)
      {
        mesh.GetCurveParameter(i, param);
        for (int j = 0; j < param.GetM(); j++)
          all_param(nb_param+j) = param(j);
        
        nb_param += param.GetM();
        all_param(i) = nb_param;
        all_param(nb_ref+i) = mesh.GetBodyNumber(i);
        all_param(2*nb_ref+i) = mesh.GetBoundaryCondition(i);
        all_param(3*nb_ref+i) = mesh.GetCurveType(i);
        all_param(4*nb_ref+i) = ref_neighbor(i);
      }    
    
    all_param(nb_param) = glob_mesh.GetPeriodicAlpha(); nb_param++;
    
    // sending number of vertices, referenced edges and elements
    int nb_vertices = mesh.GetNbVertices();
    int nb_edges = mesh.GetNbEdges();
    int nb_edges_ref = mesh.GetNbEdgesRef();
    int nb_faces_ref = mesh.GetNbBoundaryRef();
    int nb_tri_ref = 0, nb_quad_ref = 0;
    for (int i = 0; i < nb_faces_ref; i++)
      {
	if (mesh.BoundaryRef(i).GetNbVertices() == 3)
	  nb_tri_ref++;
	else
	  nb_quad_ref++;
      }
    
    int nb_int_face = 3*nb_tri_ref + 4*nb_quad_ref;
    
    int nb_elt = mesh.GetNbElt();
    int nb_intE_elt = 0, nb_intV_elt = 0, nb_intF_elt = 0;
    for (int i = 0; i < nb_elt; i++)
      {
        nb_intV_elt += mesh.Element(i).GetNbVertices();
        nb_intE_elt += mesh.Element(i).GetNbEdges();
        nb_intF_elt += mesh.Element(i).GetNbFaces();
      }
        
    // counting dofs
    int nb_periodic_dof = 0, nb_mesh_dof = 0;
    int nb_matching_proc = 0, nb_matching_dofs = 0;
    Vector<IVect> MatchingDofs(comm.Get_size());
    if (!glob_mesh_num.number_map.FormulationDG())
      {
        // returning to positive numbers
        for (int i = 0; i < MinimalProc.GetM(); i++)
          MinimalProc(i) = abs(MinimalProc(i));
        
        // we put opposite signs in MinimalProc for dofs associated with the processor proc            
        for (int i = 0; i < nb_elt; i++)
          {
            int ne = NumElement(i);
            for (int j = 0; j < glob_mesh_num.Element(ne).GetNbDof(); j++)
              {
                int num_dof = glob_mesh_num.Element(ne).GetNumberDof(j);
                if ((num_dof >= 0) && (MinimalProc(num_dof) > 0))
                  {
                    MinimalProc(num_dof) = -MinimalProc(num_dof);
                    nb_mesh_dof++;
                  }
              }
          }

        // the number of dofs shared with other processors is estimated
        IVect nb_dofs_proc(comm.Get_size());
        nb_dofs_proc.Zero();
        for (int i = 0; i < glob_mesh.GetNbElt(); i++)
          for (int j = 0; j < glob_mesh_num.Element(i).GetNbDof(); j++)
            {
              int num_dof = glob_mesh_num.Element(i).GetNumberDof(j);
              if ((num_dof >= 0) && (MinimalProc(num_dof) < 0))
                nb_dofs_proc(Epart(i))++;
            }
        
        for (int p = 0; p < MatchingDofs.GetM(); p++)
          if (p != proc)
            MatchingDofs(p).Reallocate(nb_dofs_proc(p));
        
        nb_dofs_proc.Zero();
        for (int i = 0; i < glob_mesh.GetNbElt(); i++)
          {
            int p = Epart(i);
            if (p != proc)
              for (int j = 0; j < glob_mesh_num.Element(i).GetNbDof(); j++)
                {
                  int num_dof = glob_mesh_num.Element(i).GetNumberDof(j);
                  if ((num_dof >= 0) && (MinimalProc(num_dof) < 0))
                    {
                      MatchingDofs(p)(nb_dofs_proc(p)) = num_dof;
                      nb_dofs_proc(p)++;
                    }
                }
          }
        
        // duplicates are removed and numbers sorted
        for (int p = 0; p < MatchingDofs.GetM(); p++)
          if (MatchingDofs(p).GetM() > 0)
            {
              nb_matching_proc++;
              RemoveDuplicate(MatchingDofs(p));
              nb_matching_dofs += MatchingDofs(p).GetM();
            }
        
        if (glob_mesh_num.GetNbPeriodicDof() > 0)
          {
            // now counting periodic dofs on processor proc
            for (int i = 0; i < glob_mesh_num.GetNbPeriodicDof(); i++)
              {
                int num_dof = glob_mesh_num.GetPeriodicDof(i);
                if (MinimalProc(num_dof) < 0)
                  nb_periodic_dof++;
              }
          }
      }

    num.Reallocate(30);
    num.Fill(0);
    
    num(0) = nb_vertices;
    num(1) = nb_edges_ref;
    num(2) = nb_faces_ref;
    num(3) = nb_tri_ref;
    num(4) = nb_quad_ref;
    
    num(5) = nb_elt;
    num(6) = nb_intV_elt;    
    num(7) = nb_intE_elt;    
    num(8) = nb_intF_elt; 
    num(9) = nb_ref;
    num(10) = nb_param;
    
    num(11) = mesh.GetGeometryOrder();
    num(12) = glob_mesh_num.GetOrder();
    num(13) = nb_matching_proc;
    num(14) = nb_matching_dofs;
    num(15) = 0;
    num(16) = 0;
    num(17) = nb_edges;
    num(18) = nb_periodic_dof;
    num(19) = OffsetDofV.GetM();
    num(20) = glob_mesh_num.compute_dof_pml;
    num(21) = glob_mesh_num.GetVariableOrder();
    
    if (rank != proc)
      {
        comm.Ssend(num.GetData(), 30, MPI::INTEGER, proc, tag);
        tag++;
      }
    
    // sending parameters of curves
    if (rank != proc)
      {
        MpiSsend(comm, all_param, all_param_tmp, nb_param, proc, tag);
        tag++;
      }
    
    // sending vertices and internal points of edges and faces
    int r = mesh.GetGeometryOrder();
    PointsEdgeRef.Reallocate(nb_vertices + (r-1)*nb_edges_ref
                             + (r-1)*(r-1)*nb_quad_ref + (r-1)*(r-2)/2*nb_tri_ref);
    
    for (int i = 0; i < nb_vertices; i++)
      PointsEdgeRef(i) = mesh.Vertex(i);
    
    int nb = nb_vertices;
    if (r > 1)
      {
        for (int i = 0; i < nb_edges_ref; i++)
          for (int k = 0; k < r-1; k++)
            PointsEdgeRef(nb++) = mesh.GetPointInsideEdge(i, k);
	
	int nb_nodes = 0, nb_nodes_tri = (r-1)*(r-2)/2, nb_nodes_quad = (r-1)*(r-1);
	for (int i = 0; i < nb_faces_ref; i++)
	  {
	    if (mesh.BoundaryRef(i).GetNbVertices() == 3)
	      nb_nodes = nb_nodes_tri;
	    else
	      nb_nodes = nb_nodes_quad;
	    
	    for (int k = 0; k < nb_nodes; k++)
	      PointsEdgeRef(nb++) = mesh.GetPointInsideFace(i, k);
	  }	    
      }    
    
    if (rank != proc)
      {
        MpiSsend(comm, reinterpret_cast<Real_wp*>(PointsEdgeRef.GetData()),
                 all_param_tmp, 3*nb, proc, tag);
        tag++;
      }
    
    // faces
    NumBoundary.Reallocate(15*nb_faces_ref + nb_int_face + 3*nb_edges_ref);
    NumBoundary.Fill(-1);
    nb = 0;
    int nb_dof_neighbor = 0;
    for (int i = 0; i < nb_faces_ref; i++)
      {
	NumBoundary(nb) = mesh.BoundaryRef(i).GetNbVertices();
	NumBoundary(nb+1) = mesh.BoundaryRef(i).GetReference();

        int ne = mesh.BoundaryRef(i).numElement(0);
        int num_face = mesh.GetBoundaryFromBoundaryRef(i);
        int num_loc = -1;
        int num_elem = -1;
        if (ne >= 0)
          {
            num_loc = mesh.Element(ne).GetPositionBoundary(num_face);
            num_elem = NumElement(ne);
          }
        
        int iglob = 0;
        if (num_elem >= 0)
          iglob = glob_mesh.Element(num_elem).numBoundary(num_loc);
        
	NumBoundary(nb+2) = glob_mesh_num.GetOrderQuadrature(iglob);
        int ref = mesh.BoundaryRef(i).GetReference();
        if (mesh.GetBoundaryCondition(ref) == BoundaryConditionEnum::LINE_NEIGHBOR)
          {
            // referenced edge at the interface between subdomains
            int num_elt2 = glob_mesh.Boundary(iglob).numElement(0);            
            if (num_elt2 == num_elem)
              num_elt2 = glob_mesh.Boundary(iglob).numElement(1);
            
            //int num_loc2 = glob_mesh.Element(num_elt2).numEdge(num_loc);
            int num_loc2 = -1, rot = -1;
            glob_mesh_num.GetBoundaryRotation(iglob, num_elem, num_elt2, num_loc, num_loc2, rot);
            
            NumBoundary(nb+4) = num_elt2;
            NumBoundary(nb+5) = Epart(num_elt2);
            NumBoundary(nb+6) = NumLoc(num_elt2);
            NumBoundary(nb+7) = num_loc2;
            NumBoundary(nb+8) = rot;
            NumBoundary(nb+9) = glob_mesh_num.GetOrderElement(num_elt2);
            NumBoundary(nb+10) = glob_mesh.GetTypeElement(num_elt2);
            NumBoundary(nb+11) = glob_mesh_num.GetNbLocalDof(num_elt2);
            NumBoundary(nb+12) = iglob;
            NumBoundary(nb+13) = glob_mesh_num.GetPeriodicBoundary(iglob);
            NumBoundary(nb+14) = glob_mesh.Element(num_elt2).GetReference();
            nb_dof_neighbor += glob_mesh_num.GetNbLocalDof(num_elt2);
          }
        
        nb += 15;
      }
    
    for (int i = 0; i < nb_faces_ref; i++)
      {
        for (int j = 0; j < mesh.BoundaryRef(i).GetNbVertices(); j++)
          NumBoundary(nb+j) = mesh.BoundaryRef(i).numVertex(j); 
	
        nb += mesh.BoundaryRef(i).GetNbVertices();
      }

    for (int i = 0; i < nb_edges_ref; i++)
      {
        NumBoundary(nb) = mesh.EdgeRef(i).numVertex(0);
        NumBoundary(nb+1) = mesh.EdgeRef(i).numVertex(1);
        NumBoundary(nb+2) = mesh.EdgeRef(i).GetReference();
	nb += 3;
      }
    
    if (rank != proc)
      {
        comm.Ssend(NumBoundary.GetData(), nb, MPI::INTEGER, proc, tag);
        tag++;
      }
    
    // dofs associated with neighboring elements
    if (glob_mesh_num.compute_dof_pml)
      NumDofNeighbor.Reallocate(2*nb_dof_neighbor);
    else
      NumDofNeighbor.Reallocate(nb_dof_neighbor);
    
    nb = 0;
    for (int i = 0; i < nb_faces_ref; i++)
      {
        int ref = mesh.BoundaryRef(i).GetReference();
        if (mesh.GetBoundaryCondition(ref) == BoundaryConditionEnum::LINE_NEIGHBOR)
          {
            int num_elt2 = NumBoundary(15*i + 4);
            IVect Nodle = glob_mesh_num.Element(num_elt2).GetNodle();
            if (glob_mesh_num.number_map.FormulationDG())
              {
                Nodle.Resize(glob_mesh_num.GetNbLocalDof(num_elt2));
                for (int j = 1; j < Nodle.GetM(); j++)
                  Nodle(j) = Nodle(0) + j;
              }
            
            for (int j = 0; j < Nodle.GetM(); j++)
              NumDofNeighbor(nb+j) = Nodle(j);
            
            nb += Nodle.GetM();

            if (glob_mesh_num.compute_dof_pml)
              {
                for (int j = 0; j < Nodle.GetM(); j++)
                  NumDofNeighbor(nb+j) = glob_mesh_num.GetDofPML(Nodle(j));
                
                nb += Nodle.GetM();
              }
          }
      }
    
    if (rank != proc)
      {
        comm.Ssend(NumDofNeighbor.GetData(), nb, MPI::INTEGER, proc, tag);    
        tag++;
      }

    // and elements
    NumElem.Reallocate(7*nb_elt + 2*nb_intV_elt + 2*nb_intE_elt + 2*nb_intF_elt);
    nb = 0; int nb_dof = 0;
    for (int i = 0; i < nb_elt; i++)
      {
        NumElem(nb) = mesh.Element(i).GetNbVertices();
        NumElem(nb+1) = mesh.Element(i).GetReference();
        NumElem(nb+2) = mesh.Element(i).IsPML();
        int ne = NumElement(i);
        NumElem(nb+3) = glob_mesh_num.GetOrderElement(ne);
        NumElem(nb+4) = glob_mesh_num.GetOrderInside(ne);
        NumElem(nb+5) = ne;
        NumElem(nb+6) = glob_mesh_num.GetNbLocalDof(ne);
        nb_dof += NumElem(nb+6);        
        nb += 7;
      }
    
    if (mesh.print_level >= 7)
      cout << " we send " << nb_dof << " degrees of freedom " << endl;
    
    for (int i = 0; i < nb_elt; i++)
      {
        int ne = NumElement(i);
        int nv = mesh.Element(i).GetNbVertices();
        for (int j = 0; j < nv; j++)
          {
            NumElem(nb+j) = mesh.Element(i).numVertex(j); 
            NumElem(nb+nv+j) = glob_mesh.Element(ne).numVertex(j);
          }
        
        nb += 2*nv;
        
        nv = mesh.Element(i).GetNbEdges();
        for (int j = 0; j < nv; j++)
          {
            int num_edge = glob_mesh.Element(ne).numEdge(j);
            NumElem(nb+j) = num_edge;
            NumElem(nb+nv+j) = glob_mesh_num.GetOrderEdge(num_edge);
          }
        
        nb += 2*nv;
        
        nv = mesh.Element(i).GetNbFaces();
        for (int j = 0; j < nv; j++)
          {
            int num_face = glob_mesh.Element(ne).numFace(j);
            NumElem(nb+j) = num_face;
            NumElem(nb+nv+j) = glob_mesh_num.GetOrderFace(num_face);
          }
        
        nb += 2*nv;
      }
    
    if (rank != proc)
      {
        comm.Ssend(NumElem.GetData(), nb, MPI::INTEGER, proc, tag);    
        tag++;
      }
    
    // then dofs
    if (!glob_mesh_num.number_map.FormulationDG())
      {
        if (mesh.print_level >= 7)
          cout << " we send " << nb_dof << "degrees of freedom to processor " << proc << endl;
        
        NumDof.Reallocate(nb_dof + 2*nb_matching_proc + nb_matching_dofs);
        nb = 0 ;
        for (int i = 0; i < nb_elt; i++)
          {
            int ne = NumElement(i);
            IVect Nodle = glob_mesh_num.Element(ne).GetNodle();
            for (int j = 0; j < Nodle.GetM(); j++)
              NumDof(nb+j) = Nodle(j);
            
            nb += Nodle.GetM();
          }

        for (int p = 0; p < MatchingDofs.GetM(); p++)
          if (MatchingDofs(p).GetM() > 0)
            {
              NumDof(nb) = p;
              NumDof(nb+1) = MatchingDofs(p).GetM();
              for (int j = 0; j < MatchingDofs(p).GetM(); j++)
                NumDof(nb+2+j) = MatchingDofs(p)(j);
              
              nb += 2+MatchingDofs(p).GetM();
            }
                
        if (rank != proc)
          {
            comm.Ssend(NumDof.GetData(), nb, MPI::INTEGER, proc, tag);    
            tag++;
          }

        if (glob_mesh_num.compute_dof_pml)
          {
            // we send dofs for PML if needed
            NumDofPML.Reallocate(nb_dof);
            nb = 0;
            for (int i = 0; i < nb_elt; i++)
              {
                int ne = NumElement(i);
                for (int j = 0; j < glob_mesh_num.Element(ne).GetNbDof(); j++)
                  NumDofPML(nb+j) = glob_mesh_num.GetDofPML(NumDof(nb+j));
                
                nb += glob_mesh_num.Element(ne).GetNbDof();
              } 
            
            if (rank != proc)
              {
                comm.Ssend(NumDofPML.GetData(), nb, MPI::INTEGER, proc, tag);    
                tag++;
              }        
          }
      }
    else
      {
        NumDof.Reallocate(nb_elt);
        for (int i = 0; i < nb_elt; i++)
          {
            int ne = NumElement(i);
            NumDof(i) = glob_mesh_num.Element(ne).GetNumberDof(0);
          }
        
        if (rank != proc)
          {
            comm.Ssend(NumDof.GetData(), nb_elt, MPI::INTEGER, proc, tag);
            tag++;
          }

        if (glob_mesh_num.compute_dof_pml)
          {
            // we send dofs for PML if needed
            NumDofPML.Reallocate(nb_elt);
            for (int i = 0; i < nb_elt; i++)
              NumDofPML(i) = glob_mesh_num.GetDofPML(NumDof(i));
            
            if (rank != proc)
              {
                comm.Ssend(NumDofPML.GetData(), nb_elt, MPI::INTEGER, proc, tag);    
                tag++;
              }        
          }
      }
    
    // and periodic dofs
    if (nb_periodic_dof > 0)
      {
        // for each dof, sending periodic dof, original dof
        // processor of the original dof and type of periodicity
        PeriodicDof.Reallocate(4*nb_periodic_dof);
        TranslatPeriodicDof.Reallocate(nb_periodic_dof);
        nb_periodic_dof = 0;
        for (int i = 0; i < glob_mesh_num.GetNbPeriodicDof(); i++)
          {
            int num_dof = glob_mesh_num.GetPeriodicDof(i);
            if (MinimalProc(num_dof) < 0)
              {
                int num_orig = glob_mesh_num.GetOriginalPeriodicDof(i);
                PeriodicDof(4*nb_periodic_dof) = num_dof;
                PeriodicDof(4*nb_periodic_dof+1) = num_orig;
                PeriodicDof(4*nb_periodic_dof+2) = abs(MinimalProc(num_orig)) - 1;
                PeriodicDof(4*nb_periodic_dof+3) = glob_mesh_num.GetPeriodicityTypeForDof(i);
                TranslatPeriodicDof(nb_periodic_dof) = glob_mesh_num.GetTranslationPeriodicDof(i);
                nb_periodic_dof++;
              }
          }

        if (rank != proc)
          {
            comm.Ssend(PeriodicDof.GetData(), 4*nb_periodic_dof, MPI::INTEGER, proc, tag);
            tag++;

            MpiSsend(comm, reinterpret_cast<Real_wp*>(TranslatPeriodicDof.GetData()), all_param_tmp,
                     3*nb_periodic_dof, proc, tag);
            tag++;
          }
      }

    // and offsets for vectorial dofs
    if (OffsetDofV.GetM() > 0)
      {
        if (rank != proc)
          {
            comm.Ssend(OffsetDofV.GetData(), nb_elt, MPI::INTEGER, proc, tag);
            tag++;
          }
      }
  }


  void RecvMeshFromProcessor(Mesh<Dimension3>& mesh, MeshNumbering<Dimension3>& mesh_num,
			     Vector<IVect>& ConnecFace, IVect& MatchingProc, Vector<IVect>& MatchingDofs,
                             bool dg_form, int proc, MPI::Comm& comm, int tag, int nodl_mesh,
                             IVect& num, VectReal_wp& all_param,
                             VectR3& PointsEdgeRef, IVect& NumBoundary, IVect& NumDofNeighbor,
                             IVect& NumElem, IVect& NumDof,
                             IVect& PeriodicDof, VectR3& TranslatPeriodicDof,
                             IVect& OffsetDofV, IVect& NumDofPML)
  {
    MPI::Status status;
    int rank = comm.Get_rank();

    if (mesh.print_level >= 7)
      cout << rank << " Receiving numbers of vertices, edges, etc " << endl;
    
    // receiving number of vertices, edges and elements
    if (rank != proc)
      {
        num.Reallocate(30);
        comm.Recv(num.GetData(), 30, MPI::INTEGER, proc, tag, status);    
        tag++;
      }

    Vector<int64_t> all_param_tmp;    
    int nb_vertices = num(0);
    int nb_edges_ref = num(1);
    int nb_faces_ref = num(2);
    int nb_tri_ref = num(3);
    int nb_quad_ref = num(4);
    int nb_int_face = 3*nb_tri_ref + 4*nb_quad_ref;

    int nb_elt = num(5);
    int nb_intV_elt = num(6);
    int nb_intE_elt = num(7);
    int nb_intF_elt = num(8);
    int nb_ref = num(9);
    int nb_param = num(10);
    
    int order_geom = num(11);
    int order = num(12);
    int nb_matching_proc = num(13);
    int nb_matching_dofs = num(14);
    //int nb_edges = num(17);
    int nb_periodic_dof = num(18);
    int size_offset_v = num(19);
    mesh_num.compute_dof_pml = num(20);
    bool variable_order = false;
    if (num(21) != mesh_num.CONSTANT_ORDER)
      variable_order = true;
        
    mesh.SetGeometryOrder(order_geom);
    mesh_num.SetOrder(order);
    
    if (nb_ref > mesh.GetNbReferences())
      mesh.ResizeNbReferences(nb_ref);
    
    if (mesh.print_level >= 7)
      cout << rank << " Receiving curves " << endl;
    
    // parameters of curves
    if (rank != proc)
      {
        all_param.Reallocate(nb_param);
        MpiRecv(comm, all_param, all_param_tmp, nb_param, proc, tag, status);
        tag++;
      }

    if (mesh.print_level >= 7)
      cout << rank << " Receiving curves completed" << endl;
    
    VectReal_wp param;
    IVect ref_neighbor(nb_ref+1); ref_neighbor.Fill(-3);
    nb_param = 5*nb_ref + 1;
    for (int i = 1; i <= nb_ref; i++)
      {
        int p0 = toInteger(all_param(i-1));
        int p1 = toInteger(all_param(i));
        
        mesh.SetBodyNumber(i, toInteger(all_param(nb_ref+i)));
        mesh.SetBoundaryCondition(i, toInteger(all_param(2*nb_ref+i)));
        mesh.SetCurveType(i, toInteger(all_param(3*nb_ref+i)));
        if (p1 > p0)
          {
            param.Reallocate(p1-p0);
            for (int p = p0; p < p1; p++)
              param(p-p0) = all_param(p);
            
            mesh.SetCurveParameter(i, param);
          }
        
        ref_neighbor(i) = toInteger(all_param(4*nb_ref+i));
      }

    Real_wp alpha = all_param(nb_param); nb_param++;
    mesh.SetPeriodicAlpha(alpha);
    
    mesh.SetOriginalNeighborReference(ref_neighbor);
    
    // receiving vertices and internal points of edges
    mesh.ReallocateEdgesRef(nb_edges_ref);
    mesh.ReallocateBoundariesRef(nb_faces_ref);
    mesh.ReallocateVertices(nb_vertices);
    int r = mesh.GetGeometryOrder();
    int nb_nodes_tri = (r-1)*(r-2)/2, nb_nodes_quad = (r-1)*(r-1);
    if (mesh.print_level >= 7)
      cout << rank << " Receiving internal points of the mesh " << endl;
    
    if (rank != proc)
      {
        PointsEdgeRef.Reallocate(nb_vertices + (r-1)*nb_edges_ref
                                 + (r-1)*(r-1)*nb_quad_ref + (r-1)*(r-2)/2*nb_tri_ref);
        
        MpiRecv(comm, reinterpret_cast<Real_wp*>(PointsEdgeRef.GetData()), all_param_tmp,
                3*PointsEdgeRef.GetM(), proc, tag, status);
        tag++;
      }
    
    for (int i = 0; i < nb_vertices; i++)
      mesh.Vertex(i) = PointsEdgeRef(i);
    
    int nb = nb_vertices;
    if (r > 1)
      {
        for (int i = 0; i < nb_edges_ref; i++)
          for (int k = 0; k < r-1; k++)
            mesh.SetPointInsideEdge(i, k, PointsEdgeRef(nb++));
      }
    
    int nbf = nb;
    
    // receiving connectivity of faces
    if (mesh.print_level >= 7)
      cout << rank << " Receiving referenced faces " << endl;

    ConnecFace.Reallocate(nb_faces_ref);
    if (rank != proc)
      {
        NumBoundary.Reallocate(15*nb_faces_ref + nb_int_face + 3*nb_edges_ref);
        comm.Recv(NumBoundary.GetData(), NumBoundary.GetM(),
                  MPI::INTEGER, proc, tag, status);
        
        tag++;
      }
    
    nb = 0;
    int offset = nb + 15*nb_faces_ref, nb_dof_neighbor = 0;
    IVect OrderQuad(nb_faces_ref); VectR3 Pts;
    for (int i = 0; i < nb_faces_ref; i++)
      {
	int nb_vert = NumBoundary(nb);
	int ref = NumBoundary(nb+1);
	int rf = NumBoundary(nb+2);
	num.Reallocate(nb_vert);
	for (int j = 0; j < nb_vert; j++)
          num(j) = NumBoundary(offset+j);
	
       	mesh.BoundaryRef(i).Init(num, ref);
	OrderQuad(i) = rf;
	int nb_nodes = nb_nodes_quad;
	if (nb_vert == 3)
	  nb_nodes = nb_nodes_tri;
	
	Pts.Reallocate(nb_nodes);
	for (int k = 0; k < nb_nodes; k++)
	  Pts(k) = PointsEdgeRef(nbf + k);
	
	mesh.SetPointInsideFace(i, Pts);
	
        if (mesh.GetBoundaryCondition(ref) == BoundaryConditionEnum::LINE_NEIGHBOR)
          {
            int num_elt2 = NumBoundary(nb+4);
            int proc2 = NumBoundary(nb+5);
            int ne_loc2 = NumBoundary(nb+6);
            int num_loc2 = NumBoundary(nb+7);
            int rot = NumBoundary(nb+8);
            int re = NumBoundary(nb+9);
            int type_elt = NumBoundary(nb+10);
            int nb_dof = NumBoundary(nb+11);
            int iglob = NumBoundary(nb+12);
            int ref2 = NumBoundary(nb+14);
            
            ConnecFace(i).Reallocate(11+nb_dof);
            ConnecFace(i)(0) = iglob;
            ConnecFace(i)(1) = num_elt2;
            ConnecFace(i)(2) = proc2;
            ConnecFace(i)(3) = ne_loc2;
            ConnecFace(i)(4) = num_loc2;
            ConnecFace(i)(5) = rot;
            ConnecFace(i)(6) = re;
            ConnecFace(i)(7) = type_elt;
            ConnecFace(i)(8) = nb_dof;
            ConnecFace(i)(9) = NumBoundary(nb+13);
            ConnecFace(i)(10) = 0;
            ConnecFace(i)(11) = ref2;
            nb_dof_neighbor += nb_dof;
          }
        else
          ConnecFace(i).Clear();
                
	nbf += nb_nodes;
	nb += 15;
	offset += nb_vert;
      }
    
    nb = offset;
    for (int i = 0; i < nb_edges_ref; i++)
      {
	mesh.EdgeRef(i).Init(NumBoundary(nb), NumBoundary(nb+1), NumBoundary(nb+2));
	nb += 3;
      }
    
    PointsEdgeRef.Clear();
    
    // receiving dof numbers of neighboring elements
    if (mesh.print_level >= 7)
      cout << rank << " Receiving dof number of neighboring elements " << endl;
    
    if (rank != proc)
      {
        if (mesh_num.compute_dof_pml)
          NumDofNeighbor.Reallocate(2*nb_dof_neighbor);
        else
          NumDofNeighbor.Reallocate(nb_dof_neighbor);
        
        comm.Recv(NumDofNeighbor.GetData(),
                  NumDofNeighbor.GetM(), MPI::INTEGER, proc, tag, status);
        tag++;
      }
    
    nb = 0;
    for (int i = 0; i < nb_faces_ref; i++)
      {
        int ref = mesh.BoundaryRef(i).GetReference();
        if (mesh.GetBoundaryCondition(ref) == BoundaryConditionEnum::LINE_NEIGHBOR)
          {
            for (int j = 0; j < ConnecFace(i)(8); j++)
              ConnecFace(i)(11+j) =  NumDofNeighbor(nb+j);
            
            nb += ConnecFace(i)(8);
            if (mesh_num.compute_dof_pml)
              {
                bool element_in_pml = false;
                for (int j = 0; j < ConnecFace(i)(8); j++)
                  if (NumDofNeighbor(nb+j) >= 0)
                    element_in_pml = true;
                
                if (element_in_pml)
                  {
                    ConnecFace(i).Resize(11+2*ConnecFace(i)(8));
                    ConnecFace(i)(10) = ConnecFace(i)(8);
                    for (int j = 0; j < ConnecFace(i)(8); j++)
                      ConnecFace(i)(11+ConnecFace(i)(8)+j) =  NumDofNeighbor(nb+j);
                  }
                
                nb += ConnecFace(i)(8);
              }
          }
      }
    
    // elements  
    if (mesh.print_level >= 7)
      cout << rank << " Receiving elements " << endl;
    
    mesh.ReallocateElements(nb_elt);
    if (rank != proc)
      {
        NumElem.Reallocate(7*nb_elt + 2*nb_intV_elt + 2*nb_intE_elt + 2*nb_intF_elt);
        comm.Recv(NumElem.GetData(), NumElem.GetM(),
                  MPI::INTEGER, proc, tag, status);
        
        tag++;
      }
    
    nb = 0;
    IVect OrderElt(nb_elt), OrderInside(nb_elt), NbDofElt(nb_elt);
    offset = 7*nb_elt; int nb_dof = 0;
    mesh.GlobElementNumber_Subdomain.Reallocate(nb_elt);
    mesh.GlobVertexNumber_Subdomain.Reallocate(nb_vertices);
    int rmax = 1;
    for (int i = 0; i < nb_elt; i++)
      {
        int nb_vert = NumElem(nb);
        int ref = NumElem(nb+1);
        bool pml = NumElem(nb+2);
	int order = NumElem(nb+3);
        num.Reallocate(nb_vert);
        for (int j = 0; j < nb_vert; j++)
          {
            num(j) = NumElem(offset+j);
            mesh.GlobVertexNumber_Subdomain(num(j)) = NumElem(offset + nb_vert + j);
          }
        
        mesh.Element(i).Init(num, ref);
        if (pml)
          mesh.Element(i).SetPML();
        
	OrderElt(i) = order;
        OrderInside(i) = NumElem(nb+4);
        rmax = max(rmax, OrderElt(i));
        
        mesh.GlobElementNumber_Subdomain(i) = NumElem(nb+5);
        NbDofElt(i) = NumElem(nb+6);
        nb_dof += NbDofElt(i);
        
        nb += 7;
        offset += 2*nb_vert + 2*mesh.Element(i).GetNbEdges() + 2*mesh.Element(i).GetNbFaces();
      }
    
    if (rmax != mesh_num.GetOrder())
      mesh_num.SetOrder(rmax);
    
    // updating connectivity
    mesh.ReorientElements();
    mesh.FindConnectivity();
    mesh.ProjectPointsOnCurves();
    
    if (variable_order)
      {
        mesh_num.SetVariableOrder(mesh_num.USER_ORDER);
        
        for (int i = 0; i < nb_elt; i++)
          mesh_num.SetOrderElement(i, OrderElt(i));
        
        nb = 7*nb_elt;
        for (int i = 0; i < nb_elt; i++)
          {
            mesh_num.SetOrderInside(i, OrderInside(i));
            
            nb += 2*mesh.Element(i).GetNbVertices();
            int nv = mesh.Element(i).GetNbEdges();
            for (int j = 0; j < nv; j++)
              {
                int ne = mesh.Element(i).numEdge(j);
                mesh_num.SetOrderEdge(ne, NumElem(nb + nv + j));
              }
            
            nb += 2*nv;
            nv = mesh.Element(i).GetNbFaces();
            for (int j = 0; j < nv; j++)
              {
                int ne = mesh.Element(i).numFace(j);
                mesh_num.SetOrderFace(ne, NumElem(nb + nv + j));
              }
            
            nb += 2*nv;
          }
        
        for (int i = 0; i < nb_faces_ref; i++)
          mesh_num.SetOrderQuadrature(i, OrderQuad(i));
        
        // order for intern boundary
        for (int i = nb_faces_ref; i < mesh.GetNbBoundary(); i++)
          {
            int rf = 0;
            for (int k = 0; k < mesh.Boundary(i).GetNbElements(); k++)
              {
                if (mesh.Boundary(i).numElement(k) >= 0)
                  rf = max(rf, OrderElt(mesh.Boundary(i).numElement(k)));
              }
            
            mesh_num.SetOrderQuadrature(i, rf);
          }
      }
    
    nb = 7*nb_elt;
    mesh.GlobEdgeNumber_Subdomain.Reallocate(mesh.GetNbEdges());
    mesh.GlobFaceNumber_Subdomain.Reallocate(mesh.GetNbFaces());
    mesh.GlobEdgeNumber_Subdomain.Fill(-1);
    mesh.GlobFaceNumber_Subdomain.Fill(-1);
    for (int i = 0; i < nb_elt; i++)
      {
        nb += 2*mesh.Element(i).GetNbVertices();
        int nv = mesh.Element(i).GetNbEdges();
        for (int j = 0; j < nv; j++)
          {
            int ne = mesh.Element(i).numEdge(j);
            mesh.GlobEdgeNumber_Subdomain(ne) = NumElem(nb + j);
          }
        
        nb += 2*nv;
        nv = mesh.Element(i).GetNbFaces();
        for (int j = 0; j < nv; j++)
          {
            int ne = mesh.Element(i).numFace(j);
            mesh.GlobFaceNumber_Subdomain(ne) = NumElem(nb + j);
          }
        
        nb += 2*nv;
      }
    
    // retrieving dof numbers
    if (mesh.print_level >= 7)
      cout << rank << " Receiving dof numbers " << endl;
    
    if (dg_form)
      {
        if (rank != proc)
          {
            NumDof.Reallocate(nb_elt);
            comm.Recv(NumDof.GetData(), NumDof.GetM(),
                      MPI::INTEGER, proc, tag, status);
            
            tag++;
          }
        
        nb_dof = 0;
        for (int i = 0; i < nb_elt; i++)
          nb_dof += NbDofElt(i);
        
        mesh_num.GlobDofNumber_Subdomain.Reallocate(nb_dof);
        mesh_num.GlobDofNumber_Subdomain.Fill(-1);
        offset = 0;
	mesh_num.ReallocateElements(nb_elt);
        for (int i = 0; i < nb_elt; i++)
          {
            nb = NumDof(i);
	    mesh_num.Element(i).ReallocateDof(1);
	    mesh_num.Element(i).SetNumberDof(0, offset);
            for (int j = 0; j < NbDofElt(i); j++)
              mesh_num.GlobDofNumber_Subdomain(offset+j) = nb+j;
            
            offset += NbDofElt(i);
          }
	
	mesh_num.SetNbDof(offset);
        if (mesh_num.compute_dof_pml)
          {
            IVect IndexDof(nb_elt); IndexDof.Fill(-1);
            if (rank != proc)
              {
                NumDofPML.Reallocate(nb_elt);
                comm.Recv(NumDofPML.GetData(), NumDofPML.GetM(),
                          MPI::INTEGER, proc, tag, status);
                
                tag++;
              }
            
            nb_dof = 0;
            for (int i = 0; i < nb_elt; i++)
              if (NumDofPML(i) >= 0)
                {
                  IndexDof(i) = nb_dof;
                  nb_dof += NbDofElt(i);
                }
            
            mesh_num.GlobDofPML_Subdomain.Reallocate(nb_dof);
            mesh_num.GlobDofPML_Subdomain.Fill(-1);
            mesh_num.ReallocateDofPML(nb_dof);
            nb = 0;
            for (int i = 0; i < nb_elt; i++)
              if (NumDofPML(i) >= 0)
                {
                  int npml = NumDofPML(i);
                  int n = mesh_num.Element(i).GetNumberDof(0);
                  for (int j = 0; j < NbDofElt(i); j++)
                    {
                      mesh_num.GlobDofPML_Subdomain(IndexDof(i) + j) = npml + j;
                      mesh_num.SetDofPML(n+j, IndexDof(i)+j);
                    }
                }                
          }
      }
    else
      {
        IVect IndexDof(nodl_mesh);
        IndexDof.Fill(-1);
        
        if (rank != proc)
          {
            NumDof.Reallocate(nb_dof+2*nb_matching_proc + nb_matching_dofs);
            comm.Recv(NumDof.GetData(), NumDof.GetM(),
                      MPI::INTEGER, proc, tag, status);
            
            tag++;
          }
                
        nb = 0;
	mesh_num.ReallocateElements(nb_elt);
        for (int i = 0; i < nb_elt; i++)
          {
            mesh_num.Element(i).ReallocateDof(NbDofElt(i));
            for (int j = 0; j < NbDofElt(i); j++)
              {
                int n = NumDof(nb+j);
                mesh_num.Element(i).SetNumberDof(j, n);
                if (n >= 0)
                  IndexDof(n) = 1;
              }
            
            nb += NbDofElt(i);
          }
        
        MatchingDofs.Reallocate(nb_matching_proc);
        MatchingProc.Reallocate(nb_matching_proc);
        for (int p = 0; p < nb_matching_proc; p++)
          {
            MatchingProc(p) = NumDof(nb);
            MatchingDofs(p).Reallocate(NumDof(nb+1));
            for (int j = 0; j < MatchingDofs(p).GetM(); j++)
              MatchingDofs(p)(j) = NumDof(nb+2+j);   
            
            nb += 2+MatchingDofs(p).GetM();
          }
        
        int nb_dof_true = 0;
        for (int i = 0; i < IndexDof.GetM(); i++)
          if (IndexDof(i) > 0)
            IndexDof(i) = nb_dof_true++;
        
        mesh_num.SetNbDof(nb_dof_true);
        mesh_num.GlobDofNumber_Subdomain.Reallocate(nb_dof_true);
        mesh_num.GlobDofNumber_Subdomain.Fill(-1);
        for (int i = 0; i < nb_elt; i++)
          for (int j = 0; j < NbDofElt(i); j++)
            {
              int n = mesh_num.Element(i).GetNumberDof(j);
              if (n >= 0)
                {
                  mesh_num.GlobDofNumber_Subdomain(IndexDof(n)) = n;
                  mesh_num.Element(i).SetNumberDof(j, IndexDof(n));
                }
            }
        
        if (mesh_num.compute_dof_pml)
          {
            if (rank != proc)
              {
                NumDofPML.Reallocate(nb_dof);
                comm.Recv(NumDofPML.GetData(), NumDofPML.GetM(),
                          MPI::INTEGER, proc, tag, status);
                
                tag++;
              }
            
            IndexDof.Fill(-1);
            
            nb = 0;
            for (int i = 0; i < nb_elt; i++)
              {
                for (int j = 0; j < NbDofElt(i); j++)
                  {
                    int n = NumDofPML(nb+j);
                    if (n >= 0)
                      IndexDof(n) = 1;
                  }
                
                nb += NbDofElt(i);
              }
            
            nb_dof = 0;
            for (int i = 0; i < IndexDof.GetM(); i++)
              if (IndexDof(i) > 0)
                IndexDof(i) = nb_dof++;
            
            mesh_num.GlobDofPML_Subdomain.Reallocate(nb_dof);
            mesh_num.GlobDofPML_Subdomain.Fill(-1);
            mesh_num.ReallocateDofPML(nb_dof);
            nb = 0;
            for (int i = 0; i < nb_elt; i++)
              {                
                for (int j = 0; j < NbDofElt(i); j++)
                  {
                    int n = mesh_num.Element(i).GetNumberDof(j);
                    int npml = NumDofPML(nb+j);
                    if (npml>=0)
                      {
                        mesh_num.GlobDofPML_Subdomain(IndexDof(npml)) = npml;
                        mesh_num.SetDofPML(n, IndexDof(npml));
                      }
                  }                
                nb += NbDofElt(i);
              }
          }        
      }
    
    // retrieving periodic dofs
    if (nb_periodic_dof > 0)
      {
        if (mesh.print_level >= 7)
          cout << rank << " Receiving periodic dof numbers " << endl;
    
        if (rank != proc)
          {
            PeriodicDof.Reallocate(4*nb_periodic_dof);
            comm.Recv(PeriodicDof.GetData(), PeriodicDof.GetM(),
                      MPI::INTEGER, proc, tag, status);
            
            tag++;
            
            TranslatPeriodicDof.Reallocate(nb_periodic_dof);
            MpiRecv(comm, reinterpret_cast<Real_wp*>(TranslatPeriodicDof.GetData()),
                    all_param_tmp, 3*nb_periodic_dof, proc, tag, status);
            
            tag++;
          }
        
        mesh_num.ReallocatePeriodicDof(nb_periodic_dof);
        IVect num_periodic(nb_periodic_dof), num_orig(nb_periodic_dof);
        for (int i = 0; i < mesh_num.GetNbPeriodicDof(); i++)
          {
            int nb = 4*i;
            mesh_num.SetPeriodicDof(i, PeriodicDof(nb));
            mesh_num.SetOriginalPeriodicDof(i, PeriodicDof(nb+1));
            mesh_num.SetProcOriginalPeriodicDof(i, PeriodicDof(nb+2));
            mesh_num.SetPeriodicityTypeForDof(i, PeriodicDof(nb+3));
            mesh_num.SetTranslationPeriodicDof(i, TranslatPeriodicDof(i));
            num_periodic(i) = PeriodicDof(nb);
            num_orig(i) = PeriodicDof(nb+1);
          }
        
        // now we replace global dofs numbers (returned by GetPeriodicDof) 
        // by local dof number (in the current processor)
        IVect permut(nb_periodic_dof), permut_orig(nb_periodic_dof);
        IVect LocalDofNumber(nb_dof), GlobalDofNumber;
        LocalDofNumber.Fill(); permut.Fill(); permut_orig.Fill();
        GlobalDofNumber = mesh_num.GlobDofNumber_Subdomain;
        Sort(nb_dof, GlobalDofNumber, LocalDofNumber);
        Sort(nb_periodic_dof, num_periodic, permut);
        Sort(nb_periodic_dof, num_orig, permut_orig);
        
        int k = 0, k2 = 0;
        for (int i = 0; i < nb_periodic_dof; i++)
          {
            int n = num_periodic(i);
            while ((k < nb_dof) && (GlobalDofNumber(k) < n))
              k++;
            
            if ((k < nb_dof) && (GlobalDofNumber(k) == n))
              {
                mesh_num.SetPeriodicDof(permut(i), LocalDofNumber(k));
              }
            else
              {
                cout << "Problems when searching local dof numbers for periodic dofs " << endl;
                abort();
              }

            n = num_orig(i);
            int p = permut_orig(i);
            if (mesh_num.GetProcOriginalPeriodicDof(p) == rank)
              {
                while ((k2 < nb_dof) && (GlobalDofNumber(k2) < n))
                  k2++;
                
                if ((k2 < nb_dof) && (GlobalDofNumber(k2) == n))
                  {
                    mesh_num.SetOriginalPeriodicDof(p, LocalDofNumber(k2));
                  }
                else
                  {
                    cout << "Problems when searching local dof numbers for original dofs " << endl;
                    abort();
                  }
              }
          }
      }

    // receiving offsets for vectorial dofs
    if (size_offset_v > 0)
      {
        if (rank != proc)
          {
            OffsetDofV.Reallocate(nb_elt);
            comm.Recv(OffsetDofV.GetData(), nb_elt, MPI::INTEGER, proc, tag, status);
            tag++;
          }
      }
    
    if (mesh.print_level >= 7)
      cout << rank << " Receiving completed " << endl;    
  }
#endif

}

#define MONTJOIE_FILE_PARALLEL_MESH_FUNCTIONS_CXX
#endif
