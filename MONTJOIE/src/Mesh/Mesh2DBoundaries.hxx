#ifndef MONTJOIE_FILE_MESH2D_BOUNDARIES_HXX

namespace Montjoie
{

  //! class for curved boundaries of 2-D meshes
  template<class T>
  class MeshBoundaries<Dimension2, T> : public MeshBoundaries_Base<Dimension2, T>
  {
  public :
    typedef TinyVector<T, 2> R_N;
    typedef Vector<R_N> VectR_N;
    
  protected :
    Matrix<R_N> PointsEdgeRef; //!< projection of interior points of curved edges
    //! value of parameter (for example teta for a circle) on each boundary
    Vector<R_N> value_parameter_boundary;
    //! case where parameter is an integer
    Vector<TinyVector<int, 2> > integer_parameter_boundary;
    EdgeLobatto interval_reference;

  public :
    // type of 2-D curves
    enum {NO_CURVE, CURVE_CIRCLE, CURVE_SPLINE,
	  CURVE_ELLIPSE, CURVE_PEANUT, CURVE_LOCAL_SPLINE, CURVE_FILE};
    
    MeshBoundaries();

    
    /************************
     * Convenient functions *
     ************************/
    
    
    int64_t GetMemorySize() const;
    int GetBoundaryRefFromBoundary(int) const;
    int GetBoundaryFromBoundaryRef(int) const;
    int GetEdgeRefFromEdge(int) const;
    int GetEdgeFromEdgeRef(int) const;
    bool IsBoundaryCurved(int) const;
    const R_N& GetPointInsideEdge(int, int) const;
    void SetPointInsideEdge(int, int, const R_N& );
    
    int FindFollowingVertex(int n1, int n2, int ref, const IVect&, int& e) const;
    int FindFollowingVertex(int n1, int n2, int ref,
                            const IVect&, const Vector<bool>&, int& e) const;
    
    const EdgeLobatto& GetSurfaceFiniteElement() const;
    const EdgeLobatto& GetSurfaceFiniteElement2() const;
    
    T GetAverageLengthOnBoundary() const;
    // is the point M on the edge num_glob_edge_ref ?
    bool IsPointOnBoundaryRef(int num_glob_edge_ref, const R_N& M) const;
    // all the references of the boundaries close to a vertex
    void GetAllReferencesAroundPoint(int num_point, IVect& ref) const;
    // get a point near num_point, and belonging 
    // to an edge with different reference from ref
    int GetPointWithOtherReference(int num_point, int ref, R_N&, int&) const;
    
    void ClearConnectivity();
    void ClearCurves();
    void Clear();
    
    /**************************************
     * Treatment of referenced boundaries *
     **************************************/
    
    void RemoveReference(int ref);
    void SortPeriodicBoundaries();
    void SortBoundariesRef();
    void ConstructCrossReferenceBoundaryRef(bool check_mesh = true);
    void AddBoundaryEdges();
    
    void ProjectPointsOnCurves();
    void RedistributeReferences();
    
    
    /**********************************
     * Treatment of curved boundaries *
     **********************************/
    
    
    void CheckCurveParameter(int ref);
    void ReadCurveType(const IVect& ref, const string& description);
    void ProjectToCurve(R_N& ptM, int ref) const;
    void ComputeParameterValueOnCurves();
    void GetPointsOnCurve(int i, int ref, const VectReal_wp& x, Vector<R_N>& points) const;
    
    void GetAllPointsOnReference(int ref, VectR_N& Points, VectR_N& Normale) const;
    void GetAllPointsOnReference(int ref, const IVect& ref_cond,
                                 VectR_N& Points, VectR_N& Normale) const;
    void GetDistantPointsOnReference(int ref, VectR_N& Points,
                                     VectR_N& Normale, int nb_points) const;
    void GetDistantPointsOnReference(int ref, const IVect& ref_cond,
                                     VectR_N& Points, VectR_N& Normale, int nb_points) const;
    
    void FindParametersPlane(int ref, Vector<T>& coef_plane) const;
    void FindParametersSpline(int ref, Vector<T>& coef, IVect& Point_curve);
    void FindParametersCircle(int ref, T& xc, T& yc, T& radius) const;
    void FindParametersEllipse(int ref, T& xc, T& yc, T& a, T& b) const;
    void FindParametersPeanut(int ref, T& xc, T& yc, T& a, T&b, T& c) const;
    void FindParametersCurve();

    
    /*****************
     * Other methods *
     *****************/
    

    TinyVector<T, 2> GetCenterRadialPML(T& Rmax) const;
    
    // regular layers are added on the external boundaries of the mesh
    void AddPMLElements(int nb_div);
    
    void GetBoundaryMesh(int ref, SurfacicMesh<Dimension2>& mesh,
                         const IVect& ref_cond, int ref_domain = 0) const;
    
    template<class Mesh2D_>
    void GetBoundaryMesh(int ref, Mesh2D_& mesh_surf, IVect& Index_EdgeSurf_to_EdgeRef,
			 IVect& Liste_Vertices, IVect&, IVect&,
                         const IVect& ref_cond, int ref_domain = 0) const;

#ifdef MONTJOIE_WITH_THREE_DIM
    void GenerateSurfaceOfRevolution(int ref, Mesh<Dimension3>& mesh_boundary_inside,
				     IVect& Index_FaceSurf_to_EdgeRef,
				     IVect& Index_VertexSurf_to_Vertex,
				     Vector<T>& AngleVertex, const IVect& ref_cond) const;  
    
#endif
    
  };

} // namespace Montjoie

#define MONTJOIE_FILE_MESH2D_BOUNDARIES_HXX
#endif

