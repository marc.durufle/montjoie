#ifndef MONTJOIE_FILE_PARALLEL_MESH_FUNCTIONS_HXX

namespace Montjoie
{

  void SendMeshToProcessor(const Mesh<Dimension2>& glob_mesh,
                           const MeshNumbering<Dimension2>& glob_mesh_num,
			   const IVect& NumElement, const IVect& Epart, const IVect& NumLoc,
                           Mesh<Dimension2>& mesh, int proc, const MPI::Comm& comm, int tag,
                           IVect& num, VectReal_wp& all_param, VectR2& PointsEdgeRef, IVect& NumBoundary,
                           IVect& NumDofNeighbor, IVect& NumElem, IVect& NumDof,
                           IVect& MinimalProc, IVect& PeriodicDof, VectR2& TranslatPeriodicDof,
                           IVect& OffsetDofV, IVect& NumDofPML);
  
  void RecvMeshFromProcessor(Mesh<Dimension2>& mesh, MeshNumbering<Dimension2>& mesh_num,
			     Vector<IVect>& ConnecEdge, IVect& MatchingProc, Vector<IVect>& MatchingDofs,
                             bool dg_form, int proc, MPI::Comm& comm, int tag, int nodl_mesh,
                             IVect& num, VectReal_wp& all_param,
                             VectR2& PointsEdgeRef, IVect& NumBoundary,
                             IVect& NumDofNeighbor, IVect& NumElem, IVect& NumDof,
                             IVect& PeriodicDof, VectR2& TranslatPeriodicDof,
                             IVect& OffsetDofV, IVect& NumDofPML);
  
#ifdef MONTJOIE_WITH_THREE_DIM
  void SendMeshToProcessor(const Mesh<Dimension3>& glob_mesh,
                           const MeshNumbering<Dimension3>& glob_mesh_num,
			   const IVect& NumElement, const IVect& Epart, const IVect& NumLoc,
                           Mesh<Dimension3>& mesh, int proc, const MPI::Comm& comm, int tag,
                           IVect& num, VectReal_wp& all_param, VectR3& PointsEdgeRef, IVect& NumBoundary,
                           IVect& NumDofNeighbor, IVect& NumElem, IVect& NumDof,
                           IVect& MinimalProc, IVect& PeriodicDof, VectR3& TranslatPeriodicDof,
                           IVect& OffsetDofV, IVect& NumDofPML);

  void RecvMeshFromProcessor(Mesh<Dimension3>& mesh, MeshNumbering<Dimension3>& mesh_num,
			     Vector<IVect>& ConnecFace, IVect& MatchingProc, Vector<IVect>& MatchingDofs,
                             bool dg_form, int proc, MPI::Comm& comm, int tag, int nodl_mesh,
                             IVect& num, VectReal_wp& all_param,
                             VectR3& PointsEdgeRef, IVect& NumBoundary, IVect& NumDofNeighbor,
                             IVect& NumElem, IVect& NumDof,
                             IVect& PeriodicDof, VectR3& TranslatPeriodicDof,
                             IVect& OffsetDofV, IVect& NumDofPML);
#endif

}

#define MONTJOIE_FILE_PARALLEL_MESH_FUNCTIONS_HXX
#endif
