#ifndef MONTJOIE_FILE_NUMBER_MAP_INLINE_CXX

namespace Montjoie
{
  
  //! default constructor
  inline NumberMap::NumberMap()
  {
    only_hexahedral = false;
    dg_element = false;
    nb_dof_vertex = 0;
  }
  
  
  //! returns the number of dofs per vertex
  inline int NumberMap::GetNbDofVertex(int order) const
  {
    return nb_dof_vertex;
  }
  
  
  //! returns the number of dofs per edge for a given order
  inline int NumberMap::GetNbDofEdge(int order) const
  {
    if (nb_dof_edge.GetM() > order)
      return nb_dof_edge(order);
    
    return 0;
  }
  
  
  //! returns the number of dofs per triangle for a given order
  inline int NumberMap::GetNbDofTriangle(int order) const
  {
    if (nb_dof_tri.GetM() > order)
      return nb_dof_tri(order);
    
    return 0;
  }
  
  
  //! returns the number of dofs per quadrangle for a given order
  inline int NumberMap::GetNbDofQuadrangle(int order) const
  {
    if (nb_dof_quad.GetM() > order)
      return nb_dof_quad(order);
    
    return 0;
  }
  
  
  //! returns the number of dofs per tetrahedron for a given order
  inline int NumberMap::GetNbDofTetrahedron(int order) const
  {
    if (nb_dof_tetra.GetM() > order)
      return nb_dof_tetra(order);
    
    return 0;
  }
  
  
  //! returns the number of dofs per pyramid for a given order
  inline int NumberMap::GetNbDofPyramid(int order) const
  {
    if (nb_dof_pyramid.GetM() > order)
      return nb_dof_pyramid(order);
    
    return 0;
  }
  
  
  //! returns the number of dofs per prism for a given order
  inline int NumberMap::GetNbDofWedge(int order) const
  {
    if (nb_dof_wedge.GetM() > order)
      return nb_dof_wedge(order);
    
    return 0;
  }
  
  
  //! returns the number of dofs per hexahedron for a given order
  inline int NumberMap::GetNbDofHexahedron(int order) const
  {
    if (nb_dof_hexa.GetM() > order)
      return nb_dof_hexa(order);
    
    return 0;
  }
  

  //! returns the dof number after a symmetry of the edge
  inline int NumberMap::GetSymmetricEdgeDof(int order, int k) const
  {
    if (EdgesDof_Symmetry.GetM() > order)
      return EdgesDof_Symmetry(order)(k);
    
    return 0;
  }
  
  
  //! returns true if the dof is skew-symmetric 
  //! (i.e. the sign depends on the orientation of the edge)
  inline bool NumberMap::IsSkewSymmetricEdgeDof(int r, int k) const
  {
    return EdgesDof_SkewSymmetry(r)(k);
  }
  
  
  //! returns the number of dofs strictly inside the element
  template<class Dimension>
  inline int NumberMap::GetNbDofElement(int order, const Face<Dimension>& elt) const
  {
    int nv = elt.GetNbVertices();
    if (nv == 3)
      {
	if (nb_dof_tri.GetM() > order)
	  return nb_dof_tri(order);
	
	return 0;
      }
    
    if (nb_dof_quad.GetM() > order)
      return nb_dof_quad(order);
    
    return 0;
  }
  
  
  //! returns the number of dofs strictly inside the element
  inline int NumberMap::GetNbDofElement(int order, const Volume& elt) const
  {
    int nv = elt.GetNbVertices();
    if (nv == 4)
      {
	if (nb_dof_tetra.GetM() > order)
	  return nb_dof_tetra(order);
	
	return 0;
      }
    else if (nv == 5)
      {
	if (nb_dof_pyramid.GetM() > order)
	  return nb_dof_pyramid(order);
	
	return 0;
      }
    if (nv == 6)
      {
	if (nb_dof_wedge.GetM() > order)
	  return nb_dof_wedge(order);
	
	return 0;
      }
    
    if (nb_dof_hexa.GetM() > order)
      return nb_dof_hexa(order);
    
    return 0;
  }

  
  //! returns the number of quadrature points on an edge
  inline int NumberMap::GetNbPointsQuadBoundary(int order, const Edge<Dimension2>& elt) const
  {
    return points_quadrature_edge(order).GetM();
  }
  
  
  //! returns the number of quadrature points on a face
  inline int NumberMap::GetNbPointsQuadBoundary(int order, const Face<Dimension3>& elt) const
  {
    int nv = elt.GetNbVertices();
    if (nv == 4)
      return points_quadrature_quad(order).GetM();
    
    return points_quadrature_tri(order).GetM();
  }

  
  //! returns local position of a quadrature point of an edge
  inline Real_wp NumberMap::GetQuadraturePoint(int rf, int k, const Edge<Dimension2>&) const
  {
    return points_quadrature_edge(rf)(k);
  }
  
#ifdef MONTJOIE_WITH_THREE_DIM
  //! returns local position of a quadrature point of a face
  inline const R2& NumberMap
  ::GetQuadraturePoint(int rf, int k, const Face<Dimension3>& face) const
  {
    if (face.GetNbVertices() == 3)
      return points_quadrature_tri(rf)(k);
    
    return points_quadrature_quad(rf)(k);
  }
#endif
  
  
  //! returns quadrature points used for the unit interval
  inline const VectReal_wp& NumberMap::GetEdgeQuadrature(int order) const
  {
    return points_quadrature_edge(order);
  }
  
  
  //! returns quadrature points for the unit triangle
  inline const VectR2& NumberMap::GetTriangleQuadrature(int order) const
  {
    return points_quadrature_tri(order);
  }
  
  
  //! returns quadrature points for the unit square
  inline const VectR2& NumberMap::GetQuadrangleQuadrature(int order) const
  {
    return points_quadrature_quad(order);
  }

  
  //! sets the number of dofs per vertex
  inline void NumberMap::SetNbDofVertex(int order, int nb_dof)
  {
    nb_dof_vertex = nb_dof;
  }
  
  
  //! sets the number of dofs per edge for a given order
  inline void NumberMap::SetNbDofEdge(int order, int nb_dof)
  {
    if (nb_dof_edge.GetM() <= order)
      {
	int m = nb_dof_edge.GetM();
	nb_dof_edge.Resize(order+1);
	for (int i = m; i <= order; i++)
	  nb_dof_edge(i) = 0;
      }
    
    nb_dof_edge(order) = nb_dof;
  }
  

  //! sets the number of dofs per triangle for a given order  
  inline void NumberMap::SetNbDofTriangle(int order, int nb_dof)
  {
    if (nb_dof_tri.GetM() <= order)
      {
	int m = nb_dof_tri.GetM();
	nb_dof_tri.Resize(order+1);
	for (int i = m; i <= order; i++)
	  nb_dof_tri(i) = 0;
      }
    
    nb_dof_tri(order) = nb_dof;
  }
  
  
  //! sets the number of dofs per quadrangle for a given order
  inline void NumberMap::SetNbDofQuadrangle(int order, int nb_dof)
  {
    if (nb_dof_quad.GetM() <= order)
      {
	int m = nb_dof_quad.GetM();
	nb_dof_quad.Resize(order+1);
	for (int i = m; i <= order; i++)
	  nb_dof_quad(i) = 0;
      }
    
    nb_dof_quad(order) = nb_dof;
  }
  
  
  //! sets the number of dofs per tetrahedron for a given order
  inline void NumberMap::SetNbDofTetrahedron(int order, int nb_dof)
  {
    if (nb_dof_tetra.GetM() <= order)
      {
	int m = nb_dof_tetra.GetM();
	nb_dof_tetra.Resize(order+1);
	for (int i = m; i <= order; i++)
	  nb_dof_tetra(i) = 0;
      }
    
    nb_dof_tetra(order) = nb_dof;
  }
  
  
  //! sets the number of dofs per pyramid for a given order
  inline void NumberMap::SetNbDofPyramid(int order, int nb_dof)
  {
    if (nb_dof_pyramid.GetM() <= order)
      {
	int m = nb_dof_pyramid.GetM();
	nb_dof_pyramid.Resize(order+1);
	for (int i = m; i <= order; i++)
	  nb_dof_pyramid(i) = 0;
      }
    
    nb_dof_pyramid(order) = nb_dof;
  }
  
  
  //! sets the number of dofs per prism for a given order
  inline void NumberMap::SetNbDofWedge(int order, int nb_dof)
  {
    if (nb_dof_wedge.GetM() <= order)
      {
	int m = nb_dof_wedge.GetM();
	nb_dof_wedge.Resize(order+1);
	for (int i = m; i <= order; i++)
	  nb_dof_wedge(i) = 0;
      }
    
    nb_dof_wedge(order) = nb_dof;
  }
  
  
  //! sets the number of dofs per hexahedron for a given order
  inline void NumberMap::SetNbDofHexahedron(int order, int nb_dof)
  {
    if (nb_dof_hexa.GetM() <= order)
      {
	int m = nb_dof_hexa.GetM();
	nb_dof_hexa.Resize(order+1);
	for (int i = m; i <= order; i++)
	  nb_dof_hexa(i) = 0;
      }
    
    nb_dof_hexa(order) = nb_dof;
  }
  
  
  //! sets the quadrature points to use for a triangular face
  inline void NumberMap::SetPointsQuadratureTriangle(int order, const VectR2& xi)
  {
    if (points_quadrature_tri.GetM() <= order)
      {
        points_quadrature_tri.Resize(order+1);
        FacesQuad_Rotation_Tri.Resize(order+1);
      }
    
    points_quadrature_tri(order) = xi;
#ifdef MONTJOIE_WITH_THREE_DIM
    MeshNumbering<Dimension3>::
      GetRotationTriangularFace(xi, FacesQuad_Rotation_Tri(order));
#endif
  }
  
  
  //! sets the quadrature points to use for a quadrangular face
  inline void NumberMap::SetPointsQuadratureQuadrangle(int order, const VectR2& xi)
  {
    if (points_quadrature_quad.GetM() <= order)
      {
        points_quadrature_quad.Resize(order+1);
        FacesQuad_Rotation_Quad.Resize(order+1);
      }
        
    points_quadrature_quad(order) = xi;
#ifdef MONTJOIE_WITH_THREE_DIM
    MeshNumbering<Dimension3>::
      GetRotationQuadrilateralFace(xi, FacesQuad_Rotation_Quad(order));
#endif
  }

  
  //! sets the half of quadrature weights to use for an edge
  inline void NumberMap::SetFluxWeightEdge(int order, const VectReal_wp& PoidsFlux)
  {
    if (half_weight_edge.GetM() <= order)
      half_weight_edge.Resize(order+1);
    
    half_weight_edge(order) = PoidsFlux;
  }
  
  
  //! returns the half of quadrature weights to use for an edge
  inline const VectReal_wp& NumberMap
  ::GetFluxWeight(int order, const Edge<Dimension2>& elt) const
  {
    return half_weight_edge(order);
  }
  
  
  //! sets the half of quadrature weights to use for a triangle
  inline void NumberMap::SetFluxWeightTriangle(int order, const VectReal_wp& PoidsFlux)
  {
    if (half_weight_tri.GetM() <= order)
      half_weight_tri.Resize(order+1);
    
    half_weight_tri(order) = PoidsFlux;
  }
  
  
  //! sets the half of quadrature weights to use for a quadrangle
  inline void NumberMap::SetFluxWeightQuadrangle(int order, const VectReal_wp& PoidsFlux)
  {
    if (half_weight_quad.GetM() <= order)
      half_weight_quad.Resize(order+1);
    
    half_weight_quad(order) = PoidsFlux;
  }
  

  //! returns the half of quadrature weights to use for a face
  inline const VectReal_wp& NumberMap
  ::GetFluxWeight(int order, const Face<Dimension3>& elt) const
  {
    if (elt.GetNbVertices() == 3)
      return half_weight_tri(order);
    
    return half_weight_quad(order);
  }
  
  
  //! returns true if these numbering rules are intended to Discontinuous Galerkin
  inline bool NumberMap::FormulationDG() const
  {
    return dg_element;
  }
  
  
  //! if dg_elt is true, these numbering rules are intended to Discontinuous Galerkin
  inline void NumberMap::SetFormulationDG(bool dg_elt)
  {
    dg_element = dg_elt;
  }
  
  
  //! sets the new dof number (and its sign) after symmetry of dof i
  inline void NumberMap::SetEdgesDofSymmetry(int r, int i, int j, bool skew_sym)
  {
    EdgesDof_Symmetry(r)(i) = j;
    EdgesDof_SkewSymmetry(r)(i) = skew_sym;
  }
  
  
  //! All dofs are skew-symmetric (signs are changed after symmetry)
  inline void NumberMap::SetAllEdgesDofToSkewSymmetric(int r)
  {
    EdgesDof_SkewSymmetry(r).Fill(true); 
  }
  
  
  //! Odd dofs are skew-symmetric
  inline void NumberMap::SetOddEdgesDofToSkewSymmetric(int r)
  {
    for (int i = 0; i < EdgesDof_SkewSymmetry(r).GetM(); i++)
      EdgesDof_SkewSymmetry(r)(i) = (i%2 == 1); 
  }

  
  //! Even dofs are skew-symmetric
  inline void NumberMap::SetEvenEdgesDofToSkewSymmetric(int r)
  {
    for (int i = 0; i < EdgesDof_SkewSymmetry(r).GetM(); i++)
      EdgesDof_SkewSymmetry(r)(i) = (i%2 == 0); 
  } 
  
  
  //! Provides direclty signs of dofs after symmetry
  inline void NumberMap::SetSkewSymmetryEdgesDof(int r, const Vector<bool>& skew)
  {
    EdgesDof_SkewSymmetry(r) = skew;
  }
  
  
  //! returns true if the dofs of the face are invariant by rotation
  inline bool NumberMap::DofInvariantByRotation(int order, const Face<Dimension3>& f) const
  {
    if (f.GetNbVertices() == 3)
      return !general_combination_tri(order);
    else
      return !general_combination_quad(order);
  }
  
  
  //! returns linear operator to apply to dofs of a triangular face after rotation
  inline const Vector<Matrix<Real_wp, General, RowSparse> >& NumberMap
  ::GetFacesDofRotationTri(int order) const
  {
    return coef_combination_tri(order);
  }
  
  
  //! provides dof changes of sign after rotation of a triangular face  
  inline void NumberMap::SetSignDofRotationTri(int order, const Matrix<bool>& is_neg)
  {
    SignDof_Rotation_Tri(order) = is_neg;
  }


  //! provides dof changes of sign after rotation of a quadrilateral face  
  inline void NumberMap::SetSignDofRotationQuad(int order, const Matrix<bool>& is_neg)
  {
    SignDof_Rotation_Quad(order) = is_neg;
  }


  //! returns the dof number after rotation of the face
  inline int NumberMap::GetRotationFaceDof(int rot, int r, int k, const Face<Dimension3>& f) const
  {
    if (f.GetNbVertices() == 3)
      return FacesDof_Rotation_Tri(r)(rot, k);
    
    return FacesDof_Rotation_Quad(r)(rot, k);
  }
  

  //! returns the dof number after rotation of the edge
  inline int NumberMap::GetRotationFaceDof(int rot, int r, int k, const Edge<Dimension2>& f) const
  {
    if (rot == 0)
      return k;
    
    return EdgesDof_Symmetry(r)(k);
  }
  
  
  //! returns quadrature points numbers after rotation of the face
  inline const Matrix<int>& NumberMap::
  GetRotationQuadraturePoints(int r, const Face<Dimension3>& f) const
  {
    if (f.GetNbVertices() == 3)
      return FacesQuad_Rotation_Tri(r);
    
    return FacesQuad_Rotation_Quad(r);
  }


  //! returns quadrature points numbers after rotation of the face
  inline const Matrix<int>& NumberMap::
  GetRotationQuadraturePoints(int r, int nb_vertices) const
  {
    if (nb_vertices == 3)
      return FacesQuad_Rotation_Tri(r);
    
    return FacesQuad_Rotation_Quad(r);
  }
  

  //! returns quadrature points numbers after rotation of the edge
  inline const Matrix<int>& NumberMap::
  GetRotationQuadraturePoints(int r, const Edge<Dimension2>& f) const
  {
    return EdgesQuad_Rotation(r);
  }
  
  
  //! returns true if the sign is the same after rotation of the face
  inline bool NumberMap::
  IsNegativeDof(int rot, int r, int k, const Face<Dimension3>& f) const
  {
    if (f.GetNbVertices() == 3)
      return SignDof_Rotation_Tri(r)(rot, k);
    
    return SignDof_Rotation_Quad(r)(rot, k);
  }
  
  
  //! projection from local dofs to global dofs by applying the linear operator of the face
  template<class Vector1>
  inline void NumberMap
  ::ProjectToGlobalDofs(Vector1& U, int rot, int r,
			int offset, const Face<Dimension3>& f) const
  {
    if (f.GetNbVertices() == 3)
      ApplyOperator(coef_combination_tri(r)(rot), U, offset, nb_dof_tri(r));
    else
      ApplyOperator(coef_combination_quad(r)(rot), U, offset, nb_dof_quad(r));
  }
  

  //! transpose projection from local dofs to global dofs
  //! by applying the linear operator of the face
  template<class Vector1>
  inline void NumberMap
  ::TransposeProjectToGlobalDofs(Vector1& U, int rot, int r,
				 int offset, const Face<Dimension3>& f) const
  {
    if (f.GetNbVertices() == 3)
      ApplyTransposeOperator(coef_combination_tri(r)(rot), U, offset, nb_dof_tri(r));
    else
      ApplyTransposeOperator(coef_combination_quad(r)(rot), U, offset, nb_dof_quad(r));
  }
  
  
  //! projection from global to local dofs bt applying the linear operator of the face
  template<class Vector1>
  inline void NumberMap::ProjectToLocalDofs(Vector1& U, int rot, int r,
					    int offset, const Face<Dimension3>& f) const
  {
    if (f.GetNbVertices() == 3)
      ApplyTransposeOperator(invCoef_combination_tri(r)(rot), U, offset, nb_dof_tri(r));
    else
      ApplyTransposeOperator(invCoef_combination_quad(r)(rot), U, offset, nb_dof_quad(r));
  }
  
  
  //! modification of the rows of the elementary matrix because of the linear operator
  //! of the considered face
  template<class Matrix1>
  inline void NumberMap::ModifyRowToGlobalDofs(Matrix1& A, int rot, int r, int offset,
					       const Face<Dimension3>& f) const
  {
    if (f.GetNbVertices() == 3)
      ApplyOperatorRow(coef_combination_tri(r)(rot), A, offset, nb_dof_tri(r));
    else
      ApplyOperatorRow(coef_combination_quad(r)(rot), A, offset, nb_dof_quad(r));
  }


  //! modification of the columns of the elementary matrix because of the linear operator
  //! of the considered face
  template<class Matrix1>
  inline void NumberMap::ModifyColumnToGlobalDofs(Matrix1& A, int rot, int r,
						  int offset, const Face<Dimension3>& f) const
  {
    if (f.GetNbVertices() == 3)
      ApplyOperatorColumn(coef_combination_tri(r)(rot), A, offset, nb_dof_tri(r));
    else
      ApplyOperatorColumn(coef_combination_quad(r)(rot), A, offset, nb_dof_quad(r));
  }
  
} // namespace Montjoie

#define MONTJOIE_FILE_NUMBER_MAP_INLINE_CXX
#endif
