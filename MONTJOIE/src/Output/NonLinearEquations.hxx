#ifndef MONTJOIE_FILE_NONLINEAR_EQUATIONS_HXX

namespace Montjoie

{
  // Minpack functions
  
  //! base class to derive when solving a non-linear system with Minpack algorithm
  template<class T, class VectorSol = Vector<T>, class MatrixJac = Matrix<T> >
  class NonLinearEquations_Base
  {
  public :
    // computes y = F(x) where F is the non-linear system
    virtual void EvaluateFunction(const VectorSol& x, VectorSol& y) = 0;
    
    // computes Jac = DF(x)
    virtual void EvaluateJacobian(const VectorSol&, MatrixJac& Jac) = 0;
    
  };

  // solving non-linear equations knowing derivatives
  template<class T, class Vector, class Matrix>
  int SolveMinpack(NonLinearEquations_Base<T, Vector, Matrix>& eqns,
		   Vector& x_sol, Vector& fvec, Matrix& fjac, Vector& scale_eqn,
		   const IVect& Control, const VectReal_wp& RControl);
  
  template<class Vector, class Matrix>
  void Compute_QR_Factorisation(Matrix& A, bool pivot, IVect& ipvt,
				Vector& rdiag, Vector& acnorm, Vector& wa);

  template<class Vector, class Matrix>
  void QrSolve(Matrix& r, IVect& ipvt, Vector& diag, Vector& qtb,
	       Vector& x, Vector& sdiag, Vector& wa);

  template<class Vector, class Matrix>
  void qform(Matrix& q, Vector& wa);

  template<class real, class Vector, class Vector2>
  void dogleg(Vector2& r, Vector& diag, Vector& qtb,
	      real& delta, Vector& x, Vector& wa1, Vector& wa2);
  
  template<class Vector, class Matrix>
  void r1mpyq(Matrix& a, Vector& v, Vector& w);

  template<class Vector>
  void r1mpyq(Vector& a, Vector& v, Vector& w);
  
  template<class Vector2, class Vector>
  void r1updt(Vector2& s, Vector& u, Vector& v, Vector& w, bool& sing);

  //! base class to specify a function to minimize with gsl or mkl
  template<class T>
  class VirtualMinimizedFunction
  {
  protected :
    int n;
    int type_algo;
    
  public :
    // available algorithms in Gsl
    enum{BFGS, CG, STEEPEST_DESCENT, SIMPLEX};
    
    inline VirtualMinimizedFunction() { n = 1; type_algo = BFGS; }
    
    inline int GetGslAlgorithm() const { return type_algo; }
    inline void SetGslAlgorithm(int type) { type_algo = type; }
    
    inline int GetM() const { return n; }
    
    virtual void FindInitGuess(Vector<T>& param) = 0;
    virtual void EvaluateFunction(const Vector<T>& x, T& feval) = 0;
    virtual void EvaluateFunctionGradient(const Vector<T>& x,
					  T& feval, Vector<T>& fjac) = 0;
    
#ifdef MONTJOIE_WITH_GSL
    static double my_f(const gsl_vector *v, void *params);
    static void my_df(const gsl_vector *v, void *params, gsl_vector *df);
    static void my_fdf(const gsl_vector *v, void *params, double* f, gsl_vector *df);
#endif
    
  };


#ifdef MONTJOIE_WITH_GSL
  template<class T>
  T MinimizeParametersGsl(VirtualMinimizedFunction<T>& fct, Vector<T>& xsol,
			  Real_wp epsilon = 1e-12, unsigned nb_max_iter = 5000);
#endif
  
} // end namespace

#define MONTJOIE_FILE_NONLINEAR_EQUATIONS_HXX
#endif
