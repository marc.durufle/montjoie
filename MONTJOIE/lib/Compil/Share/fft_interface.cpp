#include "MontjoieFlag.hxx"

#include "Share/MontjoieCommonHeader.hxx"
#include "Share/MontjoieCommonInline.hxx"

#ifndef SELDON_WITH_COMPILED_LIBRARY
#include "Share/FFT.cxx"
#endif

namespace Montjoie
{
  SELDON_EXTERN template class FftInterface<Real_wp>;
  SELDON_EXTERN template class FftInterface<Complex_wp>;
  
  SELDON_EXTERN template void FftInterface<Complex_wp>::
  GetCosSinAlpha(int, Real_wp&, Real_wp&) const;
 
}
