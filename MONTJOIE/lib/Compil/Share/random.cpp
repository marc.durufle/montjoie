#include "MontjoieFlag.hxx"

#include "Share/MontjoieCommonHeader.hxx"
#include "Share/MontjoieCommonInline.hxx"

#ifndef SELDON_WITH_COMPILED_LIBRARY
#include "Share/RandomGenerator.cxx"
#endif

namespace Montjoie
{
  
  SELDON_EXTERN template void EvaluateContinuousFraction(vector<int>&, Real_wp&);
  SELDON_EXTERN template void DecomposeContinuousFraction(const Real_wp&, const Real_wp&, vector<int>&);
  SELDON_EXTERN template void GetNumeratorDenominator(vector<int>&, int&, int&);
 
}
