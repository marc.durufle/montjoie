#include "MontjoieFlag.hxx"

#include "Share/MontjoieCommonHeader.hxx"
#include "Share/MontjoieCommonInline.hxx"

#ifndef SELDON_WITH_COMPILED_LIBRARY
#include "vector/Vector.cxx"
#include "Algebra/TinyVector.cxx"
#include "Algebra/TinyMatrix.cxx"
#endif

namespace Seldon
{
  
  /* TinyVector */
  
  SELDON_EXTERN template ostream& operator <<(ostream&, const Vector<TinyVector<Real_wp, 2> >&);
  SELDON_EXTERN template ostream& operator <<(ostream&, const Vector<TinyVector<Real_wp, 3> >&);
  SELDON_EXTERN template ostream& operator <<(ostream&, const Vector<Montjoie::UnivariatePolynomial<Real_wp> >&);

  SELDON_EXTERN template class Vector_Base<TinyVector<Real_wp, 1> >;
  SELDON_EXTERN template class Vector_Base<TinyVector<Real_wp, 2> >;
  SELDON_EXTERN template class Vector_Base<TinyVector<Real_wp, 3> >;
  SELDON_EXTERN template class Vector_Base<TinyVector<Real_wp, 4> >;
  SELDON_EXTERN template class Vector_Base<TinyVector<Real_wp, 5> >;
  SELDON_EXTERN template class Vector_Base<TinyVector<Real_wp, 6> >;
  SELDON_EXTERN template class Vector_Base<TinyVector<Real_wp, 7> >;

  SELDON_EXTERN template class Vector_Base<TinyVector<Complex_wp, 1> >;
  SELDON_EXTERN template class Vector_Base<TinyVector<Complex_wp, 2> >;
  SELDON_EXTERN template class Vector_Base<TinyVector<Complex_wp, 3> >;
  SELDON_EXTERN template class Vector_Base<TinyVector<Complex_wp, 4> >;
  SELDON_EXTERN template class Vector_Base<TinyVector<Complex_wp, 5> >;
  SELDON_EXTERN template class Vector_Base<TinyVector<Complex_wp, 6> >;
  SELDON_EXTERN template class Vector_Base<TinyVector<Complex_wp, 7> >;
  
  SELDON_EXTERN template Real_wp Norm2_Column(const Matrix<Real_wp>&, int, int);

  /* TinyMatrix */

  // 2x2 matrices

  SELDON_EXTERN template Real_wp Norm2_Column(const TinyMatrix<Real_wp, General, 2, 2>&, int, int);
  
  SELDON_EXTERN template void GetInverse(TinyMatrix<Real_wp, General, 2, 2>&);
  SELDON_EXTERN template void GetInverse(const TinyMatrix<Real_wp, General, 2, 2>&, TinyMatrix<Real_wp, General, 2, 2>&);
  SELDON_EXTERN template void GetInverse(TinyMatrix<Complex_wp, General, 2, 2>&);
  SELDON_EXTERN template void GetInverse(const TinyMatrix<Complex_wp, General, 2, 2>&, TinyMatrix<Complex_wp, General, 2, 2>&);

  SELDON_EXTERN template void GetInverse(TinyMatrix<Real_wp, Symmetric, 2, 2>&);
  SELDON_EXTERN template void GetInverse(const TinyMatrix<Real_wp, Symmetric, 2, 2>&, TinyMatrix<Real_wp, Symmetric, 2, 2>&);
  SELDON_EXTERN template void GetInverse(TinyMatrix<Complex_wp, Symmetric, 2, 2>&);
  SELDON_EXTERN template void GetInverse(const TinyMatrix<Complex_wp, Symmetric, 2, 2>&, TinyMatrix<Complex_wp, Symmetric, 2, 2>&);

  SELDON_EXTERN template void GetEigenvalues(TinyMatrix<Real_wp, General, 2, 2>&, TinyVector<Real_wp, 2>&, TinyVector<Real_wp, 2>&);
  SELDON_EXTERN template void GetEigenvalues(TinyMatrix<Complex_wp, General, 2, 2>&, TinyVector<Complex_wp, 2>&);

  SELDON_EXTERN template void GetEigenvalues(TinyMatrix<Real_wp, Symmetric, 2, 2>&, TinyVector<Real_wp, 2>&);
  SELDON_EXTERN template void GetEigenvalues(TinyMatrix<Complex_wp, Symmetric, 2, 2>&, TinyVector<Complex_wp, 2>&);
  SELDON_EXTERN template void GetEigenvaluesEigenvectors(TinyMatrix<Real_wp, Symmetric, 2, 2>&, TinyVector<Real_wp, 2>&, TinyMatrix<Real_wp, General, 2, 2>&);

  SELDON_EXTERN template void GetSquareRoot(TinyMatrix<Real_wp, Symmetric, 2, 2>&);
  SELDON_EXTERN template void GetAbsoluteValue(TinyMatrix<Real_wp, Symmetric, 2, 2>&, bool);

  SELDON_EXTERN template void GetEigenvaluesEigenvectors(TinyMatrix<Complex_wp, Symmetric, 2, 2>&, TinyVector<Complex_wp, 2>&, TinyMatrix<Complex_wp, General, 2, 2>&);

  SELDON_EXTERN template void GetSquareRoot(TinyMatrix<Complex_wp, Symmetric, 2, 2>&);
  SELDON_EXTERN template void GetAbsoluteValue(TinyMatrix<Complex_wp, Symmetric, 2, 2>&, bool);

  // 3x3 matrices

  SELDON_EXTERN template Real_wp Norm2_Column(const TinyMatrix<Real_wp, General, 3, 3>&, int, int);

  SELDON_EXTERN template Real_wp Det(const TinyMatrix<Real_wp, General, 3, 3>&);
  SELDON_EXTERN template Real_wp Det(const TinyMatrix<Real_wp, Symmetric, 3, 3>&);

  SELDON_EXTERN template void GetInverse(TinyMatrix<Real_wp, General, 3, 3>&);
  SELDON_EXTERN template void GetInverse(const TinyMatrix<Real_wp, General, 3, 3>&, TinyMatrix<Real_wp, General, 3, 3>&);
  SELDON_EXTERN template void GetInverse(TinyMatrix<Complex_wp, General, 3, 3>&);
  SELDON_EXTERN template void GetInverse(const TinyMatrix<Complex_wp, General, 3, 3>&, TinyMatrix<Complex_wp, General, 3, 3>&);

  SELDON_EXTERN template void GetInverse(TinyMatrix<Real_wp, Symmetric, 3, 3>&);
  SELDON_EXTERN template void GetInverse(const TinyMatrix<Real_wp, Symmetric, 3, 3>&, TinyMatrix<Real_wp, Symmetric, 3, 3>&);
  SELDON_EXTERN template void GetInverse(TinyMatrix<Complex_wp, Symmetric, 3, 3>&);
  SELDON_EXTERN template void GetInverse(const TinyMatrix<Complex_wp, Symmetric, 3, 3>&, TinyMatrix<Complex_wp, Symmetric, 3, 3>&);

  SELDON_EXTERN template void GetNormalProjector(const TinyVector<Real_wp, 3>&, TinyMatrix<Real_wp, General, 3, 3>&);
  SELDON_EXTERN template void GetNormalProjector(const TinyVector<Real_wp, 3>&, TinyMatrix<Real_wp, Symmetric, 3, 3>&);
  SELDON_EXTERN template void GetNormalProjector(const TinyVector<Real_wp, 3>&, TinyMatrix<Complex_wp, General, 3, 3>&);
  SELDON_EXTERN template void GetNormalProjector(const TinyVector<Real_wp, 3>&, TinyMatrix<Complex_wp, Symmetric, 3, 3>&);
  SELDON_EXTERN template void GetNormalProjector(const TinyVector<Complex_wp, 3>&, TinyMatrix<Complex_wp, General, 3, 3>&);
  SELDON_EXTERN template void GetNormalProjector(const TinyVector<Complex_wp, 3>&, TinyMatrix<Complex_wp, Symmetric, 3, 3>&);
  SELDON_EXTERN template void GetTangentialProjector(const TinyVector<Real_wp, 3>&, TinyMatrix<Real_wp, Symmetric, 3, 3>&);
  SELDON_EXTERN template void GetTangentialProjector(const TinyVector<Real_wp, 3>&, TinyMatrix<Complex_wp, Symmetric, 3, 3>&);

  SELDON_EXTERN template void GetEigenvalues(TinyMatrix<Real_wp, Symmetric, 3, 3>&, TinyVector<Real_wp, 3>&);
  SELDON_EXTERN template void GetEigenvalues(TinyMatrix<Complex_wp, Symmetric, 3, 3>&, TinyVector<Complex_wp, 3>&);
  SELDON_EXTERN template void GetEigenvaluesEigenvectors(TinyMatrix<Real_wp, Symmetric, 3, 3>&, TinyVector<Real_wp, 3>&, TinyMatrix<Real_wp, General, 3, 3>&);

  SELDON_EXTERN template void GetSquareRoot(TinyMatrix<Real_wp, Symmetric, 3, 3>&);
  SELDON_EXTERN template void GetAbsoluteValue(TinyMatrix<Real_wp, Symmetric, 3, 3>&, bool);

  SELDON_EXTERN template void GetEigenvaluesEigenvectors(TinyMatrix<Complex_wp, Symmetric, 3, 3>&, TinyVector<Complex_wp, 3>&, TinyMatrix<Complex_wp, General, 3, 3>&);

  SELDON_EXTERN template void GetSquareRoot(TinyMatrix<Complex_wp, Symmetric, 3, 3>&);
  SELDON_EXTERN template void GetAbsoluteValue(TinyMatrix<Complex_wp, Symmetric, 3, 3>&, bool);

  // 4x4 matrices

  SELDON_EXTERN template void GetInverse(TinyMatrix<Real_wp, General, 4, 4>&);
  SELDON_EXTERN template void GetInverse(const TinyMatrix<Real_wp, General, 4, 4>&, TinyMatrix<Real_wp, General, 4, 4>&);
  SELDON_EXTERN template void GetInverse(TinyMatrix<Complex_wp, General, 4, 4>&);
  SELDON_EXTERN template void GetInverse(const TinyMatrix<Complex_wp, General, 4, 4>&, TinyMatrix<Complex_wp, General, 4, 4>&);

  SELDON_EXTERN template void GetInverse(TinyMatrix<Real_wp, Symmetric, 4, 4>&);
  SELDON_EXTERN template void GetInverse(const TinyMatrix<Real_wp, Symmetric, 4, 4>&, TinyMatrix<Real_wp, Symmetric, 4, 4>&);
  SELDON_EXTERN template void GetInverse(TinyMatrix<Complex_wp, Symmetric, 4, 4>&);
  SELDON_EXTERN template void GetInverse(const TinyMatrix<Complex_wp, Symmetric, 4, 4>&, TinyMatrix<Complex_wp, Symmetric, 4, 4>&);

  // 5x5 matrices

  SELDON_EXTERN template void GetInverse(TinyMatrix<Real_wp, General, 5, 5>&);
  SELDON_EXTERN template void GetInverse(const TinyMatrix<Real_wp, General, 5, 5>&, TinyMatrix<Real_wp, General, 5, 5>&);
  SELDON_EXTERN template void GetInverse(TinyMatrix<Complex_wp, General, 5, 5>&);
  SELDON_EXTERN template void GetInverse(const TinyMatrix<Complex_wp, General, 5, 5>&, TinyMatrix<Complex_wp, General, 5, 5>&);

  SELDON_EXTERN template void GetInverse(TinyMatrix<Real_wp, Symmetric, 5, 5>&);
  SELDON_EXTERN template void GetInverse(const TinyMatrix<Real_wp, Symmetric, 5, 5>&, TinyMatrix<Real_wp, Symmetric, 5, 5>&);
  SELDON_EXTERN template void GetInverse(TinyMatrix<Complex_wp, Symmetric, 5, 5>&);
  SELDON_EXTERN template void GetInverse(const TinyMatrix<Complex_wp, Symmetric, 5, 5>&, TinyMatrix<Complex_wp, Symmetric, 5, 5>&);

  SELDON_EXTERN template void GetEigenvaluesEigenvectors(TinyMatrix<Real_wp, Symmetric, 5, 5>&, TinyVector<Real_wp, 5>&, TinyMatrix<Real_wp, General, 5, 5>&);

  SELDON_EXTERN template void GetSquareRoot(TinyMatrix<Real_wp, Symmetric, 5, 5>&);
  SELDON_EXTERN template void GetAbsoluteValue(TinyMatrix<Real_wp, Symmetric, 5, 5>&, bool);

  SELDON_EXTERN template void GetEigenvaluesEigenvectors(TinyMatrix<Complex_wp, Symmetric, 5, 5>&, TinyVector<Complex_wp, 5>&, TinyMatrix<Complex_wp, General, 5, 5>&);

  SELDON_EXTERN template void GetSquareRoot(TinyMatrix<Complex_wp, Symmetric, 5, 5>&);
  SELDON_EXTERN template void GetAbsoluteValue(TinyMatrix<Complex_wp, Symmetric, 5, 5>&, bool);

  SELDON_EXTERN template void GetAbsoluteValue(TinyMatrix<Real_wp, General, 5, 5>&, bool);
  SELDON_EXTERN template void GetAbsoluteValue(TinyMatrix<Complex_wp, General, 5, 5>&, bool);

  // 6x6 matrices

  SELDON_EXTERN template void GetAbsoluteValue(TinyMatrix<Real_wp, General, 6, 6>&, bool);
  SELDON_EXTERN template void GetAbsoluteValue(TinyMatrix<Complex_wp, General, 6, 6>&, bool);

  SELDON_EXTERN template void GetInverse(TinyMatrix<Real_wp, General, 6, 6>&);
  SELDON_EXTERN template void GetInverse(const TinyMatrix<Real_wp, General, 6, 6>&, TinyMatrix<Real_wp, General, 6, 6>&);
  SELDON_EXTERN template void GetInverse(TinyMatrix<Complex_wp, General, 6, 6>&);
  SELDON_EXTERN template void GetInverse(const TinyMatrix<Complex_wp, General, 6, 6>&, TinyMatrix<Complex_wp, General, 6, 6>&);

  SELDON_EXTERN template void GetInverse(TinyMatrix<Real_wp, Symmetric, 6, 6>&);
  SELDON_EXTERN template void GetInverse(const TinyMatrix<Real_wp, Symmetric, 6, 6>&, TinyMatrix<Real_wp, Symmetric, 6, 6>&);
  SELDON_EXTERN template void GetInverse(TinyMatrix<Complex_wp, Symmetric, 6, 6>&);
  SELDON_EXTERN template void GetInverse(const TinyMatrix<Complex_wp, Symmetric, 6, 6>&, TinyMatrix<Complex_wp, Symmetric, 6, 6>&);
  
  // 7x7 matrices

  SELDON_EXTERN template void GetAbsoluteValue(TinyMatrix<Real_wp, General, 7, 7>&, bool);
  SELDON_EXTERN template void GetAbsoluteValue(TinyMatrix<Complex_wp, General, 7, 7>&, bool);

  SELDON_EXTERN template void GetInverse(TinyMatrix<Real_wp, General, 7, 7>&);
  SELDON_EXTERN template void GetInverse(const TinyMatrix<Real_wp, General, 7, 7>&, TinyMatrix<Real_wp, General, 7, 7>&);
  SELDON_EXTERN template void GetInverse(TinyMatrix<Complex_wp, General, 7, 7>&);
  SELDON_EXTERN template void GetInverse(const TinyMatrix<Complex_wp, General, 7, 7>&, TinyMatrix<Complex_wp, General, 7, 7>&);

  // 9x9 matrices

  SELDON_EXTERN template void GetInverse(TinyMatrix<Real_wp, General, 9, 9>&);
  SELDON_EXTERN template void GetInverse(const TinyMatrix<Real_wp, General, 9, 9>&, TinyMatrix<Real_wp, General, 9, 9>&);
  SELDON_EXTERN template void GetInverse(TinyMatrix<Complex_wp, General, 9, 9>&);
  SELDON_EXTERN template void GetInverse(const TinyMatrix<Complex_wp, General, 9, 9>&, TinyMatrix<Complex_wp, General, 9, 9>&);
  
}
