#include "MontjoieFlag.hxx"

#include "Algebra/MontjoieAlgebraHeader.hxx"
#include "Algebra/MontjoieAlgebraInline.hxx"

#ifndef SELDON_WITH_COMPILED_LIBRARY
#include "vector/Vector.cxx"
#include "vector/SparseVector.cxx"
#include "vector/Functions_Arrays.cxx"
#include "computation/basic_functions/Functions_Vector.cxx"
#endif

namespace Seldon
{
  
  // dense vectors

  SELDON_EXTERN template class Vector_Base<bool>;
  SELDON_EXTERN template class Vector_Base<char>;
  SELDON_EXTERN template class Vector_Base<int>;
  SELDON_EXTERN template class Vector_Base<int64_t>;
  SELDON_EXTERN template class Vector_Base<double>;
  SELDON_EXTERN template class Vector_Base<complex<double> >;
  SELDON_EXTERN template class Vector_Base<float>;
  SELDON_EXTERN template class Vector_Base<complex<float> >;

  SELDON_EXTERN template class Vector<bool>;
  SELDON_EXTERN template class Vector<char>;
  SELDON_EXTERN template class Vector<int>;
  SELDON_EXTERN template class Vector<int64_t>;
  SELDON_EXTERN template class Vector<double>;
  SELDON_EXTERN template class Vector<complex<double> >;
  SELDON_EXTERN template class Vector<float>;
  SELDON_EXTERN template class Vector<complex<float> >;

#ifdef SELDON_WITH_COMPILATION32
  SELDON_EXTERN template class Vector_Base<long>;
  SELDON_EXTERN template class Vector<long>;
  SELDON_EXTERN template void Vector<unsigned long>::Resize(int);
#endif

#if defined(MONTJOIE_USE_ACCURATE_TIMING) || defined(MONTJOIE_USE_REAL_TIMING)
  SELDON_EXTERN template void Vector<struct timespec>::Resize(int);
#endif

#ifdef MONTJOIE_WITH_MULTIPLE
  SELDON_EXTERN template class Vector<Real_wp>;
  SELDON_EXTERN template class Vector<Complex_wp>;
#endif

  SELDON_EXTERN template ostream& operator <<(ostream&, const Vector<bool>&);
  SELDON_EXTERN template ostream& operator <<(ostream&, const Vector<int>&);
  SELDON_EXTERN template ostream& operator <<(ostream&, const Vector<int64_t>&);
  SELDON_EXTERN template ostream& operator <<(ostream&, const Vector<Real_wp>&);
  SELDON_EXTERN template ostream& operator <<(ostream&, const Vector<Complex_wp>&);

  // sparse vectors
  SELDON_EXTERN template class Vector<int, VectSparse>;
  SELDON_EXTERN template class Vector<Real_wp, VectSparse>;
  SELDON_EXTERN template class Vector<Complex_wp, VectSparse>;

  SELDON_EXTERN template void Vector<int, VectSparse>::SetData(const Vector<int, VectSparse>&);
  SELDON_EXTERN template void Vector<int, VectSparse>::SetData(Vector<int>&, Vector<int>&);
  SELDON_EXTERN template void Vector<Real_wp, VectSparse>::SetData(const Vector<Real_wp, VectSparse>&);
  SELDON_EXTERN template void Vector<Complex_wp, VectSparse>::SetData(const Vector<Complex_wp, VectSparse>&);

  SELDON_EXTERN template void Vector<Real_wp, VectSparse>::RemoveSmallEntry(const Real_wp&);
  SELDON_EXTERN template void Vector<Complex_wp, VectSparse>::RemoveSmallEntry(const Real_wp&);

  SELDON_EXTERN template void Vector<Real_wp, VectSparse>::AddInteractionRow(int, const Vector<int>&, const Vector<Real_wp>&, bool);
  SELDON_EXTERN template void Vector<Complex_wp, VectSparse>::AddInteractionRow(int, const Vector<int>&, const Vector<Complex_wp>&, bool);

  SELDON_EXTERN template ostream& operator <<(ostream&, const Vector<int, VectSparse>&);
  SELDON_EXTERN template ostream& operator <<(ostream&, const Vector<Real_wp, VectSparse>&);
  SELDON_EXTERN template ostream& operator <<(ostream&, const Vector<Complex_wp, VectSparse>&);
  
  // vector of  strings  
  SELDON_EXTERN template class Vector_Base<string>;
  SELDON_EXTERN template void Vector<string>::Resize(int);
  SELDON_EXTERN template void Vector<string>::ReadText(string);
  SELDON_EXTERN template void Vector<string>::WriteText(string) const;

  SELDON_EXTERN template ostream& operator <<(ostream&, const Vector<string>&);

  // vector of vectors
  SELDON_EXTERN template class Vector_Base<Vector<bool> >;
  SELDON_EXTERN template void Vector<Vector<bool> >::Resize(int);
  
  SELDON_EXTERN template class Vector_Base<Vector<int> >;
  SELDON_EXTERN template void Vector<Vector<int> >::Resize(int);

  SELDON_EXTERN template class Vector_Base<Vector<Real_wp> >;
  SELDON_EXTERN template void Vector<Vector<Real_wp> >::Resize(int);
  
  SELDON_EXTERN template class Vector_Base<Vector<Complex_wp> >;
  SELDON_EXTERN template void Vector<Vector<Complex_wp> >::Resize(int);

  // vector of sparse vectors  
  SELDON_EXTERN template void Vector<Vector<Real_wp, VectSparse>, VectSparse>::ReallocateVector(int);
  SELDON_EXTERN template void Vector<Vector<Complex_wp, VectSparse>, VectSparse>::ReallocateVector(int);
  
  SELDON_EXTERN template void Vector<Vector<Real_wp, VectSparse>, VectSparse>::Clear();
  SELDON_EXTERN template void Vector<Vector<Complex_wp, VectSparse>, VectSparse>::Clear();
  
  // Functions Sort, RemoveDuplicate, QuickSort, etc
  SELDON_EXTERN template void QuickSort(int, int, Vector<Real_wp>&);  
  SELDON_EXTERN template void QuickSort(int, int, Vector<Real_wp>&, Vector<Real_wp>&);
  SELDON_EXTERN template void QuickSort(int, int, Vector<Real_wp>&, Vector<int>&);
  SELDON_EXTERN template void QuickSort(int, int, Vector<Real_wp>&, Vector<Real_wp>&, Vector<Real_wp>&);
  SELDON_EXTERN template void QuickSort(int, int, Vector<Real_wp>&, Vector<int>&, Vector<int>&);

  SELDON_EXTERN template void MergeSort(int, int, Vector<int>&);
  SELDON_EXTERN template void MergeSort(int, int, Vector<Real_wp>&);  
  SELDON_EXTERN template void MergeSort(int, int, Vector<Real_wp>&, Vector<Real_wp>&);
  SELDON_EXTERN template void MergeSort(int, int, Vector<Real_wp>&, Vector<int>&);
  SELDON_EXTERN template void MergeSort(int, int, Vector<Real_wp>&, Vector<Real_wp>&, Vector<Real_wp>&);
  SELDON_EXTERN template void MergeSort(int, int, Vector<Real_wp>&, Vector<int>&, Vector<int>&);

  SELDON_EXTERN template void MergeSort(int, int, Vector<int>&, Vector<int>&);
  SELDON_EXTERN template void MergeSort(int, int, Vector<int>&, Vector<Real_wp>&);
  SELDON_EXTERN template void MergeSort(int, int, Vector<int>&, Vector<Complex_wp>&);
  SELDON_EXTERN template void MergeSort(int, int, Vector<int>&, Vector<int>&, Vector<int>&);
  SELDON_EXTERN template void MergeSort(int, int, Vector<int>&, Vector<int>&, Vector<Real_wp>&);
  SELDON_EXTERN template void MergeSort(int, int, Vector<int>&, Vector<int>&, Vector<Complex_wp>&);

  SELDON_EXTERN template void Sort(int, int, Vector<int>&);
  SELDON_EXTERN template void Sort(int, int, Vector<Real_wp>&);  
  SELDON_EXTERN template void Sort(int, int, Vector<Real_wp>&, Vector<Real_wp>&);
  SELDON_EXTERN template void Sort(int, int, Vector<Real_wp>&, Vector<int>&);
  SELDON_EXTERN template void Sort(int, int, Vector<Real_wp>&, Vector<Real_wp>&, Vector<Real_wp>&);
  SELDON_EXTERN template void Sort(int, int, Vector<Real_wp>&, Vector<int>&, Vector<int>&);
  SELDON_EXTERN template void Sort(int, int, Vector<int>&, Vector<Real_wp>&, Vector<int>&);
  SELDON_EXTERN template void Sort(int, int, Vector<int>&, Vector<Complex_wp>&, Vector<int>&);

  SELDON_EXTERN template void Sort(int, int, Vector<int>&, Vector<int>&);
  SELDON_EXTERN template void Sort(int, int, Vector<int>&, Vector<Real_wp>&);
  SELDON_EXTERN template void Sort(int, int, Vector<int>&, Vector<Complex_wp>&);
  SELDON_EXTERN template void Sort(int, int, Vector<int>&, Vector<int>&, Vector<int>&);
  SELDON_EXTERN template void Sort(int, int, Vector<int>&, Vector<int>&, Vector<Real_wp>&);
  SELDON_EXTERN template void Sort(int, int, Vector<int>&, Vector<int>&, Vector<Complex_wp>&);
  
  SELDON_EXTERN template void Sort(int, Vector<int>&);
  SELDON_EXTERN template void Sort(int, Vector<Real_wp>&);  
  SELDON_EXTERN template void Sort(int, Vector<Real_wp>&, Vector<Real_wp>&);
  SELDON_EXTERN template void Sort(int, Vector<Real_wp>&, Vector<int>&);
  SELDON_EXTERN template void Sort(int, Vector<Real_wp>&, Vector<Real_wp>&, Vector<Real_wp>&);

  SELDON_EXTERN template void Sort(int, Vector<int>&, Vector<int>&);
  SELDON_EXTERN template void Sort(int, Vector<int>&, Vector<Real_wp>&);
  SELDON_EXTERN template void Sort(int, Vector<int>&, Vector<Complex_wp>&);
  SELDON_EXTERN template void Sort(int, Vector<int>&, Vector<int>&, Vector<int>&);
  SELDON_EXTERN template void Sort(int, Vector<int>&, Vector<int>&, Vector<Real_wp>&);
  SELDON_EXTERN template void Sort(int, Vector<int>&, Vector<Real_wp>&, Vector<int>&);
  SELDON_EXTERN template void Sort(int, Vector<int>&, Vector<int>&, Vector<Complex_wp>&);
  
  SELDON_EXTERN template void Sort(Vector<int>&);
  SELDON_EXTERN template void Sort(Vector<Real_wp>&);
  SELDON_EXTERN template void Sort(Vector<int>&, Vector<int>&);
  SELDON_EXTERN template void Sort(Vector<int>&, Vector<Real_wp>&);
  SELDON_EXTERN template void Sort(Vector<int>&, Vector<Complex_wp>&);

  SELDON_EXTERN template void Sort(Vector<int>&, Vector<int>&, Vector<int>&);
  SELDON_EXTERN template void Sort(Vector<int>&, Vector<int>&, Vector<Real_wp>&);
  SELDON_EXTERN template void Sort(Vector<int>&, Vector<int>&, Vector<Complex_wp>&);

  SELDON_EXTERN template void Sort(Vector<Real_wp>&, Vector<Real_wp>&);
  SELDON_EXTERN template void Sort(Vector<Real_wp>&, Vector<int>&);
  SELDON_EXTERN template void Sort(Vector<Real_wp>&, Vector<Real_wp>&, Vector<Real_wp>&);
  SELDON_EXTERN template void Sort(Vector<Real_wp>&, Vector<int>&, Vector<int>&);
    
  SELDON_EXTERN template void Assemble(int&, Vector<int>&, Vector<Real_wp>&);
  SELDON_EXTERN template void Assemble(int&, Vector<int>&, Vector<Complex_wp>&);
  SELDON_EXTERN template void Assemble(int&, Vector<int>&);
  SELDON_EXTERN template void Assemble(Vector<int>&);

  SELDON_EXTERN template void RemoveDuplicate(int&, Vector<int>&, Vector<int>&);
  SELDON_EXTERN template void RemoveDuplicate(int&, Vector<int>&, Vector<Real_wp>&);
  SELDON_EXTERN template void RemoveDuplicate(int&, Vector<int>&, Vector<Complex_wp>&);
  SELDON_EXTERN template void RemoveDuplicate(int&, Vector<int>&);
  SELDON_EXTERN template void RemoveDuplicate(Vector<int>&, Vector<int>&);
  SELDON_EXTERN template void RemoveDuplicate(Vector<int>&);
  SELDON_EXTERN template void RemoveDuplicate(Vector<Real_wp>&, Vector<Complex_wp>&);
  
  SELDON_EXTERN template void SwapPointer(Vector<int>&, Vector<int>&);
  SELDON_EXTERN template void SwapPointer(Vector<Real_wp>&, Vector<Real_wp>&);
  SELDON_EXTERN template void SwapPointer(Vector<Complex_wp>&, Vector<Complex_wp>&);
  SELDON_EXTERN template void SwapPointer(Vector<Vector<int> >&, Vector<Vector<int> >&);
  SELDON_EXTERN template void SwapPointer(Vector<Vector<Real_wp, VectSparse> >&, Vector<Vector<Real_wp, VectSparse> >&);
  SELDON_EXTERN template void SwapPointer(Vector<Vector<Complex_wp, VectSparse> >&, Vector<Vector<Complex_wp, VectSparse> >&);

}
