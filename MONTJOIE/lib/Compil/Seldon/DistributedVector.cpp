#include "MontjoieFlag.hxx"

#include "Algebra/MontjoieAlgebraHeader.hxx"
#include "Algebra/MontjoieAlgebraInline.hxx"

#ifndef SELDON_WITH_COMPILED_LIBRARY
#include "vector/DistributedVector.cxx"
#include "share/MpiCommunication.cxx"
#endif

namespace Seldon
{
  
  // dense vectors
  SELDON_EXTERN template class DistributedVector<Real_wp>;
  SELDON_EXTERN template class DistributedVector<Complex_wp>;

  // fonctions DotProd, Norm2
  SELDON_EXTERN template Real_wp DotProdVector(const DistributedVector<Real_wp>&, const DistributedVector<Real_wp>&);
  SELDON_EXTERN template Complex_wp DotProdVector(const DistributedVector<Complex_wp>&, const DistributedVector<Complex_wp>&);
  SELDON_EXTERN template Real_wp DotProdConjVector(const DistributedVector<Real_wp>&, const DistributedVector<Real_wp>&);
  SELDON_EXTERN template Complex_wp DotProdConjVector(const DistributedVector<Complex_wp>&, const DistributedVector<Complex_wp>&);
  SELDON_EXTERN template Real_wp Norm2(const DistributedVector<Real_wp>&);
  SELDON_EXTERN template Real_wp Norm2(const DistributedVector<Complex_wp>&);
  
  // fonctions AssembleVector
  SELDON_EXTERN template void AssembleVector(Vector<int>&, const MPI::Op&, const IVect&, const Vector<IVect>&, const MPI::Comm&, int, int, int);
  SELDON_EXTERN template void AssembleVector(Vector<Real_wp>&, const MPI::Op&, const IVect&, const Vector<IVect>&, const MPI::Comm&, int, int, int);
  SELDON_EXTERN template void AssembleVector(Vector<Complex_wp>&, const MPI::Op&, const IVect&, const Vector<IVect>&, const MPI::Comm&, int, int, int);
  
  SELDON_EXTERN template void ExchangeVector(Vector<Real_wp>&, const IVect&, const Vector<IVect>&, const MPI::Comm&, int, int, int);
  SELDON_EXTERN template void ExchangeVector(Vector<Complex_wp>&, const IVect&, const Vector<IVect>&, const MPI::Comm&, int, int, int);
  SELDON_EXTERN template void ExchangeRelaxVector(Vector<Real_wp>&, const Real_wp&, int, const IVect&, const Vector<IVect>&, const MPI::Comm&, int, int, int);
  SELDON_EXTERN template void ExchangeRelaxVector(Vector<Complex_wp>&, const Real_wp&, int, const IVect&, const Vector<IVect>&, const MPI::Comm&, int, int, int);
  
  // MPI communications
  SELDON_EXTERN template MPI::Request MpiIsend(const MPI::Comm&, bool*, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template MPI::Request MpiIsend(const MPI::Comm&, int*, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template MPI::Request MpiIsend(const MPI::Comm&, double*, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template MPI::Request MpiIsend(const MPI::Comm&, complex<double>*, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template MPI::Request MpiIsend(const MPI::Comm&, Vector<bool>&, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template MPI::Request MpiIsend(const MPI::Comm&, Vector<int>&, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template MPI::Request MpiIsend(const MPI::Comm&, Vector<double>&, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template MPI::Request MpiIsend(const MPI::Comm&, Vector<complex<double> >&, Vector<int64_t>&, int, int, int);

  SELDON_EXTERN template MPI::Request MpiIrecv(const MPI::Comm&, bool*, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template MPI::Request MpiIrecv(const MPI::Comm&, int*, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template MPI::Request MpiIrecv(const MPI::Comm&, double*, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template MPI::Request MpiIrecv(const MPI::Comm&, complex<double>*, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template MPI::Request MpiIrecv(const MPI::Comm&, Vector<bool>&, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template MPI::Request MpiIrecv(const MPI::Comm&, Vector<int>&, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template MPI::Request MpiIrecv(const MPI::Comm&, Vector<double>&, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template MPI::Request MpiIrecv(const MPI::Comm&, Vector<complex<double> >&, Vector<int64_t>&, int, int, int);
  
  SELDON_EXTERN template void MpiCompleteIrecv(bool*, Vector<int64_t>&, int);
  SELDON_EXTERN template void MpiCompleteIrecv(int*, Vector<int64_t>&, int);
  SELDON_EXTERN template void MpiCompleteIrecv(double*, Vector<int64_t>&, int);
  SELDON_EXTERN template void MpiCompleteIrecv(complex<double>*, Vector<int64_t>&, int);
  SELDON_EXTERN template void MpiCompleteIrecv(Vector<bool>&, Vector<int64_t>&, int);
  SELDON_EXTERN template void MpiCompleteIrecv(Vector<int>&, Vector<int64_t>&, int);
  SELDON_EXTERN template void MpiCompleteIrecv(Vector<double>&, Vector<int64_t>&, int);
  SELDON_EXTERN template void MpiCompleteIrecv(Vector<complex<double> >&, Vector<int64_t>&, int);
  
  SELDON_EXTERN template void MpiSsend(const MPI::Comm&, double*, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template void MpiSsend(const MPI::Comm&, complex<double>*, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template void MpiSsend(const MPI::Comm&, Vector<double>&, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template void MpiSsend(const MPI::Comm&, Vector<complex<double> >&, Vector<int64_t>&, int, int, int);
  
  SELDON_EXTERN template void MpiSend(const MPI::Comm&, double*, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template void MpiSend(const MPI::Comm&, complex<double>*, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template void MpiSend(const MPI::Comm&, Vector<double>&, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template void MpiSend(const MPI::Comm&, Vector<complex<double> >&, Vector<int64_t>&, int, int, int);
  
  SELDON_EXTERN template void MpiGather(const MPI::Comm&, double*, Vector<int64_t>&, double*, int, int);
  SELDON_EXTERN template void MpiGather(const MPI::Comm&, complex<double>*, Vector<int64_t>&, complex<double>*, int, int);
  SELDON_EXTERN template void MpiGather(const MPI::Comm&, Vector<double>&, Vector<int64_t>&, Vector<double>&, int, int);
  SELDON_EXTERN template void MpiGather(const MPI::Comm&, Vector<complex<double> >&, Vector<int64_t>&, Vector<complex<double> >&, int, int);

  SELDON_EXTERN template void MpiAllreduce(const MPI::Comm&, double*, Vector<int64_t>&, double*, int, const MPI::Op&);
  SELDON_EXTERN template void MpiAllreduce(const MPI::Comm&, complex<double>*, Vector<int64_t>&, complex<double>*, int, const MPI::Op&);
  SELDON_EXTERN template void MpiAllreduce(const MPI::Comm&, Vector<double>&, Vector<int64_t>&, Vector<double>&, int, const MPI::Op&);
  SELDON_EXTERN template void MpiAllreduce(const MPI::Comm&, Vector<complex<double> >&, Vector<int64_t>&, Vector<complex<double> >&, int, const MPI::Op&);
  
  SELDON_EXTERN template void MpiReduce(const MPI::Comm&, double*, Vector<int64_t>&, double*, int, const MPI::Op&, int);
  SELDON_EXTERN template void MpiReduce(const MPI::Comm&, complex<double>*, Vector<int64_t>&, complex<double>*, int, const MPI::Op&, int);
  SELDON_EXTERN template void MpiReduce(const MPI::Comm&, Vector<double>&, Vector<int64_t>&, Vector<double>&, int, const MPI::Op&, int);
  SELDON_EXTERN template void MpiReduce(const MPI::Comm&, Vector<complex<double> >&, Vector<int64_t>&, Vector<complex<double> >&, int, const MPI::Op&, int);
  
  SELDON_EXTERN template void MpiRecv(const MPI::Comm&, double*, Vector<int64_t>&, int, int, int, MPI::Status&);
  SELDON_EXTERN template void MpiRecv(const MPI::Comm&, complex<double>*, Vector<int64_t>&, int, int, int, MPI::Status&);
  SELDON_EXTERN template void MpiRecv(const MPI::Comm&, Vector<double>&, Vector<int64_t>&, int, int, int, MPI::Status&);
  SELDON_EXTERN template void MpiRecv(const MPI::Comm&, Vector<complex<double> >&, Vector<int64_t>&, int, int, int, MPI::Status&);
  
  SELDON_EXTERN template void MpiBcast(const MPI::Comm&, double*, Vector<int64_t>&, int, int);
  SELDON_EXTERN template void MpiBcast(const MPI::Comm&, complex<double>*, Vector<int64_t>&, int, int);
  SELDON_EXTERN template void MpiBcast(const MPI::Comm&, Vector<double>&, Vector<int64_t>&, int, int);
  SELDON_EXTERN template void MpiBcast(const MPI::Comm&, Vector<complex<double> >&, Vector<int64_t>&, int, int);


#if defined(MONTJOIE_WITH_FLOAT80) || defined(MONTJOIE_WITH_FLOAT128)
  SELDON_EXTERN template MPI::Request MpiIsend(const MPI::Comm&, Real_wp*, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template MPI::Request MpiIsend(const MPI::Comm&, Complex_wp*, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template MPI::Request MpiIsend(const MPI::Comm&, Vector<Real_wp>&, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template MPI::Request MpiIsend(const MPI::Comm&, Vector<Complex_wp >&, Vector<int64_t>&, int, int, int);

  SELDON_EXTERN template MPI::Request MpiIrecv(const MPI::Comm&, Real_wp*, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template MPI::Request MpiIrecv(const MPI::Comm&, Complex_wp*, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template MPI::Request MpiIrecv(const MPI::Comm&, Vector<Real_wp>&, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template MPI::Request MpiIrecv(const MPI::Comm&, Vector<Complex_wp >&, Vector<int64_t>&, int, int, int);

  SELDON_EXTERN template void MpiCompleteIrecv(Real_wp*, Vector<int64_t>&, int);
  SELDON_EXTERN template void MpiCompleteIrecv(Complex_wp*, Vector<int64_t>&, int);
  SELDON_EXTERN template void MpiCompleteIrecv(Vector<Real_wp>&, Vector<int64_t>&, int);
  SELDON_EXTERN template void MpiCompleteIrecv(Vector<Complex_wp >&, Vector<int64_t>&, int);

  SELDON_EXTERN template void MpiSsend(const MPI::Comm&, Real_wp*, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template void MpiSsend(const MPI::Comm&, Complex_wp*, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template void MpiSsend(const MPI::Comm&, Vector<Real_wp>&, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template void MpiSsend(const MPI::Comm&, Vector<Complex_wp >&, Vector<int64_t>&, int, int, int);

  SELDON_EXTERN template void MpiSend(const MPI::Comm&, Real_wp*, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template void MpiSend(const MPI::Comm&, Complex_wp*, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template void MpiSend(const MPI::Comm&, Vector<Real_wp>&, Vector<int64_t>&, int, int, int);
  SELDON_EXTERN template void MpiSend(const MPI::Comm&, Vector<Complex_wp >&, Vector<int64_t>&, int, int, int);

  SELDON_EXTERN template void MpiGather(const MPI::Comm&, Real_wp*, Vector<int64_t>&, Real_wp*, int, int);
  SELDON_EXTERN template void MpiGather(const MPI::Comm&, Complex_wp*, Vector<int64_t>&, Complex_wp*, int, int);
  SELDON_EXTERN template void MpiGather(const MPI::Comm&, Vector<Real_wp>&, Vector<int64_t>&, Vector<Real_wp>&, int, int);
  SELDON_EXTERN template void MpiGather(const MPI::Comm&, Vector<Complex_wp >&, Vector<int64_t>&, Vector<Complex_wp >&, int, int);

  SELDON_EXTERN template void MpiAllreduce(const MPI::Comm&, Real_wp*, Vector<int64_t>&, Real_wp*, int, const MPI::Op&);
  SELDON_EXTERN template void MpiAllreduce(const MPI::Comm&, Complex_wp*, Vector<int64_t>&, Complex_wp*, int, const MPI::Op&);
  SELDON_EXTERN template void MpiAllreduce(const MPI::Comm&, Vector<Real_wp>&, Vector<int64_t>&, Vector<Real_wp>&, int, const MPI::Op&);
  SELDON_EXTERN template void MpiAllreduce(const MPI::Comm&, Vector<Complex_wp >&, Vector<int64_t>&, Vector<Complex_wp >&, int, const MPI::Op&);
  
  SELDON_EXTERN template void MpiReduce(const MPI::Comm&, Real_wp*, Vector<int64_t>&, Real_wp*, int, const MPI::Op&, int);
  SELDON_EXTERN template void MpiReduce(const MPI::Comm&, Complex_wp*, Vector<int64_t>&, Complex_wp*, int, const MPI::Op&, int);
  SELDON_EXTERN template void MpiReduce(const MPI::Comm&, Vector<Real_wp>&, Vector<int64_t>&, Vector<Real_wp>&, int, const MPI::Op&, int);
  SELDON_EXTERN template void MpiReduce(const MPI::Comm&, Vector<Complex_wp >&, Vector<int64_t>&, Vector<Complex_wp >&, int, const MPI::Op&, int);
  
  SELDON_EXTERN template void MpiRecv(const MPI::Comm&, Real_wp*, Vector<int64_t>&, int, int, int, MPI::Status&);
  SELDON_EXTERN template void MpiRecv(const MPI::Comm&, Complex_wp*, Vector<int64_t>&, int, int, int, MPI::Status&);
  SELDON_EXTERN template void MpiRecv(const MPI::Comm&, Vector<Real_wp>&, Vector<int64_t>&, int, int, int, MPI::Status&);
  SELDON_EXTERN template void MpiRecv(const MPI::Comm&, Vector<Complex_wp >&, Vector<int64_t>&, int, int, int, MPI::Status&);
  
  SELDON_EXTERN template void MpiBcast(const MPI::Comm&, Real_wp*, Vector<int64_t>&, int, int);
  SELDON_EXTERN template void MpiBcast(const MPI::Comm&, Complex_wp*, Vector<int64_t>&, int, int);
  SELDON_EXTERN template void MpiBcast(const MPI::Comm&, Vector<Real_wp>&, Vector<int64_t>&, int, int);
  SELDON_EXTERN template void MpiBcast(const MPI::Comm&, Vector<Complex_wp >&, Vector<int64_t>&, int, int);
#endif
  

}
