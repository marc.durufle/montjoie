#include "MontjoieFlag.hxx"

#include "Algebra/MontjoieAlgebraHeader.hxx"
#include "Algebra/MontjoieAlgebraInline.hxx"

#ifndef SELDON_WITH_COMPILED_LIBRARY
#include "computation/solver/SparseCholeskyFactorisation.cxx"
#endif

namespace Seldon
{
  SELDON_EXTERN template void
  GetCholesky(Matrix<Montjoie::Real_wp, Symmetric, ArrayRowSymSparse>&, int);

  SELDON_EXTERN template void
  SolveCholesky(const SeldonTranspose&,
                const Matrix<Montjoie::Real_wp, Symmetric, ArrayRowSymSparse>&, Vector<Montjoie::Real_wp>&);

  SELDON_EXTERN template void
  SolveCholesky(const SeldonTranspose&,
                const Matrix<Montjoie::Real_wp, Symmetric, RowSymSparse>&, Vector<Montjoie::Real_wp>&);

  SELDON_EXTERN template void
  SolveCholesky(const SeldonTranspose&,
		const Matrix<Montjoie::Real_wp, Symmetric, RowSymSparse>&, Vector<Montjoie::Complex_wp>&);
  
  SELDON_EXTERN template void
  MltCholesky(const SeldonTranspose&,
              const Matrix<Montjoie::Real_wp, Symmetric, ArrayRowSymSparse>&, Vector<Montjoie::Real_wp>&);
  
  SELDON_EXTERN template void
  MltCholesky(const SeldonTranspose&,
              const Matrix<Montjoie::Real_wp, Symmetric, RowSymSparse>&, Vector<Montjoie::Real_wp>&);

  SELDON_EXTERN template void MltCholesky(const SeldonTranspose&, const Matrix<Montjoie::Real_wp, Symmetric, RowSymSparse>&, Vector<Montjoie::Complex_wp>&);

  SELDON_EXTERN template class SparseCholeskySolver<Montjoie::Real_wp>;
  
  SELDON_EXTERN template void SparseCholeskySolver<Montjoie::Real_wp>::
  Factorize(Matrix<Montjoie::Real_wp, Symmetric, ArrayRowSymSparse>&, bool);

  SELDON_EXTERN template void SparseCholeskySolver<Montjoie::Real_wp>::
  Solve(const SeldonTranspose&, Vector<Montjoie::Real_wp>&);

  SELDON_EXTERN template void SparseCholeskySolver<Montjoie::Real_wp>::
  Mlt(const SeldonTranspose&, Vector<Montjoie::Real_wp>&);
  
}



