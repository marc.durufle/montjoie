#include "MontjoieFlag.hxx"

#include "Algebra/MontjoieAlgebraHeader.hxx"
#include "Algebra/MontjoieAlgebraInline.hxx"

#ifndef SELDON_WITH_COMPILED_LIBRARY
#include "computation/solver/DistributedSolver.cxx"
#endif

namespace Seldon
{
  SELDON_EXTERN template class SparseDistributedSolver<Real_wp>;
  SELDON_EXTERN template class SparseDistributedSolver<Complex_wp>;

  SELDON_EXTERN template void SparseDistributedSolver<Real_wp>::Factorize(Matrix<Real_wp, General, ArrayRowSparse>&, bool, bool);
  SELDON_EXTERN template void SparseDistributedSolver<Real_wp>::Factorize(Matrix<Real_wp, Symmetric, ArrayRowSymSparse>&, bool, bool);
  SELDON_EXTERN template void SparseDistributedSolver<Real_wp>::Factorize(DistributedMatrix<Real_wp, General, ArrayRowSparse>&, bool, bool);
  SELDON_EXTERN template void SparseDistributedSolver<Real_wp>::Factorize(DistributedMatrix<Real_wp, Symmetric, ArrayRowSymSparse>&, bool, bool);

  SELDON_EXTERN template void SparseDistributedSolver<Real_wp>::Solve(Vector<Real_wp>&, const Vector<Real_wp>&);
  SELDON_EXTERN template void SparseDistributedSolver<Real_wp>::Solve(Vector<Real_wp>&);
  SELDON_EXTERN template void SparseDistributedSolver<Real_wp>::TransSolve(Vector<Real_wp>&);
  SELDON_EXTERN template void SparseDistributedSolver<Real_wp>::Solve(const SeldonTranspose&, Vector<Real_wp>&);
  SELDON_EXTERN template void SparseDistributedSolver<Real_wp>::Solve(Matrix<Real_wp, General, ColMajor>&);
  SELDON_EXTERN template void SparseDistributedSolver<Real_wp>::TransSolve(Matrix<Real_wp, General, ColMajor>&);
  SELDON_EXTERN template void SparseDistributedSolver<Real_wp>::Solve(const SeldonTranspose&, Matrix<Real_wp, General, ColMajor>&);

  SELDON_EXTERN template void SparseDistributedSolver<Real_wp>::Solve(Vector<Complex_wp>&, const Vector<Complex_wp>&);
  SELDON_EXTERN template void SparseDistributedSolver<Real_wp>::Solve(Vector<Complex_wp>&);
  SELDON_EXTERN template void SparseDistributedSolver<Real_wp>::TransSolve(Vector<Complex_wp>&);
  SELDON_EXTERN template void SparseDistributedSolver<Real_wp>::Solve(const SeldonTranspose&, Vector<Complex_wp>&);
  SELDON_EXTERN template void SparseDistributedSolver<Real_wp>::Solve(Matrix<Complex_wp, General, ColMajor>&);
  SELDON_EXTERN template void SparseDistributedSolver<Real_wp>::TransSolve(Matrix<Complex_wp, General, ColMajor>&);
  SELDON_EXTERN template void SparseDistributedSolver<Real_wp>::Solve(const SeldonTranspose&, Matrix<Complex_wp, General, ColMajor>&);

  SELDON_EXTERN template void SparseDistributedSolver<Complex_wp>::Factorize(Matrix<Complex_wp, General, ArrayRowSparse>&, bool, bool);
  SELDON_EXTERN template void SparseDistributedSolver<Complex_wp>::Factorize(Matrix<Complex_wp, Symmetric, ArrayRowSymSparse>&, bool, bool);
  SELDON_EXTERN template void SparseDistributedSolver<Complex_wp>::Factorize(DistributedMatrix<Complex_wp, General, ArrayRowSparse>&, bool, bool);
  SELDON_EXTERN template void SparseDistributedSolver<Complex_wp>::Factorize(DistributedMatrix<Complex_wp, Symmetric, ArrayRowSymSparse>&, bool, bool);

  SELDON_EXTERN template void SparseDistributedSolver<Complex_wp>::Solve(Vector<Complex_wp>&, const Vector<Complex_wp>&);
  SELDON_EXTERN template void SparseDistributedSolver<Complex_wp>::Solve(Vector<Complex_wp>&);
  SELDON_EXTERN template void SparseDistributedSolver<Complex_wp>::TransSolve(Vector<Complex_wp>&);
  SELDON_EXTERN template void SparseDistributedSolver<Complex_wp>::Solve(const SeldonTranspose&, Vector<Complex_wp>&);
  SELDON_EXTERN template void SparseDistributedSolver<Complex_wp>::Solve(Matrix<Complex_wp, General, ColMajor>&);
  SELDON_EXTERN template void SparseDistributedSolver<Complex_wp>::TransSolve(Matrix<Complex_wp, General, ColMajor>&);
  SELDON_EXTERN template void SparseDistributedSolver<Complex_wp>::Solve(const SeldonTranspose&, Matrix<Complex_wp, General, ColMajor>&);
  
}



